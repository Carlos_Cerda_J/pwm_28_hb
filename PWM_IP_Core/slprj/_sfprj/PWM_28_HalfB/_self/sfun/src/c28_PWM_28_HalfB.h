#ifndef __c28_PWM_28_HalfB_h__
#define __c28_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc28_PWM_28_HalfBInstanceStruct
#define typedef_SFc28_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c28_sfEvent;
  boolean_T c28_doneDoubleBufferReInit;
  uint8_T c28_is_active_c28_PWM_28_HalfB;
  uint8_T c28_JITStateAnimation[1];
  uint8_T c28_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c28_emlrtLocationLoggingDataTables[24];
  int32_T c28_IsDebuggerActive;
  int32_T c28_IsSequenceViewerPresent;
  int32_T c28_SequenceViewerOptimization;
  void *c28_RuntimeVar;
  emlrtLocationLoggingHistogramType c28_emlrtLocLogHistTables[24];
  boolean_T c28_emlrtLocLogSimulated;
  uint32_T c28_mlFcnLineNumber;
  void *c28_fcnDataPtrs[7];
  char_T *c28_dataNames[7];
  uint32_T c28_numFcnVars;
  uint32_T c28_ssIds[7];
  uint32_T c28_statuses[7];
  void *c28_outMexFcns[7];
  void *c28_inMexFcns[7];
  CovrtStateflowInstance *c28_covrtInstance;
  void *c28_fEmlrtCtx;
  uint8_T *c28_enable;
  int16_T *c28_offset;
  int16_T *c28_max;
  int16_T *c28_sum;
  uint16_T *c28_cont;
  uint8_T *c28_init;
  uint8_T *c28_out_init;
} SFc28_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc28_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c28_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c28_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c28_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
