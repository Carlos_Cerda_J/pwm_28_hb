#ifndef __c8_PWM_28_HalfB_h__
#define __c8_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc8_PWM_28_HalfBInstanceStruct
#define typedef_SFc8_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c8_sfEvent;
  boolean_T c8_doneDoubleBufferReInit;
  uint8_T c8_is_active_c8_PWM_28_HalfB;
  uint8_T c8_JITStateAnimation[1];
  uint8_T c8_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c8_emlrtLocationLoggingDataTables[24];
  int32_T c8_IsDebuggerActive;
  int32_T c8_IsSequenceViewerPresent;
  int32_T c8_SequenceViewerOptimization;
  void *c8_RuntimeVar;
  emlrtLocationLoggingHistogramType c8_emlrtLocLogHistTables[24];
  boolean_T c8_emlrtLocLogSimulated;
  uint32_T c8_mlFcnLineNumber;
  void *c8_fcnDataPtrs[7];
  char_T *c8_dataNames[7];
  uint32_T c8_numFcnVars;
  uint32_T c8_ssIds[7];
  uint32_T c8_statuses[7];
  void *c8_outMexFcns[7];
  void *c8_inMexFcns[7];
  CovrtStateflowInstance *c8_covrtInstance;
  void *c8_fEmlrtCtx;
  uint8_T *c8_enable;
  int16_T *c8_offset;
  int16_T *c8_max;
  int16_T *c8_sum;
  uint16_T *c8_cont;
  uint8_T *c8_init;
  uint8_T *c8_out_init;
} SFc8_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc8_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c8_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c8_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c8_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
