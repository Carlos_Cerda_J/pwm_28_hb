#ifndef __c15_PWM_28_HalfB_h__
#define __c15_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc15_PWM_28_HalfBInstanceStruct
#define typedef_SFc15_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c15_sfEvent;
  boolean_T c15_doneDoubleBufferReInit;
  uint8_T c15_is_active_c15_PWM_28_HalfB;
  uint8_T c15_JITStateAnimation[1];
  uint8_T c15_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c15_emlrtLocationLoggingDataTables[24];
  int32_T c15_IsDebuggerActive;
  int32_T c15_IsSequenceViewerPresent;
  int32_T c15_SequenceViewerOptimization;
  void *c15_RuntimeVar;
  emlrtLocationLoggingHistogramType c15_emlrtLocLogHistTables[24];
  boolean_T c15_emlrtLocLogSimulated;
  uint32_T c15_mlFcnLineNumber;
  void *c15_fcnDataPtrs[7];
  char_T *c15_dataNames[7];
  uint32_T c15_numFcnVars;
  uint32_T c15_ssIds[7];
  uint32_T c15_statuses[7];
  void *c15_outMexFcns[7];
  void *c15_inMexFcns[7];
  CovrtStateflowInstance *c15_covrtInstance;
  void *c15_fEmlrtCtx;
  uint8_T *c15_enable;
  int16_T *c15_offset;
  int16_T *c15_max;
  int16_T *c15_sum;
  uint16_T *c15_cont;
  uint8_T *c15_init;
  uint8_T *c15_out_init;
} SFc15_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc15_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c15_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c15_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c15_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
