#ifndef __c21_PWM_28_HalfB_h__
#define __c21_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc21_PWM_28_HalfBInstanceStruct
#define typedef_SFc21_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c21_sfEvent;
  boolean_T c21_doneDoubleBufferReInit;
  uint8_T c21_is_active_c21_PWM_28_HalfB;
  uint8_T c21_JITStateAnimation[1];
  uint8_T c21_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c21_emlrtLocationLoggingDataTables[24];
  int32_T c21_IsDebuggerActive;
  int32_T c21_IsSequenceViewerPresent;
  int32_T c21_SequenceViewerOptimization;
  void *c21_RuntimeVar;
  emlrtLocationLoggingHistogramType c21_emlrtLocLogHistTables[24];
  boolean_T c21_emlrtLocLogSimulated;
  uint32_T c21_mlFcnLineNumber;
  void *c21_fcnDataPtrs[7];
  char_T *c21_dataNames[7];
  uint32_T c21_numFcnVars;
  uint32_T c21_ssIds[7];
  uint32_T c21_statuses[7];
  void *c21_outMexFcns[7];
  void *c21_inMexFcns[7];
  CovrtStateflowInstance *c21_covrtInstance;
  void *c21_fEmlrtCtx;
  uint8_T *c21_enable;
  int16_T *c21_offset;
  int16_T *c21_max;
  int16_T *c21_sum;
  uint16_T *c21_cont;
  uint8_T *c21_init;
  uint8_T *c21_out_init;
} SFc21_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc21_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c21_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c21_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c21_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
