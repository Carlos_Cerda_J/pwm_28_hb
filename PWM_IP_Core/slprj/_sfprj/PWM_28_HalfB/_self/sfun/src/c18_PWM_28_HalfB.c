/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c18_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c18_eml_mx;
static const mxArray *c18_b_eml_mx;
static const mxArray *c18_c_eml_mx;

/* Function Declarations */
static void initialize_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c18_update_jit_animation_state_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance);
static void c18_do_animation_call_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c18_st);
static void sf_gateway_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c18_emlrt_update_log_1(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index);
static int16_T c18_emlrt_update_log_2(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index);
static int16_T c18_emlrt_update_log_3(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index);
static boolean_T c18_emlrt_update_log_4(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index);
static uint16_T c18_emlrt_update_log_5(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index);
static int32_T c18_emlrt_update_log_6(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index);
static void c18_emlrtInitVarDataTables(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c18_dataTables[24],
  emlrtLocationLoggingHistogramType c18_histTables[24]);
static uint16_T c18_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c18_sp, const mxArray *c18_b_cont, const
  char_T *c18_identifier);
static uint16_T c18_b_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c18_sp, const mxArray *c18_u, const
  emlrtMsgIdentifier *c18_parentId);
static uint8_T c18_c_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c18_sp, const mxArray *c18_b_out_init, const
  char_T *c18_identifier);
static uint8_T c18_d_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c18_sp, const mxArray *c18_u, const
  emlrtMsgIdentifier *c18_parentId);
static uint8_T c18_e_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c18_b_is_active_c18_PWM_28_HalfB, const char_T *
  c18_identifier);
static uint8_T c18_f_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c18_u, const emlrtMsgIdentifier *c18_parentId);
static const mxArray *c18_chart_data_browse_helper
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c18_ssIdNumber);
static int32_T c18__s32_add__(SFc18_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c18_b, int32_T c18_c, int32_T c18_EMLOvCount_src_loc, uint32_T
  c18_ssid_src_loc, int32_T c18_offset_src_loc, int32_T c18_length_src_loc);
static void init_dsm_address_info(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c18_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c18_st.tls = chartInstance->c18_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c18_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c18_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c18_is_active_c18_PWM_28_HalfB = 0U;
  sf_mex_assign(&c18_c_eml_mx, sf_mex_call(&c18_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c18_b_eml_mx, sf_mex_call(&c18_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c18_eml_mx, sf_mex_call(&c18_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c18_emlrtLocLogSimulated = false;
  c18_emlrtInitVarDataTables(chartInstance,
    chartInstance->c18_emlrtLocationLoggingDataTables,
    chartInstance->c18_emlrtLocLogHistTables);
}

static void initialize_params_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c18_update_jit_animation_state_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c18_do_animation_call_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c18_st;
  const mxArray *c18_y = NULL;
  const mxArray *c18_b_y = NULL;
  uint16_T c18_u;
  const mxArray *c18_c_y = NULL;
  const mxArray *c18_d_y = NULL;
  uint8_T c18_b_u;
  const mxArray *c18_e_y = NULL;
  const mxArray *c18_f_y = NULL;
  c18_st = NULL;
  c18_st = NULL;
  c18_y = NULL;
  sf_mex_assign(&c18_y, sf_mex_createcellmatrix(3, 1), false);
  c18_b_y = NULL;
  c18_u = *chartInstance->c18_cont;
  c18_c_y = NULL;
  sf_mex_assign(&c18_c_y, sf_mex_create("y", &c18_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c18_b_y, sf_mex_create_fi(sf_mex_dup(c18_eml_mx), sf_mex_dup
    (c18_b_eml_mx), "simulinkarray", c18_c_y, false, false), false);
  sf_mex_setcell(c18_y, 0, c18_b_y);
  c18_d_y = NULL;
  c18_b_u = *chartInstance->c18_out_init;
  c18_e_y = NULL;
  sf_mex_assign(&c18_e_y, sf_mex_create("y", &c18_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c18_d_y, sf_mex_create_fi(sf_mex_dup(c18_eml_mx), sf_mex_dup
    (c18_c_eml_mx), "simulinkarray", c18_e_y, false, false), false);
  sf_mex_setcell(c18_y, 1, c18_d_y);
  c18_f_y = NULL;
  sf_mex_assign(&c18_f_y, sf_mex_create("y",
    &chartInstance->c18_is_active_c18_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c18_y, 2, c18_f_y);
  sf_mex_assign(&c18_st, c18_y, false);
  return c18_st;
}

static void set_sim_state_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c18_st)
{
  emlrtStack c18_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c18_u;
  c18_b_st.tls = chartInstance->c18_fEmlrtCtx;
  chartInstance->c18_doneDoubleBufferReInit = true;
  c18_u = sf_mex_dup(c18_st);
  *chartInstance->c18_cont = c18_emlrt_marshallIn(chartInstance, &c18_b_st,
    sf_mex_dup(sf_mex_getcell(c18_u, 0)), "cont");
  *chartInstance->c18_out_init = c18_c_emlrt_marshallIn(chartInstance, &c18_b_st,
    sf_mex_dup(sf_mex_getcell(c18_u, 1)), "out_init");
  chartInstance->c18_is_active_c18_PWM_28_HalfB = c18_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c18_u, 2)),
     "is_active_c18_PWM_28_HalfB");
  sf_mex_destroy(&c18_u);
  sf_mex_destroy(&c18_st);
}

static void sf_gateway_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c18_PICOffset;
  uint8_T c18_b_enable;
  int16_T c18_b_offset;
  int16_T c18_b_max;
  int16_T c18_b_sum;
  uint8_T c18_b_init;
  uint8_T c18_a0;
  uint8_T c18_a;
  uint8_T c18_b_a0;
  uint8_T c18_a1;
  uint8_T c18_b_a1;
  boolean_T c18_c;
  int8_T c18_i;
  int8_T c18_i1;
  real_T c18_d;
  uint8_T c18_c_a0;
  uint16_T c18_b_cont;
  uint8_T c18_b_a;
  uint8_T c18_b_out_init;
  uint8_T c18_d_a0;
  uint8_T c18_c_a1;
  uint8_T c18_d_a1;
  boolean_T c18_b_c;
  int8_T c18_i2;
  int8_T c18_i3;
  real_T c18_d1;
  int16_T c18_varargin_1;
  int16_T c18_b_varargin_1;
  int16_T c18_c_varargin_1;
  int16_T c18_d_varargin_1;
  int16_T c18_var1;
  int16_T c18_b_var1;
  int16_T c18_i4;
  int16_T c18_i5;
  boolean_T c18_covSaturation;
  boolean_T c18_b_covSaturation;
  uint16_T c18_hfi;
  uint16_T c18_b_hfi;
  uint16_T c18_u;
  uint16_T c18_u1;
  int16_T c18_e_varargin_1;
  int16_T c18_f_varargin_1;
  int16_T c18_g_varargin_1;
  int16_T c18_h_varargin_1;
  int16_T c18_c_var1;
  int16_T c18_d_var1;
  int16_T c18_i6;
  int16_T c18_i7;
  boolean_T c18_c_covSaturation;
  boolean_T c18_d_covSaturation;
  uint16_T c18_c_hfi;
  uint16_T c18_d_hfi;
  uint16_T c18_u2;
  uint16_T c18_u3;
  uint16_T c18_e_a0;
  uint16_T c18_f_a0;
  uint16_T c18_b0;
  uint16_T c18_b_b0;
  uint16_T c18_c_a;
  uint16_T c18_d_a;
  uint16_T c18_b;
  uint16_T c18_b_b;
  uint16_T c18_g_a0;
  uint16_T c18_h_a0;
  uint16_T c18_c_b0;
  uint16_T c18_d_b0;
  uint16_T c18_e_a1;
  uint16_T c18_f_a1;
  uint16_T c18_b1;
  uint16_T c18_b_b1;
  uint16_T c18_g_a1;
  uint16_T c18_h_a1;
  uint16_T c18_c_b1;
  uint16_T c18_d_b1;
  boolean_T c18_c_c;
  boolean_T c18_d_c;
  int16_T c18_i8;
  int16_T c18_i9;
  int16_T c18_i10;
  int16_T c18_i11;
  int16_T c18_i12;
  int16_T c18_i13;
  int16_T c18_i14;
  int16_T c18_i15;
  int16_T c18_i16;
  int16_T c18_i17;
  int16_T c18_i18;
  int16_T c18_i19;
  int32_T c18_i20;
  int32_T c18_i21;
  int16_T c18_i22;
  int16_T c18_i23;
  int16_T c18_i24;
  int16_T c18_i25;
  int16_T c18_i26;
  int16_T c18_i27;
  int16_T c18_i28;
  int16_T c18_i29;
  int16_T c18_i30;
  int16_T c18_i31;
  int16_T c18_i32;
  int16_T c18_i33;
  int32_T c18_i34;
  int32_T c18_i35;
  int16_T c18_i36;
  int16_T c18_i37;
  int16_T c18_i38;
  int16_T c18_i39;
  int16_T c18_i40;
  int16_T c18_i41;
  int16_T c18_i42;
  int16_T c18_i43;
  real_T c18_d2;
  real_T c18_d3;
  int16_T c18_i_varargin_1;
  int16_T c18_i_a0;
  int16_T c18_j_varargin_1;
  int16_T c18_e_b0;
  int16_T c18_e_var1;
  int16_T c18_k_varargin_1;
  int16_T c18_i44;
  int16_T c18_v;
  boolean_T c18_e_covSaturation;
  int16_T c18_val;
  int16_T c18_c_b;
  int32_T c18_i45;
  uint16_T c18_e_hfi;
  real_T c18_d4;
  int32_T c18_i46;
  real_T c18_d5;
  real_T c18_d6;
  real_T c18_d7;
  int32_T c18_i47;
  int32_T c18_i48;
  int32_T c18_i49;
  real_T c18_d8;
  real_T c18_d9;
  int32_T c18_e_c;
  int32_T c18_l_varargin_1;
  int32_T c18_m_varargin_1;
  int32_T c18_f_var1;
  int32_T c18_i50;
  boolean_T c18_f_covSaturation;
  uint16_T c18_f_hfi;
  observerLogReadPIC(&c18_PICOffset);
  chartInstance->c18_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c18_covrtInstance, 4U, (real_T)
                    *chartInstance->c18_init);
  covrtSigUpdateFcn(chartInstance->c18_covrtInstance, 3U, (real_T)
                    *chartInstance->c18_sum);
  covrtSigUpdateFcn(chartInstance->c18_covrtInstance, 2U, (real_T)
                    *chartInstance->c18_max);
  covrtSigUpdateFcn(chartInstance->c18_covrtInstance, 1U, (real_T)
                    *chartInstance->c18_offset);
  covrtSigUpdateFcn(chartInstance->c18_covrtInstance, 0U, (real_T)
                    *chartInstance->c18_enable);
  chartInstance->c18_sfEvent = CALL_EVENT;
  c18_b_enable = *chartInstance->c18_enable;
  c18_b_offset = *chartInstance->c18_offset;
  c18_b_max = *chartInstance->c18_max;
  c18_b_sum = *chartInstance->c18_sum;
  c18_b_init = *chartInstance->c18_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c18_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c18_emlrt_update_log_1(chartInstance, c18_b_enable,
    chartInstance->c18_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c18_emlrt_update_log_2(chartInstance, c18_b_offset,
    chartInstance->c18_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c18_emlrt_update_log_3(chartInstance, c18_b_max,
    chartInstance->c18_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c18_emlrt_update_log_3(chartInstance, c18_b_sum,
    chartInstance->c18_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c18_emlrt_update_log_1(chartInstance, c18_b_init,
    chartInstance->c18_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c18_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c18_covrtInstance, 4U, 0, 0, false);
  c18_a0 = c18_b_enable;
  c18_a = c18_a0;
  c18_b_a0 = c18_a;
  c18_a1 = c18_b_a0;
  c18_b_a1 = c18_a1;
  c18_c = (c18_b_a1 == 0);
  c18_i = (int8_T)c18_b_enable;
  if ((int8_T)(c18_i & 2) != 0) {
    c18_i1 = (int8_T)(c18_i | -2);
  } else {
    c18_i1 = (int8_T)(c18_i & 1);
  }

  if (c18_i1 > 0) {
    c18_d = 3.0;
  } else {
    c18_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c18_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c18_covrtInstance,
        4U, 0U, 0U, c18_d, 0.0, -2, 0U, (int32_T)c18_emlrt_update_log_4
        (chartInstance, c18_c, chartInstance->c18_emlrtLocationLoggingDataTables,
         5)))) {
    c18_b_cont = c18_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c18_emlrtLocationLoggingDataTables, 6);
    c18_b_out_init = c18_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c18_emlrtLocationLoggingDataTables, 7);
  } else {
    c18_c_a0 = c18_b_init;
    c18_b_a = c18_c_a0;
    c18_d_a0 = c18_b_a;
    c18_c_a1 = c18_d_a0;
    c18_d_a1 = c18_c_a1;
    c18_b_c = (c18_d_a1 == 0);
    c18_i2 = (int8_T)c18_b_init;
    if ((int8_T)(c18_i2 & 2) != 0) {
      c18_i3 = (int8_T)(c18_i2 | -2);
    } else {
      c18_i3 = (int8_T)(c18_i2 & 1);
    }

    if (c18_i3 > 0) {
      c18_d1 = 3.0;
    } else {
      c18_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c18_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c18_covrtInstance, 4U, 0U, 1U, c18_d1,
                        0.0, -2, 0U, (int32_T)c18_emlrt_update_log_4
                        (chartInstance, c18_b_c,
                         chartInstance->c18_emlrtLocationLoggingDataTables, 8))))
    {
      c18_b_varargin_1 = c18_b_sum;
      c18_d_varargin_1 = c18_b_varargin_1;
      c18_b_var1 = c18_d_varargin_1;
      c18_i5 = c18_b_var1;
      c18_b_covSaturation = false;
      if (c18_i5 < 0) {
        c18_i5 = 0;
      } else {
        if (c18_i5 > 4095) {
          c18_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c18_covrtInstance, 4, 0, 0, 0,
          c18_b_covSaturation);
      }

      c18_b_hfi = (uint16_T)c18_i5;
      c18_u1 = c18_emlrt_update_log_5(chartInstance, c18_b_hfi,
        chartInstance->c18_emlrtLocationLoggingDataTables, 10);
      c18_f_varargin_1 = c18_b_max;
      c18_h_varargin_1 = c18_f_varargin_1;
      c18_d_var1 = c18_h_varargin_1;
      c18_i7 = c18_d_var1;
      c18_d_covSaturation = false;
      if (c18_i7 < 0) {
        c18_i7 = 0;
      } else {
        if (c18_i7 > 4095) {
          c18_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c18_covrtInstance, 4, 0, 1, 0,
          c18_d_covSaturation);
      }

      c18_d_hfi = (uint16_T)c18_i7;
      c18_u3 = c18_emlrt_update_log_5(chartInstance, c18_d_hfi,
        chartInstance->c18_emlrtLocationLoggingDataTables, 11);
      c18_f_a0 = c18_u1;
      c18_b_b0 = c18_u3;
      c18_d_a = c18_f_a0;
      c18_b_b = c18_b_b0;
      c18_h_a0 = c18_d_a;
      c18_d_b0 = c18_b_b;
      c18_f_a1 = c18_h_a0;
      c18_b_b1 = c18_d_b0;
      c18_h_a1 = c18_f_a1;
      c18_d_b1 = c18_b_b1;
      c18_d_c = (c18_h_a1 < c18_d_b1);
      c18_i9 = (int16_T)c18_u1;
      c18_i11 = (int16_T)c18_u3;
      c18_i13 = (int16_T)c18_u3;
      c18_i15 = (int16_T)c18_u1;
      if ((int16_T)(c18_i13 & 4096) != 0) {
        c18_i17 = (int16_T)(c18_i13 | -4096);
      } else {
        c18_i17 = (int16_T)(c18_i13 & 4095);
      }

      if ((int16_T)(c18_i15 & 4096) != 0) {
        c18_i19 = (int16_T)(c18_i15 | -4096);
      } else {
        c18_i19 = (int16_T)(c18_i15 & 4095);
      }

      c18_i21 = c18_i17 - c18_i19;
      if (c18_i21 > 4095) {
        c18_i21 = 4095;
      } else {
        if (c18_i21 < -4096) {
          c18_i21 = -4096;
        }
      }

      c18_i23 = (int16_T)c18_u1;
      c18_i25 = (int16_T)c18_u3;
      c18_i27 = (int16_T)c18_u1;
      c18_i29 = (int16_T)c18_u3;
      if ((int16_T)(c18_i27 & 4096) != 0) {
        c18_i31 = (int16_T)(c18_i27 | -4096);
      } else {
        c18_i31 = (int16_T)(c18_i27 & 4095);
      }

      if ((int16_T)(c18_i29 & 4096) != 0) {
        c18_i33 = (int16_T)(c18_i29 | -4096);
      } else {
        c18_i33 = (int16_T)(c18_i29 & 4095);
      }

      c18_i35 = c18_i31 - c18_i33;
      if (c18_i35 > 4095) {
        c18_i35 = 4095;
      } else {
        if (c18_i35 < -4096) {
          c18_i35 = -4096;
        }
      }

      if ((int16_T)(c18_i9 & 4096) != 0) {
        c18_i37 = (int16_T)(c18_i9 | -4096);
      } else {
        c18_i37 = (int16_T)(c18_i9 & 4095);
      }

      if ((int16_T)(c18_i11 & 4096) != 0) {
        c18_i39 = (int16_T)(c18_i11 | -4096);
      } else {
        c18_i39 = (int16_T)(c18_i11 & 4095);
      }

      if ((int16_T)(c18_i23 & 4096) != 0) {
        c18_i41 = (int16_T)(c18_i23 | -4096);
      } else {
        c18_i41 = (int16_T)(c18_i23 & 4095);
      }

      if ((int16_T)(c18_i25 & 4096) != 0) {
        c18_i43 = (int16_T)(c18_i25 | -4096);
      } else {
        c18_i43 = (int16_T)(c18_i25 & 4095);
      }

      if (c18_i37 < c18_i39) {
        c18_d3 = (real_T)((int16_T)c18_i21 <= 1);
      } else if (c18_i41 > c18_i43) {
        if ((int16_T)c18_i35 <= 1) {
          c18_d3 = 3.0;
        } else {
          c18_d3 = 0.0;
        }
      } else {
        c18_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c18_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c18_covrtInstance, 4U, 0U, 2U, c18_d3,
                          0.0, -2, 2U, (int32_T)c18_emlrt_update_log_4
                          (chartInstance, c18_d_c,
                           chartInstance->c18_emlrtLocationLoggingDataTables, 9))))
      {
        c18_i_a0 = c18_b_sum;
        c18_e_b0 = c18_b_offset;
        c18_k_varargin_1 = c18_e_b0;
        c18_v = c18_k_varargin_1;
        c18_val = c18_v;
        c18_c_b = c18_val;
        c18_i45 = c18_i_a0;
        if (c18_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c18_d4 = 1.0;
          observerLog(315 + c18_PICOffset, &c18_d4, 1);
        }

        if (c18_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c18_d5 = 1.0;
          observerLog(315 + c18_PICOffset, &c18_d5, 1);
        }

        c18_i46 = c18_c_b;
        if (c18_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c18_d6 = 1.0;
          observerLog(318 + c18_PICOffset, &c18_d6, 1);
        }

        if (c18_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c18_d7 = 1.0;
          observerLog(318 + c18_PICOffset, &c18_d7, 1);
        }

        if ((c18_i45 & 65536) != 0) {
          c18_i47 = c18_i45 | -65536;
        } else {
          c18_i47 = c18_i45 & 65535;
        }

        if ((c18_i46 & 65536) != 0) {
          c18_i48 = c18_i46 | -65536;
        } else {
          c18_i48 = c18_i46 & 65535;
        }

        c18_i49 = c18__s32_add__(chartInstance, c18_i47, c18_i48, 317, 1U, 323,
          12);
        if (c18_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c18_d8 = 1.0;
          observerLog(323 + c18_PICOffset, &c18_d8, 1);
        }

        if (c18_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c18_d9 = 1.0;
          observerLog(323 + c18_PICOffset, &c18_d9, 1);
        }

        if ((c18_i49 & 65536) != 0) {
          c18_e_c = c18_i49 | -65536;
        } else {
          c18_e_c = c18_i49 & 65535;
        }

        c18_l_varargin_1 = c18_emlrt_update_log_6(chartInstance, c18_e_c,
          chartInstance->c18_emlrtLocationLoggingDataTables, 13);
        c18_m_varargin_1 = c18_l_varargin_1;
        c18_f_var1 = c18_m_varargin_1;
        c18_i50 = c18_f_var1;
        c18_f_covSaturation = false;
        if (c18_i50 < 0) {
          c18_i50 = 0;
        } else {
          if (c18_i50 > 4095) {
            c18_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c18_covrtInstance, 4, 0, 2, 0,
            c18_f_covSaturation);
        }

        c18_f_hfi = (uint16_T)c18_i50;
        c18_b_cont = c18_emlrt_update_log_5(chartInstance, c18_f_hfi,
          chartInstance->c18_emlrtLocationLoggingDataTables, 12);
        c18_b_out_init = c18_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c18_emlrtLocationLoggingDataTables, 14);
      } else {
        c18_b_cont = c18_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c18_emlrtLocationLoggingDataTables, 15);
        c18_b_out_init = c18_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c18_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c18_varargin_1 = c18_b_sum;
      c18_c_varargin_1 = c18_varargin_1;
      c18_var1 = c18_c_varargin_1;
      c18_i4 = c18_var1;
      c18_covSaturation = false;
      if (c18_i4 < 0) {
        c18_i4 = 0;
      } else {
        if (c18_i4 > 4095) {
          c18_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c18_covrtInstance, 4, 0, 3, 0,
          c18_covSaturation);
      }

      c18_hfi = (uint16_T)c18_i4;
      c18_u = c18_emlrt_update_log_5(chartInstance, c18_hfi,
        chartInstance->c18_emlrtLocationLoggingDataTables, 18);
      c18_e_varargin_1 = c18_b_max;
      c18_g_varargin_1 = c18_e_varargin_1;
      c18_c_var1 = c18_g_varargin_1;
      c18_i6 = c18_c_var1;
      c18_c_covSaturation = false;
      if (c18_i6 < 0) {
        c18_i6 = 0;
      } else {
        if (c18_i6 > 4095) {
          c18_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c18_covrtInstance, 4, 0, 4, 0,
          c18_c_covSaturation);
      }

      c18_c_hfi = (uint16_T)c18_i6;
      c18_u2 = c18_emlrt_update_log_5(chartInstance, c18_c_hfi,
        chartInstance->c18_emlrtLocationLoggingDataTables, 19);
      c18_e_a0 = c18_u;
      c18_b0 = c18_u2;
      c18_c_a = c18_e_a0;
      c18_b = c18_b0;
      c18_g_a0 = c18_c_a;
      c18_c_b0 = c18_b;
      c18_e_a1 = c18_g_a0;
      c18_b1 = c18_c_b0;
      c18_g_a1 = c18_e_a1;
      c18_c_b1 = c18_b1;
      c18_c_c = (c18_g_a1 < c18_c_b1);
      c18_i8 = (int16_T)c18_u;
      c18_i10 = (int16_T)c18_u2;
      c18_i12 = (int16_T)c18_u2;
      c18_i14 = (int16_T)c18_u;
      if ((int16_T)(c18_i12 & 4096) != 0) {
        c18_i16 = (int16_T)(c18_i12 | -4096);
      } else {
        c18_i16 = (int16_T)(c18_i12 & 4095);
      }

      if ((int16_T)(c18_i14 & 4096) != 0) {
        c18_i18 = (int16_T)(c18_i14 | -4096);
      } else {
        c18_i18 = (int16_T)(c18_i14 & 4095);
      }

      c18_i20 = c18_i16 - c18_i18;
      if (c18_i20 > 4095) {
        c18_i20 = 4095;
      } else {
        if (c18_i20 < -4096) {
          c18_i20 = -4096;
        }
      }

      c18_i22 = (int16_T)c18_u;
      c18_i24 = (int16_T)c18_u2;
      c18_i26 = (int16_T)c18_u;
      c18_i28 = (int16_T)c18_u2;
      if ((int16_T)(c18_i26 & 4096) != 0) {
        c18_i30 = (int16_T)(c18_i26 | -4096);
      } else {
        c18_i30 = (int16_T)(c18_i26 & 4095);
      }

      if ((int16_T)(c18_i28 & 4096) != 0) {
        c18_i32 = (int16_T)(c18_i28 | -4096);
      } else {
        c18_i32 = (int16_T)(c18_i28 & 4095);
      }

      c18_i34 = c18_i30 - c18_i32;
      if (c18_i34 > 4095) {
        c18_i34 = 4095;
      } else {
        if (c18_i34 < -4096) {
          c18_i34 = -4096;
        }
      }

      if ((int16_T)(c18_i8 & 4096) != 0) {
        c18_i36 = (int16_T)(c18_i8 | -4096);
      } else {
        c18_i36 = (int16_T)(c18_i8 & 4095);
      }

      if ((int16_T)(c18_i10 & 4096) != 0) {
        c18_i38 = (int16_T)(c18_i10 | -4096);
      } else {
        c18_i38 = (int16_T)(c18_i10 & 4095);
      }

      if ((int16_T)(c18_i22 & 4096) != 0) {
        c18_i40 = (int16_T)(c18_i22 | -4096);
      } else {
        c18_i40 = (int16_T)(c18_i22 & 4095);
      }

      if ((int16_T)(c18_i24 & 4096) != 0) {
        c18_i42 = (int16_T)(c18_i24 | -4096);
      } else {
        c18_i42 = (int16_T)(c18_i24 & 4095);
      }

      if (c18_i36 < c18_i38) {
        c18_d2 = (real_T)((int16_T)c18_i20 <= 1);
      } else if (c18_i40 > c18_i42) {
        if ((int16_T)c18_i34 <= 1) {
          c18_d2 = 3.0;
        } else {
          c18_d2 = 0.0;
        }
      } else {
        c18_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c18_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c18_covrtInstance, 4U, 0U, 3U, c18_d2,
                          0.0, -2, 2U, (int32_T)c18_emlrt_update_log_4
                          (chartInstance, c18_c_c,
                           chartInstance->c18_emlrtLocationLoggingDataTables, 17))))
      {
        c18_i_varargin_1 = c18_b_sum;
        c18_j_varargin_1 = c18_i_varargin_1;
        c18_e_var1 = c18_j_varargin_1;
        c18_i44 = c18_e_var1;
        c18_e_covSaturation = false;
        if (c18_i44 < 0) {
          c18_i44 = 0;
        } else {
          if (c18_i44 > 4095) {
            c18_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c18_covrtInstance, 4, 0, 5, 0,
            c18_e_covSaturation);
        }

        c18_e_hfi = (uint16_T)c18_i44;
        c18_b_cont = c18_emlrt_update_log_5(chartInstance, c18_e_hfi,
          chartInstance->c18_emlrtLocationLoggingDataTables, 20);
        c18_b_out_init = c18_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c18_emlrtLocationLoggingDataTables, 21);
      } else {
        c18_b_cont = c18_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c18_emlrtLocationLoggingDataTables, 22);
        c18_b_out_init = c18_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c18_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c18_cont = c18_b_cont;
  *chartInstance->c18_out_init = c18_b_out_init;
  c18_do_animation_call_c18_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c18_covrtInstance, 5U, (real_T)
                    *chartInstance->c18_cont);
  covrtSigUpdateFcn(chartInstance->c18_covrtInstance, 6U, (real_T)
                    *chartInstance->c18_out_init);
}

static void mdl_start_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c18_decisionTxtStartIdx = 0U;
  static const uint32_T c18_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c18_chart_data_browse_helper);
  chartInstance->c18_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c18_RuntimeVar,
    &chartInstance->c18_IsDebuggerActive,
    &chartInstance->c18_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c18_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c18_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c18_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c18_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c18_decisionTxtStartIdx, &c18_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c18_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c18_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c18_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c18_PWM_28_HalfB
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c18_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7617",              /* mexFileName */
    "Thu May 27 10:27:25 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c18_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c18_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c18_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c18_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7617");
    emlrtLocationLoggingPushLog(&c18_emlrtLocationLoggingFileInfo,
      c18_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c18_emlrtLocationLoggingDataTables, c18_emlrtLocationInfo,
      NULL, 0U, c18_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7617");
  }

  sfListenerLightTerminate(chartInstance->c18_RuntimeVar);
  sf_mex_destroy(&c18_eml_mx);
  sf_mex_destroy(&c18_b_eml_mx);
  sf_mex_destroy(&c18_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c18_covrtInstance);
}

static void initSimStructsc18_PWM_28_HalfB(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c18_emlrt_update_log_1(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index)
{
  boolean_T c18_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c18_b_table;
  real_T c18_d;
  uint8_T c18_u;
  uint8_T c18_localMin;
  real_T c18_d1;
  uint8_T c18_u1;
  uint8_T c18_localMax;
  emlrtLocationLoggingHistogramType *c18_histTable;
  real_T c18_inDouble;
  real_T c18_significand;
  int32_T c18_exponent;
  (void)chartInstance;
  c18_isLoggingEnabledHere = (c18_index >= 0);
  if (c18_isLoggingEnabledHere) {
    c18_b_table = (emlrtLocationLoggingDataType *)&c18_table[c18_index];
    c18_d = c18_b_table[0U].SimMin;
    if (c18_d < 2.0) {
      if (c18_d >= 0.0) {
        c18_u = (uint8_T)c18_d;
      } else {
        c18_u = 0U;
      }
    } else if (c18_d >= 2.0) {
      c18_u = 1U;
    } else {
      c18_u = 0U;
    }

    c18_localMin = c18_u;
    c18_d1 = c18_b_table[0U].SimMax;
    if (c18_d1 < 2.0) {
      if (c18_d1 >= 0.0) {
        c18_u1 = (uint8_T)c18_d1;
      } else {
        c18_u1 = 0U;
      }
    } else if (c18_d1 >= 2.0) {
      c18_u1 = 1U;
    } else {
      c18_u1 = 0U;
    }

    c18_localMax = c18_u1;
    c18_histTable = c18_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c18_in < c18_localMin) {
      c18_localMin = c18_in;
    }

    if (c18_in > c18_localMax) {
      c18_localMax = c18_in;
    }

    /* Histogram logging. */
    c18_inDouble = (real_T)c18_in;
    c18_histTable->TotalNumberOfValues++;
    if (c18_inDouble == 0.0) {
      c18_histTable->NumberOfZeros++;
    } else {
      c18_histTable->SimSum += c18_inDouble;
      if ((!muDoubleScalarIsInf(c18_inDouble)) && (!muDoubleScalarIsNaN
           (c18_inDouble))) {
        c18_significand = frexp(c18_inDouble, &c18_exponent);
        if (c18_exponent > 128) {
          c18_exponent = 128;
        }

        if (c18_exponent < -127) {
          c18_exponent = -127;
        }

        if (c18_significand < 0.0) {
          c18_histTable->NumberOfNegativeValues++;
          c18_histTable->HistogramOfNegativeValues[127 + c18_exponent]++;
        } else {
          c18_histTable->NumberOfPositiveValues++;
          c18_histTable->HistogramOfPositiveValues[127 + c18_exponent]++;
        }
      }
    }

    c18_b_table[0U].SimMin = (real_T)c18_localMin;
    c18_b_table[0U].SimMax = (real_T)c18_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c18_in;
}

static int16_T c18_emlrt_update_log_2(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index)
{
  boolean_T c18_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c18_b_table;
  real_T c18_d;
  int16_T c18_i;
  int16_T c18_localMin;
  real_T c18_d1;
  int16_T c18_i1;
  int16_T c18_localMax;
  emlrtLocationLoggingHistogramType *c18_histTable;
  real_T c18_inDouble;
  real_T c18_significand;
  int32_T c18_exponent;
  (void)chartInstance;
  c18_isLoggingEnabledHere = (c18_index >= 0);
  if (c18_isLoggingEnabledHere) {
    c18_b_table = (emlrtLocationLoggingDataType *)&c18_table[c18_index];
    c18_d = muDoubleScalarFloor(c18_b_table[0U].SimMin);
    if (c18_d < 32768.0) {
      if (c18_d >= -32768.0) {
        c18_i = (int16_T)c18_d;
      } else {
        c18_i = MIN_int16_T;
      }
    } else if (c18_d >= 32768.0) {
      c18_i = MAX_int16_T;
    } else {
      c18_i = 0;
    }

    c18_localMin = c18_i;
    c18_d1 = muDoubleScalarFloor(c18_b_table[0U].SimMax);
    if (c18_d1 < 32768.0) {
      if (c18_d1 >= -32768.0) {
        c18_i1 = (int16_T)c18_d1;
      } else {
        c18_i1 = MIN_int16_T;
      }
    } else if (c18_d1 >= 32768.0) {
      c18_i1 = MAX_int16_T;
    } else {
      c18_i1 = 0;
    }

    c18_localMax = c18_i1;
    c18_histTable = c18_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c18_in < c18_localMin) {
      c18_localMin = c18_in;
    }

    if (c18_in > c18_localMax) {
      c18_localMax = c18_in;
    }

    /* Histogram logging. */
    c18_inDouble = (real_T)c18_in;
    c18_histTable->TotalNumberOfValues++;
    if (c18_inDouble == 0.0) {
      c18_histTable->NumberOfZeros++;
    } else {
      c18_histTable->SimSum += c18_inDouble;
      if ((!muDoubleScalarIsInf(c18_inDouble)) && (!muDoubleScalarIsNaN
           (c18_inDouble))) {
        c18_significand = frexp(c18_inDouble, &c18_exponent);
        if (c18_exponent > 128) {
          c18_exponent = 128;
        }

        if (c18_exponent < -127) {
          c18_exponent = -127;
        }

        if (c18_significand < 0.0) {
          c18_histTable->NumberOfNegativeValues++;
          c18_histTable->HistogramOfNegativeValues[127 + c18_exponent]++;
        } else {
          c18_histTable->NumberOfPositiveValues++;
          c18_histTable->HistogramOfPositiveValues[127 + c18_exponent]++;
        }
      }
    }

    c18_b_table[0U].SimMin = (real_T)c18_localMin;
    c18_b_table[0U].SimMax = (real_T)c18_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c18_in;
}

static int16_T c18_emlrt_update_log_3(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index)
{
  boolean_T c18_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c18_b_table;
  real_T c18_d;
  int16_T c18_i;
  int16_T c18_localMin;
  real_T c18_d1;
  int16_T c18_i1;
  int16_T c18_localMax;
  emlrtLocationLoggingHistogramType *c18_histTable;
  real_T c18_inDouble;
  real_T c18_significand;
  int32_T c18_exponent;
  (void)chartInstance;
  c18_isLoggingEnabledHere = (c18_index >= 0);
  if (c18_isLoggingEnabledHere) {
    c18_b_table = (emlrtLocationLoggingDataType *)&c18_table[c18_index];
    c18_d = muDoubleScalarFloor(c18_b_table[0U].SimMin);
    if (c18_d < 2048.0) {
      if (c18_d >= -2048.0) {
        c18_i = (int16_T)c18_d;
      } else {
        c18_i = -2048;
      }
    } else if (c18_d >= 2048.0) {
      c18_i = 2047;
    } else {
      c18_i = 0;
    }

    c18_localMin = c18_i;
    c18_d1 = muDoubleScalarFloor(c18_b_table[0U].SimMax);
    if (c18_d1 < 2048.0) {
      if (c18_d1 >= -2048.0) {
        c18_i1 = (int16_T)c18_d1;
      } else {
        c18_i1 = -2048;
      }
    } else if (c18_d1 >= 2048.0) {
      c18_i1 = 2047;
    } else {
      c18_i1 = 0;
    }

    c18_localMax = c18_i1;
    c18_histTable = c18_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c18_in < c18_localMin) {
      c18_localMin = c18_in;
    }

    if (c18_in > c18_localMax) {
      c18_localMax = c18_in;
    }

    /* Histogram logging. */
    c18_inDouble = (real_T)c18_in;
    c18_histTable->TotalNumberOfValues++;
    if (c18_inDouble == 0.0) {
      c18_histTable->NumberOfZeros++;
    } else {
      c18_histTable->SimSum += c18_inDouble;
      if ((!muDoubleScalarIsInf(c18_inDouble)) && (!muDoubleScalarIsNaN
           (c18_inDouble))) {
        c18_significand = frexp(c18_inDouble, &c18_exponent);
        if (c18_exponent > 128) {
          c18_exponent = 128;
        }

        if (c18_exponent < -127) {
          c18_exponent = -127;
        }

        if (c18_significand < 0.0) {
          c18_histTable->NumberOfNegativeValues++;
          c18_histTable->HistogramOfNegativeValues[127 + c18_exponent]++;
        } else {
          c18_histTable->NumberOfPositiveValues++;
          c18_histTable->HistogramOfPositiveValues[127 + c18_exponent]++;
        }
      }
    }

    c18_b_table[0U].SimMin = (real_T)c18_localMin;
    c18_b_table[0U].SimMax = (real_T)c18_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c18_in;
}

static boolean_T c18_emlrt_update_log_4(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index)
{
  boolean_T c18_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c18_b_table;
  boolean_T c18_localMin;
  boolean_T c18_localMax;
  emlrtLocationLoggingHistogramType *c18_histTable;
  real_T c18_inDouble;
  real_T c18_significand;
  int32_T c18_exponent;
  (void)chartInstance;
  c18_isLoggingEnabledHere = (c18_index >= 0);
  if (c18_isLoggingEnabledHere) {
    c18_b_table = (emlrtLocationLoggingDataType *)&c18_table[c18_index];
    c18_localMin = (c18_b_table[0U].SimMin > 0.0);
    c18_localMax = (c18_b_table[0U].SimMax > 0.0);
    c18_histTable = c18_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c18_in < (int32_T)c18_localMin) {
      c18_localMin = c18_in;
    }

    if ((int32_T)c18_in > (int32_T)c18_localMax) {
      c18_localMax = c18_in;
    }

    /* Histogram logging. */
    c18_inDouble = (real_T)c18_in;
    c18_histTable->TotalNumberOfValues++;
    if (c18_inDouble == 0.0) {
      c18_histTable->NumberOfZeros++;
    } else {
      c18_histTable->SimSum += c18_inDouble;
      if ((!muDoubleScalarIsInf(c18_inDouble)) && (!muDoubleScalarIsNaN
           (c18_inDouble))) {
        c18_significand = frexp(c18_inDouble, &c18_exponent);
        if (c18_exponent > 128) {
          c18_exponent = 128;
        }

        if (c18_exponent < -127) {
          c18_exponent = -127;
        }

        if (c18_significand < 0.0) {
          c18_histTable->NumberOfNegativeValues++;
          c18_histTable->HistogramOfNegativeValues[127 + c18_exponent]++;
        } else {
          c18_histTable->NumberOfPositiveValues++;
          c18_histTable->HistogramOfPositiveValues[127 + c18_exponent]++;
        }
      }
    }

    c18_b_table[0U].SimMin = (real_T)c18_localMin;
    c18_b_table[0U].SimMax = (real_T)c18_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c18_in;
}

static uint16_T c18_emlrt_update_log_5(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index)
{
  boolean_T c18_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c18_b_table;
  real_T c18_d;
  uint16_T c18_u;
  uint16_T c18_localMin;
  real_T c18_d1;
  uint16_T c18_u1;
  uint16_T c18_localMax;
  emlrtLocationLoggingHistogramType *c18_histTable;
  real_T c18_inDouble;
  real_T c18_significand;
  int32_T c18_exponent;
  (void)chartInstance;
  c18_isLoggingEnabledHere = (c18_index >= 0);
  if (c18_isLoggingEnabledHere) {
    c18_b_table = (emlrtLocationLoggingDataType *)&c18_table[c18_index];
    c18_d = c18_b_table[0U].SimMin;
    if (c18_d < 4096.0) {
      if (c18_d >= 0.0) {
        c18_u = (uint16_T)c18_d;
      } else {
        c18_u = 0U;
      }
    } else if (c18_d >= 4096.0) {
      c18_u = 4095U;
    } else {
      c18_u = 0U;
    }

    c18_localMin = c18_u;
    c18_d1 = c18_b_table[0U].SimMax;
    if (c18_d1 < 4096.0) {
      if (c18_d1 >= 0.0) {
        c18_u1 = (uint16_T)c18_d1;
      } else {
        c18_u1 = 0U;
      }
    } else if (c18_d1 >= 4096.0) {
      c18_u1 = 4095U;
    } else {
      c18_u1 = 0U;
    }

    c18_localMax = c18_u1;
    c18_histTable = c18_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c18_in < c18_localMin) {
      c18_localMin = c18_in;
    }

    if (c18_in > c18_localMax) {
      c18_localMax = c18_in;
    }

    /* Histogram logging. */
    c18_inDouble = (real_T)c18_in;
    c18_histTable->TotalNumberOfValues++;
    if (c18_inDouble == 0.0) {
      c18_histTable->NumberOfZeros++;
    } else {
      c18_histTable->SimSum += c18_inDouble;
      if ((!muDoubleScalarIsInf(c18_inDouble)) && (!muDoubleScalarIsNaN
           (c18_inDouble))) {
        c18_significand = frexp(c18_inDouble, &c18_exponent);
        if (c18_exponent > 128) {
          c18_exponent = 128;
        }

        if (c18_exponent < -127) {
          c18_exponent = -127;
        }

        if (c18_significand < 0.0) {
          c18_histTable->NumberOfNegativeValues++;
          c18_histTable->HistogramOfNegativeValues[127 + c18_exponent]++;
        } else {
          c18_histTable->NumberOfPositiveValues++;
          c18_histTable->HistogramOfPositiveValues[127 + c18_exponent]++;
        }
      }
    }

    c18_b_table[0U].SimMin = (real_T)c18_localMin;
    c18_b_table[0U].SimMax = (real_T)c18_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c18_in;
}

static int32_T c18_emlrt_update_log_6(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c18_in, emlrtLocationLoggingDataType c18_table[],
  int32_T c18_index)
{
  boolean_T c18_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c18_b_table;
  real_T c18_d;
  int32_T c18_i;
  int32_T c18_localMin;
  real_T c18_d1;
  int32_T c18_i1;
  int32_T c18_localMax;
  emlrtLocationLoggingHistogramType *c18_histTable;
  real_T c18_inDouble;
  real_T c18_significand;
  int32_T c18_exponent;
  (void)chartInstance;
  c18_isLoggingEnabledHere = (c18_index >= 0);
  if (c18_isLoggingEnabledHere) {
    c18_b_table = (emlrtLocationLoggingDataType *)&c18_table[c18_index];
    c18_d = muDoubleScalarFloor(c18_b_table[0U].SimMin);
    if (c18_d < 65536.0) {
      if (c18_d >= -65536.0) {
        c18_i = (int32_T)c18_d;
      } else {
        c18_i = -65536;
      }
    } else if (c18_d >= 65536.0) {
      c18_i = 65535;
    } else {
      c18_i = 0;
    }

    c18_localMin = c18_i;
    c18_d1 = muDoubleScalarFloor(c18_b_table[0U].SimMax);
    if (c18_d1 < 65536.0) {
      if (c18_d1 >= -65536.0) {
        c18_i1 = (int32_T)c18_d1;
      } else {
        c18_i1 = -65536;
      }
    } else if (c18_d1 >= 65536.0) {
      c18_i1 = 65535;
    } else {
      c18_i1 = 0;
    }

    c18_localMax = c18_i1;
    c18_histTable = c18_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c18_in < c18_localMin) {
      c18_localMin = c18_in;
    }

    if (c18_in > c18_localMax) {
      c18_localMax = c18_in;
    }

    /* Histogram logging. */
    c18_inDouble = (real_T)c18_in;
    c18_histTable->TotalNumberOfValues++;
    if (c18_inDouble == 0.0) {
      c18_histTable->NumberOfZeros++;
    } else {
      c18_histTable->SimSum += c18_inDouble;
      if ((!muDoubleScalarIsInf(c18_inDouble)) && (!muDoubleScalarIsNaN
           (c18_inDouble))) {
        c18_significand = frexp(c18_inDouble, &c18_exponent);
        if (c18_exponent > 128) {
          c18_exponent = 128;
        }

        if (c18_exponent < -127) {
          c18_exponent = -127;
        }

        if (c18_significand < 0.0) {
          c18_histTable->NumberOfNegativeValues++;
          c18_histTable->HistogramOfNegativeValues[127 + c18_exponent]++;
        } else {
          c18_histTable->NumberOfPositiveValues++;
          c18_histTable->HistogramOfPositiveValues[127 + c18_exponent]++;
        }
      }
    }

    c18_b_table[0U].SimMin = (real_T)c18_localMin;
    c18_b_table[0U].SimMax = (real_T)c18_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c18_in;
}

static void c18_emlrtInitVarDataTables(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c18_dataTables[24],
  emlrtLocationLoggingHistogramType c18_histTables[24])
{
  int32_T c18_i;
  int32_T c18_iH;
  (void)chartInstance;
  for (c18_i = 0; c18_i < 24; c18_i++) {
    c18_dataTables[c18_i].SimMin = rtInf;
    c18_dataTables[c18_i].SimMax = rtMinusInf;
    c18_dataTables[c18_i].OverflowWraps = 0;
    c18_dataTables[c18_i].Saturations = 0;
    c18_dataTables[c18_i].IsAlwaysInteger = true;
    c18_dataTables[c18_i].HistogramTable = &c18_histTables[c18_i];
    c18_histTables[c18_i].NumberOfZeros = 0.0;
    c18_histTables[c18_i].NumberOfPositiveValues = 0.0;
    c18_histTables[c18_i].NumberOfNegativeValues = 0.0;
    c18_histTables[c18_i].TotalNumberOfValues = 0.0;
    c18_histTables[c18_i].SimSum = 0.0;
    for (c18_iH = 0; c18_iH < 256; c18_iH++) {
      c18_histTables[c18_i].HistogramOfPositiveValues[c18_iH] = 0.0;
      c18_histTables[c18_i].HistogramOfNegativeValues[c18_iH] = 0.0;
    }
  }
}

const mxArray *sf_c18_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c18_nameCaptureInfo = NULL;
  c18_nameCaptureInfo = NULL;
  sf_mex_assign(&c18_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c18_nameCaptureInfo;
}

static uint16_T c18_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c18_sp, const mxArray *c18_b_cont, const
  char_T *c18_identifier)
{
  uint16_T c18_y;
  emlrtMsgIdentifier c18_thisId;
  c18_thisId.fIdentifier = (const char *)c18_identifier;
  c18_thisId.fParent = NULL;
  c18_thisId.bParentIsCell = false;
  c18_y = c18_b_emlrt_marshallIn(chartInstance, c18_sp, sf_mex_dup(c18_b_cont),
    &c18_thisId);
  sf_mex_destroy(&c18_b_cont);
  return c18_y;
}

static uint16_T c18_b_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c18_sp, const mxArray *c18_u, const
  emlrtMsgIdentifier *c18_parentId)
{
  uint16_T c18_y;
  const mxArray *c18_mxFi = NULL;
  const mxArray *c18_mxInt = NULL;
  uint16_T c18_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c18_parentId, c18_u, false, 0U, NULL, c18_eml_mx, c18_b_eml_mx);
  sf_mex_assign(&c18_mxFi, sf_mex_dup(c18_u), false);
  sf_mex_assign(&c18_mxInt, sf_mex_call(c18_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c18_mxFi)), false);
  sf_mex_import(c18_parentId, sf_mex_dup(c18_mxInt), &c18_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c18_mxFi);
  sf_mex_destroy(&c18_mxInt);
  c18_y = c18_b_u;
  sf_mex_destroy(&c18_mxFi);
  sf_mex_destroy(&c18_u);
  return c18_y;
}

static uint8_T c18_c_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c18_sp, const mxArray *c18_b_out_init, const
  char_T *c18_identifier)
{
  uint8_T c18_y;
  emlrtMsgIdentifier c18_thisId;
  c18_thisId.fIdentifier = (const char *)c18_identifier;
  c18_thisId.fParent = NULL;
  c18_thisId.bParentIsCell = false;
  c18_y = c18_d_emlrt_marshallIn(chartInstance, c18_sp, sf_mex_dup
    (c18_b_out_init), &c18_thisId);
  sf_mex_destroy(&c18_b_out_init);
  return c18_y;
}

static uint8_T c18_d_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c18_sp, const mxArray *c18_u, const
  emlrtMsgIdentifier *c18_parentId)
{
  uint8_T c18_y;
  const mxArray *c18_mxFi = NULL;
  const mxArray *c18_mxInt = NULL;
  uint8_T c18_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c18_parentId, c18_u, false, 0U, NULL, c18_eml_mx, c18_c_eml_mx);
  sf_mex_assign(&c18_mxFi, sf_mex_dup(c18_u), false);
  sf_mex_assign(&c18_mxInt, sf_mex_call(c18_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c18_mxFi)), false);
  sf_mex_import(c18_parentId, sf_mex_dup(c18_mxInt), &c18_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c18_mxFi);
  sf_mex_destroy(&c18_mxInt);
  c18_y = c18_b_u;
  sf_mex_destroy(&c18_mxFi);
  sf_mex_destroy(&c18_u);
  return c18_y;
}

static uint8_T c18_e_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c18_b_is_active_c18_PWM_28_HalfB, const char_T *
  c18_identifier)
{
  uint8_T c18_y;
  emlrtMsgIdentifier c18_thisId;
  c18_thisId.fIdentifier = (const char *)c18_identifier;
  c18_thisId.fParent = NULL;
  c18_thisId.bParentIsCell = false;
  c18_y = c18_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c18_b_is_active_c18_PWM_28_HalfB), &c18_thisId);
  sf_mex_destroy(&c18_b_is_active_c18_PWM_28_HalfB);
  return c18_y;
}

static uint8_T c18_f_emlrt_marshallIn(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c18_u, const emlrtMsgIdentifier *c18_parentId)
{
  uint8_T c18_y;
  uint8_T c18_b_u;
  (void)chartInstance;
  sf_mex_import(c18_parentId, sf_mex_dup(c18_u), &c18_b_u, 1, 3, 0U, 0, 0U, 0);
  c18_y = c18_b_u;
  sf_mex_destroy(&c18_u);
  return c18_y;
}

static const mxArray *c18_chart_data_browse_helper
  (SFc18_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c18_ssIdNumber)
{
  const mxArray *c18_mxData = NULL;
  uint8_T c18_u;
  int16_T c18_i;
  int16_T c18_i1;
  int16_T c18_i2;
  uint16_T c18_u1;
  uint8_T c18_u2;
  uint8_T c18_u3;
  real_T c18_d;
  real_T c18_d1;
  real_T c18_d2;
  real_T c18_d3;
  real_T c18_d4;
  real_T c18_d5;
  c18_mxData = NULL;
  switch (c18_ssIdNumber) {
   case 18U:
    c18_u = *chartInstance->c18_enable;
    c18_d = (real_T)c18_u;
    sf_mex_assign(&c18_mxData, sf_mex_create("mxData", &c18_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c18_i = *chartInstance->c18_offset;
    sf_mex_assign(&c18_mxData, sf_mex_create("mxData", &c18_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c18_i1 = *chartInstance->c18_max;
    c18_d1 = (real_T)c18_i1;
    sf_mex_assign(&c18_mxData, sf_mex_create("mxData", &c18_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c18_i2 = *chartInstance->c18_sum;
    c18_d2 = (real_T)c18_i2;
    sf_mex_assign(&c18_mxData, sf_mex_create("mxData", &c18_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c18_u1 = *chartInstance->c18_cont;
    c18_d3 = (real_T)c18_u1;
    sf_mex_assign(&c18_mxData, sf_mex_create("mxData", &c18_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c18_u2 = *chartInstance->c18_init;
    c18_d4 = (real_T)c18_u2;
    sf_mex_assign(&c18_mxData, sf_mex_create("mxData", &c18_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c18_u3 = *chartInstance->c18_out_init;
    c18_d5 = (real_T)c18_u3;
    sf_mex_assign(&c18_mxData, sf_mex_create("mxData", &c18_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c18_mxData;
}

static int32_T c18__s32_add__(SFc18_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c18_b, int32_T c18_c, int32_T c18_EMLOvCount_src_loc, uint32_T
  c18_ssid_src_loc, int32_T c18_offset_src_loc, int32_T c18_length_src_loc)
{
  int32_T c18_a;
  int32_T c18_PICOffset;
  real_T c18_d;
  observerLogReadPIC(&c18_PICOffset);
  c18_a = c18_b + c18_c;
  if (((c18_a ^ c18_b) & (c18_a ^ c18_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c18_ssid_src_loc,
      c18_offset_src_loc, c18_length_src_loc);
    c18_d = 1.0;
    observerLog(c18_EMLOvCount_src_loc + c18_PICOffset, &c18_d, 1);
  }

  return c18_a;
}

static void init_dsm_address_info(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc18_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c18_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c18_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c18_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c18_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c18_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c18_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c18_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c18_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c18_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c18_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c18_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c18_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c18_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c18_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyyoUV8QLhvvJFFv"
    "EdiTpoTwlwQAADsJCCV"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c18_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c18_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c18_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c18_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c18_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c18_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c18_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c18_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc18_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c18_PWM_28_HalfB
      ((SFc18_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c18_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c18_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c18_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc18_PWM_28_HalfB((SFc18_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c18_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5SNDUcLspEBRods0qqIOk8CpxTEmxAKlWQudnJ4yHT+JAwxlmfmS758glmhN",
    "k2UUukF0XRW9QoEfoG4qSZYqk4igxUqADUMQMv/fN+5+RV+v0PByb+Lz81vOu4vsaPnVvOjayeW",
    "3hma43vB+z+UMUEjbuE0Vi7VUOQWJ4Clpya5gUHTGUhTAmhqBAUMQmUpkyNs1iy5kYt62gjk+/i",
    "BiNgkhaHu6hLAkPBD9FtsSaPvI0mQJq2gChiZS0o6jNyWiusTLHfgR0rG1cZYIGE9jEqaV7lhuW",
    "cGidAO0IbQhqrM90Cwwx4JuTUjOdpTqYAWWccEZEobUR0QEk6GADz5IQfw+sQaPyMBoRZfYgIhP",
    "QXTZOOaWAPCfT+OGICWKkYoS3Yu47wWXd+hz16ckQeIVDULc9BWScSCZMefyDNlraEuSIQxOO7K",
    "icLYBX1gX/OYNjUKV+G/pyAoqM4ECUbpo6pHWSRmueJcsww2J4TtQjivHTEJZmL2aODgjGCQ5Ro",
    "gwGqZEdfajYBN1bymbjjsvMVSVj42mw9SpYytaaQFUU5mxtKnzCuS6FHcqkCxPgKWuTGFINm7IW",
    "47Rm4aFEB7v0Lq8GKxgGPoP5UoSsMFyTHCDtO79gYzmPpFYbGfuYvM1ud/nzMqwjDKghoVDUBRR",
    "hGtBnqXvL2UKmXewRiFqZVL0i8DRDVqE8PbSieSzVGH1S0UTOTHARLQXGeoSxxEp4prFoqmAulq",
    "twlNAIQtdgGIcelg1iC3yiXWt7hHU3Yea0CZoqlhRF1Z0/P3ln58+NDzh/ZnL59+0FnloBj7fwd",
    "vidBfz1+nn8Rm7f+mzNjUx+d0H+69x+jZy8w225ynn97s1v3/z91Z9P3r7v/AUmb39ej9qSHjVv",
    "tn9y5WLn9mY2/37WIOcJP1nKM4fdX9CrUcD/3QL/VjbX9/bbQW/8cvvV0214rAz/NVbiZz/l+71",
    "ere+VnL6z9VuuU58mad/VinbC7ELh5sROj9l8PK+u8Mf1bH06/nm4nvyd3Xwci/zVOOevhkelMJ",
    "sl+Xi5+t/fzcsX6X8tF283l9YMmGCfyI5bu+vJT/fvr7DjZs6Om+m9YkBct4IB3d4Z9F/0Bnd3B",
    "vuED/eW+8zH1utF5bxLlvuv6Pm/Xz6/fR9yDm98pFx9zXP/suTWte+i95EvDV91nnk5/NYXbMe6",
    "98TPjf/Du9g97ods/mD+F8uPGA8LbtvZ5y6QYdHXS7DvXzzSoII=",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c18_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c18_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c18_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c18_PWM_28_HalfB(SimStruct *S)
{
  SFc18_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc18_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc18_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc18_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c18_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c18_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c18_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c18_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c18_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c18_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c18_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c18_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c18_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c18_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c18_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c18_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c18_JITStateAnimation,
    chartInstance->c18_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c18_PWM_28_HalfB(chartInstance);
}

void c18_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c18_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c18_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c18_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c18_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
