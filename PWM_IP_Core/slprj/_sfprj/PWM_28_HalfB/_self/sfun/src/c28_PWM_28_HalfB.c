/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c28_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c28_eml_mx;
static const mxArray *c28_b_eml_mx;
static const mxArray *c28_c_eml_mx;

/* Function Declarations */
static void initialize_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c28_update_jit_animation_state_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance);
static void c28_do_animation_call_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c28_st);
static void sf_gateway_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c28_emlrt_update_log_1(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index);
static int16_T c28_emlrt_update_log_2(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index);
static int16_T c28_emlrt_update_log_3(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index);
static boolean_T c28_emlrt_update_log_4(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index);
static uint16_T c28_emlrt_update_log_5(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index);
static int32_T c28_emlrt_update_log_6(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index);
static void c28_emlrtInitVarDataTables(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c28_dataTables[24],
  emlrtLocationLoggingHistogramType c28_histTables[24]);
static uint16_T c28_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c28_sp, const mxArray *c28_b_cont, const
  char_T *c28_identifier);
static uint16_T c28_b_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c28_sp, const mxArray *c28_u, const
  emlrtMsgIdentifier *c28_parentId);
static uint8_T c28_c_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c28_sp, const mxArray *c28_b_out_init, const
  char_T *c28_identifier);
static uint8_T c28_d_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c28_sp, const mxArray *c28_u, const
  emlrtMsgIdentifier *c28_parentId);
static uint8_T c28_e_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c28_b_is_active_c28_PWM_28_HalfB, const char_T *
  c28_identifier);
static uint8_T c28_f_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c28_u, const emlrtMsgIdentifier *c28_parentId);
static const mxArray *c28_chart_data_browse_helper
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c28_ssIdNumber);
static int32_T c28__s32_add__(SFc28_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c28_b, int32_T c28_c, int32_T c28_EMLOvCount_src_loc, uint32_T
  c28_ssid_src_loc, int32_T c28_offset_src_loc, int32_T c28_length_src_loc);
static void init_dsm_address_info(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c28_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c28_st.tls = chartInstance->c28_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c28_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c28_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c28_is_active_c28_PWM_28_HalfB = 0U;
  sf_mex_assign(&c28_c_eml_mx, sf_mex_call(&c28_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c28_b_eml_mx, sf_mex_call(&c28_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c28_eml_mx, sf_mex_call(&c28_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c28_emlrtLocLogSimulated = false;
  c28_emlrtInitVarDataTables(chartInstance,
    chartInstance->c28_emlrtLocationLoggingDataTables,
    chartInstance->c28_emlrtLocLogHistTables);
}

static void initialize_params_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c28_update_jit_animation_state_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c28_do_animation_call_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c28_st;
  const mxArray *c28_y = NULL;
  const mxArray *c28_b_y = NULL;
  uint16_T c28_u;
  const mxArray *c28_c_y = NULL;
  const mxArray *c28_d_y = NULL;
  uint8_T c28_b_u;
  const mxArray *c28_e_y = NULL;
  const mxArray *c28_f_y = NULL;
  c28_st = NULL;
  c28_st = NULL;
  c28_y = NULL;
  sf_mex_assign(&c28_y, sf_mex_createcellmatrix(3, 1), false);
  c28_b_y = NULL;
  c28_u = *chartInstance->c28_cont;
  c28_c_y = NULL;
  sf_mex_assign(&c28_c_y, sf_mex_create("y", &c28_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c28_b_y, sf_mex_create_fi(sf_mex_dup(c28_eml_mx), sf_mex_dup
    (c28_b_eml_mx), "simulinkarray", c28_c_y, false, false), false);
  sf_mex_setcell(c28_y, 0, c28_b_y);
  c28_d_y = NULL;
  c28_b_u = *chartInstance->c28_out_init;
  c28_e_y = NULL;
  sf_mex_assign(&c28_e_y, sf_mex_create("y", &c28_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c28_d_y, sf_mex_create_fi(sf_mex_dup(c28_eml_mx), sf_mex_dup
    (c28_c_eml_mx), "simulinkarray", c28_e_y, false, false), false);
  sf_mex_setcell(c28_y, 1, c28_d_y);
  c28_f_y = NULL;
  sf_mex_assign(&c28_f_y, sf_mex_create("y",
    &chartInstance->c28_is_active_c28_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c28_y, 2, c28_f_y);
  sf_mex_assign(&c28_st, c28_y, false);
  return c28_st;
}

static void set_sim_state_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c28_st)
{
  emlrtStack c28_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c28_u;
  c28_b_st.tls = chartInstance->c28_fEmlrtCtx;
  chartInstance->c28_doneDoubleBufferReInit = true;
  c28_u = sf_mex_dup(c28_st);
  *chartInstance->c28_cont = c28_emlrt_marshallIn(chartInstance, &c28_b_st,
    sf_mex_dup(sf_mex_getcell(c28_u, 0)), "cont");
  *chartInstance->c28_out_init = c28_c_emlrt_marshallIn(chartInstance, &c28_b_st,
    sf_mex_dup(sf_mex_getcell(c28_u, 1)), "out_init");
  chartInstance->c28_is_active_c28_PWM_28_HalfB = c28_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c28_u, 2)),
     "is_active_c28_PWM_28_HalfB");
  sf_mex_destroy(&c28_u);
  sf_mex_destroy(&c28_st);
}

static void sf_gateway_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c28_PICOffset;
  uint8_T c28_b_enable;
  int16_T c28_b_offset;
  int16_T c28_b_max;
  int16_T c28_b_sum;
  uint8_T c28_b_init;
  uint8_T c28_a0;
  uint8_T c28_a;
  uint8_T c28_b_a0;
  uint8_T c28_a1;
  uint8_T c28_b_a1;
  boolean_T c28_c;
  int8_T c28_i;
  int8_T c28_i1;
  real_T c28_d;
  uint8_T c28_c_a0;
  uint16_T c28_b_cont;
  uint8_T c28_b_a;
  uint8_T c28_b_out_init;
  uint8_T c28_d_a0;
  uint8_T c28_c_a1;
  uint8_T c28_d_a1;
  boolean_T c28_b_c;
  int8_T c28_i2;
  int8_T c28_i3;
  real_T c28_d1;
  int16_T c28_varargin_1;
  int16_T c28_b_varargin_1;
  int16_T c28_c_varargin_1;
  int16_T c28_d_varargin_1;
  int16_T c28_var1;
  int16_T c28_b_var1;
  int16_T c28_i4;
  int16_T c28_i5;
  boolean_T c28_covSaturation;
  boolean_T c28_b_covSaturation;
  uint16_T c28_hfi;
  uint16_T c28_b_hfi;
  uint16_T c28_u;
  uint16_T c28_u1;
  int16_T c28_e_varargin_1;
  int16_T c28_f_varargin_1;
  int16_T c28_g_varargin_1;
  int16_T c28_h_varargin_1;
  int16_T c28_c_var1;
  int16_T c28_d_var1;
  int16_T c28_i6;
  int16_T c28_i7;
  boolean_T c28_c_covSaturation;
  boolean_T c28_d_covSaturation;
  uint16_T c28_c_hfi;
  uint16_T c28_d_hfi;
  uint16_T c28_u2;
  uint16_T c28_u3;
  uint16_T c28_e_a0;
  uint16_T c28_f_a0;
  uint16_T c28_b0;
  uint16_T c28_b_b0;
  uint16_T c28_c_a;
  uint16_T c28_d_a;
  uint16_T c28_b;
  uint16_T c28_b_b;
  uint16_T c28_g_a0;
  uint16_T c28_h_a0;
  uint16_T c28_c_b0;
  uint16_T c28_d_b0;
  uint16_T c28_e_a1;
  uint16_T c28_f_a1;
  uint16_T c28_b1;
  uint16_T c28_b_b1;
  uint16_T c28_g_a1;
  uint16_T c28_h_a1;
  uint16_T c28_c_b1;
  uint16_T c28_d_b1;
  boolean_T c28_c_c;
  boolean_T c28_d_c;
  int16_T c28_i8;
  int16_T c28_i9;
  int16_T c28_i10;
  int16_T c28_i11;
  int16_T c28_i12;
  int16_T c28_i13;
  int16_T c28_i14;
  int16_T c28_i15;
  int16_T c28_i16;
  int16_T c28_i17;
  int16_T c28_i18;
  int16_T c28_i19;
  int32_T c28_i20;
  int32_T c28_i21;
  int16_T c28_i22;
  int16_T c28_i23;
  int16_T c28_i24;
  int16_T c28_i25;
  int16_T c28_i26;
  int16_T c28_i27;
  int16_T c28_i28;
  int16_T c28_i29;
  int16_T c28_i30;
  int16_T c28_i31;
  int16_T c28_i32;
  int16_T c28_i33;
  int32_T c28_i34;
  int32_T c28_i35;
  int16_T c28_i36;
  int16_T c28_i37;
  int16_T c28_i38;
  int16_T c28_i39;
  int16_T c28_i40;
  int16_T c28_i41;
  int16_T c28_i42;
  int16_T c28_i43;
  real_T c28_d2;
  real_T c28_d3;
  int16_T c28_i_varargin_1;
  int16_T c28_i_a0;
  int16_T c28_j_varargin_1;
  int16_T c28_e_b0;
  int16_T c28_e_var1;
  int16_T c28_k_varargin_1;
  int16_T c28_i44;
  int16_T c28_v;
  boolean_T c28_e_covSaturation;
  int16_T c28_val;
  int16_T c28_c_b;
  int32_T c28_i45;
  uint16_T c28_e_hfi;
  real_T c28_d4;
  int32_T c28_i46;
  real_T c28_d5;
  real_T c28_d6;
  real_T c28_d7;
  int32_T c28_i47;
  int32_T c28_i48;
  int32_T c28_i49;
  real_T c28_d8;
  real_T c28_d9;
  int32_T c28_e_c;
  int32_T c28_l_varargin_1;
  int32_T c28_m_varargin_1;
  int32_T c28_f_var1;
  int32_T c28_i50;
  boolean_T c28_f_covSaturation;
  uint16_T c28_f_hfi;
  observerLogReadPIC(&c28_PICOffset);
  chartInstance->c28_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c28_covrtInstance, 4U, (real_T)
                    *chartInstance->c28_init);
  covrtSigUpdateFcn(chartInstance->c28_covrtInstance, 3U, (real_T)
                    *chartInstance->c28_sum);
  covrtSigUpdateFcn(chartInstance->c28_covrtInstance, 2U, (real_T)
                    *chartInstance->c28_max);
  covrtSigUpdateFcn(chartInstance->c28_covrtInstance, 1U, (real_T)
                    *chartInstance->c28_offset);
  covrtSigUpdateFcn(chartInstance->c28_covrtInstance, 0U, (real_T)
                    *chartInstance->c28_enable);
  chartInstance->c28_sfEvent = CALL_EVENT;
  c28_b_enable = *chartInstance->c28_enable;
  c28_b_offset = *chartInstance->c28_offset;
  c28_b_max = *chartInstance->c28_max;
  c28_b_sum = *chartInstance->c28_sum;
  c28_b_init = *chartInstance->c28_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c28_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c28_emlrt_update_log_1(chartInstance, c28_b_enable,
    chartInstance->c28_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c28_emlrt_update_log_2(chartInstance, c28_b_offset,
    chartInstance->c28_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c28_emlrt_update_log_3(chartInstance, c28_b_max,
    chartInstance->c28_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c28_emlrt_update_log_3(chartInstance, c28_b_sum,
    chartInstance->c28_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c28_emlrt_update_log_1(chartInstance, c28_b_init,
    chartInstance->c28_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c28_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c28_covrtInstance, 4U, 0, 0, false);
  c28_a0 = c28_b_enable;
  c28_a = c28_a0;
  c28_b_a0 = c28_a;
  c28_a1 = c28_b_a0;
  c28_b_a1 = c28_a1;
  c28_c = (c28_b_a1 == 0);
  c28_i = (int8_T)c28_b_enable;
  if ((int8_T)(c28_i & 2) != 0) {
    c28_i1 = (int8_T)(c28_i | -2);
  } else {
    c28_i1 = (int8_T)(c28_i & 1);
  }

  if (c28_i1 > 0) {
    c28_d = 3.0;
  } else {
    c28_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c28_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c28_covrtInstance,
        4U, 0U, 0U, c28_d, 0.0, -2, 0U, (int32_T)c28_emlrt_update_log_4
        (chartInstance, c28_c, chartInstance->c28_emlrtLocationLoggingDataTables,
         5)))) {
    c28_b_cont = c28_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c28_emlrtLocationLoggingDataTables, 6);
    c28_b_out_init = c28_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c28_emlrtLocationLoggingDataTables, 7);
  } else {
    c28_c_a0 = c28_b_init;
    c28_b_a = c28_c_a0;
    c28_d_a0 = c28_b_a;
    c28_c_a1 = c28_d_a0;
    c28_d_a1 = c28_c_a1;
    c28_b_c = (c28_d_a1 == 0);
    c28_i2 = (int8_T)c28_b_init;
    if ((int8_T)(c28_i2 & 2) != 0) {
      c28_i3 = (int8_T)(c28_i2 | -2);
    } else {
      c28_i3 = (int8_T)(c28_i2 & 1);
    }

    if (c28_i3 > 0) {
      c28_d1 = 3.0;
    } else {
      c28_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c28_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c28_covrtInstance, 4U, 0U, 1U, c28_d1,
                        0.0, -2, 0U, (int32_T)c28_emlrt_update_log_4
                        (chartInstance, c28_b_c,
                         chartInstance->c28_emlrtLocationLoggingDataTables, 8))))
    {
      c28_b_varargin_1 = c28_b_sum;
      c28_d_varargin_1 = c28_b_varargin_1;
      c28_b_var1 = c28_d_varargin_1;
      c28_i5 = c28_b_var1;
      c28_b_covSaturation = false;
      if (c28_i5 < 0) {
        c28_i5 = 0;
      } else {
        if (c28_i5 > 4095) {
          c28_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c28_covrtInstance, 4, 0, 0, 0,
          c28_b_covSaturation);
      }

      c28_b_hfi = (uint16_T)c28_i5;
      c28_u1 = c28_emlrt_update_log_5(chartInstance, c28_b_hfi,
        chartInstance->c28_emlrtLocationLoggingDataTables, 10);
      c28_f_varargin_1 = c28_b_max;
      c28_h_varargin_1 = c28_f_varargin_1;
      c28_d_var1 = c28_h_varargin_1;
      c28_i7 = c28_d_var1;
      c28_d_covSaturation = false;
      if (c28_i7 < 0) {
        c28_i7 = 0;
      } else {
        if (c28_i7 > 4095) {
          c28_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c28_covrtInstance, 4, 0, 1, 0,
          c28_d_covSaturation);
      }

      c28_d_hfi = (uint16_T)c28_i7;
      c28_u3 = c28_emlrt_update_log_5(chartInstance, c28_d_hfi,
        chartInstance->c28_emlrtLocationLoggingDataTables, 11);
      c28_f_a0 = c28_u1;
      c28_b_b0 = c28_u3;
      c28_d_a = c28_f_a0;
      c28_b_b = c28_b_b0;
      c28_h_a0 = c28_d_a;
      c28_d_b0 = c28_b_b;
      c28_f_a1 = c28_h_a0;
      c28_b_b1 = c28_d_b0;
      c28_h_a1 = c28_f_a1;
      c28_d_b1 = c28_b_b1;
      c28_d_c = (c28_h_a1 < c28_d_b1);
      c28_i9 = (int16_T)c28_u1;
      c28_i11 = (int16_T)c28_u3;
      c28_i13 = (int16_T)c28_u3;
      c28_i15 = (int16_T)c28_u1;
      if ((int16_T)(c28_i13 & 4096) != 0) {
        c28_i17 = (int16_T)(c28_i13 | -4096);
      } else {
        c28_i17 = (int16_T)(c28_i13 & 4095);
      }

      if ((int16_T)(c28_i15 & 4096) != 0) {
        c28_i19 = (int16_T)(c28_i15 | -4096);
      } else {
        c28_i19 = (int16_T)(c28_i15 & 4095);
      }

      c28_i21 = c28_i17 - c28_i19;
      if (c28_i21 > 4095) {
        c28_i21 = 4095;
      } else {
        if (c28_i21 < -4096) {
          c28_i21 = -4096;
        }
      }

      c28_i23 = (int16_T)c28_u1;
      c28_i25 = (int16_T)c28_u3;
      c28_i27 = (int16_T)c28_u1;
      c28_i29 = (int16_T)c28_u3;
      if ((int16_T)(c28_i27 & 4096) != 0) {
        c28_i31 = (int16_T)(c28_i27 | -4096);
      } else {
        c28_i31 = (int16_T)(c28_i27 & 4095);
      }

      if ((int16_T)(c28_i29 & 4096) != 0) {
        c28_i33 = (int16_T)(c28_i29 | -4096);
      } else {
        c28_i33 = (int16_T)(c28_i29 & 4095);
      }

      c28_i35 = c28_i31 - c28_i33;
      if (c28_i35 > 4095) {
        c28_i35 = 4095;
      } else {
        if (c28_i35 < -4096) {
          c28_i35 = -4096;
        }
      }

      if ((int16_T)(c28_i9 & 4096) != 0) {
        c28_i37 = (int16_T)(c28_i9 | -4096);
      } else {
        c28_i37 = (int16_T)(c28_i9 & 4095);
      }

      if ((int16_T)(c28_i11 & 4096) != 0) {
        c28_i39 = (int16_T)(c28_i11 | -4096);
      } else {
        c28_i39 = (int16_T)(c28_i11 & 4095);
      }

      if ((int16_T)(c28_i23 & 4096) != 0) {
        c28_i41 = (int16_T)(c28_i23 | -4096);
      } else {
        c28_i41 = (int16_T)(c28_i23 & 4095);
      }

      if ((int16_T)(c28_i25 & 4096) != 0) {
        c28_i43 = (int16_T)(c28_i25 | -4096);
      } else {
        c28_i43 = (int16_T)(c28_i25 & 4095);
      }

      if (c28_i37 < c28_i39) {
        c28_d3 = (real_T)((int16_T)c28_i21 <= 1);
      } else if (c28_i41 > c28_i43) {
        if ((int16_T)c28_i35 <= 1) {
          c28_d3 = 3.0;
        } else {
          c28_d3 = 0.0;
        }
      } else {
        c28_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c28_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c28_covrtInstance, 4U, 0U, 2U, c28_d3,
                          0.0, -2, 2U, (int32_T)c28_emlrt_update_log_4
                          (chartInstance, c28_d_c,
                           chartInstance->c28_emlrtLocationLoggingDataTables, 9))))
      {
        c28_i_a0 = c28_b_sum;
        c28_e_b0 = c28_b_offset;
        c28_k_varargin_1 = c28_e_b0;
        c28_v = c28_k_varargin_1;
        c28_val = c28_v;
        c28_c_b = c28_val;
        c28_i45 = c28_i_a0;
        if (c28_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c28_d4 = 1.0;
          observerLog(495 + c28_PICOffset, &c28_d4, 1);
        }

        if (c28_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c28_d5 = 1.0;
          observerLog(495 + c28_PICOffset, &c28_d5, 1);
        }

        c28_i46 = c28_c_b;
        if (c28_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c28_d6 = 1.0;
          observerLog(498 + c28_PICOffset, &c28_d6, 1);
        }

        if (c28_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c28_d7 = 1.0;
          observerLog(498 + c28_PICOffset, &c28_d7, 1);
        }

        if ((c28_i45 & 65536) != 0) {
          c28_i47 = c28_i45 | -65536;
        } else {
          c28_i47 = c28_i45 & 65535;
        }

        if ((c28_i46 & 65536) != 0) {
          c28_i48 = c28_i46 | -65536;
        } else {
          c28_i48 = c28_i46 & 65535;
        }

        c28_i49 = c28__s32_add__(chartInstance, c28_i47, c28_i48, 497, 1U, 323,
          12);
        if (c28_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c28_d8 = 1.0;
          observerLog(503 + c28_PICOffset, &c28_d8, 1);
        }

        if (c28_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c28_d9 = 1.0;
          observerLog(503 + c28_PICOffset, &c28_d9, 1);
        }

        if ((c28_i49 & 65536) != 0) {
          c28_e_c = c28_i49 | -65536;
        } else {
          c28_e_c = c28_i49 & 65535;
        }

        c28_l_varargin_1 = c28_emlrt_update_log_6(chartInstance, c28_e_c,
          chartInstance->c28_emlrtLocationLoggingDataTables, 13);
        c28_m_varargin_1 = c28_l_varargin_1;
        c28_f_var1 = c28_m_varargin_1;
        c28_i50 = c28_f_var1;
        c28_f_covSaturation = false;
        if (c28_i50 < 0) {
          c28_i50 = 0;
        } else {
          if (c28_i50 > 4095) {
            c28_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c28_covrtInstance, 4, 0, 2, 0,
            c28_f_covSaturation);
        }

        c28_f_hfi = (uint16_T)c28_i50;
        c28_b_cont = c28_emlrt_update_log_5(chartInstance, c28_f_hfi,
          chartInstance->c28_emlrtLocationLoggingDataTables, 12);
        c28_b_out_init = c28_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c28_emlrtLocationLoggingDataTables, 14);
      } else {
        c28_b_cont = c28_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c28_emlrtLocationLoggingDataTables, 15);
        c28_b_out_init = c28_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c28_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c28_varargin_1 = c28_b_sum;
      c28_c_varargin_1 = c28_varargin_1;
      c28_var1 = c28_c_varargin_1;
      c28_i4 = c28_var1;
      c28_covSaturation = false;
      if (c28_i4 < 0) {
        c28_i4 = 0;
      } else {
        if (c28_i4 > 4095) {
          c28_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c28_covrtInstance, 4, 0, 3, 0,
          c28_covSaturation);
      }

      c28_hfi = (uint16_T)c28_i4;
      c28_u = c28_emlrt_update_log_5(chartInstance, c28_hfi,
        chartInstance->c28_emlrtLocationLoggingDataTables, 18);
      c28_e_varargin_1 = c28_b_max;
      c28_g_varargin_1 = c28_e_varargin_1;
      c28_c_var1 = c28_g_varargin_1;
      c28_i6 = c28_c_var1;
      c28_c_covSaturation = false;
      if (c28_i6 < 0) {
        c28_i6 = 0;
      } else {
        if (c28_i6 > 4095) {
          c28_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c28_covrtInstance, 4, 0, 4, 0,
          c28_c_covSaturation);
      }

      c28_c_hfi = (uint16_T)c28_i6;
      c28_u2 = c28_emlrt_update_log_5(chartInstance, c28_c_hfi,
        chartInstance->c28_emlrtLocationLoggingDataTables, 19);
      c28_e_a0 = c28_u;
      c28_b0 = c28_u2;
      c28_c_a = c28_e_a0;
      c28_b = c28_b0;
      c28_g_a0 = c28_c_a;
      c28_c_b0 = c28_b;
      c28_e_a1 = c28_g_a0;
      c28_b1 = c28_c_b0;
      c28_g_a1 = c28_e_a1;
      c28_c_b1 = c28_b1;
      c28_c_c = (c28_g_a1 < c28_c_b1);
      c28_i8 = (int16_T)c28_u;
      c28_i10 = (int16_T)c28_u2;
      c28_i12 = (int16_T)c28_u2;
      c28_i14 = (int16_T)c28_u;
      if ((int16_T)(c28_i12 & 4096) != 0) {
        c28_i16 = (int16_T)(c28_i12 | -4096);
      } else {
        c28_i16 = (int16_T)(c28_i12 & 4095);
      }

      if ((int16_T)(c28_i14 & 4096) != 0) {
        c28_i18 = (int16_T)(c28_i14 | -4096);
      } else {
        c28_i18 = (int16_T)(c28_i14 & 4095);
      }

      c28_i20 = c28_i16 - c28_i18;
      if (c28_i20 > 4095) {
        c28_i20 = 4095;
      } else {
        if (c28_i20 < -4096) {
          c28_i20 = -4096;
        }
      }

      c28_i22 = (int16_T)c28_u;
      c28_i24 = (int16_T)c28_u2;
      c28_i26 = (int16_T)c28_u;
      c28_i28 = (int16_T)c28_u2;
      if ((int16_T)(c28_i26 & 4096) != 0) {
        c28_i30 = (int16_T)(c28_i26 | -4096);
      } else {
        c28_i30 = (int16_T)(c28_i26 & 4095);
      }

      if ((int16_T)(c28_i28 & 4096) != 0) {
        c28_i32 = (int16_T)(c28_i28 | -4096);
      } else {
        c28_i32 = (int16_T)(c28_i28 & 4095);
      }

      c28_i34 = c28_i30 - c28_i32;
      if (c28_i34 > 4095) {
        c28_i34 = 4095;
      } else {
        if (c28_i34 < -4096) {
          c28_i34 = -4096;
        }
      }

      if ((int16_T)(c28_i8 & 4096) != 0) {
        c28_i36 = (int16_T)(c28_i8 | -4096);
      } else {
        c28_i36 = (int16_T)(c28_i8 & 4095);
      }

      if ((int16_T)(c28_i10 & 4096) != 0) {
        c28_i38 = (int16_T)(c28_i10 | -4096);
      } else {
        c28_i38 = (int16_T)(c28_i10 & 4095);
      }

      if ((int16_T)(c28_i22 & 4096) != 0) {
        c28_i40 = (int16_T)(c28_i22 | -4096);
      } else {
        c28_i40 = (int16_T)(c28_i22 & 4095);
      }

      if ((int16_T)(c28_i24 & 4096) != 0) {
        c28_i42 = (int16_T)(c28_i24 | -4096);
      } else {
        c28_i42 = (int16_T)(c28_i24 & 4095);
      }

      if (c28_i36 < c28_i38) {
        c28_d2 = (real_T)((int16_T)c28_i20 <= 1);
      } else if (c28_i40 > c28_i42) {
        if ((int16_T)c28_i34 <= 1) {
          c28_d2 = 3.0;
        } else {
          c28_d2 = 0.0;
        }
      } else {
        c28_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c28_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c28_covrtInstance, 4U, 0U, 3U, c28_d2,
                          0.0, -2, 2U, (int32_T)c28_emlrt_update_log_4
                          (chartInstance, c28_c_c,
                           chartInstance->c28_emlrtLocationLoggingDataTables, 17))))
      {
        c28_i_varargin_1 = c28_b_sum;
        c28_j_varargin_1 = c28_i_varargin_1;
        c28_e_var1 = c28_j_varargin_1;
        c28_i44 = c28_e_var1;
        c28_e_covSaturation = false;
        if (c28_i44 < 0) {
          c28_i44 = 0;
        } else {
          if (c28_i44 > 4095) {
            c28_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c28_covrtInstance, 4, 0, 5, 0,
            c28_e_covSaturation);
        }

        c28_e_hfi = (uint16_T)c28_i44;
        c28_b_cont = c28_emlrt_update_log_5(chartInstance, c28_e_hfi,
          chartInstance->c28_emlrtLocationLoggingDataTables, 20);
        c28_b_out_init = c28_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c28_emlrtLocationLoggingDataTables, 21);
      } else {
        c28_b_cont = c28_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c28_emlrtLocationLoggingDataTables, 22);
        c28_b_out_init = c28_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c28_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c28_cont = c28_b_cont;
  *chartInstance->c28_out_init = c28_b_out_init;
  c28_do_animation_call_c28_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c28_covrtInstance, 5U, (real_T)
                    *chartInstance->c28_cont);
  covrtSigUpdateFcn(chartInstance->c28_covrtInstance, 6U, (real_T)
                    *chartInstance->c28_out_init);
}

static void mdl_start_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c28_decisionTxtStartIdx = 0U;
  static const uint32_T c28_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c28_chart_data_browse_helper);
  chartInstance->c28_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c28_RuntimeVar,
    &chartInstance->c28_IsDebuggerActive,
    &chartInstance->c28_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c28_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c28_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c28_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c28_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c28_decisionTxtStartIdx, &c28_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c28_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c28_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c28_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c28_PWM_28_HalfB
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c28_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7837",              /* mexFileName */
    "Thu May 27 10:27:31 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c28_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c28_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c28_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c28_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7837");
    emlrtLocationLoggingPushLog(&c28_emlrtLocationLoggingFileInfo,
      c28_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c28_emlrtLocationLoggingDataTables, c28_emlrtLocationInfo,
      NULL, 0U, c28_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7837");
  }

  sfListenerLightTerminate(chartInstance->c28_RuntimeVar);
  sf_mex_destroy(&c28_eml_mx);
  sf_mex_destroy(&c28_b_eml_mx);
  sf_mex_destroy(&c28_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c28_covrtInstance);
}

static void initSimStructsc28_PWM_28_HalfB(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c28_emlrt_update_log_1(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index)
{
  boolean_T c28_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c28_b_table;
  real_T c28_d;
  uint8_T c28_u;
  uint8_T c28_localMin;
  real_T c28_d1;
  uint8_T c28_u1;
  uint8_T c28_localMax;
  emlrtLocationLoggingHistogramType *c28_histTable;
  real_T c28_inDouble;
  real_T c28_significand;
  int32_T c28_exponent;
  (void)chartInstance;
  c28_isLoggingEnabledHere = (c28_index >= 0);
  if (c28_isLoggingEnabledHere) {
    c28_b_table = (emlrtLocationLoggingDataType *)&c28_table[c28_index];
    c28_d = c28_b_table[0U].SimMin;
    if (c28_d < 2.0) {
      if (c28_d >= 0.0) {
        c28_u = (uint8_T)c28_d;
      } else {
        c28_u = 0U;
      }
    } else if (c28_d >= 2.0) {
      c28_u = 1U;
    } else {
      c28_u = 0U;
    }

    c28_localMin = c28_u;
    c28_d1 = c28_b_table[0U].SimMax;
    if (c28_d1 < 2.0) {
      if (c28_d1 >= 0.0) {
        c28_u1 = (uint8_T)c28_d1;
      } else {
        c28_u1 = 0U;
      }
    } else if (c28_d1 >= 2.0) {
      c28_u1 = 1U;
    } else {
      c28_u1 = 0U;
    }

    c28_localMax = c28_u1;
    c28_histTable = c28_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c28_in < c28_localMin) {
      c28_localMin = c28_in;
    }

    if (c28_in > c28_localMax) {
      c28_localMax = c28_in;
    }

    /* Histogram logging. */
    c28_inDouble = (real_T)c28_in;
    c28_histTable->TotalNumberOfValues++;
    if (c28_inDouble == 0.0) {
      c28_histTable->NumberOfZeros++;
    } else {
      c28_histTable->SimSum += c28_inDouble;
      if ((!muDoubleScalarIsInf(c28_inDouble)) && (!muDoubleScalarIsNaN
           (c28_inDouble))) {
        c28_significand = frexp(c28_inDouble, &c28_exponent);
        if (c28_exponent > 128) {
          c28_exponent = 128;
        }

        if (c28_exponent < -127) {
          c28_exponent = -127;
        }

        if (c28_significand < 0.0) {
          c28_histTable->NumberOfNegativeValues++;
          c28_histTable->HistogramOfNegativeValues[127 + c28_exponent]++;
        } else {
          c28_histTable->NumberOfPositiveValues++;
          c28_histTable->HistogramOfPositiveValues[127 + c28_exponent]++;
        }
      }
    }

    c28_b_table[0U].SimMin = (real_T)c28_localMin;
    c28_b_table[0U].SimMax = (real_T)c28_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c28_in;
}

static int16_T c28_emlrt_update_log_2(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index)
{
  boolean_T c28_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c28_b_table;
  real_T c28_d;
  int16_T c28_i;
  int16_T c28_localMin;
  real_T c28_d1;
  int16_T c28_i1;
  int16_T c28_localMax;
  emlrtLocationLoggingHistogramType *c28_histTable;
  real_T c28_inDouble;
  real_T c28_significand;
  int32_T c28_exponent;
  (void)chartInstance;
  c28_isLoggingEnabledHere = (c28_index >= 0);
  if (c28_isLoggingEnabledHere) {
    c28_b_table = (emlrtLocationLoggingDataType *)&c28_table[c28_index];
    c28_d = muDoubleScalarFloor(c28_b_table[0U].SimMin);
    if (c28_d < 32768.0) {
      if (c28_d >= -32768.0) {
        c28_i = (int16_T)c28_d;
      } else {
        c28_i = MIN_int16_T;
      }
    } else if (c28_d >= 32768.0) {
      c28_i = MAX_int16_T;
    } else {
      c28_i = 0;
    }

    c28_localMin = c28_i;
    c28_d1 = muDoubleScalarFloor(c28_b_table[0U].SimMax);
    if (c28_d1 < 32768.0) {
      if (c28_d1 >= -32768.0) {
        c28_i1 = (int16_T)c28_d1;
      } else {
        c28_i1 = MIN_int16_T;
      }
    } else if (c28_d1 >= 32768.0) {
      c28_i1 = MAX_int16_T;
    } else {
      c28_i1 = 0;
    }

    c28_localMax = c28_i1;
    c28_histTable = c28_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c28_in < c28_localMin) {
      c28_localMin = c28_in;
    }

    if (c28_in > c28_localMax) {
      c28_localMax = c28_in;
    }

    /* Histogram logging. */
    c28_inDouble = (real_T)c28_in;
    c28_histTable->TotalNumberOfValues++;
    if (c28_inDouble == 0.0) {
      c28_histTable->NumberOfZeros++;
    } else {
      c28_histTable->SimSum += c28_inDouble;
      if ((!muDoubleScalarIsInf(c28_inDouble)) && (!muDoubleScalarIsNaN
           (c28_inDouble))) {
        c28_significand = frexp(c28_inDouble, &c28_exponent);
        if (c28_exponent > 128) {
          c28_exponent = 128;
        }

        if (c28_exponent < -127) {
          c28_exponent = -127;
        }

        if (c28_significand < 0.0) {
          c28_histTable->NumberOfNegativeValues++;
          c28_histTable->HistogramOfNegativeValues[127 + c28_exponent]++;
        } else {
          c28_histTable->NumberOfPositiveValues++;
          c28_histTable->HistogramOfPositiveValues[127 + c28_exponent]++;
        }
      }
    }

    c28_b_table[0U].SimMin = (real_T)c28_localMin;
    c28_b_table[0U].SimMax = (real_T)c28_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c28_in;
}

static int16_T c28_emlrt_update_log_3(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index)
{
  boolean_T c28_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c28_b_table;
  real_T c28_d;
  int16_T c28_i;
  int16_T c28_localMin;
  real_T c28_d1;
  int16_T c28_i1;
  int16_T c28_localMax;
  emlrtLocationLoggingHistogramType *c28_histTable;
  real_T c28_inDouble;
  real_T c28_significand;
  int32_T c28_exponent;
  (void)chartInstance;
  c28_isLoggingEnabledHere = (c28_index >= 0);
  if (c28_isLoggingEnabledHere) {
    c28_b_table = (emlrtLocationLoggingDataType *)&c28_table[c28_index];
    c28_d = muDoubleScalarFloor(c28_b_table[0U].SimMin);
    if (c28_d < 2048.0) {
      if (c28_d >= -2048.0) {
        c28_i = (int16_T)c28_d;
      } else {
        c28_i = -2048;
      }
    } else if (c28_d >= 2048.0) {
      c28_i = 2047;
    } else {
      c28_i = 0;
    }

    c28_localMin = c28_i;
    c28_d1 = muDoubleScalarFloor(c28_b_table[0U].SimMax);
    if (c28_d1 < 2048.0) {
      if (c28_d1 >= -2048.0) {
        c28_i1 = (int16_T)c28_d1;
      } else {
        c28_i1 = -2048;
      }
    } else if (c28_d1 >= 2048.0) {
      c28_i1 = 2047;
    } else {
      c28_i1 = 0;
    }

    c28_localMax = c28_i1;
    c28_histTable = c28_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c28_in < c28_localMin) {
      c28_localMin = c28_in;
    }

    if (c28_in > c28_localMax) {
      c28_localMax = c28_in;
    }

    /* Histogram logging. */
    c28_inDouble = (real_T)c28_in;
    c28_histTable->TotalNumberOfValues++;
    if (c28_inDouble == 0.0) {
      c28_histTable->NumberOfZeros++;
    } else {
      c28_histTable->SimSum += c28_inDouble;
      if ((!muDoubleScalarIsInf(c28_inDouble)) && (!muDoubleScalarIsNaN
           (c28_inDouble))) {
        c28_significand = frexp(c28_inDouble, &c28_exponent);
        if (c28_exponent > 128) {
          c28_exponent = 128;
        }

        if (c28_exponent < -127) {
          c28_exponent = -127;
        }

        if (c28_significand < 0.0) {
          c28_histTable->NumberOfNegativeValues++;
          c28_histTable->HistogramOfNegativeValues[127 + c28_exponent]++;
        } else {
          c28_histTable->NumberOfPositiveValues++;
          c28_histTable->HistogramOfPositiveValues[127 + c28_exponent]++;
        }
      }
    }

    c28_b_table[0U].SimMin = (real_T)c28_localMin;
    c28_b_table[0U].SimMax = (real_T)c28_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c28_in;
}

static boolean_T c28_emlrt_update_log_4(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index)
{
  boolean_T c28_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c28_b_table;
  boolean_T c28_localMin;
  boolean_T c28_localMax;
  emlrtLocationLoggingHistogramType *c28_histTable;
  real_T c28_inDouble;
  real_T c28_significand;
  int32_T c28_exponent;
  (void)chartInstance;
  c28_isLoggingEnabledHere = (c28_index >= 0);
  if (c28_isLoggingEnabledHere) {
    c28_b_table = (emlrtLocationLoggingDataType *)&c28_table[c28_index];
    c28_localMin = (c28_b_table[0U].SimMin > 0.0);
    c28_localMax = (c28_b_table[0U].SimMax > 0.0);
    c28_histTable = c28_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c28_in < (int32_T)c28_localMin) {
      c28_localMin = c28_in;
    }

    if ((int32_T)c28_in > (int32_T)c28_localMax) {
      c28_localMax = c28_in;
    }

    /* Histogram logging. */
    c28_inDouble = (real_T)c28_in;
    c28_histTable->TotalNumberOfValues++;
    if (c28_inDouble == 0.0) {
      c28_histTable->NumberOfZeros++;
    } else {
      c28_histTable->SimSum += c28_inDouble;
      if ((!muDoubleScalarIsInf(c28_inDouble)) && (!muDoubleScalarIsNaN
           (c28_inDouble))) {
        c28_significand = frexp(c28_inDouble, &c28_exponent);
        if (c28_exponent > 128) {
          c28_exponent = 128;
        }

        if (c28_exponent < -127) {
          c28_exponent = -127;
        }

        if (c28_significand < 0.0) {
          c28_histTable->NumberOfNegativeValues++;
          c28_histTable->HistogramOfNegativeValues[127 + c28_exponent]++;
        } else {
          c28_histTable->NumberOfPositiveValues++;
          c28_histTable->HistogramOfPositiveValues[127 + c28_exponent]++;
        }
      }
    }

    c28_b_table[0U].SimMin = (real_T)c28_localMin;
    c28_b_table[0U].SimMax = (real_T)c28_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c28_in;
}

static uint16_T c28_emlrt_update_log_5(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index)
{
  boolean_T c28_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c28_b_table;
  real_T c28_d;
  uint16_T c28_u;
  uint16_T c28_localMin;
  real_T c28_d1;
  uint16_T c28_u1;
  uint16_T c28_localMax;
  emlrtLocationLoggingHistogramType *c28_histTable;
  real_T c28_inDouble;
  real_T c28_significand;
  int32_T c28_exponent;
  (void)chartInstance;
  c28_isLoggingEnabledHere = (c28_index >= 0);
  if (c28_isLoggingEnabledHere) {
    c28_b_table = (emlrtLocationLoggingDataType *)&c28_table[c28_index];
    c28_d = c28_b_table[0U].SimMin;
    if (c28_d < 4096.0) {
      if (c28_d >= 0.0) {
        c28_u = (uint16_T)c28_d;
      } else {
        c28_u = 0U;
      }
    } else if (c28_d >= 4096.0) {
      c28_u = 4095U;
    } else {
      c28_u = 0U;
    }

    c28_localMin = c28_u;
    c28_d1 = c28_b_table[0U].SimMax;
    if (c28_d1 < 4096.0) {
      if (c28_d1 >= 0.0) {
        c28_u1 = (uint16_T)c28_d1;
      } else {
        c28_u1 = 0U;
      }
    } else if (c28_d1 >= 4096.0) {
      c28_u1 = 4095U;
    } else {
      c28_u1 = 0U;
    }

    c28_localMax = c28_u1;
    c28_histTable = c28_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c28_in < c28_localMin) {
      c28_localMin = c28_in;
    }

    if (c28_in > c28_localMax) {
      c28_localMax = c28_in;
    }

    /* Histogram logging. */
    c28_inDouble = (real_T)c28_in;
    c28_histTable->TotalNumberOfValues++;
    if (c28_inDouble == 0.0) {
      c28_histTable->NumberOfZeros++;
    } else {
      c28_histTable->SimSum += c28_inDouble;
      if ((!muDoubleScalarIsInf(c28_inDouble)) && (!muDoubleScalarIsNaN
           (c28_inDouble))) {
        c28_significand = frexp(c28_inDouble, &c28_exponent);
        if (c28_exponent > 128) {
          c28_exponent = 128;
        }

        if (c28_exponent < -127) {
          c28_exponent = -127;
        }

        if (c28_significand < 0.0) {
          c28_histTable->NumberOfNegativeValues++;
          c28_histTable->HistogramOfNegativeValues[127 + c28_exponent]++;
        } else {
          c28_histTable->NumberOfPositiveValues++;
          c28_histTable->HistogramOfPositiveValues[127 + c28_exponent]++;
        }
      }
    }

    c28_b_table[0U].SimMin = (real_T)c28_localMin;
    c28_b_table[0U].SimMax = (real_T)c28_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c28_in;
}

static int32_T c28_emlrt_update_log_6(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c28_in, emlrtLocationLoggingDataType c28_table[],
  int32_T c28_index)
{
  boolean_T c28_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c28_b_table;
  real_T c28_d;
  int32_T c28_i;
  int32_T c28_localMin;
  real_T c28_d1;
  int32_T c28_i1;
  int32_T c28_localMax;
  emlrtLocationLoggingHistogramType *c28_histTable;
  real_T c28_inDouble;
  real_T c28_significand;
  int32_T c28_exponent;
  (void)chartInstance;
  c28_isLoggingEnabledHere = (c28_index >= 0);
  if (c28_isLoggingEnabledHere) {
    c28_b_table = (emlrtLocationLoggingDataType *)&c28_table[c28_index];
    c28_d = muDoubleScalarFloor(c28_b_table[0U].SimMin);
    if (c28_d < 65536.0) {
      if (c28_d >= -65536.0) {
        c28_i = (int32_T)c28_d;
      } else {
        c28_i = -65536;
      }
    } else if (c28_d >= 65536.0) {
      c28_i = 65535;
    } else {
      c28_i = 0;
    }

    c28_localMin = c28_i;
    c28_d1 = muDoubleScalarFloor(c28_b_table[0U].SimMax);
    if (c28_d1 < 65536.0) {
      if (c28_d1 >= -65536.0) {
        c28_i1 = (int32_T)c28_d1;
      } else {
        c28_i1 = -65536;
      }
    } else if (c28_d1 >= 65536.0) {
      c28_i1 = 65535;
    } else {
      c28_i1 = 0;
    }

    c28_localMax = c28_i1;
    c28_histTable = c28_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c28_in < c28_localMin) {
      c28_localMin = c28_in;
    }

    if (c28_in > c28_localMax) {
      c28_localMax = c28_in;
    }

    /* Histogram logging. */
    c28_inDouble = (real_T)c28_in;
    c28_histTable->TotalNumberOfValues++;
    if (c28_inDouble == 0.0) {
      c28_histTable->NumberOfZeros++;
    } else {
      c28_histTable->SimSum += c28_inDouble;
      if ((!muDoubleScalarIsInf(c28_inDouble)) && (!muDoubleScalarIsNaN
           (c28_inDouble))) {
        c28_significand = frexp(c28_inDouble, &c28_exponent);
        if (c28_exponent > 128) {
          c28_exponent = 128;
        }

        if (c28_exponent < -127) {
          c28_exponent = -127;
        }

        if (c28_significand < 0.0) {
          c28_histTable->NumberOfNegativeValues++;
          c28_histTable->HistogramOfNegativeValues[127 + c28_exponent]++;
        } else {
          c28_histTable->NumberOfPositiveValues++;
          c28_histTable->HistogramOfPositiveValues[127 + c28_exponent]++;
        }
      }
    }

    c28_b_table[0U].SimMin = (real_T)c28_localMin;
    c28_b_table[0U].SimMax = (real_T)c28_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c28_in;
}

static void c28_emlrtInitVarDataTables(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c28_dataTables[24],
  emlrtLocationLoggingHistogramType c28_histTables[24])
{
  int32_T c28_i;
  int32_T c28_iH;
  (void)chartInstance;
  for (c28_i = 0; c28_i < 24; c28_i++) {
    c28_dataTables[c28_i].SimMin = rtInf;
    c28_dataTables[c28_i].SimMax = rtMinusInf;
    c28_dataTables[c28_i].OverflowWraps = 0;
    c28_dataTables[c28_i].Saturations = 0;
    c28_dataTables[c28_i].IsAlwaysInteger = true;
    c28_dataTables[c28_i].HistogramTable = &c28_histTables[c28_i];
    c28_histTables[c28_i].NumberOfZeros = 0.0;
    c28_histTables[c28_i].NumberOfPositiveValues = 0.0;
    c28_histTables[c28_i].NumberOfNegativeValues = 0.0;
    c28_histTables[c28_i].TotalNumberOfValues = 0.0;
    c28_histTables[c28_i].SimSum = 0.0;
    for (c28_iH = 0; c28_iH < 256; c28_iH++) {
      c28_histTables[c28_i].HistogramOfPositiveValues[c28_iH] = 0.0;
      c28_histTables[c28_i].HistogramOfNegativeValues[c28_iH] = 0.0;
    }
  }
}

const mxArray *sf_c28_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c28_nameCaptureInfo = NULL;
  c28_nameCaptureInfo = NULL;
  sf_mex_assign(&c28_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c28_nameCaptureInfo;
}

static uint16_T c28_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c28_sp, const mxArray *c28_b_cont, const
  char_T *c28_identifier)
{
  uint16_T c28_y;
  emlrtMsgIdentifier c28_thisId;
  c28_thisId.fIdentifier = (const char *)c28_identifier;
  c28_thisId.fParent = NULL;
  c28_thisId.bParentIsCell = false;
  c28_y = c28_b_emlrt_marshallIn(chartInstance, c28_sp, sf_mex_dup(c28_b_cont),
    &c28_thisId);
  sf_mex_destroy(&c28_b_cont);
  return c28_y;
}

static uint16_T c28_b_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c28_sp, const mxArray *c28_u, const
  emlrtMsgIdentifier *c28_parentId)
{
  uint16_T c28_y;
  const mxArray *c28_mxFi = NULL;
  const mxArray *c28_mxInt = NULL;
  uint16_T c28_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c28_parentId, c28_u, false, 0U, NULL, c28_eml_mx, c28_b_eml_mx);
  sf_mex_assign(&c28_mxFi, sf_mex_dup(c28_u), false);
  sf_mex_assign(&c28_mxInt, sf_mex_call(c28_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c28_mxFi)), false);
  sf_mex_import(c28_parentId, sf_mex_dup(c28_mxInt), &c28_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c28_mxFi);
  sf_mex_destroy(&c28_mxInt);
  c28_y = c28_b_u;
  sf_mex_destroy(&c28_mxFi);
  sf_mex_destroy(&c28_u);
  return c28_y;
}

static uint8_T c28_c_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c28_sp, const mxArray *c28_b_out_init, const
  char_T *c28_identifier)
{
  uint8_T c28_y;
  emlrtMsgIdentifier c28_thisId;
  c28_thisId.fIdentifier = (const char *)c28_identifier;
  c28_thisId.fParent = NULL;
  c28_thisId.bParentIsCell = false;
  c28_y = c28_d_emlrt_marshallIn(chartInstance, c28_sp, sf_mex_dup
    (c28_b_out_init), &c28_thisId);
  sf_mex_destroy(&c28_b_out_init);
  return c28_y;
}

static uint8_T c28_d_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c28_sp, const mxArray *c28_u, const
  emlrtMsgIdentifier *c28_parentId)
{
  uint8_T c28_y;
  const mxArray *c28_mxFi = NULL;
  const mxArray *c28_mxInt = NULL;
  uint8_T c28_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c28_parentId, c28_u, false, 0U, NULL, c28_eml_mx, c28_c_eml_mx);
  sf_mex_assign(&c28_mxFi, sf_mex_dup(c28_u), false);
  sf_mex_assign(&c28_mxInt, sf_mex_call(c28_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c28_mxFi)), false);
  sf_mex_import(c28_parentId, sf_mex_dup(c28_mxInt), &c28_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c28_mxFi);
  sf_mex_destroy(&c28_mxInt);
  c28_y = c28_b_u;
  sf_mex_destroy(&c28_mxFi);
  sf_mex_destroy(&c28_u);
  return c28_y;
}

static uint8_T c28_e_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c28_b_is_active_c28_PWM_28_HalfB, const char_T *
  c28_identifier)
{
  uint8_T c28_y;
  emlrtMsgIdentifier c28_thisId;
  c28_thisId.fIdentifier = (const char *)c28_identifier;
  c28_thisId.fParent = NULL;
  c28_thisId.bParentIsCell = false;
  c28_y = c28_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c28_b_is_active_c28_PWM_28_HalfB), &c28_thisId);
  sf_mex_destroy(&c28_b_is_active_c28_PWM_28_HalfB);
  return c28_y;
}

static uint8_T c28_f_emlrt_marshallIn(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c28_u, const emlrtMsgIdentifier *c28_parentId)
{
  uint8_T c28_y;
  uint8_T c28_b_u;
  (void)chartInstance;
  sf_mex_import(c28_parentId, sf_mex_dup(c28_u), &c28_b_u, 1, 3, 0U, 0, 0U, 0);
  c28_y = c28_b_u;
  sf_mex_destroy(&c28_u);
  return c28_y;
}

static const mxArray *c28_chart_data_browse_helper
  (SFc28_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c28_ssIdNumber)
{
  const mxArray *c28_mxData = NULL;
  uint8_T c28_u;
  int16_T c28_i;
  int16_T c28_i1;
  int16_T c28_i2;
  uint16_T c28_u1;
  uint8_T c28_u2;
  uint8_T c28_u3;
  real_T c28_d;
  real_T c28_d1;
  real_T c28_d2;
  real_T c28_d3;
  real_T c28_d4;
  real_T c28_d5;
  c28_mxData = NULL;
  switch (c28_ssIdNumber) {
   case 18U:
    c28_u = *chartInstance->c28_enable;
    c28_d = (real_T)c28_u;
    sf_mex_assign(&c28_mxData, sf_mex_create("mxData", &c28_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c28_i = *chartInstance->c28_offset;
    sf_mex_assign(&c28_mxData, sf_mex_create("mxData", &c28_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c28_i1 = *chartInstance->c28_max;
    c28_d1 = (real_T)c28_i1;
    sf_mex_assign(&c28_mxData, sf_mex_create("mxData", &c28_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c28_i2 = *chartInstance->c28_sum;
    c28_d2 = (real_T)c28_i2;
    sf_mex_assign(&c28_mxData, sf_mex_create("mxData", &c28_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c28_u1 = *chartInstance->c28_cont;
    c28_d3 = (real_T)c28_u1;
    sf_mex_assign(&c28_mxData, sf_mex_create("mxData", &c28_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c28_u2 = *chartInstance->c28_init;
    c28_d4 = (real_T)c28_u2;
    sf_mex_assign(&c28_mxData, sf_mex_create("mxData", &c28_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c28_u3 = *chartInstance->c28_out_init;
    c28_d5 = (real_T)c28_u3;
    sf_mex_assign(&c28_mxData, sf_mex_create("mxData", &c28_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c28_mxData;
}

static int32_T c28__s32_add__(SFc28_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c28_b, int32_T c28_c, int32_T c28_EMLOvCount_src_loc, uint32_T
  c28_ssid_src_loc, int32_T c28_offset_src_loc, int32_T c28_length_src_loc)
{
  int32_T c28_a;
  int32_T c28_PICOffset;
  real_T c28_d;
  observerLogReadPIC(&c28_PICOffset);
  c28_a = c28_b + c28_c;
  if (((c28_a ^ c28_b) & (c28_a ^ c28_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c28_ssid_src_loc,
      c28_offset_src_loc, c28_length_src_loc);
    c28_d = 1.0;
    observerLog(c28_EMLOvCount_src_loc + c28_PICOffset, &c28_d, 1);
  }

  return c28_a;
}

static void init_dsm_address_info(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc28_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c28_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c28_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c28_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c28_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c28_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c28_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c28_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c28_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c28_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c28_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c28_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c28_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c28_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c28_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyykUV8QLhvPJDyS"
    "MxJc0KYCwIA7EEglg=="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c28_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c28_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c28_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c28_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c28_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c28_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c28_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c28_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc28_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c28_PWM_28_HalfB
      ((SFc28_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c28_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c28_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c28_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc28_PWM_28_HalfB((SFc28_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c28_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV92OEzcUnoQsAkFXCzdICKnclStUqhZxxS47SdhICRuY5ecu8npOMlY89uCf7C7PwUu0T9B",
    "LLvoCvetF1TdA6iP0eDLJZiczE5aUFZVqaWLZ/s7n8+djx6t1eh62Tfze3PS8y9hfwa/uTdtGNq",
    "4tfNP5hvddNt5GIWHjPlEk1l5lEySGF6Alt4ZJ0RFDWQhjYggKBEVsIpUpY9MstpyJcdsK6vj06",
    "4jRKIik5eEuypJwX/ATZEus6SNPkymgpg0QmkhJO4ranIzmGitz5EdAx9rGVSZoMIFNnFq6Z7lh",
    "CYfWMdCO0IagxvpUt8AQA745LjXTWaqDGVDGCWdEFFobER1Agg428DIJ8XffGjQqD6MRUWYXIjI",
    "B3WXjlFMKyHMyjQuHTBAjFSO8FXPfCS7r1ueoT0+GwCscgrrtKiDjRDJhyuMftNHSliCHHJpwaE",
    "flbAG8tS74rxgcgSr129CXE1BkBPuidNPUIa3jNFrzLFmGGRbDK6KeUIyfhrA0ezFzdEAwTnCAE",
    "mUwSI3s6APFJujeUjYbd1xmrjoyNp4GW6+CpWytCVRFYc7WpsInnOtS2IFMujABnrI2iSHVsClr",
    "MU5rFh5IdLBL7/LTYAXDwGcwX4qQFYZrkgOkdecZFpazSGq1kbGPydvsdpeXl2EdYUANCYWiKqA",
    "I04A+S91bzhYy7WKPQNTKpOoVgacZsgrl6aEVzSOpxuiTiiJyaoKLaCkw1iOMJZ6ElxoPTRXMxX",
    "IVjhIaQegKDOPQw2OD2AKfaFfanuC5mzBz0gRNFUuKourun++90/vn+ifcPzO5fH9vgadWwOMt9",
    "A7/aAF/tX4Wv5Hbtz6bcy2T31mQ/ya3XyMn73Bb7uS8/+2Xn298vPbn819/7/wFJm9/Xo/akh41",
    "b7Z/cul89/ZmNr4zK5DzhJ8s5ZnD7i3o1Sjgv7XAv5WN9Y977aA3fvPg7YsH8FQZ/i5W4qGf8n2",
    "oV+t7KafvbP6uq9QnSVp3taKdMHtQuDGx02s2H8/LK/xxNZuftr+315O/v5OPY5G/Gmf81fCoFG",
    "azJB8vVv+fdvLyRfpfycXbjaU1AybYv2TH3Z315Kf791fYcTtnx+30XTEgrlrBgP7waNB/3Rtgt",
    "0f4cHe5znzueT2vnHfBcv8VPf/3y5e371Pu4Y3PlKuvee9flNy69p33PfK14avuMy+H3/qK7Vj3",
    "nfil8X9453vHfZuNH8//YvkR42HBaztb7gIZFq1egH3/AEQfoIM=",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c28_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c28_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c28_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c28_PWM_28_HalfB(SimStruct *S)
{
  SFc28_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc28_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc28_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc28_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c28_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c28_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c28_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c28_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c28_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c28_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c28_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c28_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c28_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c28_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c28_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c28_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c28_JITStateAnimation,
    chartInstance->c28_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c28_PWM_28_HalfB(chartInstance);
}

void c28_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c28_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c28_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c28_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c28_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
