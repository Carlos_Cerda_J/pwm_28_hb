#ifndef __c1_PWM_28_HalfB_h__
#define __c1_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc1_PWM_28_HalfBInstanceStruct
#define typedef_SFc1_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c1_sfEvent;
  boolean_T c1_doneDoubleBufferReInit;
  uint8_T c1_is_active_c1_PWM_28_HalfB;
  uint8_T c1_JITStateAnimation[1];
  uint8_T c1_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c1_emlrtLocationLoggingDataTables[24];
  int32_T c1_IsDebuggerActive;
  int32_T c1_IsSequenceViewerPresent;
  int32_T c1_SequenceViewerOptimization;
  void *c1_RuntimeVar;
  emlrtLocationLoggingHistogramType c1_emlrtLocLogHistTables[24];
  boolean_T c1_emlrtLocLogSimulated;
  uint32_T c1_mlFcnLineNumber;
  void *c1_fcnDataPtrs[7];
  char_T *c1_dataNames[7];
  uint32_T c1_numFcnVars;
  uint32_T c1_ssIds[7];
  uint32_T c1_statuses[7];
  void *c1_outMexFcns[7];
  void *c1_inMexFcns[7];
  CovrtStateflowInstance *c1_covrtInstance;
  void *c1_fEmlrtCtx;
  uint8_T *c1_enable;
  int16_T *c1_offset;
  int16_T *c1_max;
  int16_T *c1_sum;
  uint16_T *c1_cont;
  uint8_T *c1_init;
  uint8_T *c1_out_init;
} SFc1_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc1_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c1_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
