/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c2_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c2_eml_mx;
static const mxArray *c2_b_eml_mx;
static const mxArray *c2_c_eml_mx;

/* Function Declarations */
static void initialize_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void enable_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c2_update_jit_animation_state_c2_PWM_28_HalfB
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance);
static void c2_do_animation_call_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void ext_mode_exec_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c2_PWM_28_HalfB
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c2_st);
static void sf_gateway_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c2_PWM_28_HalfB
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c2_PWM_28_HalfB
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c2_emlrt_update_log_1(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index);
static int16_T c2_emlrt_update_log_2(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index);
static boolean_T c2_emlrt_update_log_3(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index);
static uint16_T c2_emlrt_update_log_4(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index);
static int16_T c2_emlrt_update_log_5(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index);
static void c2_emlrtInitVarDataTables(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c2_dataTables[24],
  emlrtLocationLoggingHistogramType c2_histTables[24]);
static uint16_T c2_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c2_sp, const mxArray *c2_b_cont, const
  char_T *c2_identifier);
static uint16_T c2_b_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c2_sp, const mxArray *c2_u, const
  emlrtMsgIdentifier *c2_parentId);
static uint8_T c2_c_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c2_sp, const mxArray *c2_b_out_init, const
  char_T *c2_identifier);
static uint8_T c2_d_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c2_sp, const mxArray *c2_u, const
  emlrtMsgIdentifier *c2_parentId);
static uint8_T c2_e_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c2_b_is_active_c2_PWM_28_HalfB, const char_T
  *c2_identifier);
static uint8_T c2_f_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static const mxArray *c2_chart_data_browse_helper
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c2_ssIdNumber);
static int16_T c2__s16_s32_(SFc2_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c2_b, int32_T c2_EMLOvCount_src_loc, uint32_T c2_ssid_src_loc, int32_T
  c2_offset_src_loc, int32_T c2_length_src_loc);
static void init_dsm_address_info(SFc2_PWM_28_HalfBInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c2_st = { NULL,           /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c2_st.tls = chartInstance->c2_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c2_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c2_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c2_is_active_c2_PWM_28_HalfB = 0U;
  sf_mex_assign(&c2_c_eml_mx, sf_mex_call(&c2_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c2_b_eml_mx, sf_mex_call(&c2_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c2_eml_mx, sf_mex_call(&c2_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c2_emlrtLocLogSimulated = false;
  c2_emlrtInitVarDataTables(chartInstance,
    chartInstance->c2_emlrtLocationLoggingDataTables,
    chartInstance->c2_emlrtLocLogHistTables);
}

static void initialize_params_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c2_update_jit_animation_state_c2_PWM_28_HalfB
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c2_do_animation_call_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c2_PWM_28_HalfB
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c2_st;
  const mxArray *c2_y = NULL;
  const mxArray *c2_b_y = NULL;
  uint16_T c2_u;
  const mxArray *c2_c_y = NULL;
  const mxArray *c2_d_y = NULL;
  uint8_T c2_b_u;
  const mxArray *c2_e_y = NULL;
  const mxArray *c2_f_y = NULL;
  c2_st = NULL;
  c2_st = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_createcellmatrix(3, 1), false);
  c2_b_y = NULL;
  c2_u = *chartInstance->c2_cont;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_b_y, sf_mex_create_fi(sf_mex_dup(c2_eml_mx), sf_mex_dup
    (c2_b_eml_mx), "simulinkarray", c2_c_y, false, false), false);
  sf_mex_setcell(c2_y, 0, c2_b_y);
  c2_d_y = NULL;
  c2_b_u = *chartInstance->c2_out_init;
  c2_e_y = NULL;
  sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_d_y, sf_mex_create_fi(sf_mex_dup(c2_eml_mx), sf_mex_dup
    (c2_c_eml_mx), "simulinkarray", c2_e_y, false, false), false);
  sf_mex_setcell(c2_y, 1, c2_d_y);
  c2_f_y = NULL;
  sf_mex_assign(&c2_f_y, sf_mex_create("y",
    &chartInstance->c2_is_active_c2_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c2_y, 2, c2_f_y);
  sf_mex_assign(&c2_st, c2_y, false);
  return c2_st;
}

static void set_sim_state_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c2_st)
{
  emlrtStack c2_b_st = { NULL,         /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c2_u;
  c2_b_st.tls = chartInstance->c2_fEmlrtCtx;
  chartInstance->c2_doneDoubleBufferReInit = true;
  c2_u = sf_mex_dup(c2_st);
  *chartInstance->c2_cont = c2_emlrt_marshallIn(chartInstance, &c2_b_st,
    sf_mex_dup(sf_mex_getcell(c2_u, 0)), "cont");
  *chartInstance->c2_out_init = c2_c_emlrt_marshallIn(chartInstance, &c2_b_st,
    sf_mex_dup(sf_mex_getcell(c2_u, 1)), "out_init");
  chartInstance->c2_is_active_c2_PWM_28_HalfB = c2_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c2_u, 2)),
     "is_active_c2_PWM_28_HalfB");
  sf_mex_destroy(&c2_u);
  sf_mex_destroy(&c2_st);
}

static void sf_gateway_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c2_PICOffset;
  uint8_T c2_b_enable;
  int16_T c2_b_offset;
  int16_T c2_b_max;
  int16_T c2_b_sum;
  uint8_T c2_b_init;
  uint8_T c2_a0;
  uint8_T c2_a;
  uint8_T c2_b_a0;
  uint8_T c2_a1;
  uint8_T c2_b_a1;
  boolean_T c2_c;
  int8_T c2_i;
  int8_T c2_i1;
  real_T c2_d;
  uint8_T c2_c_a0;
  uint16_T c2_b_cont;
  uint8_T c2_b_a;
  uint8_T c2_b_out_init;
  uint8_T c2_d_a0;
  uint8_T c2_c_a1;
  uint8_T c2_d_a1;
  boolean_T c2_b_c;
  int8_T c2_i2;
  int8_T c2_i3;
  real_T c2_d1;
  int16_T c2_varargin_1;
  int16_T c2_b_varargin_1;
  int16_T c2_c_varargin_1;
  int16_T c2_d_varargin_1;
  int16_T c2_var1;
  int16_T c2_b_var1;
  int16_T c2_i4;
  int16_T c2_i5;
  boolean_T c2_covSaturation;
  boolean_T c2_b_covSaturation;
  uint16_T c2_hfi;
  uint16_T c2_b_hfi;
  uint16_T c2_u;
  uint16_T c2_u1;
  int16_T c2_e_varargin_1;
  int16_T c2_f_varargin_1;
  int16_T c2_g_varargin_1;
  int16_T c2_h_varargin_1;
  int16_T c2_c_var1;
  int16_T c2_d_var1;
  int16_T c2_i6;
  int16_T c2_i7;
  boolean_T c2_c_covSaturation;
  boolean_T c2_d_covSaturation;
  uint16_T c2_c_hfi;
  uint16_T c2_d_hfi;
  uint16_T c2_u2;
  uint16_T c2_u3;
  uint16_T c2_e_a0;
  uint16_T c2_f_a0;
  uint16_T c2_b0;
  uint16_T c2_b_b0;
  uint16_T c2_c_a;
  uint16_T c2_d_a;
  uint16_T c2_b;
  uint16_T c2_b_b;
  uint16_T c2_g_a0;
  uint16_T c2_h_a0;
  uint16_T c2_c_b0;
  uint16_T c2_d_b0;
  uint16_T c2_e_a1;
  uint16_T c2_f_a1;
  uint16_T c2_b1;
  uint16_T c2_b_b1;
  uint16_T c2_g_a1;
  uint16_T c2_h_a1;
  uint16_T c2_c_b1;
  uint16_T c2_d_b1;
  boolean_T c2_c_c;
  boolean_T c2_d_c;
  int16_T c2_i8;
  int16_T c2_i9;
  int16_T c2_i10;
  int16_T c2_i11;
  int16_T c2_i12;
  int16_T c2_i13;
  int16_T c2_i14;
  int16_T c2_i15;
  int16_T c2_i16;
  int16_T c2_i17;
  int16_T c2_i18;
  int16_T c2_i19;
  int32_T c2_i20;
  int32_T c2_i21;
  int16_T c2_i22;
  int16_T c2_i23;
  int16_T c2_i24;
  int16_T c2_i25;
  int16_T c2_i26;
  int16_T c2_i27;
  int16_T c2_i28;
  int16_T c2_i29;
  int16_T c2_i30;
  int16_T c2_i31;
  int16_T c2_i32;
  int16_T c2_i33;
  int32_T c2_i34;
  int32_T c2_i35;
  int16_T c2_i36;
  int16_T c2_i37;
  int16_T c2_i38;
  int16_T c2_i39;
  int16_T c2_i40;
  int16_T c2_i41;
  int16_T c2_i42;
  int16_T c2_i43;
  real_T c2_d2;
  real_T c2_d3;
  int16_T c2_i_varargin_1;
  int16_T c2_i_a0;
  int16_T c2_j_varargin_1;
  int16_T c2_e_b0;
  int16_T c2_e_var1;
  int16_T c2_i44;
  int16_T c2_i45;
  boolean_T c2_e_covSaturation;
  real_T c2_d4;
  int16_T c2_i46;
  real_T c2_d5;
  uint16_T c2_e_hfi;
  real_T c2_d6;
  real_T c2_d7;
  int16_T c2_i47;
  int16_T c2_i48;
  int16_T c2_i49;
  real_T c2_d8;
  real_T c2_d9;
  int16_T c2_e_c;
  int16_T c2_k_varargin_1;
  int16_T c2_l_varargin_1;
  int16_T c2_f_var1;
  int16_T c2_i50;
  boolean_T c2_f_covSaturation;
  uint16_T c2_f_hfi;
  observerLogReadPIC(&c2_PICOffset);
  chartInstance->c2_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c2_covrtInstance, 4U, (real_T)
                    *chartInstance->c2_init);
  covrtSigUpdateFcn(chartInstance->c2_covrtInstance, 3U, (real_T)
                    *chartInstance->c2_sum);
  covrtSigUpdateFcn(chartInstance->c2_covrtInstance, 2U, (real_T)
                    *chartInstance->c2_max);
  covrtSigUpdateFcn(chartInstance->c2_covrtInstance, 1U, (real_T)
                    *chartInstance->c2_offset);
  covrtSigUpdateFcn(chartInstance->c2_covrtInstance, 0U, (real_T)
                    *chartInstance->c2_enable);
  chartInstance->c2_sfEvent = CALL_EVENT;
  c2_b_enable = *chartInstance->c2_enable;
  c2_b_offset = *chartInstance->c2_offset;
  c2_b_max = *chartInstance->c2_max;
  c2_b_sum = *chartInstance->c2_sum;
  c2_b_init = *chartInstance->c2_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c2_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c2_emlrt_update_log_1(chartInstance, c2_b_enable,
                        chartInstance->c2_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c2_emlrt_update_log_2(chartInstance, c2_b_offset,
                        chartInstance->c2_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c2_emlrt_update_log_2(chartInstance, c2_b_max,
                        chartInstance->c2_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c2_emlrt_update_log_2(chartInstance, c2_b_sum,
                        chartInstance->c2_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c2_emlrt_update_log_1(chartInstance, c2_b_init,
                        chartInstance->c2_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c2_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c2_covrtInstance, 4U, 0, 0, false);
  c2_a0 = c2_b_enable;
  c2_a = c2_a0;
  c2_b_a0 = c2_a;
  c2_a1 = c2_b_a0;
  c2_b_a1 = c2_a1;
  c2_c = (c2_b_a1 == 0);
  c2_i = (int8_T)c2_b_enable;
  if ((int8_T)(c2_i & 2) != 0) {
    c2_i1 = (int8_T)(c2_i | -2);
  } else {
    c2_i1 = (int8_T)(c2_i & 1);
  }

  if (c2_i1 > 0) {
    c2_d = 3.0;
  } else {
    c2_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c2_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c2_covrtInstance,
        4U, 0U, 0U, c2_d, 0.0, -2, 0U, (int32_T)c2_emlrt_update_log_3
        (chartInstance, c2_c, chartInstance->c2_emlrtLocationLoggingDataTables,
         5)))) {
    c2_b_cont = c2_emlrt_update_log_4(chartInstance, 0U,
      chartInstance->c2_emlrtLocationLoggingDataTables, 6);
    c2_b_out_init = c2_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c2_emlrtLocationLoggingDataTables, 7);
  } else {
    c2_c_a0 = c2_b_init;
    c2_b_a = c2_c_a0;
    c2_d_a0 = c2_b_a;
    c2_c_a1 = c2_d_a0;
    c2_d_a1 = c2_c_a1;
    c2_b_c = (c2_d_a1 == 0);
    c2_i2 = (int8_T)c2_b_init;
    if ((int8_T)(c2_i2 & 2) != 0) {
      c2_i3 = (int8_T)(c2_i2 | -2);
    } else {
      c2_i3 = (int8_T)(c2_i2 & 1);
    }

    if (c2_i3 > 0) {
      c2_d1 = 3.0;
    } else {
      c2_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c2_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c2_covrtInstance, 4U, 0U, 1U, c2_d1, 0.0,
                        -2, 0U, (int32_T)c2_emlrt_update_log_3(chartInstance,
           c2_b_c, chartInstance->c2_emlrtLocationLoggingDataTables, 8)))) {
      c2_b_varargin_1 = c2_b_sum;
      c2_d_varargin_1 = c2_b_varargin_1;
      c2_b_var1 = c2_d_varargin_1;
      c2_i5 = c2_b_var1;
      c2_b_covSaturation = false;
      if (c2_i5 < 0) {
        c2_i5 = 0;
      } else {
        if (c2_i5 > 4095) {
          c2_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c2_covrtInstance, 4, 0, 0, 0,
          c2_b_covSaturation);
      }

      c2_b_hfi = (uint16_T)c2_i5;
      c2_u1 = c2_emlrt_update_log_4(chartInstance, c2_b_hfi,
        chartInstance->c2_emlrtLocationLoggingDataTables, 10);
      c2_f_varargin_1 = c2_b_max;
      c2_h_varargin_1 = c2_f_varargin_1;
      c2_d_var1 = c2_h_varargin_1;
      c2_i7 = c2_d_var1;
      c2_d_covSaturation = false;
      if (c2_i7 < 0) {
        c2_i7 = 0;
      } else {
        if (c2_i7 > 4095) {
          c2_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c2_covrtInstance, 4, 0, 1, 0,
          c2_d_covSaturation);
      }

      c2_d_hfi = (uint16_T)c2_i7;
      c2_u3 = c2_emlrt_update_log_4(chartInstance, c2_d_hfi,
        chartInstance->c2_emlrtLocationLoggingDataTables, 11);
      c2_f_a0 = c2_u1;
      c2_b_b0 = c2_u3;
      c2_d_a = c2_f_a0;
      c2_b_b = c2_b_b0;
      c2_h_a0 = c2_d_a;
      c2_d_b0 = c2_b_b;
      c2_f_a1 = c2_h_a0;
      c2_b_b1 = c2_d_b0;
      c2_h_a1 = c2_f_a1;
      c2_d_b1 = c2_b_b1;
      c2_d_c = (c2_h_a1 < c2_d_b1);
      c2_i9 = (int16_T)c2_u1;
      c2_i11 = (int16_T)c2_u3;
      c2_i13 = (int16_T)c2_u3;
      c2_i15 = (int16_T)c2_u1;
      if ((int16_T)(c2_i13 & 4096) != 0) {
        c2_i17 = (int16_T)(c2_i13 | -4096);
      } else {
        c2_i17 = (int16_T)(c2_i13 & 4095);
      }

      if ((int16_T)(c2_i15 & 4096) != 0) {
        c2_i19 = (int16_T)(c2_i15 | -4096);
      } else {
        c2_i19 = (int16_T)(c2_i15 & 4095);
      }

      c2_i21 = c2_i17 - c2_i19;
      if (c2_i21 > 4095) {
        c2_i21 = 4095;
      } else {
        if (c2_i21 < -4096) {
          c2_i21 = -4096;
        }
      }

      c2_i23 = (int16_T)c2_u1;
      c2_i25 = (int16_T)c2_u3;
      c2_i27 = (int16_T)c2_u1;
      c2_i29 = (int16_T)c2_u3;
      if ((int16_T)(c2_i27 & 4096) != 0) {
        c2_i31 = (int16_T)(c2_i27 | -4096);
      } else {
        c2_i31 = (int16_T)(c2_i27 & 4095);
      }

      if ((int16_T)(c2_i29 & 4096) != 0) {
        c2_i33 = (int16_T)(c2_i29 | -4096);
      } else {
        c2_i33 = (int16_T)(c2_i29 & 4095);
      }

      c2_i35 = c2_i31 - c2_i33;
      if (c2_i35 > 4095) {
        c2_i35 = 4095;
      } else {
        if (c2_i35 < -4096) {
          c2_i35 = -4096;
        }
      }

      if ((int16_T)(c2_i9 & 4096) != 0) {
        c2_i37 = (int16_T)(c2_i9 | -4096);
      } else {
        c2_i37 = (int16_T)(c2_i9 & 4095);
      }

      if ((int16_T)(c2_i11 & 4096) != 0) {
        c2_i39 = (int16_T)(c2_i11 | -4096);
      } else {
        c2_i39 = (int16_T)(c2_i11 & 4095);
      }

      if ((int16_T)(c2_i23 & 4096) != 0) {
        c2_i41 = (int16_T)(c2_i23 | -4096);
      } else {
        c2_i41 = (int16_T)(c2_i23 & 4095);
      }

      if ((int16_T)(c2_i25 & 4096) != 0) {
        c2_i43 = (int16_T)(c2_i25 | -4096);
      } else {
        c2_i43 = (int16_T)(c2_i25 & 4095);
      }

      if (c2_i37 < c2_i39) {
        c2_d3 = (real_T)((int16_T)c2_i21 <= 1);
      } else if (c2_i41 > c2_i43) {
        if ((int16_T)c2_i35 <= 1) {
          c2_d3 = 3.0;
        } else {
          c2_d3 = 0.0;
        }
      } else {
        c2_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c2_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c2_covrtInstance, 4U, 0U, 2U, c2_d3,
                          0.0, -2, 2U, (int32_T)c2_emlrt_update_log_3
                          (chartInstance, c2_d_c,
                           chartInstance->c2_emlrtLocationLoggingDataTables, 9))))
      {
        c2_i_a0 = c2_b_sum;
        c2_e_b0 = c2_b_offset;
        c2_i44 = c2_i_a0;
        if (c2_i44 > 4095) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c2_d4 = 1.0;
          observerLog(25 + c2_PICOffset, &c2_d4, 1);
        }

        if (c2_i44 < -4096) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c2_d5 = 1.0;
          observerLog(25 + c2_PICOffset, &c2_d5, 1);
        }

        c2_i46 = c2_e_b0;
        if (c2_i46 > 4095) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c2_d6 = 1.0;
          observerLog(30 + c2_PICOffset, &c2_d6, 1);
        }

        if (c2_i46 < -4096) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c2_d7 = 1.0;
          observerLog(30 + c2_PICOffset, &c2_d7, 1);
        }

        if ((int16_T)(c2_i44 & 4096) != 0) {
          c2_i47 = (int16_T)(c2_i44 | -4096);
        } else {
          c2_i47 = (int16_T)(c2_i44 & 4095);
        }

        if ((int16_T)(c2_i46 & 4096) != 0) {
          c2_i48 = (int16_T)(c2_i46 | -4096);
        } else {
          c2_i48 = (int16_T)(c2_i46 & 4095);
        }

        c2_i49 = c2__s16_s32_(chartInstance, c2_i47 + c2_i48, 29, 1U, 323, 12);
        if (c2_i49 > 4095) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c2_d8 = 1.0;
          observerLog(35 + c2_PICOffset, &c2_d8, 1);
        }

        if (c2_i49 < -4096) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c2_d9 = 1.0;
          observerLog(35 + c2_PICOffset, &c2_d9, 1);
        }

        if ((int16_T)(c2_i49 & 4096) != 0) {
          c2_e_c = (int16_T)(c2_i49 | -4096);
        } else {
          c2_e_c = (int16_T)(c2_i49 & 4095);
        }

        c2_k_varargin_1 = c2_emlrt_update_log_5(chartInstance, c2_e_c,
          chartInstance->c2_emlrtLocationLoggingDataTables, 13);
        c2_l_varargin_1 = c2_k_varargin_1;
        c2_f_var1 = c2_l_varargin_1;
        c2_i50 = c2_f_var1;
        c2_f_covSaturation = false;
        if (c2_i50 < 0) {
          c2_i50 = 0;
        } else {
          if (c2_i50 > 4095) {
            c2_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c2_covrtInstance, 4, 0, 2, 0,
            c2_f_covSaturation);
        }

        c2_f_hfi = (uint16_T)c2_i50;
        c2_b_cont = c2_emlrt_update_log_4(chartInstance, c2_f_hfi,
          chartInstance->c2_emlrtLocationLoggingDataTables, 12);
        c2_b_out_init = c2_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c2_emlrtLocationLoggingDataTables, 14);
      } else {
        c2_b_cont = c2_emlrt_update_log_4(chartInstance, 0U,
          chartInstance->c2_emlrtLocationLoggingDataTables, 15);
        c2_b_out_init = c2_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c2_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c2_varargin_1 = c2_b_sum;
      c2_c_varargin_1 = c2_varargin_1;
      c2_var1 = c2_c_varargin_1;
      c2_i4 = c2_var1;
      c2_covSaturation = false;
      if (c2_i4 < 0) {
        c2_i4 = 0;
      } else {
        if (c2_i4 > 4095) {
          c2_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c2_covrtInstance, 4, 0, 3, 0,
          c2_covSaturation);
      }

      c2_hfi = (uint16_T)c2_i4;
      c2_u = c2_emlrt_update_log_4(chartInstance, c2_hfi,
        chartInstance->c2_emlrtLocationLoggingDataTables, 18);
      c2_e_varargin_1 = c2_b_max;
      c2_g_varargin_1 = c2_e_varargin_1;
      c2_c_var1 = c2_g_varargin_1;
      c2_i6 = c2_c_var1;
      c2_c_covSaturation = false;
      if (c2_i6 < 0) {
        c2_i6 = 0;
      } else {
        if (c2_i6 > 4095) {
          c2_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c2_covrtInstance, 4, 0, 4, 0,
          c2_c_covSaturation);
      }

      c2_c_hfi = (uint16_T)c2_i6;
      c2_u2 = c2_emlrt_update_log_4(chartInstance, c2_c_hfi,
        chartInstance->c2_emlrtLocationLoggingDataTables, 19);
      c2_e_a0 = c2_u;
      c2_b0 = c2_u2;
      c2_c_a = c2_e_a0;
      c2_b = c2_b0;
      c2_g_a0 = c2_c_a;
      c2_c_b0 = c2_b;
      c2_e_a1 = c2_g_a0;
      c2_b1 = c2_c_b0;
      c2_g_a1 = c2_e_a1;
      c2_c_b1 = c2_b1;
      c2_c_c = (c2_g_a1 < c2_c_b1);
      c2_i8 = (int16_T)c2_u;
      c2_i10 = (int16_T)c2_u2;
      c2_i12 = (int16_T)c2_u2;
      c2_i14 = (int16_T)c2_u;
      if ((int16_T)(c2_i12 & 4096) != 0) {
        c2_i16 = (int16_T)(c2_i12 | -4096);
      } else {
        c2_i16 = (int16_T)(c2_i12 & 4095);
      }

      if ((int16_T)(c2_i14 & 4096) != 0) {
        c2_i18 = (int16_T)(c2_i14 | -4096);
      } else {
        c2_i18 = (int16_T)(c2_i14 & 4095);
      }

      c2_i20 = c2_i16 - c2_i18;
      if (c2_i20 > 4095) {
        c2_i20 = 4095;
      } else {
        if (c2_i20 < -4096) {
          c2_i20 = -4096;
        }
      }

      c2_i22 = (int16_T)c2_u;
      c2_i24 = (int16_T)c2_u2;
      c2_i26 = (int16_T)c2_u;
      c2_i28 = (int16_T)c2_u2;
      if ((int16_T)(c2_i26 & 4096) != 0) {
        c2_i30 = (int16_T)(c2_i26 | -4096);
      } else {
        c2_i30 = (int16_T)(c2_i26 & 4095);
      }

      if ((int16_T)(c2_i28 & 4096) != 0) {
        c2_i32 = (int16_T)(c2_i28 | -4096);
      } else {
        c2_i32 = (int16_T)(c2_i28 & 4095);
      }

      c2_i34 = c2_i30 - c2_i32;
      if (c2_i34 > 4095) {
        c2_i34 = 4095;
      } else {
        if (c2_i34 < -4096) {
          c2_i34 = -4096;
        }
      }

      if ((int16_T)(c2_i8 & 4096) != 0) {
        c2_i36 = (int16_T)(c2_i8 | -4096);
      } else {
        c2_i36 = (int16_T)(c2_i8 & 4095);
      }

      if ((int16_T)(c2_i10 & 4096) != 0) {
        c2_i38 = (int16_T)(c2_i10 | -4096);
      } else {
        c2_i38 = (int16_T)(c2_i10 & 4095);
      }

      if ((int16_T)(c2_i22 & 4096) != 0) {
        c2_i40 = (int16_T)(c2_i22 | -4096);
      } else {
        c2_i40 = (int16_T)(c2_i22 & 4095);
      }

      if ((int16_T)(c2_i24 & 4096) != 0) {
        c2_i42 = (int16_T)(c2_i24 | -4096);
      } else {
        c2_i42 = (int16_T)(c2_i24 & 4095);
      }

      if (c2_i36 < c2_i38) {
        c2_d2 = (real_T)((int16_T)c2_i20 <= 1);
      } else if (c2_i40 > c2_i42) {
        if ((int16_T)c2_i34 <= 1) {
          c2_d2 = 3.0;
        } else {
          c2_d2 = 0.0;
        }
      } else {
        c2_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c2_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c2_covrtInstance, 4U, 0U, 3U, c2_d2,
                          0.0, -2, 2U, (int32_T)c2_emlrt_update_log_3
                          (chartInstance, c2_c_c,
                           chartInstance->c2_emlrtLocationLoggingDataTables, 17))))
      {
        c2_i_varargin_1 = c2_b_sum;
        c2_j_varargin_1 = c2_i_varargin_1;
        c2_e_var1 = c2_j_varargin_1;
        c2_i45 = c2_e_var1;
        c2_e_covSaturation = false;
        if (c2_i45 < 0) {
          c2_i45 = 0;
        } else {
          if (c2_i45 > 4095) {
            c2_i45 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c2_covrtInstance, 4, 0, 5, 0,
            c2_e_covSaturation);
        }

        c2_e_hfi = (uint16_T)c2_i45;
        c2_b_cont = c2_emlrt_update_log_4(chartInstance, c2_e_hfi,
          chartInstance->c2_emlrtLocationLoggingDataTables, 20);
        c2_b_out_init = c2_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c2_emlrtLocationLoggingDataTables, 21);
      } else {
        c2_b_cont = c2_emlrt_update_log_4(chartInstance, 0U,
          chartInstance->c2_emlrtLocationLoggingDataTables, 22);
        c2_b_out_init = c2_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c2_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c2_cont = c2_b_cont;
  *chartInstance->c2_out_init = c2_b_out_init;
  c2_do_animation_call_c2_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c2_covrtInstance, 5U, (real_T)
                    *chartInstance->c2_cont);
  covrtSigUpdateFcn(chartInstance->c2_covrtInstance, 6U, (real_T)
                    *chartInstance->c2_out_init);
}

static void mdl_start_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c2_PWM_28_HalfB
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c2_decisionTxtStartIdx = 0U;
  static const uint32_T c2_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c2_chart_data_browse_helper);
  chartInstance->c2_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c2_RuntimeVar,
    &chartInstance->c2_IsDebuggerActive,
    &chartInstance->c2_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c2_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c2_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c2_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c2_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c2_decisionTxtStartIdx, &c2_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c2_covrtInstance, 0U, 0, NULL, NULL, 0U, NULL);
  covrtEmlInitFcn(chartInstance->c2_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 0U, 266, -1,
    279);
  covrtEmlSaturationInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 1U, 282, -1,
    295);
  covrtEmlSaturationInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 2U, 319, -1,
    341);
  covrtEmlSaturationInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 3U, 518, -1,
    531);
  covrtEmlSaturationInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 4U, 534, -1,
    547);
  covrtEmlSaturationInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 5U, 571, -1,
    584);
  covrtEmlIfInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 0U, 70, 86, -1, 121);
  covrtEmlIfInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c2_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c2_PWM_28_HalfB
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c2_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:6677",              /* mexFileName */
    "Thu May 27 10:27:16 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c2_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c2_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c2_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c2_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:6677");
    emlrtLocationLoggingPushLog(&c2_emlrtLocationLoggingFileInfo,
      c2_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c2_emlrtLocationLoggingDataTables, c2_emlrtLocationInfo,
      NULL, 0U, c2_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:6677");
  }

  sfListenerLightTerminate(chartInstance->c2_RuntimeVar);
  sf_mex_destroy(&c2_eml_mx);
  sf_mex_destroy(&c2_b_eml_mx);
  sf_mex_destroy(&c2_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c2_covrtInstance);
}

static void initSimStructsc2_PWM_28_HalfB(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c2_emlrt_update_log_1(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index)
{
  boolean_T c2_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c2_b_table;
  real_T c2_d;
  uint8_T c2_u;
  uint8_T c2_localMin;
  real_T c2_d1;
  uint8_T c2_u1;
  uint8_T c2_localMax;
  emlrtLocationLoggingHistogramType *c2_histTable;
  real_T c2_inDouble;
  real_T c2_significand;
  int32_T c2_exponent;
  (void)chartInstance;
  c2_isLoggingEnabledHere = (c2_index >= 0);
  if (c2_isLoggingEnabledHere) {
    c2_b_table = (emlrtLocationLoggingDataType *)&c2_table[c2_index];
    c2_d = c2_b_table[0U].SimMin;
    if (c2_d < 2.0) {
      if (c2_d >= 0.0) {
        c2_u = (uint8_T)c2_d;
      } else {
        c2_u = 0U;
      }
    } else if (c2_d >= 2.0) {
      c2_u = 1U;
    } else {
      c2_u = 0U;
    }

    c2_localMin = c2_u;
    c2_d1 = c2_b_table[0U].SimMax;
    if (c2_d1 < 2.0) {
      if (c2_d1 >= 0.0) {
        c2_u1 = (uint8_T)c2_d1;
      } else {
        c2_u1 = 0U;
      }
    } else if (c2_d1 >= 2.0) {
      c2_u1 = 1U;
    } else {
      c2_u1 = 0U;
    }

    c2_localMax = c2_u1;
    c2_histTable = c2_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c2_in < c2_localMin) {
      c2_localMin = c2_in;
    }

    if (c2_in > c2_localMax) {
      c2_localMax = c2_in;
    }

    /* Histogram logging. */
    c2_inDouble = (real_T)c2_in;
    c2_histTable->TotalNumberOfValues++;
    if (c2_inDouble == 0.0) {
      c2_histTable->NumberOfZeros++;
    } else {
      c2_histTable->SimSum += c2_inDouble;
      if ((!muDoubleScalarIsInf(c2_inDouble)) && (!muDoubleScalarIsNaN
           (c2_inDouble))) {
        c2_significand = frexp(c2_inDouble, &c2_exponent);
        if (c2_exponent > 128) {
          c2_exponent = 128;
        }

        if (c2_exponent < -127) {
          c2_exponent = -127;
        }

        if (c2_significand < 0.0) {
          c2_histTable->NumberOfNegativeValues++;
          c2_histTable->HistogramOfNegativeValues[127 + c2_exponent]++;
        } else {
          c2_histTable->NumberOfPositiveValues++;
          c2_histTable->HistogramOfPositiveValues[127 + c2_exponent]++;
        }
      }
    }

    c2_b_table[0U].SimMin = (real_T)c2_localMin;
    c2_b_table[0U].SimMax = (real_T)c2_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c2_in;
}

static int16_T c2_emlrt_update_log_2(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index)
{
  boolean_T c2_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c2_b_table;
  real_T c2_d;
  int16_T c2_i;
  int16_T c2_localMin;
  real_T c2_d1;
  int16_T c2_i1;
  int16_T c2_localMax;
  emlrtLocationLoggingHistogramType *c2_histTable;
  real_T c2_inDouble;
  real_T c2_significand;
  int32_T c2_exponent;
  (void)chartInstance;
  c2_isLoggingEnabledHere = (c2_index >= 0);
  if (c2_isLoggingEnabledHere) {
    c2_b_table = (emlrtLocationLoggingDataType *)&c2_table[c2_index];
    c2_d = muDoubleScalarFloor(c2_b_table[0U].SimMin);
    if (c2_d < 2048.0) {
      if (c2_d >= -2048.0) {
        c2_i = (int16_T)c2_d;
      } else {
        c2_i = -2048;
      }
    } else if (c2_d >= 2048.0) {
      c2_i = 2047;
    } else {
      c2_i = 0;
    }

    c2_localMin = c2_i;
    c2_d1 = muDoubleScalarFloor(c2_b_table[0U].SimMax);
    if (c2_d1 < 2048.0) {
      if (c2_d1 >= -2048.0) {
        c2_i1 = (int16_T)c2_d1;
      } else {
        c2_i1 = -2048;
      }
    } else if (c2_d1 >= 2048.0) {
      c2_i1 = 2047;
    } else {
      c2_i1 = 0;
    }

    c2_localMax = c2_i1;
    c2_histTable = c2_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c2_in < c2_localMin) {
      c2_localMin = c2_in;
    }

    if (c2_in > c2_localMax) {
      c2_localMax = c2_in;
    }

    /* Histogram logging. */
    c2_inDouble = (real_T)c2_in;
    c2_histTable->TotalNumberOfValues++;
    if (c2_inDouble == 0.0) {
      c2_histTable->NumberOfZeros++;
    } else {
      c2_histTable->SimSum += c2_inDouble;
      if ((!muDoubleScalarIsInf(c2_inDouble)) && (!muDoubleScalarIsNaN
           (c2_inDouble))) {
        c2_significand = frexp(c2_inDouble, &c2_exponent);
        if (c2_exponent > 128) {
          c2_exponent = 128;
        }

        if (c2_exponent < -127) {
          c2_exponent = -127;
        }

        if (c2_significand < 0.0) {
          c2_histTable->NumberOfNegativeValues++;
          c2_histTable->HistogramOfNegativeValues[127 + c2_exponent]++;
        } else {
          c2_histTable->NumberOfPositiveValues++;
          c2_histTable->HistogramOfPositiveValues[127 + c2_exponent]++;
        }
      }
    }

    c2_b_table[0U].SimMin = (real_T)c2_localMin;
    c2_b_table[0U].SimMax = (real_T)c2_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c2_in;
}

static boolean_T c2_emlrt_update_log_3(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index)
{
  boolean_T c2_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c2_b_table;
  boolean_T c2_localMin;
  boolean_T c2_localMax;
  emlrtLocationLoggingHistogramType *c2_histTable;
  real_T c2_inDouble;
  real_T c2_significand;
  int32_T c2_exponent;
  (void)chartInstance;
  c2_isLoggingEnabledHere = (c2_index >= 0);
  if (c2_isLoggingEnabledHere) {
    c2_b_table = (emlrtLocationLoggingDataType *)&c2_table[c2_index];
    c2_localMin = (c2_b_table[0U].SimMin > 0.0);
    c2_localMax = (c2_b_table[0U].SimMax > 0.0);
    c2_histTable = c2_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c2_in < (int32_T)c2_localMin) {
      c2_localMin = c2_in;
    }

    if ((int32_T)c2_in > (int32_T)c2_localMax) {
      c2_localMax = c2_in;
    }

    /* Histogram logging. */
    c2_inDouble = (real_T)c2_in;
    c2_histTable->TotalNumberOfValues++;
    if (c2_inDouble == 0.0) {
      c2_histTable->NumberOfZeros++;
    } else {
      c2_histTable->SimSum += c2_inDouble;
      if ((!muDoubleScalarIsInf(c2_inDouble)) && (!muDoubleScalarIsNaN
           (c2_inDouble))) {
        c2_significand = frexp(c2_inDouble, &c2_exponent);
        if (c2_exponent > 128) {
          c2_exponent = 128;
        }

        if (c2_exponent < -127) {
          c2_exponent = -127;
        }

        if (c2_significand < 0.0) {
          c2_histTable->NumberOfNegativeValues++;
          c2_histTable->HistogramOfNegativeValues[127 + c2_exponent]++;
        } else {
          c2_histTable->NumberOfPositiveValues++;
          c2_histTable->HistogramOfPositiveValues[127 + c2_exponent]++;
        }
      }
    }

    c2_b_table[0U].SimMin = (real_T)c2_localMin;
    c2_b_table[0U].SimMax = (real_T)c2_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c2_in;
}

static uint16_T c2_emlrt_update_log_4(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index)
{
  boolean_T c2_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c2_b_table;
  real_T c2_d;
  uint16_T c2_u;
  uint16_T c2_localMin;
  real_T c2_d1;
  uint16_T c2_u1;
  uint16_T c2_localMax;
  emlrtLocationLoggingHistogramType *c2_histTable;
  real_T c2_inDouble;
  real_T c2_significand;
  int32_T c2_exponent;
  (void)chartInstance;
  c2_isLoggingEnabledHere = (c2_index >= 0);
  if (c2_isLoggingEnabledHere) {
    c2_b_table = (emlrtLocationLoggingDataType *)&c2_table[c2_index];
    c2_d = c2_b_table[0U].SimMin;
    if (c2_d < 4096.0) {
      if (c2_d >= 0.0) {
        c2_u = (uint16_T)c2_d;
      } else {
        c2_u = 0U;
      }
    } else if (c2_d >= 4096.0) {
      c2_u = 4095U;
    } else {
      c2_u = 0U;
    }

    c2_localMin = c2_u;
    c2_d1 = c2_b_table[0U].SimMax;
    if (c2_d1 < 4096.0) {
      if (c2_d1 >= 0.0) {
        c2_u1 = (uint16_T)c2_d1;
      } else {
        c2_u1 = 0U;
      }
    } else if (c2_d1 >= 4096.0) {
      c2_u1 = 4095U;
    } else {
      c2_u1 = 0U;
    }

    c2_localMax = c2_u1;
    c2_histTable = c2_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c2_in < c2_localMin) {
      c2_localMin = c2_in;
    }

    if (c2_in > c2_localMax) {
      c2_localMax = c2_in;
    }

    /* Histogram logging. */
    c2_inDouble = (real_T)c2_in;
    c2_histTable->TotalNumberOfValues++;
    if (c2_inDouble == 0.0) {
      c2_histTable->NumberOfZeros++;
    } else {
      c2_histTable->SimSum += c2_inDouble;
      if ((!muDoubleScalarIsInf(c2_inDouble)) && (!muDoubleScalarIsNaN
           (c2_inDouble))) {
        c2_significand = frexp(c2_inDouble, &c2_exponent);
        if (c2_exponent > 128) {
          c2_exponent = 128;
        }

        if (c2_exponent < -127) {
          c2_exponent = -127;
        }

        if (c2_significand < 0.0) {
          c2_histTable->NumberOfNegativeValues++;
          c2_histTable->HistogramOfNegativeValues[127 + c2_exponent]++;
        } else {
          c2_histTable->NumberOfPositiveValues++;
          c2_histTable->HistogramOfPositiveValues[127 + c2_exponent]++;
        }
      }
    }

    c2_b_table[0U].SimMin = (real_T)c2_localMin;
    c2_b_table[0U].SimMax = (real_T)c2_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c2_in;
}

static int16_T c2_emlrt_update_log_5(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c2_in, emlrtLocationLoggingDataType c2_table[],
  int32_T c2_index)
{
  boolean_T c2_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c2_b_table;
  real_T c2_d;
  int16_T c2_i;
  int16_T c2_localMin;
  real_T c2_d1;
  int16_T c2_i1;
  int16_T c2_localMax;
  emlrtLocationLoggingHistogramType *c2_histTable;
  real_T c2_inDouble;
  real_T c2_significand;
  int32_T c2_exponent;
  (void)chartInstance;
  c2_isLoggingEnabledHere = (c2_index >= 0);
  if (c2_isLoggingEnabledHere) {
    c2_b_table = (emlrtLocationLoggingDataType *)&c2_table[c2_index];
    c2_d = muDoubleScalarFloor(c2_b_table[0U].SimMin);
    if (c2_d < 4096.0) {
      if (c2_d >= -4096.0) {
        c2_i = (int16_T)c2_d;
      } else {
        c2_i = -4096;
      }
    } else if (c2_d >= 4096.0) {
      c2_i = 4095;
    } else {
      c2_i = 0;
    }

    c2_localMin = c2_i;
    c2_d1 = muDoubleScalarFloor(c2_b_table[0U].SimMax);
    if (c2_d1 < 4096.0) {
      if (c2_d1 >= -4096.0) {
        c2_i1 = (int16_T)c2_d1;
      } else {
        c2_i1 = -4096;
      }
    } else if (c2_d1 >= 4096.0) {
      c2_i1 = 4095;
    } else {
      c2_i1 = 0;
    }

    c2_localMax = c2_i1;
    c2_histTable = c2_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c2_in < c2_localMin) {
      c2_localMin = c2_in;
    }

    if (c2_in > c2_localMax) {
      c2_localMax = c2_in;
    }

    /* Histogram logging. */
    c2_inDouble = (real_T)c2_in;
    c2_histTable->TotalNumberOfValues++;
    if (c2_inDouble == 0.0) {
      c2_histTable->NumberOfZeros++;
    } else {
      c2_histTable->SimSum += c2_inDouble;
      if ((!muDoubleScalarIsInf(c2_inDouble)) && (!muDoubleScalarIsNaN
           (c2_inDouble))) {
        c2_significand = frexp(c2_inDouble, &c2_exponent);
        if (c2_exponent > 128) {
          c2_exponent = 128;
        }

        if (c2_exponent < -127) {
          c2_exponent = -127;
        }

        if (c2_significand < 0.0) {
          c2_histTable->NumberOfNegativeValues++;
          c2_histTable->HistogramOfNegativeValues[127 + c2_exponent]++;
        } else {
          c2_histTable->NumberOfPositiveValues++;
          c2_histTable->HistogramOfPositiveValues[127 + c2_exponent]++;
        }
      }
    }

    c2_b_table[0U].SimMin = (real_T)c2_localMin;
    c2_b_table[0U].SimMax = (real_T)c2_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c2_in;
}

static void c2_emlrtInitVarDataTables(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c2_dataTables[24],
  emlrtLocationLoggingHistogramType c2_histTables[24])
{
  int32_T c2_i;
  int32_T c2_iH;
  (void)chartInstance;
  for (c2_i = 0; c2_i < 24; c2_i++) {
    c2_dataTables[c2_i].SimMin = rtInf;
    c2_dataTables[c2_i].SimMax = rtMinusInf;
    c2_dataTables[c2_i].OverflowWraps = 0;
    c2_dataTables[c2_i].Saturations = 0;
    c2_dataTables[c2_i].IsAlwaysInteger = true;
    c2_dataTables[c2_i].HistogramTable = &c2_histTables[c2_i];
    c2_histTables[c2_i].NumberOfZeros = 0.0;
    c2_histTables[c2_i].NumberOfPositiveValues = 0.0;
    c2_histTables[c2_i].NumberOfNegativeValues = 0.0;
    c2_histTables[c2_i].TotalNumberOfValues = 0.0;
    c2_histTables[c2_i].SimSum = 0.0;
    for (c2_iH = 0; c2_iH < 256; c2_iH++) {
      c2_histTables[c2_i].HistogramOfPositiveValues[c2_iH] = 0.0;
      c2_histTables[c2_i].HistogramOfNegativeValues[c2_iH] = 0.0;
    }
  }
}

const mxArray *sf_c2_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c2_nameCaptureInfo = NULL;
  c2_nameCaptureInfo = NULL;
  sf_mex_assign(&c2_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c2_nameCaptureInfo;
}

static uint16_T c2_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c2_sp, const mxArray *c2_b_cont, const
  char_T *c2_identifier)
{
  uint16_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_b_emlrt_marshallIn(chartInstance, c2_sp, sf_mex_dup(c2_b_cont),
    &c2_thisId);
  sf_mex_destroy(&c2_b_cont);
  return c2_y;
}

static uint16_T c2_b_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c2_sp, const mxArray *c2_u, const
  emlrtMsgIdentifier *c2_parentId)
{
  uint16_T c2_y;
  const mxArray *c2_mxFi = NULL;
  const mxArray *c2_mxInt = NULL;
  uint16_T c2_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c2_parentId, c2_u, false, 0U, NULL, c2_eml_mx, c2_b_eml_mx);
  sf_mex_assign(&c2_mxFi, sf_mex_dup(c2_u), false);
  sf_mex_assign(&c2_mxInt, sf_mex_call(c2_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c2_mxFi)), false);
  sf_mex_import(c2_parentId, sf_mex_dup(c2_mxInt), &c2_b_u, 1, 5, 0U, 0, 0U, 0);
  sf_mex_destroy(&c2_mxFi);
  sf_mex_destroy(&c2_mxInt);
  c2_y = c2_b_u;
  sf_mex_destroy(&c2_mxFi);
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static uint8_T c2_c_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c2_sp, const mxArray *c2_b_out_init, const
  char_T *c2_identifier)
{
  uint8_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_d_emlrt_marshallIn(chartInstance, c2_sp, sf_mex_dup(c2_b_out_init),
    &c2_thisId);
  sf_mex_destroy(&c2_b_out_init);
  return c2_y;
}

static uint8_T c2_d_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c2_sp, const mxArray *c2_u, const
  emlrtMsgIdentifier *c2_parentId)
{
  uint8_T c2_y;
  const mxArray *c2_mxFi = NULL;
  const mxArray *c2_mxInt = NULL;
  uint8_T c2_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c2_parentId, c2_u, false, 0U, NULL, c2_eml_mx, c2_c_eml_mx);
  sf_mex_assign(&c2_mxFi, sf_mex_dup(c2_u), false);
  sf_mex_assign(&c2_mxInt, sf_mex_call(c2_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c2_mxFi)), false);
  sf_mex_import(c2_parentId, sf_mex_dup(c2_mxInt), &c2_b_u, 1, 3, 0U, 0, 0U, 0);
  sf_mex_destroy(&c2_mxFi);
  sf_mex_destroy(&c2_mxInt);
  c2_y = c2_b_u;
  sf_mex_destroy(&c2_mxFi);
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static uint8_T c2_e_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c2_b_is_active_c2_PWM_28_HalfB, const char_T
  *c2_identifier)
{
  uint8_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_is_active_c2_PWM_28_HalfB), &c2_thisId);
  sf_mex_destroy(&c2_b_is_active_c2_PWM_28_HalfB);
  return c2_y;
}

static uint8_T c2_f_emlrt_marshallIn(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  uint8_T c2_y;
  uint8_T c2_b_u;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_b_u, 1, 3, 0U, 0, 0U, 0);
  c2_y = c2_b_u;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static const mxArray *c2_chart_data_browse_helper
  (SFc2_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c2_ssIdNumber)
{
  const mxArray *c2_mxData = NULL;
  uint8_T c2_u;
  int16_T c2_i;
  int16_T c2_i1;
  int16_T c2_i2;
  uint16_T c2_u1;
  uint8_T c2_u2;
  uint8_T c2_u3;
  real_T c2_d;
  real_T c2_d1;
  real_T c2_d2;
  real_T c2_d3;
  real_T c2_d4;
  real_T c2_d5;
  real_T c2_d6;
  c2_mxData = NULL;
  switch (c2_ssIdNumber) {
   case 18U:
    c2_u = *chartInstance->c2_enable;
    c2_d = (real_T)c2_u;
    sf_mex_assign(&c2_mxData, sf_mex_create("mxData", &c2_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c2_i = *chartInstance->c2_offset;
    c2_d1 = (real_T)c2_i;
    sf_mex_assign(&c2_mxData, sf_mex_create("mxData", &c2_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c2_i1 = *chartInstance->c2_max;
    c2_d2 = (real_T)c2_i1;
    sf_mex_assign(&c2_mxData, sf_mex_create("mxData", &c2_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c2_i2 = *chartInstance->c2_sum;
    c2_d3 = (real_T)c2_i2;
    sf_mex_assign(&c2_mxData, sf_mex_create("mxData", &c2_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c2_u1 = *chartInstance->c2_cont;
    c2_d4 = (real_T)c2_u1;
    sf_mex_assign(&c2_mxData, sf_mex_create("mxData", &c2_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c2_u2 = *chartInstance->c2_init;
    c2_d5 = (real_T)c2_u2;
    sf_mex_assign(&c2_mxData, sf_mex_create("mxData", &c2_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c2_u3 = *chartInstance->c2_out_init;
    c2_d6 = (real_T)c2_u3;
    sf_mex_assign(&c2_mxData, sf_mex_create("mxData", &c2_d6, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c2_mxData;
}

static int16_T c2__s16_s32_(SFc2_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c2_b, int32_T c2_EMLOvCount_src_loc, uint32_T c2_ssid_src_loc, int32_T
  c2_offset_src_loc, int32_T c2_length_src_loc)
{
  int16_T c2_a;
  int32_T c2_PICOffset;
  real_T c2_d;
  observerLogReadPIC(&c2_PICOffset);
  c2_a = (int16_T)c2_b;
  if (c2_a != c2_b) {
    sf_data_overflow_error(chartInstance->S, c2_ssid_src_loc, c2_offset_src_loc,
      c2_length_src_loc);
    c2_d = 1.0;
    observerLog(c2_EMLOvCount_src_loc + c2_PICOffset, &c2_d, 1);
  }

  return c2_a;
}

static void init_dsm_address_info(SFc2_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc2_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c2_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c2_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c2_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c2_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c2_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c2_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c2_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c2_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c2_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c2_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(355642179U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3368501620U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1323223078U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(627781315U);
}

mxArray *sf_c2_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c2_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c2_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c2_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMSzR8gfmZxfGJySWZZanyyUXxAuG+8kUW8R"
    "2JOmhOSuSAAAOnxIFw="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c2_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "sNUPPTm0ulf2I8iDuONHcnG";
}

static void sf_opaque_initialize_c2_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c2_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c2_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c2_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c2_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c2_PWM_28_HalfB(SimStruct* S, const mxArray *
  st)
{
  set_sim_state_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c2_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc2_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c2_PWM_28_HalfB
      ((SFc2_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c2_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c2_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c2_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc2_PWM_28_HalfB((SFc2_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c2_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV91u2zYUll2naIEtCHpTFCiw3rVXWxGgQK7WNLLdGLATb3baXdVgqGOLMEWq/FGSJ9gT9DH",
    "6DnuMvUSB3Qy93aEsO44syU3dBi0wArRA8jsfzx8Paa/W6XnYtrH/cc/zbuP3Dva6N2tb2bi21G",
    "fzDe9xNn6OQsJGfaJIpL3KJkgEv4OW3BomRUeMZSGMiTEoEBSxsVSmjE2zyHImpm0rqOPTr0NGw",
    "0EoLQ8OUJYEx4JfIFtsTR95mkwBNW2AwIRK2knY5mSy0FiZMz8EOtU2qjJBgxnY2Kmle5YbFnNo",
    "nQPtCG0IaqwvdRsYYsA356VmOkv1YA6UUcwZEYXWhkQPIEYHGziJA/w9tgaNysNoSJQ5gJAkoLt",
    "smnJKAXlOpnHhlAlipGKEtyLuO8FV3foc9enJAHiFQ1C3AwVkGksmTHn8B220tCXIKYcmnNpJOd",
    "sA3loX/FcMzkCV+m3sywQUmcCxKN00dUjrPI3WIktWYYZF8IqoFxTjpyEozV7MHD0gGCcYokQZD",
    "FIjO3qoWILuLWWzUcdl5rojY6NZsPU6WMrWSqAqCgu2NhU+4VyXwoYy7kICPGVtEkOqYTPWYpzW",
    "LBhKdLBL7/LTYAXDwGcwX4qAFYYryQHSunOEheUqklptZORj8ja73dXlVVhHGFBjQqGoCijCNKD",
    "PUveWswVMu9gjELUyqXpF4FmGrEN5emxF80yqKfqkoohcmuAiWgqM9ARjiSfhROOhqYK5WK7DUU",
    "JDCFyBYRx6eGwQW+AT7UrbCzx3CTMXTdBUsbgoqu7+eepd3j8/fML9M5fLf58s8dQKeLylr8PvL",
    "eHv1q/it3L71udzrmXy+0vyP+b2a+TkHW4H+7/vHpLmn2/835IPH395P93P25/Xo7aiR82b7x/f",
    "ut69vZ2NH84L5CLhk5U8c9jDJb0aBfz3l/h3srE+Oun3h9FTy8e7nT3WtMdHh1S8TPn+qlfreyu",
    "n73z+kavUF3Fad7WinSB7ULgxsbNrNh/P22v8cTebn7V/nm8m//NKHIv81bjir4ZHpTDbJfl4s/",
    "o/28/LF+l/JxdvN5bWjJhgX8iOR/ubyc/276+x40HOjgfpu2JEXLWCEd0d9V/3Rrt7o0PCxwcFd",
    "eZzz+t15bwblvte9PzfL1/fvk+5h7c+U66+4b1/U3Kb2nfd98i3hq+6z7wcfucbtmPTd+LXxv/t",
    "Xe8d91M2/nXxF8sPGQ8KXtvZchfIuGj1Buz7D0A3nts=",
    ""
  };

  static char newstr [1245] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c2_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c2_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(1629263349U));
  ssSetChecksum1(S,(1130268484U));
  ssSetChecksum2(S,(4159534673U));
  ssSetChecksum3(S,(1080798511U));
}

static void mdlRTW_c2_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c2_PWM_28_HalfB(SimStruct *S)
{
  SFc2_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc2_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc2_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc2_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c2_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c2_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c2_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c2_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c2_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c2_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c2_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c2_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c2_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c2_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c2_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c2_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c2_JITStateAnimation,
    chartInstance->c2_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c2_PWM_28_HalfB(chartInstance);
}

void c2_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c2_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c2_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c2_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c2_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
