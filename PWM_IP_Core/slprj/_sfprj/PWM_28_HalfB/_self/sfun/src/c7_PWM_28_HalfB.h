#ifndef __c7_PWM_28_HalfB_h__
#define __c7_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc7_PWM_28_HalfBInstanceStruct
#define typedef_SFc7_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c7_sfEvent;
  boolean_T c7_doneDoubleBufferReInit;
  uint8_T c7_is_active_c7_PWM_28_HalfB;
  uint8_T c7_JITStateAnimation[1];
  uint8_T c7_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c7_emlrtLocationLoggingDataTables[24];
  int32_T c7_IsDebuggerActive;
  int32_T c7_IsSequenceViewerPresent;
  int32_T c7_SequenceViewerOptimization;
  void *c7_RuntimeVar;
  emlrtLocationLoggingHistogramType c7_emlrtLocLogHistTables[24];
  boolean_T c7_emlrtLocLogSimulated;
  uint32_T c7_mlFcnLineNumber;
  void *c7_fcnDataPtrs[7];
  char_T *c7_dataNames[7];
  uint32_T c7_numFcnVars;
  uint32_T c7_ssIds[7];
  uint32_T c7_statuses[7];
  void *c7_outMexFcns[7];
  void *c7_inMexFcns[7];
  CovrtStateflowInstance *c7_covrtInstance;
  void *c7_fEmlrtCtx;
  uint8_T *c7_enable;
  int16_T *c7_offset;
  int16_T *c7_max;
  int16_T *c7_sum;
  uint16_T *c7_cont;
  uint8_T *c7_init;
  uint8_T *c7_out_init;
} SFc7_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc7_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c7_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c7_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c7_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
