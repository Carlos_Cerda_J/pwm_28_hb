#ifndef __c3_PWM_28_HalfB_h__
#define __c3_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc3_PWM_28_HalfBInstanceStruct
#define typedef_SFc3_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c3_sfEvent;
  boolean_T c3_doneDoubleBufferReInit;
  uint8_T c3_is_active_c3_PWM_28_HalfB;
  uint8_T c3_JITStateAnimation[1];
  uint8_T c3_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c3_emlrtLocationLoggingDataTables[24];
  int32_T c3_IsDebuggerActive;
  int32_T c3_IsSequenceViewerPresent;
  int32_T c3_SequenceViewerOptimization;
  void *c3_RuntimeVar;
  emlrtLocationLoggingHistogramType c3_emlrtLocLogHistTables[24];
  boolean_T c3_emlrtLocLogSimulated;
  uint32_T c3_mlFcnLineNumber;
  void *c3_fcnDataPtrs[7];
  char_T *c3_dataNames[7];
  uint32_T c3_numFcnVars;
  uint32_T c3_ssIds[7];
  uint32_T c3_statuses[7];
  void *c3_outMexFcns[7];
  void *c3_inMexFcns[7];
  CovrtStateflowInstance *c3_covrtInstance;
  void *c3_fEmlrtCtx;
  uint8_T *c3_enable;
  int16_T *c3_offset;
  int16_T *c3_max;
  int16_T *c3_sum;
  uint16_T *c3_cont;
  uint8_T *c3_init;
  uint8_T *c3_out_init;
} SFc3_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc3_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c3_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c3_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c3_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
