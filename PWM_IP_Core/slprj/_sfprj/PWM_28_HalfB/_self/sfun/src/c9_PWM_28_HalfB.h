#ifndef __c9_PWM_28_HalfB_h__
#define __c9_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc9_PWM_28_HalfBInstanceStruct
#define typedef_SFc9_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c9_sfEvent;
  boolean_T c9_doneDoubleBufferReInit;
  uint8_T c9_is_active_c9_PWM_28_HalfB;
  uint8_T c9_JITStateAnimation[1];
  uint8_T c9_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c9_emlrtLocationLoggingDataTables[24];
  int32_T c9_IsDebuggerActive;
  int32_T c9_IsSequenceViewerPresent;
  int32_T c9_SequenceViewerOptimization;
  void *c9_RuntimeVar;
  emlrtLocationLoggingHistogramType c9_emlrtLocLogHistTables[24];
  boolean_T c9_emlrtLocLogSimulated;
  uint32_T c9_mlFcnLineNumber;
  void *c9_fcnDataPtrs[7];
  char_T *c9_dataNames[7];
  uint32_T c9_numFcnVars;
  uint32_T c9_ssIds[7];
  uint32_T c9_statuses[7];
  void *c9_outMexFcns[7];
  void *c9_inMexFcns[7];
  CovrtStateflowInstance *c9_covrtInstance;
  void *c9_fEmlrtCtx;
  uint8_T *c9_enable;
  int16_T *c9_offset;
  int16_T *c9_max;
  int16_T *c9_sum;
  uint16_T *c9_cont;
  uint8_T *c9_init;
  uint8_T *c9_out_init;
} SFc9_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc9_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c9_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c9_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c9_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
