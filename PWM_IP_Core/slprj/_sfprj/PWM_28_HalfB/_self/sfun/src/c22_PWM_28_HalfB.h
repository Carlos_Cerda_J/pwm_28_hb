#ifndef __c22_PWM_28_HalfB_h__
#define __c22_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc22_PWM_28_HalfBInstanceStruct
#define typedef_SFc22_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c22_sfEvent;
  boolean_T c22_doneDoubleBufferReInit;
  uint8_T c22_is_active_c22_PWM_28_HalfB;
  uint8_T c22_JITStateAnimation[1];
  uint8_T c22_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c22_emlrtLocationLoggingDataTables[24];
  int32_T c22_IsDebuggerActive;
  int32_T c22_IsSequenceViewerPresent;
  int32_T c22_SequenceViewerOptimization;
  void *c22_RuntimeVar;
  emlrtLocationLoggingHistogramType c22_emlrtLocLogHistTables[24];
  boolean_T c22_emlrtLocLogSimulated;
  uint32_T c22_mlFcnLineNumber;
  void *c22_fcnDataPtrs[7];
  char_T *c22_dataNames[7];
  uint32_T c22_numFcnVars;
  uint32_T c22_ssIds[7];
  uint32_T c22_statuses[7];
  void *c22_outMexFcns[7];
  void *c22_inMexFcns[7];
  CovrtStateflowInstance *c22_covrtInstance;
  void *c22_fEmlrtCtx;
  uint8_T *c22_enable;
  int16_T *c22_offset;
  int16_T *c22_max;
  int16_T *c22_sum;
  uint16_T *c22_cont;
  uint8_T *c22_init;
  uint8_T *c22_out_init;
} SFc22_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc22_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c22_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c22_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c22_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
