/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c7_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c7_eml_mx;
static const mxArray *c7_b_eml_mx;
static const mxArray *c7_c_eml_mx;

/* Function Declarations */
static void initialize_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void enable_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c7_update_jit_animation_state_c7_PWM_28_HalfB
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance);
static void c7_do_animation_call_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void ext_mode_exec_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c7_PWM_28_HalfB
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c7_st);
static void sf_gateway_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c7_PWM_28_HalfB
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c7_PWM_28_HalfB
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c7_emlrt_update_log_1(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index);
static int16_T c7_emlrt_update_log_2(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index);
static int16_T c7_emlrt_update_log_3(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index);
static boolean_T c7_emlrt_update_log_4(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index);
static uint16_T c7_emlrt_update_log_5(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index);
static int32_T c7_emlrt_update_log_6(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index);
static void c7_emlrtInitVarDataTables(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c7_dataTables[24],
  emlrtLocationLoggingHistogramType c7_histTables[24]);
static uint16_T c7_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c7_sp, const mxArray *c7_b_cont, const
  char_T *c7_identifier);
static uint16_T c7_b_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c7_sp, const mxArray *c7_u, const
  emlrtMsgIdentifier *c7_parentId);
static uint8_T c7_c_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c7_sp, const mxArray *c7_b_out_init, const
  char_T *c7_identifier);
static uint8_T c7_d_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c7_sp, const mxArray *c7_u, const
  emlrtMsgIdentifier *c7_parentId);
static uint8_T c7_e_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c7_b_is_active_c7_PWM_28_HalfB, const char_T
  *c7_identifier);
static uint8_T c7_f_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static const mxArray *c7_chart_data_browse_helper
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c7_ssIdNumber);
static int32_T c7__s32_add__(SFc7_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c7_b, int32_T c7_c, int32_T c7_EMLOvCount_src_loc, uint32_T
  c7_ssid_src_loc, int32_T c7_offset_src_loc, int32_T c7_length_src_loc);
static void init_dsm_address_info(SFc7_PWM_28_HalfBInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c7_st = { NULL,           /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c7_st.tls = chartInstance->c7_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c7_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c7_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c7_is_active_c7_PWM_28_HalfB = 0U;
  sf_mex_assign(&c7_c_eml_mx, sf_mex_call(&c7_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c7_b_eml_mx, sf_mex_call(&c7_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c7_eml_mx, sf_mex_call(&c7_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c7_emlrtLocLogSimulated = false;
  c7_emlrtInitVarDataTables(chartInstance,
    chartInstance->c7_emlrtLocationLoggingDataTables,
    chartInstance->c7_emlrtLocLogHistTables);
}

static void initialize_params_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c7_update_jit_animation_state_c7_PWM_28_HalfB
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c7_do_animation_call_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c7_PWM_28_HalfB
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c7_st;
  const mxArray *c7_y = NULL;
  const mxArray *c7_b_y = NULL;
  uint16_T c7_u;
  const mxArray *c7_c_y = NULL;
  const mxArray *c7_d_y = NULL;
  uint8_T c7_b_u;
  const mxArray *c7_e_y = NULL;
  const mxArray *c7_f_y = NULL;
  c7_st = NULL;
  c7_st = NULL;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_createcellmatrix(3, 1), false);
  c7_b_y = NULL;
  c7_u = *chartInstance->c7_cont;
  c7_c_y = NULL;
  sf_mex_assign(&c7_c_y, sf_mex_create("y", &c7_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_b_y, sf_mex_create_fi(sf_mex_dup(c7_eml_mx), sf_mex_dup
    (c7_b_eml_mx), "simulinkarray", c7_c_y, false, false), false);
  sf_mex_setcell(c7_y, 0, c7_b_y);
  c7_d_y = NULL;
  c7_b_u = *chartInstance->c7_out_init;
  c7_e_y = NULL;
  sf_mex_assign(&c7_e_y, sf_mex_create("y", &c7_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_d_y, sf_mex_create_fi(sf_mex_dup(c7_eml_mx), sf_mex_dup
    (c7_c_eml_mx), "simulinkarray", c7_e_y, false, false), false);
  sf_mex_setcell(c7_y, 1, c7_d_y);
  c7_f_y = NULL;
  sf_mex_assign(&c7_f_y, sf_mex_create("y",
    &chartInstance->c7_is_active_c7_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 2, c7_f_y);
  sf_mex_assign(&c7_st, c7_y, false);
  return c7_st;
}

static void set_sim_state_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c7_st)
{
  emlrtStack c7_b_st = { NULL,         /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c7_u;
  c7_b_st.tls = chartInstance->c7_fEmlrtCtx;
  chartInstance->c7_doneDoubleBufferReInit = true;
  c7_u = sf_mex_dup(c7_st);
  *chartInstance->c7_cont = c7_emlrt_marshallIn(chartInstance, &c7_b_st,
    sf_mex_dup(sf_mex_getcell(c7_u, 0)), "cont");
  *chartInstance->c7_out_init = c7_c_emlrt_marshallIn(chartInstance, &c7_b_st,
    sf_mex_dup(sf_mex_getcell(c7_u, 1)), "out_init");
  chartInstance->c7_is_active_c7_PWM_28_HalfB = c7_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 2)),
     "is_active_c7_PWM_28_HalfB");
  sf_mex_destroy(&c7_u);
  sf_mex_destroy(&c7_st);
}

static void sf_gateway_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c7_PICOffset;
  uint8_T c7_b_enable;
  int16_T c7_b_offset;
  int16_T c7_b_max;
  int16_T c7_b_sum;
  uint8_T c7_b_init;
  uint8_T c7_a0;
  uint8_T c7_a;
  uint8_T c7_b_a0;
  uint8_T c7_a1;
  uint8_T c7_b_a1;
  boolean_T c7_c;
  int8_T c7_i;
  int8_T c7_i1;
  real_T c7_d;
  uint8_T c7_c_a0;
  uint16_T c7_b_cont;
  uint8_T c7_b_a;
  uint8_T c7_b_out_init;
  uint8_T c7_d_a0;
  uint8_T c7_c_a1;
  uint8_T c7_d_a1;
  boolean_T c7_b_c;
  int8_T c7_i2;
  int8_T c7_i3;
  real_T c7_d1;
  int16_T c7_varargin_1;
  int16_T c7_b_varargin_1;
  int16_T c7_c_varargin_1;
  int16_T c7_d_varargin_1;
  int16_T c7_var1;
  int16_T c7_b_var1;
  int16_T c7_i4;
  int16_T c7_i5;
  boolean_T c7_covSaturation;
  boolean_T c7_b_covSaturation;
  uint16_T c7_hfi;
  uint16_T c7_b_hfi;
  uint16_T c7_u;
  uint16_T c7_u1;
  int16_T c7_e_varargin_1;
  int16_T c7_f_varargin_1;
  int16_T c7_g_varargin_1;
  int16_T c7_h_varargin_1;
  int16_T c7_c_var1;
  int16_T c7_d_var1;
  int16_T c7_i6;
  int16_T c7_i7;
  boolean_T c7_c_covSaturation;
  boolean_T c7_d_covSaturation;
  uint16_T c7_c_hfi;
  uint16_T c7_d_hfi;
  uint16_T c7_u2;
  uint16_T c7_u3;
  uint16_T c7_e_a0;
  uint16_T c7_f_a0;
  uint16_T c7_b0;
  uint16_T c7_b_b0;
  uint16_T c7_c_a;
  uint16_T c7_d_a;
  uint16_T c7_b;
  uint16_T c7_b_b;
  uint16_T c7_g_a0;
  uint16_T c7_h_a0;
  uint16_T c7_c_b0;
  uint16_T c7_d_b0;
  uint16_T c7_e_a1;
  uint16_T c7_f_a1;
  uint16_T c7_b1;
  uint16_T c7_b_b1;
  uint16_T c7_g_a1;
  uint16_T c7_h_a1;
  uint16_T c7_c_b1;
  uint16_T c7_d_b1;
  boolean_T c7_c_c;
  boolean_T c7_d_c;
  int16_T c7_i8;
  int16_T c7_i9;
  int16_T c7_i10;
  int16_T c7_i11;
  int16_T c7_i12;
  int16_T c7_i13;
  int16_T c7_i14;
  int16_T c7_i15;
  int16_T c7_i16;
  int16_T c7_i17;
  int16_T c7_i18;
  int16_T c7_i19;
  int32_T c7_i20;
  int32_T c7_i21;
  int16_T c7_i22;
  int16_T c7_i23;
  int16_T c7_i24;
  int16_T c7_i25;
  int16_T c7_i26;
  int16_T c7_i27;
  int16_T c7_i28;
  int16_T c7_i29;
  int16_T c7_i30;
  int16_T c7_i31;
  int16_T c7_i32;
  int16_T c7_i33;
  int32_T c7_i34;
  int32_T c7_i35;
  int16_T c7_i36;
  int16_T c7_i37;
  int16_T c7_i38;
  int16_T c7_i39;
  int16_T c7_i40;
  int16_T c7_i41;
  int16_T c7_i42;
  int16_T c7_i43;
  real_T c7_d2;
  real_T c7_d3;
  int16_T c7_i_varargin_1;
  int16_T c7_i_a0;
  int16_T c7_j_varargin_1;
  int16_T c7_e_b0;
  int16_T c7_e_var1;
  int16_T c7_k_varargin_1;
  int16_T c7_i44;
  int16_T c7_v;
  boolean_T c7_e_covSaturation;
  int16_T c7_val;
  int16_T c7_c_b;
  int32_T c7_i45;
  uint16_T c7_e_hfi;
  real_T c7_d4;
  int32_T c7_i46;
  real_T c7_d5;
  real_T c7_d6;
  real_T c7_d7;
  int32_T c7_i47;
  int32_T c7_i48;
  int32_T c7_i49;
  real_T c7_d8;
  real_T c7_d9;
  int32_T c7_e_c;
  int32_T c7_l_varargin_1;
  int32_T c7_m_varargin_1;
  int32_T c7_f_var1;
  int32_T c7_i50;
  boolean_T c7_f_covSaturation;
  uint16_T c7_f_hfi;
  observerLogReadPIC(&c7_PICOffset);
  chartInstance->c7_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c7_covrtInstance, 4U, (real_T)
                    *chartInstance->c7_init);
  covrtSigUpdateFcn(chartInstance->c7_covrtInstance, 3U, (real_T)
                    *chartInstance->c7_sum);
  covrtSigUpdateFcn(chartInstance->c7_covrtInstance, 2U, (real_T)
                    *chartInstance->c7_max);
  covrtSigUpdateFcn(chartInstance->c7_covrtInstance, 1U, (real_T)
                    *chartInstance->c7_offset);
  covrtSigUpdateFcn(chartInstance->c7_covrtInstance, 0U, (real_T)
                    *chartInstance->c7_enable);
  chartInstance->c7_sfEvent = CALL_EVENT;
  c7_b_enable = *chartInstance->c7_enable;
  c7_b_offset = *chartInstance->c7_offset;
  c7_b_max = *chartInstance->c7_max;
  c7_b_sum = *chartInstance->c7_sum;
  c7_b_init = *chartInstance->c7_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c7_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c7_emlrt_update_log_1(chartInstance, c7_b_enable,
                        chartInstance->c7_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c7_emlrt_update_log_2(chartInstance, c7_b_offset,
                        chartInstance->c7_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c7_emlrt_update_log_3(chartInstance, c7_b_max,
                        chartInstance->c7_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c7_emlrt_update_log_3(chartInstance, c7_b_sum,
                        chartInstance->c7_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c7_emlrt_update_log_1(chartInstance, c7_b_init,
                        chartInstance->c7_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c7_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c7_covrtInstance, 4U, 0, 0, false);
  c7_a0 = c7_b_enable;
  c7_a = c7_a0;
  c7_b_a0 = c7_a;
  c7_a1 = c7_b_a0;
  c7_b_a1 = c7_a1;
  c7_c = (c7_b_a1 == 0);
  c7_i = (int8_T)c7_b_enable;
  if ((int8_T)(c7_i & 2) != 0) {
    c7_i1 = (int8_T)(c7_i | -2);
  } else {
    c7_i1 = (int8_T)(c7_i & 1);
  }

  if (c7_i1 > 0) {
    c7_d = 3.0;
  } else {
    c7_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c7_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c7_covrtInstance,
        4U, 0U, 0U, c7_d, 0.0, -2, 0U, (int32_T)c7_emlrt_update_log_4
        (chartInstance, c7_c, chartInstance->c7_emlrtLocationLoggingDataTables,
         5)))) {
    c7_b_cont = c7_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c7_emlrtLocationLoggingDataTables, 6);
    c7_b_out_init = c7_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c7_emlrtLocationLoggingDataTables, 7);
  } else {
    c7_c_a0 = c7_b_init;
    c7_b_a = c7_c_a0;
    c7_d_a0 = c7_b_a;
    c7_c_a1 = c7_d_a0;
    c7_d_a1 = c7_c_a1;
    c7_b_c = (c7_d_a1 == 0);
    c7_i2 = (int8_T)c7_b_init;
    if ((int8_T)(c7_i2 & 2) != 0) {
      c7_i3 = (int8_T)(c7_i2 | -2);
    } else {
      c7_i3 = (int8_T)(c7_i2 & 1);
    }

    if (c7_i3 > 0) {
      c7_d1 = 3.0;
    } else {
      c7_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c7_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c7_covrtInstance, 4U, 0U, 1U, c7_d1, 0.0,
                        -2, 0U, (int32_T)c7_emlrt_update_log_4(chartInstance,
           c7_b_c, chartInstance->c7_emlrtLocationLoggingDataTables, 8)))) {
      c7_b_varargin_1 = c7_b_sum;
      c7_d_varargin_1 = c7_b_varargin_1;
      c7_b_var1 = c7_d_varargin_1;
      c7_i5 = c7_b_var1;
      c7_b_covSaturation = false;
      if (c7_i5 < 0) {
        c7_i5 = 0;
      } else {
        if (c7_i5 > 4095) {
          c7_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c7_covrtInstance, 4, 0, 0, 0,
          c7_b_covSaturation);
      }

      c7_b_hfi = (uint16_T)c7_i5;
      c7_u1 = c7_emlrt_update_log_5(chartInstance, c7_b_hfi,
        chartInstance->c7_emlrtLocationLoggingDataTables, 10);
      c7_f_varargin_1 = c7_b_max;
      c7_h_varargin_1 = c7_f_varargin_1;
      c7_d_var1 = c7_h_varargin_1;
      c7_i7 = c7_d_var1;
      c7_d_covSaturation = false;
      if (c7_i7 < 0) {
        c7_i7 = 0;
      } else {
        if (c7_i7 > 4095) {
          c7_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c7_covrtInstance, 4, 0, 1, 0,
          c7_d_covSaturation);
      }

      c7_d_hfi = (uint16_T)c7_i7;
      c7_u3 = c7_emlrt_update_log_5(chartInstance, c7_d_hfi,
        chartInstance->c7_emlrtLocationLoggingDataTables, 11);
      c7_f_a0 = c7_u1;
      c7_b_b0 = c7_u3;
      c7_d_a = c7_f_a0;
      c7_b_b = c7_b_b0;
      c7_h_a0 = c7_d_a;
      c7_d_b0 = c7_b_b;
      c7_f_a1 = c7_h_a0;
      c7_b_b1 = c7_d_b0;
      c7_h_a1 = c7_f_a1;
      c7_d_b1 = c7_b_b1;
      c7_d_c = (c7_h_a1 < c7_d_b1);
      c7_i9 = (int16_T)c7_u1;
      c7_i11 = (int16_T)c7_u3;
      c7_i13 = (int16_T)c7_u3;
      c7_i15 = (int16_T)c7_u1;
      if ((int16_T)(c7_i13 & 4096) != 0) {
        c7_i17 = (int16_T)(c7_i13 | -4096);
      } else {
        c7_i17 = (int16_T)(c7_i13 & 4095);
      }

      if ((int16_T)(c7_i15 & 4096) != 0) {
        c7_i19 = (int16_T)(c7_i15 | -4096);
      } else {
        c7_i19 = (int16_T)(c7_i15 & 4095);
      }

      c7_i21 = c7_i17 - c7_i19;
      if (c7_i21 > 4095) {
        c7_i21 = 4095;
      } else {
        if (c7_i21 < -4096) {
          c7_i21 = -4096;
        }
      }

      c7_i23 = (int16_T)c7_u1;
      c7_i25 = (int16_T)c7_u3;
      c7_i27 = (int16_T)c7_u1;
      c7_i29 = (int16_T)c7_u3;
      if ((int16_T)(c7_i27 & 4096) != 0) {
        c7_i31 = (int16_T)(c7_i27 | -4096);
      } else {
        c7_i31 = (int16_T)(c7_i27 & 4095);
      }

      if ((int16_T)(c7_i29 & 4096) != 0) {
        c7_i33 = (int16_T)(c7_i29 | -4096);
      } else {
        c7_i33 = (int16_T)(c7_i29 & 4095);
      }

      c7_i35 = c7_i31 - c7_i33;
      if (c7_i35 > 4095) {
        c7_i35 = 4095;
      } else {
        if (c7_i35 < -4096) {
          c7_i35 = -4096;
        }
      }

      if ((int16_T)(c7_i9 & 4096) != 0) {
        c7_i37 = (int16_T)(c7_i9 | -4096);
      } else {
        c7_i37 = (int16_T)(c7_i9 & 4095);
      }

      if ((int16_T)(c7_i11 & 4096) != 0) {
        c7_i39 = (int16_T)(c7_i11 | -4096);
      } else {
        c7_i39 = (int16_T)(c7_i11 & 4095);
      }

      if ((int16_T)(c7_i23 & 4096) != 0) {
        c7_i41 = (int16_T)(c7_i23 | -4096);
      } else {
        c7_i41 = (int16_T)(c7_i23 & 4095);
      }

      if ((int16_T)(c7_i25 & 4096) != 0) {
        c7_i43 = (int16_T)(c7_i25 | -4096);
      } else {
        c7_i43 = (int16_T)(c7_i25 & 4095);
      }

      if (c7_i37 < c7_i39) {
        c7_d3 = (real_T)((int16_T)c7_i21 <= 1);
      } else if (c7_i41 > c7_i43) {
        if ((int16_T)c7_i35 <= 1) {
          c7_d3 = 3.0;
        } else {
          c7_d3 = 0.0;
        }
      } else {
        c7_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c7_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c7_covrtInstance, 4U, 0U, 2U, c7_d3,
                          0.0, -2, 2U, (int32_T)c7_emlrt_update_log_4
                          (chartInstance, c7_d_c,
                           chartInstance->c7_emlrtLocationLoggingDataTables, 9))))
      {
        c7_i_a0 = c7_b_sum;
        c7_e_b0 = c7_b_offset;
        c7_k_varargin_1 = c7_e_b0;
        c7_v = c7_k_varargin_1;
        c7_val = c7_v;
        c7_c_b = c7_val;
        c7_i45 = c7_i_a0;
        if (c7_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c7_d4 = 1.0;
          observerLog(117 + c7_PICOffset, &c7_d4, 1);
        }

        if (c7_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c7_d5 = 1.0;
          observerLog(117 + c7_PICOffset, &c7_d5, 1);
        }

        c7_i46 = c7_c_b;
        if (c7_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c7_d6 = 1.0;
          observerLog(120 + c7_PICOffset, &c7_d6, 1);
        }

        if (c7_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c7_d7 = 1.0;
          observerLog(120 + c7_PICOffset, &c7_d7, 1);
        }

        if ((c7_i45 & 65536) != 0) {
          c7_i47 = c7_i45 | -65536;
        } else {
          c7_i47 = c7_i45 & 65535;
        }

        if ((c7_i46 & 65536) != 0) {
          c7_i48 = c7_i46 | -65536;
        } else {
          c7_i48 = c7_i46 & 65535;
        }

        c7_i49 = c7__s32_add__(chartInstance, c7_i47, c7_i48, 119, 1U, 323, 12);
        if (c7_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c7_d8 = 1.0;
          observerLog(125 + c7_PICOffset, &c7_d8, 1);
        }

        if (c7_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c7_d9 = 1.0;
          observerLog(125 + c7_PICOffset, &c7_d9, 1);
        }

        if ((c7_i49 & 65536) != 0) {
          c7_e_c = c7_i49 | -65536;
        } else {
          c7_e_c = c7_i49 & 65535;
        }

        c7_l_varargin_1 = c7_emlrt_update_log_6(chartInstance, c7_e_c,
          chartInstance->c7_emlrtLocationLoggingDataTables, 13);
        c7_m_varargin_1 = c7_l_varargin_1;
        c7_f_var1 = c7_m_varargin_1;
        c7_i50 = c7_f_var1;
        c7_f_covSaturation = false;
        if (c7_i50 < 0) {
          c7_i50 = 0;
        } else {
          if (c7_i50 > 4095) {
            c7_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c7_covrtInstance, 4, 0, 2, 0,
            c7_f_covSaturation);
        }

        c7_f_hfi = (uint16_T)c7_i50;
        c7_b_cont = c7_emlrt_update_log_5(chartInstance, c7_f_hfi,
          chartInstance->c7_emlrtLocationLoggingDataTables, 12);
        c7_b_out_init = c7_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c7_emlrtLocationLoggingDataTables, 14);
      } else {
        c7_b_cont = c7_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c7_emlrtLocationLoggingDataTables, 15);
        c7_b_out_init = c7_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c7_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c7_varargin_1 = c7_b_sum;
      c7_c_varargin_1 = c7_varargin_1;
      c7_var1 = c7_c_varargin_1;
      c7_i4 = c7_var1;
      c7_covSaturation = false;
      if (c7_i4 < 0) {
        c7_i4 = 0;
      } else {
        if (c7_i4 > 4095) {
          c7_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c7_covrtInstance, 4, 0, 3, 0,
          c7_covSaturation);
      }

      c7_hfi = (uint16_T)c7_i4;
      c7_u = c7_emlrt_update_log_5(chartInstance, c7_hfi,
        chartInstance->c7_emlrtLocationLoggingDataTables, 18);
      c7_e_varargin_1 = c7_b_max;
      c7_g_varargin_1 = c7_e_varargin_1;
      c7_c_var1 = c7_g_varargin_1;
      c7_i6 = c7_c_var1;
      c7_c_covSaturation = false;
      if (c7_i6 < 0) {
        c7_i6 = 0;
      } else {
        if (c7_i6 > 4095) {
          c7_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c7_covrtInstance, 4, 0, 4, 0,
          c7_c_covSaturation);
      }

      c7_c_hfi = (uint16_T)c7_i6;
      c7_u2 = c7_emlrt_update_log_5(chartInstance, c7_c_hfi,
        chartInstance->c7_emlrtLocationLoggingDataTables, 19);
      c7_e_a0 = c7_u;
      c7_b0 = c7_u2;
      c7_c_a = c7_e_a0;
      c7_b = c7_b0;
      c7_g_a0 = c7_c_a;
      c7_c_b0 = c7_b;
      c7_e_a1 = c7_g_a0;
      c7_b1 = c7_c_b0;
      c7_g_a1 = c7_e_a1;
      c7_c_b1 = c7_b1;
      c7_c_c = (c7_g_a1 < c7_c_b1);
      c7_i8 = (int16_T)c7_u;
      c7_i10 = (int16_T)c7_u2;
      c7_i12 = (int16_T)c7_u2;
      c7_i14 = (int16_T)c7_u;
      if ((int16_T)(c7_i12 & 4096) != 0) {
        c7_i16 = (int16_T)(c7_i12 | -4096);
      } else {
        c7_i16 = (int16_T)(c7_i12 & 4095);
      }

      if ((int16_T)(c7_i14 & 4096) != 0) {
        c7_i18 = (int16_T)(c7_i14 | -4096);
      } else {
        c7_i18 = (int16_T)(c7_i14 & 4095);
      }

      c7_i20 = c7_i16 - c7_i18;
      if (c7_i20 > 4095) {
        c7_i20 = 4095;
      } else {
        if (c7_i20 < -4096) {
          c7_i20 = -4096;
        }
      }

      c7_i22 = (int16_T)c7_u;
      c7_i24 = (int16_T)c7_u2;
      c7_i26 = (int16_T)c7_u;
      c7_i28 = (int16_T)c7_u2;
      if ((int16_T)(c7_i26 & 4096) != 0) {
        c7_i30 = (int16_T)(c7_i26 | -4096);
      } else {
        c7_i30 = (int16_T)(c7_i26 & 4095);
      }

      if ((int16_T)(c7_i28 & 4096) != 0) {
        c7_i32 = (int16_T)(c7_i28 | -4096);
      } else {
        c7_i32 = (int16_T)(c7_i28 & 4095);
      }

      c7_i34 = c7_i30 - c7_i32;
      if (c7_i34 > 4095) {
        c7_i34 = 4095;
      } else {
        if (c7_i34 < -4096) {
          c7_i34 = -4096;
        }
      }

      if ((int16_T)(c7_i8 & 4096) != 0) {
        c7_i36 = (int16_T)(c7_i8 | -4096);
      } else {
        c7_i36 = (int16_T)(c7_i8 & 4095);
      }

      if ((int16_T)(c7_i10 & 4096) != 0) {
        c7_i38 = (int16_T)(c7_i10 | -4096);
      } else {
        c7_i38 = (int16_T)(c7_i10 & 4095);
      }

      if ((int16_T)(c7_i22 & 4096) != 0) {
        c7_i40 = (int16_T)(c7_i22 | -4096);
      } else {
        c7_i40 = (int16_T)(c7_i22 & 4095);
      }

      if ((int16_T)(c7_i24 & 4096) != 0) {
        c7_i42 = (int16_T)(c7_i24 | -4096);
      } else {
        c7_i42 = (int16_T)(c7_i24 & 4095);
      }

      if (c7_i36 < c7_i38) {
        c7_d2 = (real_T)((int16_T)c7_i20 <= 1);
      } else if (c7_i40 > c7_i42) {
        if ((int16_T)c7_i34 <= 1) {
          c7_d2 = 3.0;
        } else {
          c7_d2 = 0.0;
        }
      } else {
        c7_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c7_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c7_covrtInstance, 4U, 0U, 3U, c7_d2,
                          0.0, -2, 2U, (int32_T)c7_emlrt_update_log_4
                          (chartInstance, c7_c_c,
                           chartInstance->c7_emlrtLocationLoggingDataTables, 17))))
      {
        c7_i_varargin_1 = c7_b_sum;
        c7_j_varargin_1 = c7_i_varargin_1;
        c7_e_var1 = c7_j_varargin_1;
        c7_i44 = c7_e_var1;
        c7_e_covSaturation = false;
        if (c7_i44 < 0) {
          c7_i44 = 0;
        } else {
          if (c7_i44 > 4095) {
            c7_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c7_covrtInstance, 4, 0, 5, 0,
            c7_e_covSaturation);
        }

        c7_e_hfi = (uint16_T)c7_i44;
        c7_b_cont = c7_emlrt_update_log_5(chartInstance, c7_e_hfi,
          chartInstance->c7_emlrtLocationLoggingDataTables, 20);
        c7_b_out_init = c7_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c7_emlrtLocationLoggingDataTables, 21);
      } else {
        c7_b_cont = c7_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c7_emlrtLocationLoggingDataTables, 22);
        c7_b_out_init = c7_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c7_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c7_cont = c7_b_cont;
  *chartInstance->c7_out_init = c7_b_out_init;
  c7_do_animation_call_c7_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c7_covrtInstance, 5U, (real_T)
                    *chartInstance->c7_cont);
  covrtSigUpdateFcn(chartInstance->c7_covrtInstance, 6U, (real_T)
                    *chartInstance->c7_out_init);
}

static void mdl_start_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c7_PWM_28_HalfB
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c7_decisionTxtStartIdx = 0U;
  static const uint32_T c7_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c7_chart_data_browse_helper);
  chartInstance->c7_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c7_RuntimeVar,
    &chartInstance->c7_IsDebuggerActive,
    &chartInstance->c7_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c7_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c7_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c7_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c7_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c7_decisionTxtStartIdx, &c7_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c7_covrtInstance, 0U, 0, NULL, NULL, 0U, NULL);
  covrtEmlInitFcn(chartInstance->c7_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 0U, 266, -1,
    279);
  covrtEmlSaturationInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 1U, 282, -1,
    295);
  covrtEmlSaturationInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 2U, 319, -1,
    341);
  covrtEmlSaturationInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 3U, 518, -1,
    531);
  covrtEmlSaturationInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 4U, 534, -1,
    547);
  covrtEmlSaturationInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 5U, 571, -1,
    584);
  covrtEmlIfInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 0U, 70, 86, -1, 121);
  covrtEmlIfInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c7_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c7_PWM_28_HalfB
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c7_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7375",              /* mexFileName */
    "Thu May 27 10:27:18 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c7_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c7_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c7_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c7_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7375");
    emlrtLocationLoggingPushLog(&c7_emlrtLocationLoggingFileInfo,
      c7_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c7_emlrtLocationLoggingDataTables, c7_emlrtLocationInfo,
      NULL, 0U, c7_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7375");
  }

  sfListenerLightTerminate(chartInstance->c7_RuntimeVar);
  sf_mex_destroy(&c7_eml_mx);
  sf_mex_destroy(&c7_b_eml_mx);
  sf_mex_destroy(&c7_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c7_covrtInstance);
}

static void initSimStructsc7_PWM_28_HalfB(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c7_emlrt_update_log_1(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index)
{
  boolean_T c7_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c7_b_table;
  real_T c7_d;
  uint8_T c7_u;
  uint8_T c7_localMin;
  real_T c7_d1;
  uint8_T c7_u1;
  uint8_T c7_localMax;
  emlrtLocationLoggingHistogramType *c7_histTable;
  real_T c7_inDouble;
  real_T c7_significand;
  int32_T c7_exponent;
  (void)chartInstance;
  c7_isLoggingEnabledHere = (c7_index >= 0);
  if (c7_isLoggingEnabledHere) {
    c7_b_table = (emlrtLocationLoggingDataType *)&c7_table[c7_index];
    c7_d = c7_b_table[0U].SimMin;
    if (c7_d < 2.0) {
      if (c7_d >= 0.0) {
        c7_u = (uint8_T)c7_d;
      } else {
        c7_u = 0U;
      }
    } else if (c7_d >= 2.0) {
      c7_u = 1U;
    } else {
      c7_u = 0U;
    }

    c7_localMin = c7_u;
    c7_d1 = c7_b_table[0U].SimMax;
    if (c7_d1 < 2.0) {
      if (c7_d1 >= 0.0) {
        c7_u1 = (uint8_T)c7_d1;
      } else {
        c7_u1 = 0U;
      }
    } else if (c7_d1 >= 2.0) {
      c7_u1 = 1U;
    } else {
      c7_u1 = 0U;
    }

    c7_localMax = c7_u1;
    c7_histTable = c7_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c7_in < c7_localMin) {
      c7_localMin = c7_in;
    }

    if (c7_in > c7_localMax) {
      c7_localMax = c7_in;
    }

    /* Histogram logging. */
    c7_inDouble = (real_T)c7_in;
    c7_histTable->TotalNumberOfValues++;
    if (c7_inDouble == 0.0) {
      c7_histTable->NumberOfZeros++;
    } else {
      c7_histTable->SimSum += c7_inDouble;
      if ((!muDoubleScalarIsInf(c7_inDouble)) && (!muDoubleScalarIsNaN
           (c7_inDouble))) {
        c7_significand = frexp(c7_inDouble, &c7_exponent);
        if (c7_exponent > 128) {
          c7_exponent = 128;
        }

        if (c7_exponent < -127) {
          c7_exponent = -127;
        }

        if (c7_significand < 0.0) {
          c7_histTable->NumberOfNegativeValues++;
          c7_histTable->HistogramOfNegativeValues[127 + c7_exponent]++;
        } else {
          c7_histTable->NumberOfPositiveValues++;
          c7_histTable->HistogramOfPositiveValues[127 + c7_exponent]++;
        }
      }
    }

    c7_b_table[0U].SimMin = (real_T)c7_localMin;
    c7_b_table[0U].SimMax = (real_T)c7_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c7_in;
}

static int16_T c7_emlrt_update_log_2(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index)
{
  boolean_T c7_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c7_b_table;
  real_T c7_d;
  int16_T c7_i;
  int16_T c7_localMin;
  real_T c7_d1;
  int16_T c7_i1;
  int16_T c7_localMax;
  emlrtLocationLoggingHistogramType *c7_histTable;
  real_T c7_inDouble;
  real_T c7_significand;
  int32_T c7_exponent;
  (void)chartInstance;
  c7_isLoggingEnabledHere = (c7_index >= 0);
  if (c7_isLoggingEnabledHere) {
    c7_b_table = (emlrtLocationLoggingDataType *)&c7_table[c7_index];
    c7_d = muDoubleScalarFloor(c7_b_table[0U].SimMin);
    if (c7_d < 32768.0) {
      if (c7_d >= -32768.0) {
        c7_i = (int16_T)c7_d;
      } else {
        c7_i = MIN_int16_T;
      }
    } else if (c7_d >= 32768.0) {
      c7_i = MAX_int16_T;
    } else {
      c7_i = 0;
    }

    c7_localMin = c7_i;
    c7_d1 = muDoubleScalarFloor(c7_b_table[0U].SimMax);
    if (c7_d1 < 32768.0) {
      if (c7_d1 >= -32768.0) {
        c7_i1 = (int16_T)c7_d1;
      } else {
        c7_i1 = MIN_int16_T;
      }
    } else if (c7_d1 >= 32768.0) {
      c7_i1 = MAX_int16_T;
    } else {
      c7_i1 = 0;
    }

    c7_localMax = c7_i1;
    c7_histTable = c7_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c7_in < c7_localMin) {
      c7_localMin = c7_in;
    }

    if (c7_in > c7_localMax) {
      c7_localMax = c7_in;
    }

    /* Histogram logging. */
    c7_inDouble = (real_T)c7_in;
    c7_histTable->TotalNumberOfValues++;
    if (c7_inDouble == 0.0) {
      c7_histTable->NumberOfZeros++;
    } else {
      c7_histTable->SimSum += c7_inDouble;
      if ((!muDoubleScalarIsInf(c7_inDouble)) && (!muDoubleScalarIsNaN
           (c7_inDouble))) {
        c7_significand = frexp(c7_inDouble, &c7_exponent);
        if (c7_exponent > 128) {
          c7_exponent = 128;
        }

        if (c7_exponent < -127) {
          c7_exponent = -127;
        }

        if (c7_significand < 0.0) {
          c7_histTable->NumberOfNegativeValues++;
          c7_histTable->HistogramOfNegativeValues[127 + c7_exponent]++;
        } else {
          c7_histTable->NumberOfPositiveValues++;
          c7_histTable->HistogramOfPositiveValues[127 + c7_exponent]++;
        }
      }
    }

    c7_b_table[0U].SimMin = (real_T)c7_localMin;
    c7_b_table[0U].SimMax = (real_T)c7_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c7_in;
}

static int16_T c7_emlrt_update_log_3(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index)
{
  boolean_T c7_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c7_b_table;
  real_T c7_d;
  int16_T c7_i;
  int16_T c7_localMin;
  real_T c7_d1;
  int16_T c7_i1;
  int16_T c7_localMax;
  emlrtLocationLoggingHistogramType *c7_histTable;
  real_T c7_inDouble;
  real_T c7_significand;
  int32_T c7_exponent;
  (void)chartInstance;
  c7_isLoggingEnabledHere = (c7_index >= 0);
  if (c7_isLoggingEnabledHere) {
    c7_b_table = (emlrtLocationLoggingDataType *)&c7_table[c7_index];
    c7_d = muDoubleScalarFloor(c7_b_table[0U].SimMin);
    if (c7_d < 2048.0) {
      if (c7_d >= -2048.0) {
        c7_i = (int16_T)c7_d;
      } else {
        c7_i = -2048;
      }
    } else if (c7_d >= 2048.0) {
      c7_i = 2047;
    } else {
      c7_i = 0;
    }

    c7_localMin = c7_i;
    c7_d1 = muDoubleScalarFloor(c7_b_table[0U].SimMax);
    if (c7_d1 < 2048.0) {
      if (c7_d1 >= -2048.0) {
        c7_i1 = (int16_T)c7_d1;
      } else {
        c7_i1 = -2048;
      }
    } else if (c7_d1 >= 2048.0) {
      c7_i1 = 2047;
    } else {
      c7_i1 = 0;
    }

    c7_localMax = c7_i1;
    c7_histTable = c7_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c7_in < c7_localMin) {
      c7_localMin = c7_in;
    }

    if (c7_in > c7_localMax) {
      c7_localMax = c7_in;
    }

    /* Histogram logging. */
    c7_inDouble = (real_T)c7_in;
    c7_histTable->TotalNumberOfValues++;
    if (c7_inDouble == 0.0) {
      c7_histTable->NumberOfZeros++;
    } else {
      c7_histTable->SimSum += c7_inDouble;
      if ((!muDoubleScalarIsInf(c7_inDouble)) && (!muDoubleScalarIsNaN
           (c7_inDouble))) {
        c7_significand = frexp(c7_inDouble, &c7_exponent);
        if (c7_exponent > 128) {
          c7_exponent = 128;
        }

        if (c7_exponent < -127) {
          c7_exponent = -127;
        }

        if (c7_significand < 0.0) {
          c7_histTable->NumberOfNegativeValues++;
          c7_histTable->HistogramOfNegativeValues[127 + c7_exponent]++;
        } else {
          c7_histTable->NumberOfPositiveValues++;
          c7_histTable->HistogramOfPositiveValues[127 + c7_exponent]++;
        }
      }
    }

    c7_b_table[0U].SimMin = (real_T)c7_localMin;
    c7_b_table[0U].SimMax = (real_T)c7_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c7_in;
}

static boolean_T c7_emlrt_update_log_4(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index)
{
  boolean_T c7_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c7_b_table;
  boolean_T c7_localMin;
  boolean_T c7_localMax;
  emlrtLocationLoggingHistogramType *c7_histTable;
  real_T c7_inDouble;
  real_T c7_significand;
  int32_T c7_exponent;
  (void)chartInstance;
  c7_isLoggingEnabledHere = (c7_index >= 0);
  if (c7_isLoggingEnabledHere) {
    c7_b_table = (emlrtLocationLoggingDataType *)&c7_table[c7_index];
    c7_localMin = (c7_b_table[0U].SimMin > 0.0);
    c7_localMax = (c7_b_table[0U].SimMax > 0.0);
    c7_histTable = c7_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c7_in < (int32_T)c7_localMin) {
      c7_localMin = c7_in;
    }

    if ((int32_T)c7_in > (int32_T)c7_localMax) {
      c7_localMax = c7_in;
    }

    /* Histogram logging. */
    c7_inDouble = (real_T)c7_in;
    c7_histTable->TotalNumberOfValues++;
    if (c7_inDouble == 0.0) {
      c7_histTable->NumberOfZeros++;
    } else {
      c7_histTable->SimSum += c7_inDouble;
      if ((!muDoubleScalarIsInf(c7_inDouble)) && (!muDoubleScalarIsNaN
           (c7_inDouble))) {
        c7_significand = frexp(c7_inDouble, &c7_exponent);
        if (c7_exponent > 128) {
          c7_exponent = 128;
        }

        if (c7_exponent < -127) {
          c7_exponent = -127;
        }

        if (c7_significand < 0.0) {
          c7_histTable->NumberOfNegativeValues++;
          c7_histTable->HistogramOfNegativeValues[127 + c7_exponent]++;
        } else {
          c7_histTable->NumberOfPositiveValues++;
          c7_histTable->HistogramOfPositiveValues[127 + c7_exponent]++;
        }
      }
    }

    c7_b_table[0U].SimMin = (real_T)c7_localMin;
    c7_b_table[0U].SimMax = (real_T)c7_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c7_in;
}

static uint16_T c7_emlrt_update_log_5(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index)
{
  boolean_T c7_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c7_b_table;
  real_T c7_d;
  uint16_T c7_u;
  uint16_T c7_localMin;
  real_T c7_d1;
  uint16_T c7_u1;
  uint16_T c7_localMax;
  emlrtLocationLoggingHistogramType *c7_histTable;
  real_T c7_inDouble;
  real_T c7_significand;
  int32_T c7_exponent;
  (void)chartInstance;
  c7_isLoggingEnabledHere = (c7_index >= 0);
  if (c7_isLoggingEnabledHere) {
    c7_b_table = (emlrtLocationLoggingDataType *)&c7_table[c7_index];
    c7_d = c7_b_table[0U].SimMin;
    if (c7_d < 4096.0) {
      if (c7_d >= 0.0) {
        c7_u = (uint16_T)c7_d;
      } else {
        c7_u = 0U;
      }
    } else if (c7_d >= 4096.0) {
      c7_u = 4095U;
    } else {
      c7_u = 0U;
    }

    c7_localMin = c7_u;
    c7_d1 = c7_b_table[0U].SimMax;
    if (c7_d1 < 4096.0) {
      if (c7_d1 >= 0.0) {
        c7_u1 = (uint16_T)c7_d1;
      } else {
        c7_u1 = 0U;
      }
    } else if (c7_d1 >= 4096.0) {
      c7_u1 = 4095U;
    } else {
      c7_u1 = 0U;
    }

    c7_localMax = c7_u1;
    c7_histTable = c7_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c7_in < c7_localMin) {
      c7_localMin = c7_in;
    }

    if (c7_in > c7_localMax) {
      c7_localMax = c7_in;
    }

    /* Histogram logging. */
    c7_inDouble = (real_T)c7_in;
    c7_histTable->TotalNumberOfValues++;
    if (c7_inDouble == 0.0) {
      c7_histTable->NumberOfZeros++;
    } else {
      c7_histTable->SimSum += c7_inDouble;
      if ((!muDoubleScalarIsInf(c7_inDouble)) && (!muDoubleScalarIsNaN
           (c7_inDouble))) {
        c7_significand = frexp(c7_inDouble, &c7_exponent);
        if (c7_exponent > 128) {
          c7_exponent = 128;
        }

        if (c7_exponent < -127) {
          c7_exponent = -127;
        }

        if (c7_significand < 0.0) {
          c7_histTable->NumberOfNegativeValues++;
          c7_histTable->HistogramOfNegativeValues[127 + c7_exponent]++;
        } else {
          c7_histTable->NumberOfPositiveValues++;
          c7_histTable->HistogramOfPositiveValues[127 + c7_exponent]++;
        }
      }
    }

    c7_b_table[0U].SimMin = (real_T)c7_localMin;
    c7_b_table[0U].SimMax = (real_T)c7_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c7_in;
}

static int32_T c7_emlrt_update_log_6(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c7_in, emlrtLocationLoggingDataType c7_table[],
  int32_T c7_index)
{
  boolean_T c7_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c7_b_table;
  real_T c7_d;
  int32_T c7_i;
  int32_T c7_localMin;
  real_T c7_d1;
  int32_T c7_i1;
  int32_T c7_localMax;
  emlrtLocationLoggingHistogramType *c7_histTable;
  real_T c7_inDouble;
  real_T c7_significand;
  int32_T c7_exponent;
  (void)chartInstance;
  c7_isLoggingEnabledHere = (c7_index >= 0);
  if (c7_isLoggingEnabledHere) {
    c7_b_table = (emlrtLocationLoggingDataType *)&c7_table[c7_index];
    c7_d = muDoubleScalarFloor(c7_b_table[0U].SimMin);
    if (c7_d < 65536.0) {
      if (c7_d >= -65536.0) {
        c7_i = (int32_T)c7_d;
      } else {
        c7_i = -65536;
      }
    } else if (c7_d >= 65536.0) {
      c7_i = 65535;
    } else {
      c7_i = 0;
    }

    c7_localMin = c7_i;
    c7_d1 = muDoubleScalarFloor(c7_b_table[0U].SimMax);
    if (c7_d1 < 65536.0) {
      if (c7_d1 >= -65536.0) {
        c7_i1 = (int32_T)c7_d1;
      } else {
        c7_i1 = -65536;
      }
    } else if (c7_d1 >= 65536.0) {
      c7_i1 = 65535;
    } else {
      c7_i1 = 0;
    }

    c7_localMax = c7_i1;
    c7_histTable = c7_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c7_in < c7_localMin) {
      c7_localMin = c7_in;
    }

    if (c7_in > c7_localMax) {
      c7_localMax = c7_in;
    }

    /* Histogram logging. */
    c7_inDouble = (real_T)c7_in;
    c7_histTable->TotalNumberOfValues++;
    if (c7_inDouble == 0.0) {
      c7_histTable->NumberOfZeros++;
    } else {
      c7_histTable->SimSum += c7_inDouble;
      if ((!muDoubleScalarIsInf(c7_inDouble)) && (!muDoubleScalarIsNaN
           (c7_inDouble))) {
        c7_significand = frexp(c7_inDouble, &c7_exponent);
        if (c7_exponent > 128) {
          c7_exponent = 128;
        }

        if (c7_exponent < -127) {
          c7_exponent = -127;
        }

        if (c7_significand < 0.0) {
          c7_histTable->NumberOfNegativeValues++;
          c7_histTable->HistogramOfNegativeValues[127 + c7_exponent]++;
        } else {
          c7_histTable->NumberOfPositiveValues++;
          c7_histTable->HistogramOfPositiveValues[127 + c7_exponent]++;
        }
      }
    }

    c7_b_table[0U].SimMin = (real_T)c7_localMin;
    c7_b_table[0U].SimMax = (real_T)c7_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c7_in;
}

static void c7_emlrtInitVarDataTables(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c7_dataTables[24],
  emlrtLocationLoggingHistogramType c7_histTables[24])
{
  int32_T c7_i;
  int32_T c7_iH;
  (void)chartInstance;
  for (c7_i = 0; c7_i < 24; c7_i++) {
    c7_dataTables[c7_i].SimMin = rtInf;
    c7_dataTables[c7_i].SimMax = rtMinusInf;
    c7_dataTables[c7_i].OverflowWraps = 0;
    c7_dataTables[c7_i].Saturations = 0;
    c7_dataTables[c7_i].IsAlwaysInteger = true;
    c7_dataTables[c7_i].HistogramTable = &c7_histTables[c7_i];
    c7_histTables[c7_i].NumberOfZeros = 0.0;
    c7_histTables[c7_i].NumberOfPositiveValues = 0.0;
    c7_histTables[c7_i].NumberOfNegativeValues = 0.0;
    c7_histTables[c7_i].TotalNumberOfValues = 0.0;
    c7_histTables[c7_i].SimSum = 0.0;
    for (c7_iH = 0; c7_iH < 256; c7_iH++) {
      c7_histTables[c7_i].HistogramOfPositiveValues[c7_iH] = 0.0;
      c7_histTables[c7_i].HistogramOfNegativeValues[c7_iH] = 0.0;
    }
  }
}

const mxArray *sf_c7_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c7_nameCaptureInfo = NULL;
  c7_nameCaptureInfo = NULL;
  sf_mex_assign(&c7_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c7_nameCaptureInfo;
}

static uint16_T c7_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c7_sp, const mxArray *c7_b_cont, const
  char_T *c7_identifier)
{
  uint16_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = (const char *)c7_identifier;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_y = c7_b_emlrt_marshallIn(chartInstance, c7_sp, sf_mex_dup(c7_b_cont),
    &c7_thisId);
  sf_mex_destroy(&c7_b_cont);
  return c7_y;
}

static uint16_T c7_b_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c7_sp, const mxArray *c7_u, const
  emlrtMsgIdentifier *c7_parentId)
{
  uint16_T c7_y;
  const mxArray *c7_mxFi = NULL;
  const mxArray *c7_mxInt = NULL;
  uint16_T c7_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c7_parentId, c7_u, false, 0U, NULL, c7_eml_mx, c7_b_eml_mx);
  sf_mex_assign(&c7_mxFi, sf_mex_dup(c7_u), false);
  sf_mex_assign(&c7_mxInt, sf_mex_call(c7_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c7_mxFi)), false);
  sf_mex_import(c7_parentId, sf_mex_dup(c7_mxInt), &c7_b_u, 1, 5, 0U, 0, 0U, 0);
  sf_mex_destroy(&c7_mxFi);
  sf_mex_destroy(&c7_mxInt);
  c7_y = c7_b_u;
  sf_mex_destroy(&c7_mxFi);
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static uint8_T c7_c_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c7_sp, const mxArray *c7_b_out_init, const
  char_T *c7_identifier)
{
  uint8_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = (const char *)c7_identifier;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_y = c7_d_emlrt_marshallIn(chartInstance, c7_sp, sf_mex_dup(c7_b_out_init),
    &c7_thisId);
  sf_mex_destroy(&c7_b_out_init);
  return c7_y;
}

static uint8_T c7_d_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c7_sp, const mxArray *c7_u, const
  emlrtMsgIdentifier *c7_parentId)
{
  uint8_T c7_y;
  const mxArray *c7_mxFi = NULL;
  const mxArray *c7_mxInt = NULL;
  uint8_T c7_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c7_parentId, c7_u, false, 0U, NULL, c7_eml_mx, c7_c_eml_mx);
  sf_mex_assign(&c7_mxFi, sf_mex_dup(c7_u), false);
  sf_mex_assign(&c7_mxInt, sf_mex_call(c7_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c7_mxFi)), false);
  sf_mex_import(c7_parentId, sf_mex_dup(c7_mxInt), &c7_b_u, 1, 3, 0U, 0, 0U, 0);
  sf_mex_destroy(&c7_mxFi);
  sf_mex_destroy(&c7_mxInt);
  c7_y = c7_b_u;
  sf_mex_destroy(&c7_mxFi);
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static uint8_T c7_e_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c7_b_is_active_c7_PWM_28_HalfB, const char_T
  *c7_identifier)
{
  uint8_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = (const char *)c7_identifier;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_y = c7_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_b_is_active_c7_PWM_28_HalfB), &c7_thisId);
  sf_mex_destroy(&c7_b_is_active_c7_PWM_28_HalfB);
  return c7_y;
}

static uint8_T c7_f_emlrt_marshallIn(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  uint8_T c7_y;
  uint8_T c7_b_u;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_b_u, 1, 3, 0U, 0, 0U, 0);
  c7_y = c7_b_u;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static const mxArray *c7_chart_data_browse_helper
  (SFc7_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c7_ssIdNumber)
{
  const mxArray *c7_mxData = NULL;
  uint8_T c7_u;
  int16_T c7_i;
  int16_T c7_i1;
  int16_T c7_i2;
  uint16_T c7_u1;
  uint8_T c7_u2;
  uint8_T c7_u3;
  real_T c7_d;
  real_T c7_d1;
  real_T c7_d2;
  real_T c7_d3;
  real_T c7_d4;
  real_T c7_d5;
  c7_mxData = NULL;
  switch (c7_ssIdNumber) {
   case 18U:
    c7_u = *chartInstance->c7_enable;
    c7_d = (real_T)c7_u;
    sf_mex_assign(&c7_mxData, sf_mex_create("mxData", &c7_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c7_i = *chartInstance->c7_offset;
    sf_mex_assign(&c7_mxData, sf_mex_create("mxData", &c7_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c7_i1 = *chartInstance->c7_max;
    c7_d1 = (real_T)c7_i1;
    sf_mex_assign(&c7_mxData, sf_mex_create("mxData", &c7_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c7_i2 = *chartInstance->c7_sum;
    c7_d2 = (real_T)c7_i2;
    sf_mex_assign(&c7_mxData, sf_mex_create("mxData", &c7_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c7_u1 = *chartInstance->c7_cont;
    c7_d3 = (real_T)c7_u1;
    sf_mex_assign(&c7_mxData, sf_mex_create("mxData", &c7_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c7_u2 = *chartInstance->c7_init;
    c7_d4 = (real_T)c7_u2;
    sf_mex_assign(&c7_mxData, sf_mex_create("mxData", &c7_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c7_u3 = *chartInstance->c7_out_init;
    c7_d5 = (real_T)c7_u3;
    sf_mex_assign(&c7_mxData, sf_mex_create("mxData", &c7_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c7_mxData;
}

static int32_T c7__s32_add__(SFc7_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c7_b, int32_T c7_c, int32_T c7_EMLOvCount_src_loc, uint32_T
  c7_ssid_src_loc, int32_T c7_offset_src_loc, int32_T c7_length_src_loc)
{
  int32_T c7_a;
  int32_T c7_PICOffset;
  real_T c7_d;
  observerLogReadPIC(&c7_PICOffset);
  c7_a = c7_b + c7_c;
  if (((c7_a ^ c7_b) & (c7_a ^ c7_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c7_ssid_src_loc, c7_offset_src_loc,
      c7_length_src_loc);
    c7_d = 1.0;
    observerLog(c7_EMLOvCount_src_loc + c7_PICOffset, &c7_d, 1);
  }

  return c7_a;
}

static void init_dsm_address_info(SFc7_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc7_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c7_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c7_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c7_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c7_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c7_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c7_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c7_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c7_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c7_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c7_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c7_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c7_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c7_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c7_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMSzR8gfmZxfGJySWZZanyyeXxAuG+8kUW8R"
    "2JOmhOSuSAAAOqCIGE="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c7_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c7_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c7_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c7_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c7_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c7_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c7_PWM_28_HalfB(SimStruct* S, const mxArray *
  st)
{
  set_sim_state_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c7_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc7_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c7_PWM_28_HalfB
      ((SFc7_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c7_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c7_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c7_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc7_PWM_28_HalfB((SFc7_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c7_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV92O00YUdkIWgVpWS28QEhLctVdVQVC4Kss6CRsp6aZ4+bmLZscn8SjjGTM/2V2eoy9RnqC",
    "XXPAC3HFR9Q0q9RF6xnGyWcd2WAIrkDqSY834O9+c/5l4tU7Pw7GJz4vvPO8ivi/hU/emYyOb1x",
    "ae6XrD+z6bP0QhYeM+USTWXuUQJIYnoCW3hknREUNZCGNiCAoERWwilSlj0yy2nIlx2wrq+PTzi",
    "NEoiKTl4Q7KknBP8GNkS6zpI0+TKaCmDRCaSEk7itqcjOYaK3PoR0DH2sZVJmgwgU2cWrpnuWEJ",
    "h9YR0I7QhqDG+kS3wBADvjkqNdNZqoMZUMYJZ0QUWhsRHUCCDjbwNAnxd88aNCoPoxFRZgciMgH",
    "dZeOUUwrIczKNHw6YIEYqRngr5r4TXNatz1GfngyBVzgEddtRQMaJZMKUxz9oo6UtQQ44NOHAjs",
    "rZAnhpXfCfMTgEVeq3oS8noMgI9kTppqlDWkdptOZZsgwzLIZnRD2iGD8NYWn2YubogGCcYB8ly",
    "mCQGtnR+4pN0L2lbDbuuMxcVTI2ngZbr4KlbK0JVEVhztamwiec61LYvky6MAGesjaJIdWwKWsx",
    "TmsW7kt0sEvv8mqwgmHgM5gvRcgKwzXJAdK+8ys2ltNIarWRsY/J2+x2lz8vwzrCgBoSCkVdQBG",
    "mAX2WurecLWTaxR6BqJVJ1SsCTzNkFcrTQyuah1KN0ScVTeTEBBfRUmCsRxhLrISnGoumCuZiuQ",
    "pHCY0gdA2Gcehh2SC2wCfatbZHWHcTZo6boKliSVFU3fnzk3dy/nz7AefPTC7//mGBp1bA4y28H",
    "f7BAv5y/TR+I7dvfbbmRia/vSB/JbdfIyfvcFuucn5/+/qPq/9889dvf77r/A0mb39ej9qSHjVv",
    "tn9y4Wzn9mY2vzFrkPOEnyzlmcPuLujVKOC/tsC/lc313d120Bu/uP3yyW14rAx/FSvxs5/yval",
    "X63shp+9s/Zbr1MdJ2ne1op0wu1C4ObHTYzYfz4sr/HE5W5+Ofx+uJ//jdj6ORf5qnPJXw6NSmM",
    "2SfDxf/e9t5+WL9L+Ui7ebS2sGTLBPZMet7fXkp/v3V9hxPWfH9fReMSCuW8GA3h/0n/cGdx4Md",
    "gkf7hT0mY+t17PKeecs97Xo+b9fPr99H3IOb3ykXH3Nc/+85Na176z3kS8NX3WeeTn81hdsx7r3",
    "xM+Nf++d7R53M5v/Mv+L5UeMhwW37exzF8iw6Os52PcfxVKgTg==",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c7_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c7_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c7_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c7_PWM_28_HalfB(SimStruct *S)
{
  SFc7_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc7_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc7_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc7_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c7_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c7_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c7_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c7_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c7_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c7_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c7_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c7_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c7_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c7_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c7_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c7_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c7_JITStateAnimation,
    chartInstance->c7_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c7_PWM_28_HalfB(chartInstance);
}

void c7_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c7_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c7_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c7_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c7_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
