#ifndef __c5_PWM_28_HalfB_h__
#define __c5_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc5_PWM_28_HalfBInstanceStruct
#define typedef_SFc5_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c5_sfEvent;
  boolean_T c5_doneDoubleBufferReInit;
  uint8_T c5_is_active_c5_PWM_28_HalfB;
  uint8_T c5_JITStateAnimation[1];
  uint8_T c5_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c5_emlrtLocationLoggingDataTables[24];
  int32_T c5_IsDebuggerActive;
  int32_T c5_IsSequenceViewerPresent;
  int32_T c5_SequenceViewerOptimization;
  void *c5_RuntimeVar;
  emlrtLocationLoggingHistogramType c5_emlrtLocLogHistTables[24];
  boolean_T c5_emlrtLocLogSimulated;
  uint32_T c5_mlFcnLineNumber;
  void *c5_fcnDataPtrs[7];
  char_T *c5_dataNames[7];
  uint32_T c5_numFcnVars;
  uint32_T c5_ssIds[7];
  uint32_T c5_statuses[7];
  void *c5_outMexFcns[7];
  void *c5_inMexFcns[7];
  CovrtStateflowInstance *c5_covrtInstance;
  void *c5_fEmlrtCtx;
  uint8_T *c5_enable;
  int16_T *c5_offset;
  int16_T *c5_max;
  int16_T *c5_sum;
  uint16_T *c5_cont;
  uint8_T *c5_init;
  uint8_T *c5_out_init;
} SFc5_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc5_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c5_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c5_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c5_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
