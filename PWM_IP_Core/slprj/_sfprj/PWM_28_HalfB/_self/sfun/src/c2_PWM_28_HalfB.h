#ifndef __c2_PWM_28_HalfB_h__
#define __c2_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc2_PWM_28_HalfBInstanceStruct
#define typedef_SFc2_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c2_sfEvent;
  boolean_T c2_doneDoubleBufferReInit;
  uint8_T c2_is_active_c2_PWM_28_HalfB;
  uint8_T c2_JITStateAnimation[1];
  uint8_T c2_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c2_emlrtLocationLoggingDataTables[24];
  int32_T c2_IsDebuggerActive;
  int32_T c2_IsSequenceViewerPresent;
  int32_T c2_SequenceViewerOptimization;
  void *c2_RuntimeVar;
  emlrtLocationLoggingHistogramType c2_emlrtLocLogHistTables[24];
  boolean_T c2_emlrtLocLogSimulated;
  uint32_T c2_mlFcnLineNumber;
  void *c2_fcnDataPtrs[7];
  char_T *c2_dataNames[7];
  uint32_T c2_numFcnVars;
  uint32_T c2_ssIds[7];
  uint32_T c2_statuses[7];
  void *c2_outMexFcns[7];
  void *c2_inMexFcns[7];
  CovrtStateflowInstance *c2_covrtInstance;
  void *c2_fEmlrtCtx;
  uint8_T *c2_enable;
  int16_T *c2_offset;
  int16_T *c2_max;
  int16_T *c2_sum;
  uint16_T *c2_cont;
  uint8_T *c2_init;
  uint8_T *c2_out_init;
} SFc2_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc2_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c2_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
