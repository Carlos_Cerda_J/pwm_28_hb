#ifndef __c10_PWM_28_HalfB_h__
#define __c10_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc10_PWM_28_HalfBInstanceStruct
#define typedef_SFc10_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c10_sfEvent;
  boolean_T c10_doneDoubleBufferReInit;
  uint8_T c10_is_active_c10_PWM_28_HalfB;
  uint8_T c10_JITStateAnimation[1];
  uint8_T c10_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c10_emlrtLocationLoggingDataTables[24];
  int32_T c10_IsDebuggerActive;
  int32_T c10_IsSequenceViewerPresent;
  int32_T c10_SequenceViewerOptimization;
  void *c10_RuntimeVar;
  emlrtLocationLoggingHistogramType c10_emlrtLocLogHistTables[24];
  boolean_T c10_emlrtLocLogSimulated;
  uint32_T c10_mlFcnLineNumber;
  void *c10_fcnDataPtrs[7];
  char_T *c10_dataNames[7];
  uint32_T c10_numFcnVars;
  uint32_T c10_ssIds[7];
  uint32_T c10_statuses[7];
  void *c10_outMexFcns[7];
  void *c10_inMexFcns[7];
  CovrtStateflowInstance *c10_covrtInstance;
  void *c10_fEmlrtCtx;
  uint8_T *c10_enable;
  int16_T *c10_offset;
  int16_T *c10_max;
  int16_T *c10_sum;
  uint16_T *c10_cont;
  uint8_T *c10_init;
  uint8_T *c10_out_init;
} SFc10_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc10_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c10_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c10_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c10_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
