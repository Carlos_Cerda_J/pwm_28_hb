#ifndef __c14_PWM_28_HalfB_h__
#define __c14_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc14_PWM_28_HalfBInstanceStruct
#define typedef_SFc14_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c14_sfEvent;
  boolean_T c14_doneDoubleBufferReInit;
  uint8_T c14_is_active_c14_PWM_28_HalfB;
  uint8_T c14_JITStateAnimation[1];
  uint8_T c14_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c14_emlrtLocationLoggingDataTables[24];
  int32_T c14_IsDebuggerActive;
  int32_T c14_IsSequenceViewerPresent;
  int32_T c14_SequenceViewerOptimization;
  void *c14_RuntimeVar;
  emlrtLocationLoggingHistogramType c14_emlrtLocLogHistTables[24];
  boolean_T c14_emlrtLocLogSimulated;
  uint32_T c14_mlFcnLineNumber;
  void *c14_fcnDataPtrs[7];
  char_T *c14_dataNames[7];
  uint32_T c14_numFcnVars;
  uint32_T c14_ssIds[7];
  uint32_T c14_statuses[7];
  void *c14_outMexFcns[7];
  void *c14_inMexFcns[7];
  CovrtStateflowInstance *c14_covrtInstance;
  void *c14_fEmlrtCtx;
  uint8_T *c14_enable;
  int16_T *c14_offset;
  int16_T *c14_max;
  int16_T *c14_sum;
  uint16_T *c14_cont;
  uint8_T *c14_init;
  uint8_T *c14_out_init;
} SFc14_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc14_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c14_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c14_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c14_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
