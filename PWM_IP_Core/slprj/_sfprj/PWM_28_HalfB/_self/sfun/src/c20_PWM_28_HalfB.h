#ifndef __c20_PWM_28_HalfB_h__
#define __c20_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc20_PWM_28_HalfBInstanceStruct
#define typedef_SFc20_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c20_sfEvent;
  boolean_T c20_doneDoubleBufferReInit;
  uint8_T c20_is_active_c20_PWM_28_HalfB;
  uint8_T c20_JITStateAnimation[1];
  uint8_T c20_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c20_emlrtLocationLoggingDataTables[24];
  int32_T c20_IsDebuggerActive;
  int32_T c20_IsSequenceViewerPresent;
  int32_T c20_SequenceViewerOptimization;
  void *c20_RuntimeVar;
  emlrtLocationLoggingHistogramType c20_emlrtLocLogHistTables[24];
  boolean_T c20_emlrtLocLogSimulated;
  uint32_T c20_mlFcnLineNumber;
  void *c20_fcnDataPtrs[7];
  char_T *c20_dataNames[7];
  uint32_T c20_numFcnVars;
  uint32_T c20_ssIds[7];
  uint32_T c20_statuses[7];
  void *c20_outMexFcns[7];
  void *c20_inMexFcns[7];
  CovrtStateflowInstance *c20_covrtInstance;
  void *c20_fEmlrtCtx;
  uint8_T *c20_enable;
  int16_T *c20_offset;
  int16_T *c20_max;
  int16_T *c20_sum;
  uint16_T *c20_cont;
  uint8_T *c20_init;
  uint8_T *c20_out_init;
} SFc20_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc20_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c20_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c20_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c20_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
