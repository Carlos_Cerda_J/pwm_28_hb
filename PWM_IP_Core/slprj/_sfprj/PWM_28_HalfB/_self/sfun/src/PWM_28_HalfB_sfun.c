/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c1_PWM_28_HalfB.h"
#include "c2_PWM_28_HalfB.h"
#include "c3_PWM_28_HalfB.h"
#include "c4_PWM_28_HalfB.h"
#include "c5_PWM_28_HalfB.h"
#include "c6_PWM_28_HalfB.h"
#include "c7_PWM_28_HalfB.h"
#include "c8_PWM_28_HalfB.h"
#include "c9_PWM_28_HalfB.h"
#include "c10_PWM_28_HalfB.h"
#include "c11_PWM_28_HalfB.h"
#include "c12_PWM_28_HalfB.h"
#include "c13_PWM_28_HalfB.h"
#include "c14_PWM_28_HalfB.h"
#include "c15_PWM_28_HalfB.h"
#include "c16_PWM_28_HalfB.h"
#include "c17_PWM_28_HalfB.h"
#include "c18_PWM_28_HalfB.h"
#include "c19_PWM_28_HalfB.h"
#include "c20_PWM_28_HalfB.h"
#include "c21_PWM_28_HalfB.h"
#include "c22_PWM_28_HalfB.h"
#include "c23_PWM_28_HalfB.h"
#include "c24_PWM_28_HalfB.h"
#include "c25_PWM_28_HalfB.h"
#include "c26_PWM_28_HalfB.h"
#include "c27_PWM_28_HalfB.h"
#include "c28_PWM_28_HalfB.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void PWM_28_HalfB_initializer(void)
{
}

void PWM_28_HalfB_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_PWM_28_HalfB_method_dispatcher(SimStruct *simstructPtr, unsigned
  int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==2) {
    c2_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==5) {
    c5_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==6) {
    c6_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==7) {
    c7_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==8) {
    c8_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==9) {
    c9_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==10) {
    c10_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==11) {
    c11_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==12) {
    c12_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==13) {
    c13_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==14) {
    c14_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==15) {
    c15_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==16) {
    c16_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==17) {
    c17_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==18) {
    c18_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==19) {
    c19_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==20) {
    c20_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==21) {
    c21_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==22) {
    c22_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==23) {
    c23_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==24) {
    c24_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==25) {
    c25_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==26) {
    c26_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==27) {
    c27_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==28) {
    c28_PWM_28_HalfB_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

unsigned int sf_PWM_28_HalfB_process_check_sum_call( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1786970946U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3633781590U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1837253129U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(251253177U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c1_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 2:
        {
          extern void sf_c2_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c2_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c3_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c4_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 5:
        {
          extern void sf_c5_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c5_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 6:
        {
          extern void sf_c6_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c6_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 7:
        {
          extern void sf_c7_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c7_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 8:
        {
          extern void sf_c8_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c8_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 9:
        {
          extern void sf_c9_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c9_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 10:
        {
          extern void sf_c10_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c10_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 11:
        {
          extern void sf_c11_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c11_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 12:
        {
          extern void sf_c12_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c12_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 13:
        {
          extern void sf_c13_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c13_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 14:
        {
          extern void sf_c14_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c14_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 15:
        {
          extern void sf_c15_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c15_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 16:
        {
          extern void sf_c16_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c16_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 17:
        {
          extern void sf_c17_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c17_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 18:
        {
          extern void sf_c18_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c18_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 19:
        {
          extern void sf_c19_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c19_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 20:
        {
          extern void sf_c20_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c20_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 21:
        {
          extern void sf_c21_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c21_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 22:
        {
          extern void sf_c22_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c22_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 23:
        {
          extern void sf_c23_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c23_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 24:
        {
          extern void sf_c24_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c24_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 25:
        {
          extern void sf_c25_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c25_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 26:
        {
          extern void sf_c26_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c26_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 27:
        {
          extern void sf_c27_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c27_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       case 28:
        {
          extern void sf_c28_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
          sf_c28_PWM_28_HalfB_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3897003198U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(556691881U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(671159145U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(712225018U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4139711424U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(48445491U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3691022433U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2550341031U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_PWM_28_HalfB_get_eml_resolved_functions_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  char instanceChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    mxGetString(prhs[2], instanceChksum,sizeof(instanceChksum)/sizeof(char));
    instanceChksum[(sizeof(instanceChksum)/sizeof(char)-1)] = '\0';
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c1_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c1_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 2:
      {
        if (strcmp(instanceChksum, "sNUPPTm0ulf2I8iDuONHcnG") == 0) {
          extern const mxArray
            *sf_c2_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c2_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 3:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c3_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c3_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 4:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c4_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c4_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 5:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c5_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c5_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 6:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c6_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c6_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 7:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c7_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c7_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 8:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c8_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c8_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 9:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c9_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c9_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 10:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c10_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c10_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 11:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c11_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c11_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 12:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c12_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c12_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 13:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c13_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c13_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 14:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c14_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c14_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 15:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c15_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c15_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 16:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c16_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c16_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 17:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c17_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c17_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 18:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c18_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c18_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 19:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c19_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c19_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 20:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c20_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c20_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 21:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c21_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c21_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 22:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c22_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c22_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 23:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c23_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c23_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 24:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c24_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c24_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 25:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c25_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c25_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 26:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c26_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c26_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 27:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c27_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c27_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     case 28:
      {
        if (strcmp(instanceChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern const mxArray
            *sf_c28_PWM_28_HalfB_get_eml_resolved_functions_info(void);
          mxArray *persistentMxArray = (mxArray *)
            sf_c28_PWM_28_HalfB_get_eml_resolved_functions_info();
          plhs[0] = mxDuplicateArray(persistentMxArray);
          mxDestroyArray(persistentMxArray);
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_PWM_28_HalfB_third_party_uses_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c1_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c1_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "sNUPPTm0ulf2I8iDuONHcnG") == 0) {
          extern mxArray *sf_c2_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c2_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c3_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c3_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c4_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c4_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c5_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c5_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c6_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c6_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c7_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c7_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c8_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c8_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c9_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c9_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c10_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c10_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c11_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c11_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c12_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c12_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c13_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c13_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 14:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c14_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c14_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c15_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c15_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c16_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c16_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c17_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c17_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c18_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c18_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c19_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c19_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c20_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c20_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c21_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c21_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c22_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c22_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c23_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c23_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 24:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c24_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c24_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 25:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c25_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c25_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 26:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c26_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c26_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 27:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c27_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c27_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     case 28:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c28_PWM_28_HalfB_third_party_uses_info(void);
          plhs[0] = sf_c28_PWM_28_HalfB_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_PWM_28_HalfB_jit_fallback_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c1_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c1_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "sNUPPTm0ulf2I8iDuONHcnG") == 0) {
          extern mxArray *sf_c2_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c2_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c3_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c3_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c4_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c4_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c5_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c5_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c6_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c6_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c7_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c7_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c8_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c8_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c9_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c9_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c10_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c10_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c11_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c11_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c12_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c12_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c13_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c13_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 14:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c14_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c14_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c15_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c15_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c16_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c16_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c17_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c17_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c18_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c18_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c19_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c19_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c20_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c20_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c21_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c21_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c22_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c22_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c23_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c23_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 24:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c24_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c24_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 25:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c25_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c25_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 26:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c26_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c26_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 27:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c27_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c27_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     case 28:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c28_PWM_28_HalfB_jit_fallback_info(void);
          plhs[0] = sf_c28_PWM_28_HalfB_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_PWM_28_HalfB_get_post_codegen_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get_post_codegen_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_post_codegen_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c1_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c1_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "sNUPPTm0ulf2I8iDuONHcnG") == 0) {
          const char *sf_c2_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c2_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c3_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c3_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c4_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c4_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c5_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c5_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c6_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c6_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c7_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c7_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c8_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c8_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c9_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c9_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c10_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c10_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c11_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c11_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c12_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c12_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c13_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c13_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 14:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c14_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c14_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c15_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c15_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c16_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c16_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c17_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c17_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c18_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c18_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c19_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c19_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c20_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c20_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c21_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c21_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c22_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c22_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c23_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c23_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 24:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c24_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c24_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 25:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c25_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c25_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 26:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c26_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c26_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 27:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c27_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c27_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     case 28:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          const char *sf_c28_PWM_28_HalfB_get_post_codegen_info(void);
          const char* encoded_post_codegen_info =
            sf_c28_PWM_28_HalfB_get_post_codegen_info();
          plhs[0] = sf_mex_decode(encoded_post_codegen_info);
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_PWM_28_HalfB_updateBuildInfo_args_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c1_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c1_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "sNUPPTm0ulf2I8iDuONHcnG") == 0) {
          extern mxArray *sf_c2_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c2_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c3_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c3_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c4_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c4_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c5_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c5_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c6_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c6_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c7_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c7_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c8_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c8_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c9_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c9_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c10_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c10_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c11_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c11_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c12_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c12_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c13_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c13_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 14:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c14_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c14_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c15_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c15_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c16_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c16_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c17_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c17_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c18_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c18_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c19_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c19_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c20_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c20_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c21_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c21_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c22_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c22_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c23_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c23_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 24:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c24_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c24_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 25:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c25_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c25_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 26:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c26_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c26_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 27:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c27_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c27_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     case 28:
      {
        if (strcmp(tpChksum, "s4HFSMkX1qR1eGrtlzmrn6C") == 0) {
          extern mxArray *sf_c28_PWM_28_HalfB_updateBuildInfo_args_info(void);
          plhs[0] = sf_c28_PWM_28_HalfB_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void PWM_28_HalfB_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
typedef struct SfOptimizationInfoFlagsTag {
  boolean_T isRtwGen;
  boolean_T isModelRef;
  boolean_T isExternal;
} SfOptimizationInfoFlags;

static SfOptimizationInfoFlags sOptimizationInfoFlags;
void unload_PWM_28_HalfB_optimization_info(void);
mxArray* load_PWM_28_HalfB_optimization_info(boolean_T isRtwGen, boolean_T
  isModelRef, boolean_T isExternal)
{
  if (sOptimizationInfoFlags.isRtwGen != isRtwGen ||
      sOptimizationInfoFlags.isModelRef != isModelRef ||
      sOptimizationInfoFlags.isExternal != isExternal) {
    unload_PWM_28_HalfB_optimization_info();
  }

  sOptimizationInfoFlags.isRtwGen = isRtwGen;
  sOptimizationInfoFlags.isModelRef = isModelRef;
  sOptimizationInfoFlags.isExternal = isExternal;
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info("PWM_28_HalfB",
      "PWM_28_HalfB");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_PWM_28_HalfB_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}
