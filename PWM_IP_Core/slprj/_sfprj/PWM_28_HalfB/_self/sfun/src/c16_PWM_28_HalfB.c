/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c16_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c16_eml_mx;
static const mxArray *c16_b_eml_mx;
static const mxArray *c16_c_eml_mx;

/* Function Declarations */
static void initialize_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c16_update_jit_animation_state_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance);
static void c16_do_animation_call_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c16_st);
static void sf_gateway_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c16_emlrt_update_log_1(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index);
static int16_T c16_emlrt_update_log_2(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index);
static int16_T c16_emlrt_update_log_3(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index);
static boolean_T c16_emlrt_update_log_4(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index);
static uint16_T c16_emlrt_update_log_5(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index);
static int32_T c16_emlrt_update_log_6(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index);
static void c16_emlrtInitVarDataTables(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c16_dataTables[24],
  emlrtLocationLoggingHistogramType c16_histTables[24]);
static uint16_T c16_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c16_sp, const mxArray *c16_b_cont, const
  char_T *c16_identifier);
static uint16_T c16_b_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c16_sp, const mxArray *c16_u, const
  emlrtMsgIdentifier *c16_parentId);
static uint8_T c16_c_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c16_sp, const mxArray *c16_b_out_init, const
  char_T *c16_identifier);
static uint8_T c16_d_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c16_sp, const mxArray *c16_u, const
  emlrtMsgIdentifier *c16_parentId);
static uint8_T c16_e_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c16_b_is_active_c16_PWM_28_HalfB, const char_T *
  c16_identifier);
static uint8_T c16_f_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static const mxArray *c16_chart_data_browse_helper
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c16_ssIdNumber);
static int32_T c16__s32_add__(SFc16_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c16_b, int32_T c16_c, int32_T c16_EMLOvCount_src_loc, uint32_T
  c16_ssid_src_loc, int32_T c16_offset_src_loc, int32_T c16_length_src_loc);
static void init_dsm_address_info(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c16_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c16_st.tls = chartInstance->c16_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c16_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c16_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c16_is_active_c16_PWM_28_HalfB = 0U;
  sf_mex_assign(&c16_c_eml_mx, sf_mex_call(&c16_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c16_b_eml_mx, sf_mex_call(&c16_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c16_eml_mx, sf_mex_call(&c16_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c16_emlrtLocLogSimulated = false;
  c16_emlrtInitVarDataTables(chartInstance,
    chartInstance->c16_emlrtLocationLoggingDataTables,
    chartInstance->c16_emlrtLocLogHistTables);
}

static void initialize_params_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c16_update_jit_animation_state_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c16_do_animation_call_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c16_st;
  const mxArray *c16_y = NULL;
  const mxArray *c16_b_y = NULL;
  uint16_T c16_u;
  const mxArray *c16_c_y = NULL;
  const mxArray *c16_d_y = NULL;
  uint8_T c16_b_u;
  const mxArray *c16_e_y = NULL;
  const mxArray *c16_f_y = NULL;
  c16_st = NULL;
  c16_st = NULL;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_createcellmatrix(3, 1), false);
  c16_b_y = NULL;
  c16_u = *chartInstance->c16_cont;
  c16_c_y = NULL;
  sf_mex_assign(&c16_c_y, sf_mex_create("y", &c16_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c16_b_y, sf_mex_create_fi(sf_mex_dup(c16_eml_mx), sf_mex_dup
    (c16_b_eml_mx), "simulinkarray", c16_c_y, false, false), false);
  sf_mex_setcell(c16_y, 0, c16_b_y);
  c16_d_y = NULL;
  c16_b_u = *chartInstance->c16_out_init;
  c16_e_y = NULL;
  sf_mex_assign(&c16_e_y, sf_mex_create("y", &c16_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c16_d_y, sf_mex_create_fi(sf_mex_dup(c16_eml_mx), sf_mex_dup
    (c16_c_eml_mx), "simulinkarray", c16_e_y, false, false), false);
  sf_mex_setcell(c16_y, 1, c16_d_y);
  c16_f_y = NULL;
  sf_mex_assign(&c16_f_y, sf_mex_create("y",
    &chartInstance->c16_is_active_c16_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c16_y, 2, c16_f_y);
  sf_mex_assign(&c16_st, c16_y, false);
  return c16_st;
}

static void set_sim_state_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c16_st)
{
  emlrtStack c16_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c16_u;
  c16_b_st.tls = chartInstance->c16_fEmlrtCtx;
  chartInstance->c16_doneDoubleBufferReInit = true;
  c16_u = sf_mex_dup(c16_st);
  *chartInstance->c16_cont = c16_emlrt_marshallIn(chartInstance, &c16_b_st,
    sf_mex_dup(sf_mex_getcell(c16_u, 0)), "cont");
  *chartInstance->c16_out_init = c16_c_emlrt_marshallIn(chartInstance, &c16_b_st,
    sf_mex_dup(sf_mex_getcell(c16_u, 1)), "out_init");
  chartInstance->c16_is_active_c16_PWM_28_HalfB = c16_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c16_u, 2)),
     "is_active_c16_PWM_28_HalfB");
  sf_mex_destroy(&c16_u);
  sf_mex_destroy(&c16_st);
}

static void sf_gateway_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c16_PICOffset;
  uint8_T c16_b_enable;
  int16_T c16_b_offset;
  int16_T c16_b_max;
  int16_T c16_b_sum;
  uint8_T c16_b_init;
  uint8_T c16_a0;
  uint8_T c16_a;
  uint8_T c16_b_a0;
  uint8_T c16_a1;
  uint8_T c16_b_a1;
  boolean_T c16_c;
  int8_T c16_i;
  int8_T c16_i1;
  real_T c16_d;
  uint8_T c16_c_a0;
  uint16_T c16_b_cont;
  uint8_T c16_b_a;
  uint8_T c16_b_out_init;
  uint8_T c16_d_a0;
  uint8_T c16_c_a1;
  uint8_T c16_d_a1;
  boolean_T c16_b_c;
  int8_T c16_i2;
  int8_T c16_i3;
  real_T c16_d1;
  int16_T c16_varargin_1;
  int16_T c16_b_varargin_1;
  int16_T c16_c_varargin_1;
  int16_T c16_d_varargin_1;
  int16_T c16_var1;
  int16_T c16_b_var1;
  int16_T c16_i4;
  int16_T c16_i5;
  boolean_T c16_covSaturation;
  boolean_T c16_b_covSaturation;
  uint16_T c16_hfi;
  uint16_T c16_b_hfi;
  uint16_T c16_u;
  uint16_T c16_u1;
  int16_T c16_e_varargin_1;
  int16_T c16_f_varargin_1;
  int16_T c16_g_varargin_1;
  int16_T c16_h_varargin_1;
  int16_T c16_c_var1;
  int16_T c16_d_var1;
  int16_T c16_i6;
  int16_T c16_i7;
  boolean_T c16_c_covSaturation;
  boolean_T c16_d_covSaturation;
  uint16_T c16_c_hfi;
  uint16_T c16_d_hfi;
  uint16_T c16_u2;
  uint16_T c16_u3;
  uint16_T c16_e_a0;
  uint16_T c16_f_a0;
  uint16_T c16_b0;
  uint16_T c16_b_b0;
  uint16_T c16_c_a;
  uint16_T c16_d_a;
  uint16_T c16_b;
  uint16_T c16_b_b;
  uint16_T c16_g_a0;
  uint16_T c16_h_a0;
  uint16_T c16_c_b0;
  uint16_T c16_d_b0;
  uint16_T c16_e_a1;
  uint16_T c16_f_a1;
  uint16_T c16_b1;
  uint16_T c16_b_b1;
  uint16_T c16_g_a1;
  uint16_T c16_h_a1;
  uint16_T c16_c_b1;
  uint16_T c16_d_b1;
  boolean_T c16_c_c;
  boolean_T c16_d_c;
  int16_T c16_i8;
  int16_T c16_i9;
  int16_T c16_i10;
  int16_T c16_i11;
  int16_T c16_i12;
  int16_T c16_i13;
  int16_T c16_i14;
  int16_T c16_i15;
  int16_T c16_i16;
  int16_T c16_i17;
  int16_T c16_i18;
  int16_T c16_i19;
  int32_T c16_i20;
  int32_T c16_i21;
  int16_T c16_i22;
  int16_T c16_i23;
  int16_T c16_i24;
  int16_T c16_i25;
  int16_T c16_i26;
  int16_T c16_i27;
  int16_T c16_i28;
  int16_T c16_i29;
  int16_T c16_i30;
  int16_T c16_i31;
  int16_T c16_i32;
  int16_T c16_i33;
  int32_T c16_i34;
  int32_T c16_i35;
  int16_T c16_i36;
  int16_T c16_i37;
  int16_T c16_i38;
  int16_T c16_i39;
  int16_T c16_i40;
  int16_T c16_i41;
  int16_T c16_i42;
  int16_T c16_i43;
  real_T c16_d2;
  real_T c16_d3;
  int16_T c16_i_varargin_1;
  int16_T c16_i_a0;
  int16_T c16_j_varargin_1;
  int16_T c16_e_b0;
  int16_T c16_e_var1;
  int16_T c16_k_varargin_1;
  int16_T c16_i44;
  int16_T c16_v;
  boolean_T c16_e_covSaturation;
  int16_T c16_val;
  int16_T c16_c_b;
  int32_T c16_i45;
  uint16_T c16_e_hfi;
  real_T c16_d4;
  int32_T c16_i46;
  real_T c16_d5;
  real_T c16_d6;
  real_T c16_d7;
  int32_T c16_i47;
  int32_T c16_i48;
  int32_T c16_i49;
  real_T c16_d8;
  real_T c16_d9;
  int32_T c16_e_c;
  int32_T c16_l_varargin_1;
  int32_T c16_m_varargin_1;
  int32_T c16_f_var1;
  int32_T c16_i50;
  boolean_T c16_f_covSaturation;
  uint16_T c16_f_hfi;
  observerLogReadPIC(&c16_PICOffset);
  chartInstance->c16_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c16_covrtInstance, 4U, (real_T)
                    *chartInstance->c16_init);
  covrtSigUpdateFcn(chartInstance->c16_covrtInstance, 3U, (real_T)
                    *chartInstance->c16_sum);
  covrtSigUpdateFcn(chartInstance->c16_covrtInstance, 2U, (real_T)
                    *chartInstance->c16_max);
  covrtSigUpdateFcn(chartInstance->c16_covrtInstance, 1U, (real_T)
                    *chartInstance->c16_offset);
  covrtSigUpdateFcn(chartInstance->c16_covrtInstance, 0U, (real_T)
                    *chartInstance->c16_enable);
  chartInstance->c16_sfEvent = CALL_EVENT;
  c16_b_enable = *chartInstance->c16_enable;
  c16_b_offset = *chartInstance->c16_offset;
  c16_b_max = *chartInstance->c16_max;
  c16_b_sum = *chartInstance->c16_sum;
  c16_b_init = *chartInstance->c16_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c16_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c16_emlrt_update_log_1(chartInstance, c16_b_enable,
    chartInstance->c16_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c16_emlrt_update_log_2(chartInstance, c16_b_offset,
    chartInstance->c16_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c16_emlrt_update_log_3(chartInstance, c16_b_max,
    chartInstance->c16_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c16_emlrt_update_log_3(chartInstance, c16_b_sum,
    chartInstance->c16_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c16_emlrt_update_log_1(chartInstance, c16_b_init,
    chartInstance->c16_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c16_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c16_covrtInstance, 4U, 0, 0, false);
  c16_a0 = c16_b_enable;
  c16_a = c16_a0;
  c16_b_a0 = c16_a;
  c16_a1 = c16_b_a0;
  c16_b_a1 = c16_a1;
  c16_c = (c16_b_a1 == 0);
  c16_i = (int8_T)c16_b_enable;
  if ((int8_T)(c16_i & 2) != 0) {
    c16_i1 = (int8_T)(c16_i | -2);
  } else {
    c16_i1 = (int8_T)(c16_i & 1);
  }

  if (c16_i1 > 0) {
    c16_d = 3.0;
  } else {
    c16_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c16_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c16_covrtInstance,
        4U, 0U, 0U, c16_d, 0.0, -2, 0U, (int32_T)c16_emlrt_update_log_4
        (chartInstance, c16_c, chartInstance->c16_emlrtLocationLoggingDataTables,
         5)))) {
    c16_b_cont = c16_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c16_emlrtLocationLoggingDataTables, 6);
    c16_b_out_init = c16_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c16_emlrtLocationLoggingDataTables, 7);
  } else {
    c16_c_a0 = c16_b_init;
    c16_b_a = c16_c_a0;
    c16_d_a0 = c16_b_a;
    c16_c_a1 = c16_d_a0;
    c16_d_a1 = c16_c_a1;
    c16_b_c = (c16_d_a1 == 0);
    c16_i2 = (int8_T)c16_b_init;
    if ((int8_T)(c16_i2 & 2) != 0) {
      c16_i3 = (int8_T)(c16_i2 | -2);
    } else {
      c16_i3 = (int8_T)(c16_i2 & 1);
    }

    if (c16_i3 > 0) {
      c16_d1 = 3.0;
    } else {
      c16_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c16_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c16_covrtInstance, 4U, 0U, 1U, c16_d1,
                        0.0, -2, 0U, (int32_T)c16_emlrt_update_log_4
                        (chartInstance, c16_b_c,
                         chartInstance->c16_emlrtLocationLoggingDataTables, 8))))
    {
      c16_b_varargin_1 = c16_b_sum;
      c16_d_varargin_1 = c16_b_varargin_1;
      c16_b_var1 = c16_d_varargin_1;
      c16_i5 = c16_b_var1;
      c16_b_covSaturation = false;
      if (c16_i5 < 0) {
        c16_i5 = 0;
      } else {
        if (c16_i5 > 4095) {
          c16_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c16_covrtInstance, 4, 0, 0, 0,
          c16_b_covSaturation);
      }

      c16_b_hfi = (uint16_T)c16_i5;
      c16_u1 = c16_emlrt_update_log_5(chartInstance, c16_b_hfi,
        chartInstance->c16_emlrtLocationLoggingDataTables, 10);
      c16_f_varargin_1 = c16_b_max;
      c16_h_varargin_1 = c16_f_varargin_1;
      c16_d_var1 = c16_h_varargin_1;
      c16_i7 = c16_d_var1;
      c16_d_covSaturation = false;
      if (c16_i7 < 0) {
        c16_i7 = 0;
      } else {
        if (c16_i7 > 4095) {
          c16_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c16_covrtInstance, 4, 0, 1, 0,
          c16_d_covSaturation);
      }

      c16_d_hfi = (uint16_T)c16_i7;
      c16_u3 = c16_emlrt_update_log_5(chartInstance, c16_d_hfi,
        chartInstance->c16_emlrtLocationLoggingDataTables, 11);
      c16_f_a0 = c16_u1;
      c16_b_b0 = c16_u3;
      c16_d_a = c16_f_a0;
      c16_b_b = c16_b_b0;
      c16_h_a0 = c16_d_a;
      c16_d_b0 = c16_b_b;
      c16_f_a1 = c16_h_a0;
      c16_b_b1 = c16_d_b0;
      c16_h_a1 = c16_f_a1;
      c16_d_b1 = c16_b_b1;
      c16_d_c = (c16_h_a1 < c16_d_b1);
      c16_i9 = (int16_T)c16_u1;
      c16_i11 = (int16_T)c16_u3;
      c16_i13 = (int16_T)c16_u3;
      c16_i15 = (int16_T)c16_u1;
      if ((int16_T)(c16_i13 & 4096) != 0) {
        c16_i17 = (int16_T)(c16_i13 | -4096);
      } else {
        c16_i17 = (int16_T)(c16_i13 & 4095);
      }

      if ((int16_T)(c16_i15 & 4096) != 0) {
        c16_i19 = (int16_T)(c16_i15 | -4096);
      } else {
        c16_i19 = (int16_T)(c16_i15 & 4095);
      }

      c16_i21 = c16_i17 - c16_i19;
      if (c16_i21 > 4095) {
        c16_i21 = 4095;
      } else {
        if (c16_i21 < -4096) {
          c16_i21 = -4096;
        }
      }

      c16_i23 = (int16_T)c16_u1;
      c16_i25 = (int16_T)c16_u3;
      c16_i27 = (int16_T)c16_u1;
      c16_i29 = (int16_T)c16_u3;
      if ((int16_T)(c16_i27 & 4096) != 0) {
        c16_i31 = (int16_T)(c16_i27 | -4096);
      } else {
        c16_i31 = (int16_T)(c16_i27 & 4095);
      }

      if ((int16_T)(c16_i29 & 4096) != 0) {
        c16_i33 = (int16_T)(c16_i29 | -4096);
      } else {
        c16_i33 = (int16_T)(c16_i29 & 4095);
      }

      c16_i35 = c16_i31 - c16_i33;
      if (c16_i35 > 4095) {
        c16_i35 = 4095;
      } else {
        if (c16_i35 < -4096) {
          c16_i35 = -4096;
        }
      }

      if ((int16_T)(c16_i9 & 4096) != 0) {
        c16_i37 = (int16_T)(c16_i9 | -4096);
      } else {
        c16_i37 = (int16_T)(c16_i9 & 4095);
      }

      if ((int16_T)(c16_i11 & 4096) != 0) {
        c16_i39 = (int16_T)(c16_i11 | -4096);
      } else {
        c16_i39 = (int16_T)(c16_i11 & 4095);
      }

      if ((int16_T)(c16_i23 & 4096) != 0) {
        c16_i41 = (int16_T)(c16_i23 | -4096);
      } else {
        c16_i41 = (int16_T)(c16_i23 & 4095);
      }

      if ((int16_T)(c16_i25 & 4096) != 0) {
        c16_i43 = (int16_T)(c16_i25 | -4096);
      } else {
        c16_i43 = (int16_T)(c16_i25 & 4095);
      }

      if (c16_i37 < c16_i39) {
        c16_d3 = (real_T)((int16_T)c16_i21 <= 1);
      } else if (c16_i41 > c16_i43) {
        if ((int16_T)c16_i35 <= 1) {
          c16_d3 = 3.0;
        } else {
          c16_d3 = 0.0;
        }
      } else {
        c16_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c16_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c16_covrtInstance, 4U, 0U, 2U, c16_d3,
                          0.0, -2, 2U, (int32_T)c16_emlrt_update_log_4
                          (chartInstance, c16_d_c,
                           chartInstance->c16_emlrtLocationLoggingDataTables, 9))))
      {
        c16_i_a0 = c16_b_sum;
        c16_e_b0 = c16_b_offset;
        c16_k_varargin_1 = c16_e_b0;
        c16_v = c16_k_varargin_1;
        c16_val = c16_v;
        c16_c_b = c16_val;
        c16_i45 = c16_i_a0;
        if (c16_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c16_d4 = 1.0;
          observerLog(279 + c16_PICOffset, &c16_d4, 1);
        }

        if (c16_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c16_d5 = 1.0;
          observerLog(279 + c16_PICOffset, &c16_d5, 1);
        }

        c16_i46 = c16_c_b;
        if (c16_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c16_d6 = 1.0;
          observerLog(282 + c16_PICOffset, &c16_d6, 1);
        }

        if (c16_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c16_d7 = 1.0;
          observerLog(282 + c16_PICOffset, &c16_d7, 1);
        }

        if ((c16_i45 & 65536) != 0) {
          c16_i47 = c16_i45 | -65536;
        } else {
          c16_i47 = c16_i45 & 65535;
        }

        if ((c16_i46 & 65536) != 0) {
          c16_i48 = c16_i46 | -65536;
        } else {
          c16_i48 = c16_i46 & 65535;
        }

        c16_i49 = c16__s32_add__(chartInstance, c16_i47, c16_i48, 281, 1U, 323,
          12);
        if (c16_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c16_d8 = 1.0;
          observerLog(287 + c16_PICOffset, &c16_d8, 1);
        }

        if (c16_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c16_d9 = 1.0;
          observerLog(287 + c16_PICOffset, &c16_d9, 1);
        }

        if ((c16_i49 & 65536) != 0) {
          c16_e_c = c16_i49 | -65536;
        } else {
          c16_e_c = c16_i49 & 65535;
        }

        c16_l_varargin_1 = c16_emlrt_update_log_6(chartInstance, c16_e_c,
          chartInstance->c16_emlrtLocationLoggingDataTables, 13);
        c16_m_varargin_1 = c16_l_varargin_1;
        c16_f_var1 = c16_m_varargin_1;
        c16_i50 = c16_f_var1;
        c16_f_covSaturation = false;
        if (c16_i50 < 0) {
          c16_i50 = 0;
        } else {
          if (c16_i50 > 4095) {
            c16_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c16_covrtInstance, 4, 0, 2, 0,
            c16_f_covSaturation);
        }

        c16_f_hfi = (uint16_T)c16_i50;
        c16_b_cont = c16_emlrt_update_log_5(chartInstance, c16_f_hfi,
          chartInstance->c16_emlrtLocationLoggingDataTables, 12);
        c16_b_out_init = c16_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c16_emlrtLocationLoggingDataTables, 14);
      } else {
        c16_b_cont = c16_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c16_emlrtLocationLoggingDataTables, 15);
        c16_b_out_init = c16_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c16_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c16_varargin_1 = c16_b_sum;
      c16_c_varargin_1 = c16_varargin_1;
      c16_var1 = c16_c_varargin_1;
      c16_i4 = c16_var1;
      c16_covSaturation = false;
      if (c16_i4 < 0) {
        c16_i4 = 0;
      } else {
        if (c16_i4 > 4095) {
          c16_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c16_covrtInstance, 4, 0, 3, 0,
          c16_covSaturation);
      }

      c16_hfi = (uint16_T)c16_i4;
      c16_u = c16_emlrt_update_log_5(chartInstance, c16_hfi,
        chartInstance->c16_emlrtLocationLoggingDataTables, 18);
      c16_e_varargin_1 = c16_b_max;
      c16_g_varargin_1 = c16_e_varargin_1;
      c16_c_var1 = c16_g_varargin_1;
      c16_i6 = c16_c_var1;
      c16_c_covSaturation = false;
      if (c16_i6 < 0) {
        c16_i6 = 0;
      } else {
        if (c16_i6 > 4095) {
          c16_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c16_covrtInstance, 4, 0, 4, 0,
          c16_c_covSaturation);
      }

      c16_c_hfi = (uint16_T)c16_i6;
      c16_u2 = c16_emlrt_update_log_5(chartInstance, c16_c_hfi,
        chartInstance->c16_emlrtLocationLoggingDataTables, 19);
      c16_e_a0 = c16_u;
      c16_b0 = c16_u2;
      c16_c_a = c16_e_a0;
      c16_b = c16_b0;
      c16_g_a0 = c16_c_a;
      c16_c_b0 = c16_b;
      c16_e_a1 = c16_g_a0;
      c16_b1 = c16_c_b0;
      c16_g_a1 = c16_e_a1;
      c16_c_b1 = c16_b1;
      c16_c_c = (c16_g_a1 < c16_c_b1);
      c16_i8 = (int16_T)c16_u;
      c16_i10 = (int16_T)c16_u2;
      c16_i12 = (int16_T)c16_u2;
      c16_i14 = (int16_T)c16_u;
      if ((int16_T)(c16_i12 & 4096) != 0) {
        c16_i16 = (int16_T)(c16_i12 | -4096);
      } else {
        c16_i16 = (int16_T)(c16_i12 & 4095);
      }

      if ((int16_T)(c16_i14 & 4096) != 0) {
        c16_i18 = (int16_T)(c16_i14 | -4096);
      } else {
        c16_i18 = (int16_T)(c16_i14 & 4095);
      }

      c16_i20 = c16_i16 - c16_i18;
      if (c16_i20 > 4095) {
        c16_i20 = 4095;
      } else {
        if (c16_i20 < -4096) {
          c16_i20 = -4096;
        }
      }

      c16_i22 = (int16_T)c16_u;
      c16_i24 = (int16_T)c16_u2;
      c16_i26 = (int16_T)c16_u;
      c16_i28 = (int16_T)c16_u2;
      if ((int16_T)(c16_i26 & 4096) != 0) {
        c16_i30 = (int16_T)(c16_i26 | -4096);
      } else {
        c16_i30 = (int16_T)(c16_i26 & 4095);
      }

      if ((int16_T)(c16_i28 & 4096) != 0) {
        c16_i32 = (int16_T)(c16_i28 | -4096);
      } else {
        c16_i32 = (int16_T)(c16_i28 & 4095);
      }

      c16_i34 = c16_i30 - c16_i32;
      if (c16_i34 > 4095) {
        c16_i34 = 4095;
      } else {
        if (c16_i34 < -4096) {
          c16_i34 = -4096;
        }
      }

      if ((int16_T)(c16_i8 & 4096) != 0) {
        c16_i36 = (int16_T)(c16_i8 | -4096);
      } else {
        c16_i36 = (int16_T)(c16_i8 & 4095);
      }

      if ((int16_T)(c16_i10 & 4096) != 0) {
        c16_i38 = (int16_T)(c16_i10 | -4096);
      } else {
        c16_i38 = (int16_T)(c16_i10 & 4095);
      }

      if ((int16_T)(c16_i22 & 4096) != 0) {
        c16_i40 = (int16_T)(c16_i22 | -4096);
      } else {
        c16_i40 = (int16_T)(c16_i22 & 4095);
      }

      if ((int16_T)(c16_i24 & 4096) != 0) {
        c16_i42 = (int16_T)(c16_i24 | -4096);
      } else {
        c16_i42 = (int16_T)(c16_i24 & 4095);
      }

      if (c16_i36 < c16_i38) {
        c16_d2 = (real_T)((int16_T)c16_i20 <= 1);
      } else if (c16_i40 > c16_i42) {
        if ((int16_T)c16_i34 <= 1) {
          c16_d2 = 3.0;
        } else {
          c16_d2 = 0.0;
        }
      } else {
        c16_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c16_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c16_covrtInstance, 4U, 0U, 3U, c16_d2,
                          0.0, -2, 2U, (int32_T)c16_emlrt_update_log_4
                          (chartInstance, c16_c_c,
                           chartInstance->c16_emlrtLocationLoggingDataTables, 17))))
      {
        c16_i_varargin_1 = c16_b_sum;
        c16_j_varargin_1 = c16_i_varargin_1;
        c16_e_var1 = c16_j_varargin_1;
        c16_i44 = c16_e_var1;
        c16_e_covSaturation = false;
        if (c16_i44 < 0) {
          c16_i44 = 0;
        } else {
          if (c16_i44 > 4095) {
            c16_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c16_covrtInstance, 4, 0, 5, 0,
            c16_e_covSaturation);
        }

        c16_e_hfi = (uint16_T)c16_i44;
        c16_b_cont = c16_emlrt_update_log_5(chartInstance, c16_e_hfi,
          chartInstance->c16_emlrtLocationLoggingDataTables, 20);
        c16_b_out_init = c16_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c16_emlrtLocationLoggingDataTables, 21);
      } else {
        c16_b_cont = c16_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c16_emlrtLocationLoggingDataTables, 22);
        c16_b_out_init = c16_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c16_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c16_cont = c16_b_cont;
  *chartInstance->c16_out_init = c16_b_out_init;
  c16_do_animation_call_c16_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c16_covrtInstance, 5U, (real_T)
                    *chartInstance->c16_cont);
  covrtSigUpdateFcn(chartInstance->c16_covrtInstance, 6U, (real_T)
                    *chartInstance->c16_out_init);
}

static void mdl_start_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c16_decisionTxtStartIdx = 0U;
  static const uint32_T c16_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c16_chart_data_browse_helper);
  chartInstance->c16_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c16_RuntimeVar,
    &chartInstance->c16_IsDebuggerActive,
    &chartInstance->c16_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c16_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c16_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c16_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c16_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c16_decisionTxtStartIdx, &c16_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c16_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c16_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c16_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c16_PWM_28_HalfB
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c16_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7573",              /* mexFileName */
    "Thu May 27 10:27:24 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c16_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c16_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c16_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c16_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7573");
    emlrtLocationLoggingPushLog(&c16_emlrtLocationLoggingFileInfo,
      c16_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c16_emlrtLocationLoggingDataTables, c16_emlrtLocationInfo,
      NULL, 0U, c16_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7573");
  }

  sfListenerLightTerminate(chartInstance->c16_RuntimeVar);
  sf_mex_destroy(&c16_eml_mx);
  sf_mex_destroy(&c16_b_eml_mx);
  sf_mex_destroy(&c16_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c16_covrtInstance);
}

static void initSimStructsc16_PWM_28_HalfB(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c16_emlrt_update_log_1(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index)
{
  boolean_T c16_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c16_b_table;
  real_T c16_d;
  uint8_T c16_u;
  uint8_T c16_localMin;
  real_T c16_d1;
  uint8_T c16_u1;
  uint8_T c16_localMax;
  emlrtLocationLoggingHistogramType *c16_histTable;
  real_T c16_inDouble;
  real_T c16_significand;
  int32_T c16_exponent;
  (void)chartInstance;
  c16_isLoggingEnabledHere = (c16_index >= 0);
  if (c16_isLoggingEnabledHere) {
    c16_b_table = (emlrtLocationLoggingDataType *)&c16_table[c16_index];
    c16_d = c16_b_table[0U].SimMin;
    if (c16_d < 2.0) {
      if (c16_d >= 0.0) {
        c16_u = (uint8_T)c16_d;
      } else {
        c16_u = 0U;
      }
    } else if (c16_d >= 2.0) {
      c16_u = 1U;
    } else {
      c16_u = 0U;
    }

    c16_localMin = c16_u;
    c16_d1 = c16_b_table[0U].SimMax;
    if (c16_d1 < 2.0) {
      if (c16_d1 >= 0.0) {
        c16_u1 = (uint8_T)c16_d1;
      } else {
        c16_u1 = 0U;
      }
    } else if (c16_d1 >= 2.0) {
      c16_u1 = 1U;
    } else {
      c16_u1 = 0U;
    }

    c16_localMax = c16_u1;
    c16_histTable = c16_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c16_in < c16_localMin) {
      c16_localMin = c16_in;
    }

    if (c16_in > c16_localMax) {
      c16_localMax = c16_in;
    }

    /* Histogram logging. */
    c16_inDouble = (real_T)c16_in;
    c16_histTable->TotalNumberOfValues++;
    if (c16_inDouble == 0.0) {
      c16_histTable->NumberOfZeros++;
    } else {
      c16_histTable->SimSum += c16_inDouble;
      if ((!muDoubleScalarIsInf(c16_inDouble)) && (!muDoubleScalarIsNaN
           (c16_inDouble))) {
        c16_significand = frexp(c16_inDouble, &c16_exponent);
        if (c16_exponent > 128) {
          c16_exponent = 128;
        }

        if (c16_exponent < -127) {
          c16_exponent = -127;
        }

        if (c16_significand < 0.0) {
          c16_histTable->NumberOfNegativeValues++;
          c16_histTable->HistogramOfNegativeValues[127 + c16_exponent]++;
        } else {
          c16_histTable->NumberOfPositiveValues++;
          c16_histTable->HistogramOfPositiveValues[127 + c16_exponent]++;
        }
      }
    }

    c16_b_table[0U].SimMin = (real_T)c16_localMin;
    c16_b_table[0U].SimMax = (real_T)c16_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c16_in;
}

static int16_T c16_emlrt_update_log_2(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index)
{
  boolean_T c16_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c16_b_table;
  real_T c16_d;
  int16_T c16_i;
  int16_T c16_localMin;
  real_T c16_d1;
  int16_T c16_i1;
  int16_T c16_localMax;
  emlrtLocationLoggingHistogramType *c16_histTable;
  real_T c16_inDouble;
  real_T c16_significand;
  int32_T c16_exponent;
  (void)chartInstance;
  c16_isLoggingEnabledHere = (c16_index >= 0);
  if (c16_isLoggingEnabledHere) {
    c16_b_table = (emlrtLocationLoggingDataType *)&c16_table[c16_index];
    c16_d = muDoubleScalarFloor(c16_b_table[0U].SimMin);
    if (c16_d < 32768.0) {
      if (c16_d >= -32768.0) {
        c16_i = (int16_T)c16_d;
      } else {
        c16_i = MIN_int16_T;
      }
    } else if (c16_d >= 32768.0) {
      c16_i = MAX_int16_T;
    } else {
      c16_i = 0;
    }

    c16_localMin = c16_i;
    c16_d1 = muDoubleScalarFloor(c16_b_table[0U].SimMax);
    if (c16_d1 < 32768.0) {
      if (c16_d1 >= -32768.0) {
        c16_i1 = (int16_T)c16_d1;
      } else {
        c16_i1 = MIN_int16_T;
      }
    } else if (c16_d1 >= 32768.0) {
      c16_i1 = MAX_int16_T;
    } else {
      c16_i1 = 0;
    }

    c16_localMax = c16_i1;
    c16_histTable = c16_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c16_in < c16_localMin) {
      c16_localMin = c16_in;
    }

    if (c16_in > c16_localMax) {
      c16_localMax = c16_in;
    }

    /* Histogram logging. */
    c16_inDouble = (real_T)c16_in;
    c16_histTable->TotalNumberOfValues++;
    if (c16_inDouble == 0.0) {
      c16_histTable->NumberOfZeros++;
    } else {
      c16_histTable->SimSum += c16_inDouble;
      if ((!muDoubleScalarIsInf(c16_inDouble)) && (!muDoubleScalarIsNaN
           (c16_inDouble))) {
        c16_significand = frexp(c16_inDouble, &c16_exponent);
        if (c16_exponent > 128) {
          c16_exponent = 128;
        }

        if (c16_exponent < -127) {
          c16_exponent = -127;
        }

        if (c16_significand < 0.0) {
          c16_histTable->NumberOfNegativeValues++;
          c16_histTable->HistogramOfNegativeValues[127 + c16_exponent]++;
        } else {
          c16_histTable->NumberOfPositiveValues++;
          c16_histTable->HistogramOfPositiveValues[127 + c16_exponent]++;
        }
      }
    }

    c16_b_table[0U].SimMin = (real_T)c16_localMin;
    c16_b_table[0U].SimMax = (real_T)c16_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c16_in;
}

static int16_T c16_emlrt_update_log_3(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index)
{
  boolean_T c16_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c16_b_table;
  real_T c16_d;
  int16_T c16_i;
  int16_T c16_localMin;
  real_T c16_d1;
  int16_T c16_i1;
  int16_T c16_localMax;
  emlrtLocationLoggingHistogramType *c16_histTable;
  real_T c16_inDouble;
  real_T c16_significand;
  int32_T c16_exponent;
  (void)chartInstance;
  c16_isLoggingEnabledHere = (c16_index >= 0);
  if (c16_isLoggingEnabledHere) {
    c16_b_table = (emlrtLocationLoggingDataType *)&c16_table[c16_index];
    c16_d = muDoubleScalarFloor(c16_b_table[0U].SimMin);
    if (c16_d < 2048.0) {
      if (c16_d >= -2048.0) {
        c16_i = (int16_T)c16_d;
      } else {
        c16_i = -2048;
      }
    } else if (c16_d >= 2048.0) {
      c16_i = 2047;
    } else {
      c16_i = 0;
    }

    c16_localMin = c16_i;
    c16_d1 = muDoubleScalarFloor(c16_b_table[0U].SimMax);
    if (c16_d1 < 2048.0) {
      if (c16_d1 >= -2048.0) {
        c16_i1 = (int16_T)c16_d1;
      } else {
        c16_i1 = -2048;
      }
    } else if (c16_d1 >= 2048.0) {
      c16_i1 = 2047;
    } else {
      c16_i1 = 0;
    }

    c16_localMax = c16_i1;
    c16_histTable = c16_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c16_in < c16_localMin) {
      c16_localMin = c16_in;
    }

    if (c16_in > c16_localMax) {
      c16_localMax = c16_in;
    }

    /* Histogram logging. */
    c16_inDouble = (real_T)c16_in;
    c16_histTable->TotalNumberOfValues++;
    if (c16_inDouble == 0.0) {
      c16_histTable->NumberOfZeros++;
    } else {
      c16_histTable->SimSum += c16_inDouble;
      if ((!muDoubleScalarIsInf(c16_inDouble)) && (!muDoubleScalarIsNaN
           (c16_inDouble))) {
        c16_significand = frexp(c16_inDouble, &c16_exponent);
        if (c16_exponent > 128) {
          c16_exponent = 128;
        }

        if (c16_exponent < -127) {
          c16_exponent = -127;
        }

        if (c16_significand < 0.0) {
          c16_histTable->NumberOfNegativeValues++;
          c16_histTable->HistogramOfNegativeValues[127 + c16_exponent]++;
        } else {
          c16_histTable->NumberOfPositiveValues++;
          c16_histTable->HistogramOfPositiveValues[127 + c16_exponent]++;
        }
      }
    }

    c16_b_table[0U].SimMin = (real_T)c16_localMin;
    c16_b_table[0U].SimMax = (real_T)c16_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c16_in;
}

static boolean_T c16_emlrt_update_log_4(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index)
{
  boolean_T c16_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c16_b_table;
  boolean_T c16_localMin;
  boolean_T c16_localMax;
  emlrtLocationLoggingHistogramType *c16_histTable;
  real_T c16_inDouble;
  real_T c16_significand;
  int32_T c16_exponent;
  (void)chartInstance;
  c16_isLoggingEnabledHere = (c16_index >= 0);
  if (c16_isLoggingEnabledHere) {
    c16_b_table = (emlrtLocationLoggingDataType *)&c16_table[c16_index];
    c16_localMin = (c16_b_table[0U].SimMin > 0.0);
    c16_localMax = (c16_b_table[0U].SimMax > 0.0);
    c16_histTable = c16_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c16_in < (int32_T)c16_localMin) {
      c16_localMin = c16_in;
    }

    if ((int32_T)c16_in > (int32_T)c16_localMax) {
      c16_localMax = c16_in;
    }

    /* Histogram logging. */
    c16_inDouble = (real_T)c16_in;
    c16_histTable->TotalNumberOfValues++;
    if (c16_inDouble == 0.0) {
      c16_histTable->NumberOfZeros++;
    } else {
      c16_histTable->SimSum += c16_inDouble;
      if ((!muDoubleScalarIsInf(c16_inDouble)) && (!muDoubleScalarIsNaN
           (c16_inDouble))) {
        c16_significand = frexp(c16_inDouble, &c16_exponent);
        if (c16_exponent > 128) {
          c16_exponent = 128;
        }

        if (c16_exponent < -127) {
          c16_exponent = -127;
        }

        if (c16_significand < 0.0) {
          c16_histTable->NumberOfNegativeValues++;
          c16_histTable->HistogramOfNegativeValues[127 + c16_exponent]++;
        } else {
          c16_histTable->NumberOfPositiveValues++;
          c16_histTable->HistogramOfPositiveValues[127 + c16_exponent]++;
        }
      }
    }

    c16_b_table[0U].SimMin = (real_T)c16_localMin;
    c16_b_table[0U].SimMax = (real_T)c16_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c16_in;
}

static uint16_T c16_emlrt_update_log_5(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index)
{
  boolean_T c16_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c16_b_table;
  real_T c16_d;
  uint16_T c16_u;
  uint16_T c16_localMin;
  real_T c16_d1;
  uint16_T c16_u1;
  uint16_T c16_localMax;
  emlrtLocationLoggingHistogramType *c16_histTable;
  real_T c16_inDouble;
  real_T c16_significand;
  int32_T c16_exponent;
  (void)chartInstance;
  c16_isLoggingEnabledHere = (c16_index >= 0);
  if (c16_isLoggingEnabledHere) {
    c16_b_table = (emlrtLocationLoggingDataType *)&c16_table[c16_index];
    c16_d = c16_b_table[0U].SimMin;
    if (c16_d < 4096.0) {
      if (c16_d >= 0.0) {
        c16_u = (uint16_T)c16_d;
      } else {
        c16_u = 0U;
      }
    } else if (c16_d >= 4096.0) {
      c16_u = 4095U;
    } else {
      c16_u = 0U;
    }

    c16_localMin = c16_u;
    c16_d1 = c16_b_table[0U].SimMax;
    if (c16_d1 < 4096.0) {
      if (c16_d1 >= 0.0) {
        c16_u1 = (uint16_T)c16_d1;
      } else {
        c16_u1 = 0U;
      }
    } else if (c16_d1 >= 4096.0) {
      c16_u1 = 4095U;
    } else {
      c16_u1 = 0U;
    }

    c16_localMax = c16_u1;
    c16_histTable = c16_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c16_in < c16_localMin) {
      c16_localMin = c16_in;
    }

    if (c16_in > c16_localMax) {
      c16_localMax = c16_in;
    }

    /* Histogram logging. */
    c16_inDouble = (real_T)c16_in;
    c16_histTable->TotalNumberOfValues++;
    if (c16_inDouble == 0.0) {
      c16_histTable->NumberOfZeros++;
    } else {
      c16_histTable->SimSum += c16_inDouble;
      if ((!muDoubleScalarIsInf(c16_inDouble)) && (!muDoubleScalarIsNaN
           (c16_inDouble))) {
        c16_significand = frexp(c16_inDouble, &c16_exponent);
        if (c16_exponent > 128) {
          c16_exponent = 128;
        }

        if (c16_exponent < -127) {
          c16_exponent = -127;
        }

        if (c16_significand < 0.0) {
          c16_histTable->NumberOfNegativeValues++;
          c16_histTable->HistogramOfNegativeValues[127 + c16_exponent]++;
        } else {
          c16_histTable->NumberOfPositiveValues++;
          c16_histTable->HistogramOfPositiveValues[127 + c16_exponent]++;
        }
      }
    }

    c16_b_table[0U].SimMin = (real_T)c16_localMin;
    c16_b_table[0U].SimMax = (real_T)c16_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c16_in;
}

static int32_T c16_emlrt_update_log_6(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c16_in, emlrtLocationLoggingDataType c16_table[],
  int32_T c16_index)
{
  boolean_T c16_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c16_b_table;
  real_T c16_d;
  int32_T c16_i;
  int32_T c16_localMin;
  real_T c16_d1;
  int32_T c16_i1;
  int32_T c16_localMax;
  emlrtLocationLoggingHistogramType *c16_histTable;
  real_T c16_inDouble;
  real_T c16_significand;
  int32_T c16_exponent;
  (void)chartInstance;
  c16_isLoggingEnabledHere = (c16_index >= 0);
  if (c16_isLoggingEnabledHere) {
    c16_b_table = (emlrtLocationLoggingDataType *)&c16_table[c16_index];
    c16_d = muDoubleScalarFloor(c16_b_table[0U].SimMin);
    if (c16_d < 65536.0) {
      if (c16_d >= -65536.0) {
        c16_i = (int32_T)c16_d;
      } else {
        c16_i = -65536;
      }
    } else if (c16_d >= 65536.0) {
      c16_i = 65535;
    } else {
      c16_i = 0;
    }

    c16_localMin = c16_i;
    c16_d1 = muDoubleScalarFloor(c16_b_table[0U].SimMax);
    if (c16_d1 < 65536.0) {
      if (c16_d1 >= -65536.0) {
        c16_i1 = (int32_T)c16_d1;
      } else {
        c16_i1 = -65536;
      }
    } else if (c16_d1 >= 65536.0) {
      c16_i1 = 65535;
    } else {
      c16_i1 = 0;
    }

    c16_localMax = c16_i1;
    c16_histTable = c16_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c16_in < c16_localMin) {
      c16_localMin = c16_in;
    }

    if (c16_in > c16_localMax) {
      c16_localMax = c16_in;
    }

    /* Histogram logging. */
    c16_inDouble = (real_T)c16_in;
    c16_histTable->TotalNumberOfValues++;
    if (c16_inDouble == 0.0) {
      c16_histTable->NumberOfZeros++;
    } else {
      c16_histTable->SimSum += c16_inDouble;
      if ((!muDoubleScalarIsInf(c16_inDouble)) && (!muDoubleScalarIsNaN
           (c16_inDouble))) {
        c16_significand = frexp(c16_inDouble, &c16_exponent);
        if (c16_exponent > 128) {
          c16_exponent = 128;
        }

        if (c16_exponent < -127) {
          c16_exponent = -127;
        }

        if (c16_significand < 0.0) {
          c16_histTable->NumberOfNegativeValues++;
          c16_histTable->HistogramOfNegativeValues[127 + c16_exponent]++;
        } else {
          c16_histTable->NumberOfPositiveValues++;
          c16_histTable->HistogramOfPositiveValues[127 + c16_exponent]++;
        }
      }
    }

    c16_b_table[0U].SimMin = (real_T)c16_localMin;
    c16_b_table[0U].SimMax = (real_T)c16_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c16_in;
}

static void c16_emlrtInitVarDataTables(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c16_dataTables[24],
  emlrtLocationLoggingHistogramType c16_histTables[24])
{
  int32_T c16_i;
  int32_T c16_iH;
  (void)chartInstance;
  for (c16_i = 0; c16_i < 24; c16_i++) {
    c16_dataTables[c16_i].SimMin = rtInf;
    c16_dataTables[c16_i].SimMax = rtMinusInf;
    c16_dataTables[c16_i].OverflowWraps = 0;
    c16_dataTables[c16_i].Saturations = 0;
    c16_dataTables[c16_i].IsAlwaysInteger = true;
    c16_dataTables[c16_i].HistogramTable = &c16_histTables[c16_i];
    c16_histTables[c16_i].NumberOfZeros = 0.0;
    c16_histTables[c16_i].NumberOfPositiveValues = 0.0;
    c16_histTables[c16_i].NumberOfNegativeValues = 0.0;
    c16_histTables[c16_i].TotalNumberOfValues = 0.0;
    c16_histTables[c16_i].SimSum = 0.0;
    for (c16_iH = 0; c16_iH < 256; c16_iH++) {
      c16_histTables[c16_i].HistogramOfPositiveValues[c16_iH] = 0.0;
      c16_histTables[c16_i].HistogramOfNegativeValues[c16_iH] = 0.0;
    }
  }
}

const mxArray *sf_c16_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c16_nameCaptureInfo = NULL;
  c16_nameCaptureInfo = NULL;
  sf_mex_assign(&c16_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c16_nameCaptureInfo;
}

static uint16_T c16_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c16_sp, const mxArray *c16_b_cont, const
  char_T *c16_identifier)
{
  uint16_T c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = (const char *)c16_identifier;
  c16_thisId.fParent = NULL;
  c16_thisId.bParentIsCell = false;
  c16_y = c16_b_emlrt_marshallIn(chartInstance, c16_sp, sf_mex_dup(c16_b_cont),
    &c16_thisId);
  sf_mex_destroy(&c16_b_cont);
  return c16_y;
}

static uint16_T c16_b_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c16_sp, const mxArray *c16_u, const
  emlrtMsgIdentifier *c16_parentId)
{
  uint16_T c16_y;
  const mxArray *c16_mxFi = NULL;
  const mxArray *c16_mxInt = NULL;
  uint16_T c16_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c16_parentId, c16_u, false, 0U, NULL, c16_eml_mx, c16_b_eml_mx);
  sf_mex_assign(&c16_mxFi, sf_mex_dup(c16_u), false);
  sf_mex_assign(&c16_mxInt, sf_mex_call(c16_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c16_mxFi)), false);
  sf_mex_import(c16_parentId, sf_mex_dup(c16_mxInt), &c16_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c16_mxFi);
  sf_mex_destroy(&c16_mxInt);
  c16_y = c16_b_u;
  sf_mex_destroy(&c16_mxFi);
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static uint8_T c16_c_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c16_sp, const mxArray *c16_b_out_init, const
  char_T *c16_identifier)
{
  uint8_T c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = (const char *)c16_identifier;
  c16_thisId.fParent = NULL;
  c16_thisId.bParentIsCell = false;
  c16_y = c16_d_emlrt_marshallIn(chartInstance, c16_sp, sf_mex_dup
    (c16_b_out_init), &c16_thisId);
  sf_mex_destroy(&c16_b_out_init);
  return c16_y;
}

static uint8_T c16_d_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c16_sp, const mxArray *c16_u, const
  emlrtMsgIdentifier *c16_parentId)
{
  uint8_T c16_y;
  const mxArray *c16_mxFi = NULL;
  const mxArray *c16_mxInt = NULL;
  uint8_T c16_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c16_parentId, c16_u, false, 0U, NULL, c16_eml_mx, c16_c_eml_mx);
  sf_mex_assign(&c16_mxFi, sf_mex_dup(c16_u), false);
  sf_mex_assign(&c16_mxInt, sf_mex_call(c16_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c16_mxFi)), false);
  sf_mex_import(c16_parentId, sf_mex_dup(c16_mxInt), &c16_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c16_mxFi);
  sf_mex_destroy(&c16_mxInt);
  c16_y = c16_b_u;
  sf_mex_destroy(&c16_mxFi);
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static uint8_T c16_e_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c16_b_is_active_c16_PWM_28_HalfB, const char_T *
  c16_identifier)
{
  uint8_T c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = (const char *)c16_identifier;
  c16_thisId.fParent = NULL;
  c16_thisId.bParentIsCell = false;
  c16_y = c16_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c16_b_is_active_c16_PWM_28_HalfB), &c16_thisId);
  sf_mex_destroy(&c16_b_is_active_c16_PWM_28_HalfB);
  return c16_y;
}

static uint8_T c16_f_emlrt_marshallIn(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  uint8_T c16_y;
  uint8_T c16_b_u;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_b_u, 1, 3, 0U, 0, 0U, 0);
  c16_y = c16_b_u;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static const mxArray *c16_chart_data_browse_helper
  (SFc16_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c16_ssIdNumber)
{
  const mxArray *c16_mxData = NULL;
  uint8_T c16_u;
  int16_T c16_i;
  int16_T c16_i1;
  int16_T c16_i2;
  uint16_T c16_u1;
  uint8_T c16_u2;
  uint8_T c16_u3;
  real_T c16_d;
  real_T c16_d1;
  real_T c16_d2;
  real_T c16_d3;
  real_T c16_d4;
  real_T c16_d5;
  c16_mxData = NULL;
  switch (c16_ssIdNumber) {
   case 18U:
    c16_u = *chartInstance->c16_enable;
    c16_d = (real_T)c16_u;
    sf_mex_assign(&c16_mxData, sf_mex_create("mxData", &c16_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c16_i = *chartInstance->c16_offset;
    sf_mex_assign(&c16_mxData, sf_mex_create("mxData", &c16_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c16_i1 = *chartInstance->c16_max;
    c16_d1 = (real_T)c16_i1;
    sf_mex_assign(&c16_mxData, sf_mex_create("mxData", &c16_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c16_i2 = *chartInstance->c16_sum;
    c16_d2 = (real_T)c16_i2;
    sf_mex_assign(&c16_mxData, sf_mex_create("mxData", &c16_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c16_u1 = *chartInstance->c16_cont;
    c16_d3 = (real_T)c16_u1;
    sf_mex_assign(&c16_mxData, sf_mex_create("mxData", &c16_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c16_u2 = *chartInstance->c16_init;
    c16_d4 = (real_T)c16_u2;
    sf_mex_assign(&c16_mxData, sf_mex_create("mxData", &c16_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c16_u3 = *chartInstance->c16_out_init;
    c16_d5 = (real_T)c16_u3;
    sf_mex_assign(&c16_mxData, sf_mex_create("mxData", &c16_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c16_mxData;
}

static int32_T c16__s32_add__(SFc16_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c16_b, int32_T c16_c, int32_T c16_EMLOvCount_src_loc, uint32_T
  c16_ssid_src_loc, int32_T c16_offset_src_loc, int32_T c16_length_src_loc)
{
  int32_T c16_a;
  int32_T c16_PICOffset;
  real_T c16_d;
  observerLogReadPIC(&c16_PICOffset);
  c16_a = c16_b + c16_c;
  if (((c16_a ^ c16_b) & (c16_a ^ c16_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c16_ssid_src_loc,
      c16_offset_src_loc, c16_length_src_loc);
    c16_d = 1.0;
    observerLog(c16_EMLOvCount_src_loc + c16_PICOffset, &c16_d, 1);
  }

  return c16_a;
}

static void init_dsm_address_info(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc16_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c16_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c16_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c16_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c16_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c16_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c16_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c16_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c16_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c16_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c16_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c16_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c16_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c16_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c16_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyyoVl8QLhvvJFFv"
    "EdiTpoTwlwQAADr7CCT"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c16_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c16_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c16_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c16_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c16_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c16_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c16_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c16_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc16_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c16_PWM_28_HalfB
      ((SFc16_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c16_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c16_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c16_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc16_PWM_28_HalfB((SFc16_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c16_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV91uE0cUXhsHgdpGgZtKqFK5a68QqQriCkLWdmPJJi4bfu6syeyxd+TZmWV+nKTP0ZeAJ+C",
    "Si75A73pR8QZIPELPrNeOs95dE1wikBhpvZrZ73xz/mfs1To9D8cmPs+ve95lfF/Bp+5Nx0Y2ry",
    "080/WG91M2f4BCwsZ9okisvcohSAyPQUtuDZOiI4ayEMbEEBQIithEKlPGpllsORPjthXU8elnE",
    "aNREEnLw12UJeG+4CfIlljTR54mU0BNGyA0kZJ2FLU5Gc01VubIj4COtY2rTNBgAps4tXTPcsMS",
    "Dq1joB2hDUGN9alugSEGfHNcaqazVAczoIwTzogotDYiOoAEHWzgSRLi7741aFQeRiOizC5EZAK",
    "6y8YppxSQ52QaPxwyQYxUjPBWzH0nuKxbn6M+PRkCr3AI6rargIwTyYQpj3/QRktbghxyaMKhHZ",
    "WzBfDCuuA/ZXAEqtRvQ19OQJER7IvSTVOHtI7TaM2zZBlmWAxPiXpIMX4awtLsxczRAcE4wQFKl",
    "MEgNbKjDxSboHtL2WzccZm5qmRsPA22XgVL2VoTqIrCnK1NhU8416WwA5l0YQI8ZW0SQ6phU9Zi",
    "nNYsPJDoYJfe5dVgBcPAZzBfipAVhmuSA6R95xE2lrNIarWRsY/J2+x2lz8vwzrCgBoSCkVdQBG",
    "mAX2WurecLWTaxR6BqJVJ1SsCTzNkFcrTQyuaR1KN0ScVTeTUBBfRUmCsRxhLrIQnGoumCuZiuQ",
    "pHCY0gdA2Gcehh2SC2wCfatbaHWHcTZk6aoKliSVFU3flz2zs9f779gPNnJpd//7zAUyvg8RbeD",
    "n9vAX+1fha/kdu3PltzI5PfWZD/LrdfIyfvcFuucv7869XLa++++ff313933oLJ25/Xo7akR82b",
    "7Z9cOt+5vZnNf5g1yHnCT5byzGH3FvRqFPB/v8C/lc31r3vtoDd+vv3i8Tb8pgz/I1birp/yval",
    "X63spp+9s/abr1CdJ2ne1op0wu1C4ObHTYzYfz8sr/HE1W5+O9w/Wk7+1k49jkb8aZ/zV8KgUZr",
    "MkHy9W/zs7efki/a/k4u3m0poBE+x/suPmznry0/37K+y4kbPjRnqvGBDXrWBAt+8O+s96g1/uD",
    "fYIH+4u95mPrdfzynkXLPel6PnVL5/evg85hzc+Uq6+5rl/UXLr2nfe+8jnhq86z7wcfusztmPd",
    "e+Knxv/jne8e92M2vz//i+VHjIcFt+3scxfIsOjrBdj3Hy46oIA=",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c16_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c16_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c16_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c16_PWM_28_HalfB(SimStruct *S)
{
  SFc16_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc16_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc16_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc16_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c16_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c16_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c16_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c16_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c16_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c16_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c16_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c16_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c16_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c16_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c16_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c16_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c16_JITStateAnimation,
    chartInstance->c16_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c16_PWM_28_HalfB(chartInstance);
}

void c16_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c16_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c16_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c16_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c16_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
