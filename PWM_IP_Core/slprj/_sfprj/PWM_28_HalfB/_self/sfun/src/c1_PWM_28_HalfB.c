/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c1_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c1_eml_mx;
static const mxArray *c1_b_eml_mx;
static const mxArray *c1_c_eml_mx;

/* Function Declarations */
static void initialize_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void enable_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c1_update_jit_animation_state_c1_PWM_28_HalfB
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance);
static void c1_do_animation_call_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void ext_mode_exec_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c1_PWM_28_HalfB
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c1_st);
static void sf_gateway_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c1_PWM_28_HalfB
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c1_PWM_28_HalfB
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c1_emlrt_update_log_1(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index);
static int16_T c1_emlrt_update_log_2(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index);
static int16_T c1_emlrt_update_log_3(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index);
static boolean_T c1_emlrt_update_log_4(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index);
static uint16_T c1_emlrt_update_log_5(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index);
static int32_T c1_emlrt_update_log_6(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index);
static void c1_emlrtInitVarDataTables(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c1_dataTables[24],
  emlrtLocationLoggingHistogramType c1_histTables[24]);
static uint16_T c1_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c1_sp, const mxArray *c1_b_cont, const
  char_T *c1_identifier);
static uint16_T c1_b_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c1_sp, const mxArray *c1_u, const
  emlrtMsgIdentifier *c1_parentId);
static uint8_T c1_c_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c1_sp, const mxArray *c1_b_out_init, const
  char_T *c1_identifier);
static uint8_T c1_d_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c1_sp, const mxArray *c1_u, const
  emlrtMsgIdentifier *c1_parentId);
static uint8_T c1_e_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c1_b_is_active_c1_PWM_28_HalfB, const char_T
  *c1_identifier);
static uint8_T c1_f_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static const mxArray *c1_chart_data_browse_helper
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c1_ssIdNumber);
static int32_T c1__s32_add__(SFc1_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c1_b, int32_T c1_c, int32_T c1_EMLOvCount_src_loc, uint32_T
  c1_ssid_src_loc, int32_T c1_offset_src_loc, int32_T c1_length_src_loc);
static void init_dsm_address_info(SFc1_PWM_28_HalfBInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c1_st = { NULL,           /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c1_st.tls = chartInstance->c1_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c1_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_is_active_c1_PWM_28_HalfB = 0U;
  sf_mex_assign(&c1_c_eml_mx, sf_mex_call(&c1_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c1_b_eml_mx, sf_mex_call(&c1_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c1_eml_mx, sf_mex_call(&c1_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c1_emlrtLocLogSimulated = false;
  c1_emlrtInitVarDataTables(chartInstance,
    chartInstance->c1_emlrtLocationLoggingDataTables,
    chartInstance->c1_emlrtLocLogHistTables);
}

static void initialize_params_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c1_update_jit_animation_state_c1_PWM_28_HalfB
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_do_animation_call_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c1_PWM_28_HalfB
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c1_st;
  const mxArray *c1_y = NULL;
  const mxArray *c1_b_y = NULL;
  uint16_T c1_u;
  const mxArray *c1_c_y = NULL;
  const mxArray *c1_d_y = NULL;
  uint8_T c1_b_u;
  const mxArray *c1_e_y = NULL;
  const mxArray *c1_f_y = NULL;
  c1_st = NULL;
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(3, 1), false);
  c1_b_y = NULL;
  c1_u = *chartInstance->c1_cont;
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", &c1_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_b_y, sf_mex_create_fi(sf_mex_dup(c1_eml_mx), sf_mex_dup
    (c1_b_eml_mx), "simulinkarray", c1_c_y, false, false), false);
  sf_mex_setcell(c1_y, 0, c1_b_y);
  c1_d_y = NULL;
  c1_b_u = *chartInstance->c1_out_init;
  c1_e_y = NULL;
  sf_mex_assign(&c1_e_y, sf_mex_create("y", &c1_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_d_y, sf_mex_create_fi(sf_mex_dup(c1_eml_mx), sf_mex_dup
    (c1_c_eml_mx), "simulinkarray", c1_e_y, false, false), false);
  sf_mex_setcell(c1_y, 1, c1_d_y);
  c1_f_y = NULL;
  sf_mex_assign(&c1_f_y, sf_mex_create("y",
    &chartInstance->c1_is_active_c1_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c1_y, 2, c1_f_y);
  sf_mex_assign(&c1_st, c1_y, false);
  return c1_st;
}

static void set_sim_state_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c1_st)
{
  emlrtStack c1_b_st = { NULL,         /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c1_u;
  c1_b_st.tls = chartInstance->c1_fEmlrtCtx;
  chartInstance->c1_doneDoubleBufferReInit = true;
  c1_u = sf_mex_dup(c1_st);
  *chartInstance->c1_cont = c1_emlrt_marshallIn(chartInstance, &c1_b_st,
    sf_mex_dup(sf_mex_getcell(c1_u, 0)), "cont");
  *chartInstance->c1_out_init = c1_c_emlrt_marshallIn(chartInstance, &c1_b_st,
    sf_mex_dup(sf_mex_getcell(c1_u, 1)), "out_init");
  chartInstance->c1_is_active_c1_PWM_28_HalfB = c1_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 2)),
     "is_active_c1_PWM_28_HalfB");
  sf_mex_destroy(&c1_u);
  sf_mex_destroy(&c1_st);
}

static void sf_gateway_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c1_PICOffset;
  uint8_T c1_b_enable;
  int16_T c1_b_offset;
  int16_T c1_b_max;
  int16_T c1_b_sum;
  uint8_T c1_b_init;
  uint8_T c1_a0;
  uint8_T c1_a;
  uint8_T c1_b_a0;
  uint8_T c1_a1;
  uint8_T c1_b_a1;
  boolean_T c1_c;
  int8_T c1_i;
  int8_T c1_i1;
  real_T c1_d;
  uint8_T c1_c_a0;
  uint16_T c1_b_cont;
  uint8_T c1_b_a;
  uint8_T c1_b_out_init;
  uint8_T c1_d_a0;
  uint8_T c1_c_a1;
  uint8_T c1_d_a1;
  boolean_T c1_b_c;
  int8_T c1_i2;
  int8_T c1_i3;
  real_T c1_d1;
  int16_T c1_varargin_1;
  int16_T c1_b_varargin_1;
  int16_T c1_c_varargin_1;
  int16_T c1_d_varargin_1;
  int16_T c1_var1;
  int16_T c1_b_var1;
  int16_T c1_i4;
  int16_T c1_i5;
  boolean_T c1_covSaturation;
  boolean_T c1_b_covSaturation;
  uint16_T c1_hfi;
  uint16_T c1_b_hfi;
  uint16_T c1_u;
  uint16_T c1_u1;
  int16_T c1_e_varargin_1;
  int16_T c1_f_varargin_1;
  int16_T c1_g_varargin_1;
  int16_T c1_h_varargin_1;
  int16_T c1_c_var1;
  int16_T c1_d_var1;
  int16_T c1_i6;
  int16_T c1_i7;
  boolean_T c1_c_covSaturation;
  boolean_T c1_d_covSaturation;
  uint16_T c1_c_hfi;
  uint16_T c1_d_hfi;
  uint16_T c1_u2;
  uint16_T c1_u3;
  uint16_T c1_e_a0;
  uint16_T c1_f_a0;
  uint16_T c1_b0;
  uint16_T c1_b_b0;
  uint16_T c1_c_a;
  uint16_T c1_d_a;
  uint16_T c1_b;
  uint16_T c1_b_b;
  uint16_T c1_g_a0;
  uint16_T c1_h_a0;
  uint16_T c1_c_b0;
  uint16_T c1_d_b0;
  uint16_T c1_e_a1;
  uint16_T c1_f_a1;
  uint16_T c1_b1;
  uint16_T c1_b_b1;
  uint16_T c1_g_a1;
  uint16_T c1_h_a1;
  uint16_T c1_c_b1;
  uint16_T c1_d_b1;
  boolean_T c1_c_c;
  boolean_T c1_d_c;
  int16_T c1_i8;
  int16_T c1_i9;
  int16_T c1_i10;
  int16_T c1_i11;
  int16_T c1_i12;
  int16_T c1_i13;
  int16_T c1_i14;
  int16_T c1_i15;
  int16_T c1_i16;
  int16_T c1_i17;
  int16_T c1_i18;
  int16_T c1_i19;
  int32_T c1_i20;
  int32_T c1_i21;
  int16_T c1_i22;
  int16_T c1_i23;
  int16_T c1_i24;
  int16_T c1_i25;
  int16_T c1_i26;
  int16_T c1_i27;
  int16_T c1_i28;
  int16_T c1_i29;
  int16_T c1_i30;
  int16_T c1_i31;
  int16_T c1_i32;
  int16_T c1_i33;
  int32_T c1_i34;
  int32_T c1_i35;
  int16_T c1_i36;
  int16_T c1_i37;
  int16_T c1_i38;
  int16_T c1_i39;
  int16_T c1_i40;
  int16_T c1_i41;
  int16_T c1_i42;
  int16_T c1_i43;
  real_T c1_d2;
  real_T c1_d3;
  int16_T c1_i_varargin_1;
  int16_T c1_i_a0;
  int16_T c1_j_varargin_1;
  int16_T c1_e_b0;
  int16_T c1_e_var1;
  int16_T c1_k_varargin_1;
  int16_T c1_i44;
  int16_T c1_v;
  boolean_T c1_e_covSaturation;
  int16_T c1_val;
  int16_T c1_c_b;
  int32_T c1_i45;
  uint16_T c1_e_hfi;
  real_T c1_d4;
  int32_T c1_i46;
  real_T c1_d5;
  real_T c1_d6;
  real_T c1_d7;
  int32_T c1_i47;
  int32_T c1_i48;
  int32_T c1_i49;
  real_T c1_d8;
  real_T c1_d9;
  int32_T c1_e_c;
  int32_T c1_l_varargin_1;
  int32_T c1_m_varargin_1;
  int32_T c1_f_var1;
  int32_T c1_i50;
  boolean_T c1_f_covSaturation;
  uint16_T c1_f_hfi;
  observerLogReadPIC(&c1_PICOffset);
  chartInstance->c1_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                    *chartInstance->c1_init);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                    *chartInstance->c1_sum);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                    *chartInstance->c1_max);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                    *chartInstance->c1_offset);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 0U, (real_T)
                    *chartInstance->c1_enable);
  chartInstance->c1_sfEvent = CALL_EVENT;
  c1_b_enable = *chartInstance->c1_enable;
  c1_b_offset = *chartInstance->c1_offset;
  c1_b_max = *chartInstance->c1_max;
  c1_b_sum = *chartInstance->c1_sum;
  c1_b_init = *chartInstance->c1_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c1_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c1_emlrt_update_log_1(chartInstance, c1_b_enable,
                        chartInstance->c1_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c1_emlrt_update_log_2(chartInstance, c1_b_offset,
                        chartInstance->c1_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c1_emlrt_update_log_3(chartInstance, c1_b_max,
                        chartInstance->c1_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c1_emlrt_update_log_3(chartInstance, c1_b_sum,
                        chartInstance->c1_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c1_emlrt_update_log_1(chartInstance, c1_b_init,
                        chartInstance->c1_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c1_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c1_covrtInstance, 4U, 0, 0, false);
  c1_a0 = c1_b_enable;
  c1_a = c1_a0;
  c1_b_a0 = c1_a;
  c1_a1 = c1_b_a0;
  c1_b_a1 = c1_a1;
  c1_c = (c1_b_a1 == 0);
  c1_i = (int8_T)c1_b_enable;
  if ((int8_T)(c1_i & 2) != 0) {
    c1_i1 = (int8_T)(c1_i | -2);
  } else {
    c1_i1 = (int8_T)(c1_i & 1);
  }

  if (c1_i1 > 0) {
    c1_d = 3.0;
  } else {
    c1_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c1_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        4U, 0U, 0U, c1_d, 0.0, -2, 0U, (int32_T)c1_emlrt_update_log_4
        (chartInstance, c1_c, chartInstance->c1_emlrtLocationLoggingDataTables,
         5)))) {
    c1_b_cont = c1_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c1_emlrtLocationLoggingDataTables, 6);
    c1_b_out_init = c1_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c1_emlrtLocationLoggingDataTables, 7);
  } else {
    c1_c_a0 = c1_b_init;
    c1_b_a = c1_c_a0;
    c1_d_a0 = c1_b_a;
    c1_c_a1 = c1_d_a0;
    c1_d_a1 = c1_c_a1;
    c1_b_c = (c1_d_a1 == 0);
    c1_i2 = (int8_T)c1_b_init;
    if ((int8_T)(c1_i2 & 2) != 0) {
      c1_i3 = (int8_T)(c1_i2 | -2);
    } else {
      c1_i3 = (int8_T)(c1_i2 & 1);
    }

    if (c1_i3 > 0) {
      c1_d1 = 3.0;
    } else {
      c1_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c1_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c1_covrtInstance, 4U, 0U, 1U, c1_d1, 0.0,
                        -2, 0U, (int32_T)c1_emlrt_update_log_4(chartInstance,
           c1_b_c, chartInstance->c1_emlrtLocationLoggingDataTables, 8)))) {
      c1_b_varargin_1 = c1_b_sum;
      c1_d_varargin_1 = c1_b_varargin_1;
      c1_b_var1 = c1_d_varargin_1;
      c1_i5 = c1_b_var1;
      c1_b_covSaturation = false;
      if (c1_i5 < 0) {
        c1_i5 = 0;
      } else {
        if (c1_i5 > 4095) {
          c1_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 0, 0, 0,
          c1_b_covSaturation);
      }

      c1_b_hfi = (uint16_T)c1_i5;
      c1_u1 = c1_emlrt_update_log_5(chartInstance, c1_b_hfi,
        chartInstance->c1_emlrtLocationLoggingDataTables, 10);
      c1_f_varargin_1 = c1_b_max;
      c1_h_varargin_1 = c1_f_varargin_1;
      c1_d_var1 = c1_h_varargin_1;
      c1_i7 = c1_d_var1;
      c1_d_covSaturation = false;
      if (c1_i7 < 0) {
        c1_i7 = 0;
      } else {
        if (c1_i7 > 4095) {
          c1_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 0, 1, 0,
          c1_d_covSaturation);
      }

      c1_d_hfi = (uint16_T)c1_i7;
      c1_u3 = c1_emlrt_update_log_5(chartInstance, c1_d_hfi,
        chartInstance->c1_emlrtLocationLoggingDataTables, 11);
      c1_f_a0 = c1_u1;
      c1_b_b0 = c1_u3;
      c1_d_a = c1_f_a0;
      c1_b_b = c1_b_b0;
      c1_h_a0 = c1_d_a;
      c1_d_b0 = c1_b_b;
      c1_f_a1 = c1_h_a0;
      c1_b_b1 = c1_d_b0;
      c1_h_a1 = c1_f_a1;
      c1_d_b1 = c1_b_b1;
      c1_d_c = (c1_h_a1 < c1_d_b1);
      c1_i9 = (int16_T)c1_u1;
      c1_i11 = (int16_T)c1_u3;
      c1_i13 = (int16_T)c1_u3;
      c1_i15 = (int16_T)c1_u1;
      if ((int16_T)(c1_i13 & 4096) != 0) {
        c1_i17 = (int16_T)(c1_i13 | -4096);
      } else {
        c1_i17 = (int16_T)(c1_i13 & 4095);
      }

      if ((int16_T)(c1_i15 & 4096) != 0) {
        c1_i19 = (int16_T)(c1_i15 | -4096);
      } else {
        c1_i19 = (int16_T)(c1_i15 & 4095);
      }

      c1_i21 = c1_i17 - c1_i19;
      if (c1_i21 > 4095) {
        c1_i21 = 4095;
      } else {
        if (c1_i21 < -4096) {
          c1_i21 = -4096;
        }
      }

      c1_i23 = (int16_T)c1_u1;
      c1_i25 = (int16_T)c1_u3;
      c1_i27 = (int16_T)c1_u1;
      c1_i29 = (int16_T)c1_u3;
      if ((int16_T)(c1_i27 & 4096) != 0) {
        c1_i31 = (int16_T)(c1_i27 | -4096);
      } else {
        c1_i31 = (int16_T)(c1_i27 & 4095);
      }

      if ((int16_T)(c1_i29 & 4096) != 0) {
        c1_i33 = (int16_T)(c1_i29 | -4096);
      } else {
        c1_i33 = (int16_T)(c1_i29 & 4095);
      }

      c1_i35 = c1_i31 - c1_i33;
      if (c1_i35 > 4095) {
        c1_i35 = 4095;
      } else {
        if (c1_i35 < -4096) {
          c1_i35 = -4096;
        }
      }

      if ((int16_T)(c1_i9 & 4096) != 0) {
        c1_i37 = (int16_T)(c1_i9 | -4096);
      } else {
        c1_i37 = (int16_T)(c1_i9 & 4095);
      }

      if ((int16_T)(c1_i11 & 4096) != 0) {
        c1_i39 = (int16_T)(c1_i11 | -4096);
      } else {
        c1_i39 = (int16_T)(c1_i11 & 4095);
      }

      if ((int16_T)(c1_i23 & 4096) != 0) {
        c1_i41 = (int16_T)(c1_i23 | -4096);
      } else {
        c1_i41 = (int16_T)(c1_i23 & 4095);
      }

      if ((int16_T)(c1_i25 & 4096) != 0) {
        c1_i43 = (int16_T)(c1_i25 | -4096);
      } else {
        c1_i43 = (int16_T)(c1_i25 & 4095);
      }

      if (c1_i37 < c1_i39) {
        c1_d3 = (real_T)((int16_T)c1_i21 <= 1);
      } else if (c1_i41 > c1_i43) {
        if ((int16_T)c1_i35 <= 1) {
          c1_d3 = 3.0;
        } else {
          c1_d3 = 0.0;
        }
      } else {
        c1_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c1_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c1_covrtInstance, 4U, 0U, 2U, c1_d3,
                          0.0, -2, 2U, (int32_T)c1_emlrt_update_log_4
                          (chartInstance, c1_d_c,
                           chartInstance->c1_emlrtLocationLoggingDataTables, 9))))
      {
        c1_i_a0 = c1_b_sum;
        c1_e_b0 = c1_b_offset;
        c1_k_varargin_1 = c1_e_b0;
        c1_v = c1_k_varargin_1;
        c1_val = c1_v;
        c1_c_b = c1_val;
        c1_i45 = c1_i_a0;
        if (c1_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c1_d4 = 1.0;
          observerLog(9 + c1_PICOffset, &c1_d4, 1);
        }

        if (c1_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c1_d5 = 1.0;
          observerLog(9 + c1_PICOffset, &c1_d5, 1);
        }

        c1_i46 = c1_c_b;
        if (c1_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c1_d6 = 1.0;
          observerLog(12 + c1_PICOffset, &c1_d6, 1);
        }

        if (c1_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c1_d7 = 1.0;
          observerLog(12 + c1_PICOffset, &c1_d7, 1);
        }

        if ((c1_i45 & 65536) != 0) {
          c1_i47 = c1_i45 | -65536;
        } else {
          c1_i47 = c1_i45 & 65535;
        }

        if ((c1_i46 & 65536) != 0) {
          c1_i48 = c1_i46 | -65536;
        } else {
          c1_i48 = c1_i46 & 65535;
        }

        c1_i49 = c1__s32_add__(chartInstance, c1_i47, c1_i48, 11, 1U, 323, 12);
        if (c1_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c1_d8 = 1.0;
          observerLog(17 + c1_PICOffset, &c1_d8, 1);
        }

        if (c1_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c1_d9 = 1.0;
          observerLog(17 + c1_PICOffset, &c1_d9, 1);
        }

        if ((c1_i49 & 65536) != 0) {
          c1_e_c = c1_i49 | -65536;
        } else {
          c1_e_c = c1_i49 & 65535;
        }

        c1_l_varargin_1 = c1_emlrt_update_log_6(chartInstance, c1_e_c,
          chartInstance->c1_emlrtLocationLoggingDataTables, 13);
        c1_m_varargin_1 = c1_l_varargin_1;
        c1_f_var1 = c1_m_varargin_1;
        c1_i50 = c1_f_var1;
        c1_f_covSaturation = false;
        if (c1_i50 < 0) {
          c1_i50 = 0;
        } else {
          if (c1_i50 > 4095) {
            c1_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 0, 2, 0,
            c1_f_covSaturation);
        }

        c1_f_hfi = (uint16_T)c1_i50;
        c1_b_cont = c1_emlrt_update_log_5(chartInstance, c1_f_hfi,
          chartInstance->c1_emlrtLocationLoggingDataTables, 12);
        c1_b_out_init = c1_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c1_emlrtLocationLoggingDataTables, 14);
      } else {
        c1_b_cont = c1_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c1_emlrtLocationLoggingDataTables, 15);
        c1_b_out_init = c1_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c1_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c1_varargin_1 = c1_b_sum;
      c1_c_varargin_1 = c1_varargin_1;
      c1_var1 = c1_c_varargin_1;
      c1_i4 = c1_var1;
      c1_covSaturation = false;
      if (c1_i4 < 0) {
        c1_i4 = 0;
      } else {
        if (c1_i4 > 4095) {
          c1_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 0, 3, 0,
          c1_covSaturation);
      }

      c1_hfi = (uint16_T)c1_i4;
      c1_u = c1_emlrt_update_log_5(chartInstance, c1_hfi,
        chartInstance->c1_emlrtLocationLoggingDataTables, 18);
      c1_e_varargin_1 = c1_b_max;
      c1_g_varargin_1 = c1_e_varargin_1;
      c1_c_var1 = c1_g_varargin_1;
      c1_i6 = c1_c_var1;
      c1_c_covSaturation = false;
      if (c1_i6 < 0) {
        c1_i6 = 0;
      } else {
        if (c1_i6 > 4095) {
          c1_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 0, 4, 0,
          c1_c_covSaturation);
      }

      c1_c_hfi = (uint16_T)c1_i6;
      c1_u2 = c1_emlrt_update_log_5(chartInstance, c1_c_hfi,
        chartInstance->c1_emlrtLocationLoggingDataTables, 19);
      c1_e_a0 = c1_u;
      c1_b0 = c1_u2;
      c1_c_a = c1_e_a0;
      c1_b = c1_b0;
      c1_g_a0 = c1_c_a;
      c1_c_b0 = c1_b;
      c1_e_a1 = c1_g_a0;
      c1_b1 = c1_c_b0;
      c1_g_a1 = c1_e_a1;
      c1_c_b1 = c1_b1;
      c1_c_c = (c1_g_a1 < c1_c_b1);
      c1_i8 = (int16_T)c1_u;
      c1_i10 = (int16_T)c1_u2;
      c1_i12 = (int16_T)c1_u2;
      c1_i14 = (int16_T)c1_u;
      if ((int16_T)(c1_i12 & 4096) != 0) {
        c1_i16 = (int16_T)(c1_i12 | -4096);
      } else {
        c1_i16 = (int16_T)(c1_i12 & 4095);
      }

      if ((int16_T)(c1_i14 & 4096) != 0) {
        c1_i18 = (int16_T)(c1_i14 | -4096);
      } else {
        c1_i18 = (int16_T)(c1_i14 & 4095);
      }

      c1_i20 = c1_i16 - c1_i18;
      if (c1_i20 > 4095) {
        c1_i20 = 4095;
      } else {
        if (c1_i20 < -4096) {
          c1_i20 = -4096;
        }
      }

      c1_i22 = (int16_T)c1_u;
      c1_i24 = (int16_T)c1_u2;
      c1_i26 = (int16_T)c1_u;
      c1_i28 = (int16_T)c1_u2;
      if ((int16_T)(c1_i26 & 4096) != 0) {
        c1_i30 = (int16_T)(c1_i26 | -4096);
      } else {
        c1_i30 = (int16_T)(c1_i26 & 4095);
      }

      if ((int16_T)(c1_i28 & 4096) != 0) {
        c1_i32 = (int16_T)(c1_i28 | -4096);
      } else {
        c1_i32 = (int16_T)(c1_i28 & 4095);
      }

      c1_i34 = c1_i30 - c1_i32;
      if (c1_i34 > 4095) {
        c1_i34 = 4095;
      } else {
        if (c1_i34 < -4096) {
          c1_i34 = -4096;
        }
      }

      if ((int16_T)(c1_i8 & 4096) != 0) {
        c1_i36 = (int16_T)(c1_i8 | -4096);
      } else {
        c1_i36 = (int16_T)(c1_i8 & 4095);
      }

      if ((int16_T)(c1_i10 & 4096) != 0) {
        c1_i38 = (int16_T)(c1_i10 | -4096);
      } else {
        c1_i38 = (int16_T)(c1_i10 & 4095);
      }

      if ((int16_T)(c1_i22 & 4096) != 0) {
        c1_i40 = (int16_T)(c1_i22 | -4096);
      } else {
        c1_i40 = (int16_T)(c1_i22 & 4095);
      }

      if ((int16_T)(c1_i24 & 4096) != 0) {
        c1_i42 = (int16_T)(c1_i24 | -4096);
      } else {
        c1_i42 = (int16_T)(c1_i24 & 4095);
      }

      if (c1_i36 < c1_i38) {
        c1_d2 = (real_T)((int16_T)c1_i20 <= 1);
      } else if (c1_i40 > c1_i42) {
        if ((int16_T)c1_i34 <= 1) {
          c1_d2 = 3.0;
        } else {
          c1_d2 = 0.0;
        }
      } else {
        c1_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c1_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c1_covrtInstance, 4U, 0U, 3U, c1_d2,
                          0.0, -2, 2U, (int32_T)c1_emlrt_update_log_4
                          (chartInstance, c1_c_c,
                           chartInstance->c1_emlrtLocationLoggingDataTables, 17))))
      {
        c1_i_varargin_1 = c1_b_sum;
        c1_j_varargin_1 = c1_i_varargin_1;
        c1_e_var1 = c1_j_varargin_1;
        c1_i44 = c1_e_var1;
        c1_e_covSaturation = false;
        if (c1_i44 < 0) {
          c1_i44 = 0;
        } else {
          if (c1_i44 > 4095) {
            c1_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 0, 5, 0,
            c1_e_covSaturation);
        }

        c1_e_hfi = (uint16_T)c1_i44;
        c1_b_cont = c1_emlrt_update_log_5(chartInstance, c1_e_hfi,
          chartInstance->c1_emlrtLocationLoggingDataTables, 20);
        c1_b_out_init = c1_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c1_emlrtLocationLoggingDataTables, 21);
      } else {
        c1_b_cont = c1_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c1_emlrtLocationLoggingDataTables, 22);
        c1_b_out_init = c1_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c1_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c1_cont = c1_b_cont;
  *chartInstance->c1_out_init = c1_b_out_init;
  c1_do_animation_call_c1_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                    *chartInstance->c1_cont);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                    *chartInstance->c1_out_init);
}

static void mdl_start_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c1_PWM_28_HalfB
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c1_decisionTxtStartIdx = 0U;
  static const uint32_T c1_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c1_chart_data_browse_helper);
  chartInstance->c1_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c1_RuntimeVar,
    &chartInstance->c1_IsDebuggerActive,
    &chartInstance->c1_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c1_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c1_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c1_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c1_decisionTxtStartIdx, &c1_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 0U, 0, NULL, NULL, 0U, NULL);
  covrtEmlInitFcn(chartInstance->c1_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 0U, 266, -1,
    279);
  covrtEmlSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 1U, 282, -1,
    295);
  covrtEmlSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 2U, 319, -1,
    341);
  covrtEmlSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 3U, 518, -1,
    531);
  covrtEmlSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 4U, 534, -1,
    547);
  covrtEmlSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 5U, 571, -1,
    584);
  covrtEmlIfInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 0U, 70, 86, -1, 121);
  covrtEmlIfInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c1_PWM_28_HalfB
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c1_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:6920",              /* mexFileName */
    "Thu May 27 10:27:15 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c1_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c1_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c1_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c1_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:6920");
    emlrtLocationLoggingPushLog(&c1_emlrtLocationLoggingFileInfo,
      c1_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c1_emlrtLocationLoggingDataTables, c1_emlrtLocationInfo,
      NULL, 0U, c1_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:6920");
  }

  sfListenerLightTerminate(chartInstance->c1_RuntimeVar);
  sf_mex_destroy(&c1_eml_mx);
  sf_mex_destroy(&c1_b_eml_mx);
  sf_mex_destroy(&c1_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c1_covrtInstance);
}

static void initSimStructsc1_PWM_28_HalfB(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c1_emlrt_update_log_1(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index)
{
  boolean_T c1_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c1_b_table;
  real_T c1_d;
  uint8_T c1_u;
  uint8_T c1_localMin;
  real_T c1_d1;
  uint8_T c1_u1;
  uint8_T c1_localMax;
  emlrtLocationLoggingHistogramType *c1_histTable;
  real_T c1_inDouble;
  real_T c1_significand;
  int32_T c1_exponent;
  (void)chartInstance;
  c1_isLoggingEnabledHere = (c1_index >= 0);
  if (c1_isLoggingEnabledHere) {
    c1_b_table = (emlrtLocationLoggingDataType *)&c1_table[c1_index];
    c1_d = c1_b_table[0U].SimMin;
    if (c1_d < 2.0) {
      if (c1_d >= 0.0) {
        c1_u = (uint8_T)c1_d;
      } else {
        c1_u = 0U;
      }
    } else if (c1_d >= 2.0) {
      c1_u = 1U;
    } else {
      c1_u = 0U;
    }

    c1_localMin = c1_u;
    c1_d1 = c1_b_table[0U].SimMax;
    if (c1_d1 < 2.0) {
      if (c1_d1 >= 0.0) {
        c1_u1 = (uint8_T)c1_d1;
      } else {
        c1_u1 = 0U;
      }
    } else if (c1_d1 >= 2.0) {
      c1_u1 = 1U;
    } else {
      c1_u1 = 0U;
    }

    c1_localMax = c1_u1;
    c1_histTable = c1_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c1_in < c1_localMin) {
      c1_localMin = c1_in;
    }

    if (c1_in > c1_localMax) {
      c1_localMax = c1_in;
    }

    /* Histogram logging. */
    c1_inDouble = (real_T)c1_in;
    c1_histTable->TotalNumberOfValues++;
    if (c1_inDouble == 0.0) {
      c1_histTable->NumberOfZeros++;
    } else {
      c1_histTable->SimSum += c1_inDouble;
      if ((!muDoubleScalarIsInf(c1_inDouble)) && (!muDoubleScalarIsNaN
           (c1_inDouble))) {
        c1_significand = frexp(c1_inDouble, &c1_exponent);
        if (c1_exponent > 128) {
          c1_exponent = 128;
        }

        if (c1_exponent < -127) {
          c1_exponent = -127;
        }

        if (c1_significand < 0.0) {
          c1_histTable->NumberOfNegativeValues++;
          c1_histTable->HistogramOfNegativeValues[127 + c1_exponent]++;
        } else {
          c1_histTable->NumberOfPositiveValues++;
          c1_histTable->HistogramOfPositiveValues[127 + c1_exponent]++;
        }
      }
    }

    c1_b_table[0U].SimMin = (real_T)c1_localMin;
    c1_b_table[0U].SimMax = (real_T)c1_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c1_in;
}

static int16_T c1_emlrt_update_log_2(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index)
{
  boolean_T c1_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c1_b_table;
  real_T c1_d;
  int16_T c1_i;
  int16_T c1_localMin;
  real_T c1_d1;
  int16_T c1_i1;
  int16_T c1_localMax;
  emlrtLocationLoggingHistogramType *c1_histTable;
  real_T c1_inDouble;
  real_T c1_significand;
  int32_T c1_exponent;
  (void)chartInstance;
  c1_isLoggingEnabledHere = (c1_index >= 0);
  if (c1_isLoggingEnabledHere) {
    c1_b_table = (emlrtLocationLoggingDataType *)&c1_table[c1_index];
    c1_d = muDoubleScalarFloor(c1_b_table[0U].SimMin);
    if (c1_d < 32768.0) {
      if (c1_d >= -32768.0) {
        c1_i = (int16_T)c1_d;
      } else {
        c1_i = MIN_int16_T;
      }
    } else if (c1_d >= 32768.0) {
      c1_i = MAX_int16_T;
    } else {
      c1_i = 0;
    }

    c1_localMin = c1_i;
    c1_d1 = muDoubleScalarFloor(c1_b_table[0U].SimMax);
    if (c1_d1 < 32768.0) {
      if (c1_d1 >= -32768.0) {
        c1_i1 = (int16_T)c1_d1;
      } else {
        c1_i1 = MIN_int16_T;
      }
    } else if (c1_d1 >= 32768.0) {
      c1_i1 = MAX_int16_T;
    } else {
      c1_i1 = 0;
    }

    c1_localMax = c1_i1;
    c1_histTable = c1_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c1_in < c1_localMin) {
      c1_localMin = c1_in;
    }

    if (c1_in > c1_localMax) {
      c1_localMax = c1_in;
    }

    /* Histogram logging. */
    c1_inDouble = (real_T)c1_in;
    c1_histTable->TotalNumberOfValues++;
    if (c1_inDouble == 0.0) {
      c1_histTable->NumberOfZeros++;
    } else {
      c1_histTable->SimSum += c1_inDouble;
      if ((!muDoubleScalarIsInf(c1_inDouble)) && (!muDoubleScalarIsNaN
           (c1_inDouble))) {
        c1_significand = frexp(c1_inDouble, &c1_exponent);
        if (c1_exponent > 128) {
          c1_exponent = 128;
        }

        if (c1_exponent < -127) {
          c1_exponent = -127;
        }

        if (c1_significand < 0.0) {
          c1_histTable->NumberOfNegativeValues++;
          c1_histTable->HistogramOfNegativeValues[127 + c1_exponent]++;
        } else {
          c1_histTable->NumberOfPositiveValues++;
          c1_histTable->HistogramOfPositiveValues[127 + c1_exponent]++;
        }
      }
    }

    c1_b_table[0U].SimMin = (real_T)c1_localMin;
    c1_b_table[0U].SimMax = (real_T)c1_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c1_in;
}

static int16_T c1_emlrt_update_log_3(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index)
{
  boolean_T c1_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c1_b_table;
  real_T c1_d;
  int16_T c1_i;
  int16_T c1_localMin;
  real_T c1_d1;
  int16_T c1_i1;
  int16_T c1_localMax;
  emlrtLocationLoggingHistogramType *c1_histTable;
  real_T c1_inDouble;
  real_T c1_significand;
  int32_T c1_exponent;
  (void)chartInstance;
  c1_isLoggingEnabledHere = (c1_index >= 0);
  if (c1_isLoggingEnabledHere) {
    c1_b_table = (emlrtLocationLoggingDataType *)&c1_table[c1_index];
    c1_d = muDoubleScalarFloor(c1_b_table[0U].SimMin);
    if (c1_d < 2048.0) {
      if (c1_d >= -2048.0) {
        c1_i = (int16_T)c1_d;
      } else {
        c1_i = -2048;
      }
    } else if (c1_d >= 2048.0) {
      c1_i = 2047;
    } else {
      c1_i = 0;
    }

    c1_localMin = c1_i;
    c1_d1 = muDoubleScalarFloor(c1_b_table[0U].SimMax);
    if (c1_d1 < 2048.0) {
      if (c1_d1 >= -2048.0) {
        c1_i1 = (int16_T)c1_d1;
      } else {
        c1_i1 = -2048;
      }
    } else if (c1_d1 >= 2048.0) {
      c1_i1 = 2047;
    } else {
      c1_i1 = 0;
    }

    c1_localMax = c1_i1;
    c1_histTable = c1_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c1_in < c1_localMin) {
      c1_localMin = c1_in;
    }

    if (c1_in > c1_localMax) {
      c1_localMax = c1_in;
    }

    /* Histogram logging. */
    c1_inDouble = (real_T)c1_in;
    c1_histTable->TotalNumberOfValues++;
    if (c1_inDouble == 0.0) {
      c1_histTable->NumberOfZeros++;
    } else {
      c1_histTable->SimSum += c1_inDouble;
      if ((!muDoubleScalarIsInf(c1_inDouble)) && (!muDoubleScalarIsNaN
           (c1_inDouble))) {
        c1_significand = frexp(c1_inDouble, &c1_exponent);
        if (c1_exponent > 128) {
          c1_exponent = 128;
        }

        if (c1_exponent < -127) {
          c1_exponent = -127;
        }

        if (c1_significand < 0.0) {
          c1_histTable->NumberOfNegativeValues++;
          c1_histTable->HistogramOfNegativeValues[127 + c1_exponent]++;
        } else {
          c1_histTable->NumberOfPositiveValues++;
          c1_histTable->HistogramOfPositiveValues[127 + c1_exponent]++;
        }
      }
    }

    c1_b_table[0U].SimMin = (real_T)c1_localMin;
    c1_b_table[0U].SimMax = (real_T)c1_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c1_in;
}

static boolean_T c1_emlrt_update_log_4(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index)
{
  boolean_T c1_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c1_b_table;
  boolean_T c1_localMin;
  boolean_T c1_localMax;
  emlrtLocationLoggingHistogramType *c1_histTable;
  real_T c1_inDouble;
  real_T c1_significand;
  int32_T c1_exponent;
  (void)chartInstance;
  c1_isLoggingEnabledHere = (c1_index >= 0);
  if (c1_isLoggingEnabledHere) {
    c1_b_table = (emlrtLocationLoggingDataType *)&c1_table[c1_index];
    c1_localMin = (c1_b_table[0U].SimMin > 0.0);
    c1_localMax = (c1_b_table[0U].SimMax > 0.0);
    c1_histTable = c1_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c1_in < (int32_T)c1_localMin) {
      c1_localMin = c1_in;
    }

    if ((int32_T)c1_in > (int32_T)c1_localMax) {
      c1_localMax = c1_in;
    }

    /* Histogram logging. */
    c1_inDouble = (real_T)c1_in;
    c1_histTable->TotalNumberOfValues++;
    if (c1_inDouble == 0.0) {
      c1_histTable->NumberOfZeros++;
    } else {
      c1_histTable->SimSum += c1_inDouble;
      if ((!muDoubleScalarIsInf(c1_inDouble)) && (!muDoubleScalarIsNaN
           (c1_inDouble))) {
        c1_significand = frexp(c1_inDouble, &c1_exponent);
        if (c1_exponent > 128) {
          c1_exponent = 128;
        }

        if (c1_exponent < -127) {
          c1_exponent = -127;
        }

        if (c1_significand < 0.0) {
          c1_histTable->NumberOfNegativeValues++;
          c1_histTable->HistogramOfNegativeValues[127 + c1_exponent]++;
        } else {
          c1_histTable->NumberOfPositiveValues++;
          c1_histTable->HistogramOfPositiveValues[127 + c1_exponent]++;
        }
      }
    }

    c1_b_table[0U].SimMin = (real_T)c1_localMin;
    c1_b_table[0U].SimMax = (real_T)c1_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c1_in;
}

static uint16_T c1_emlrt_update_log_5(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index)
{
  boolean_T c1_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c1_b_table;
  real_T c1_d;
  uint16_T c1_u;
  uint16_T c1_localMin;
  real_T c1_d1;
  uint16_T c1_u1;
  uint16_T c1_localMax;
  emlrtLocationLoggingHistogramType *c1_histTable;
  real_T c1_inDouble;
  real_T c1_significand;
  int32_T c1_exponent;
  (void)chartInstance;
  c1_isLoggingEnabledHere = (c1_index >= 0);
  if (c1_isLoggingEnabledHere) {
    c1_b_table = (emlrtLocationLoggingDataType *)&c1_table[c1_index];
    c1_d = c1_b_table[0U].SimMin;
    if (c1_d < 4096.0) {
      if (c1_d >= 0.0) {
        c1_u = (uint16_T)c1_d;
      } else {
        c1_u = 0U;
      }
    } else if (c1_d >= 4096.0) {
      c1_u = 4095U;
    } else {
      c1_u = 0U;
    }

    c1_localMin = c1_u;
    c1_d1 = c1_b_table[0U].SimMax;
    if (c1_d1 < 4096.0) {
      if (c1_d1 >= 0.0) {
        c1_u1 = (uint16_T)c1_d1;
      } else {
        c1_u1 = 0U;
      }
    } else if (c1_d1 >= 4096.0) {
      c1_u1 = 4095U;
    } else {
      c1_u1 = 0U;
    }

    c1_localMax = c1_u1;
    c1_histTable = c1_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c1_in < c1_localMin) {
      c1_localMin = c1_in;
    }

    if (c1_in > c1_localMax) {
      c1_localMax = c1_in;
    }

    /* Histogram logging. */
    c1_inDouble = (real_T)c1_in;
    c1_histTable->TotalNumberOfValues++;
    if (c1_inDouble == 0.0) {
      c1_histTable->NumberOfZeros++;
    } else {
      c1_histTable->SimSum += c1_inDouble;
      if ((!muDoubleScalarIsInf(c1_inDouble)) && (!muDoubleScalarIsNaN
           (c1_inDouble))) {
        c1_significand = frexp(c1_inDouble, &c1_exponent);
        if (c1_exponent > 128) {
          c1_exponent = 128;
        }

        if (c1_exponent < -127) {
          c1_exponent = -127;
        }

        if (c1_significand < 0.0) {
          c1_histTable->NumberOfNegativeValues++;
          c1_histTable->HistogramOfNegativeValues[127 + c1_exponent]++;
        } else {
          c1_histTable->NumberOfPositiveValues++;
          c1_histTable->HistogramOfPositiveValues[127 + c1_exponent]++;
        }
      }
    }

    c1_b_table[0U].SimMin = (real_T)c1_localMin;
    c1_b_table[0U].SimMax = (real_T)c1_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c1_in;
}

static int32_T c1_emlrt_update_log_6(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c1_in, emlrtLocationLoggingDataType c1_table[],
  int32_T c1_index)
{
  boolean_T c1_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c1_b_table;
  real_T c1_d;
  int32_T c1_i;
  int32_T c1_localMin;
  real_T c1_d1;
  int32_T c1_i1;
  int32_T c1_localMax;
  emlrtLocationLoggingHistogramType *c1_histTable;
  real_T c1_inDouble;
  real_T c1_significand;
  int32_T c1_exponent;
  (void)chartInstance;
  c1_isLoggingEnabledHere = (c1_index >= 0);
  if (c1_isLoggingEnabledHere) {
    c1_b_table = (emlrtLocationLoggingDataType *)&c1_table[c1_index];
    c1_d = muDoubleScalarFloor(c1_b_table[0U].SimMin);
    if (c1_d < 65536.0) {
      if (c1_d >= -65536.0) {
        c1_i = (int32_T)c1_d;
      } else {
        c1_i = -65536;
      }
    } else if (c1_d >= 65536.0) {
      c1_i = 65535;
    } else {
      c1_i = 0;
    }

    c1_localMin = c1_i;
    c1_d1 = muDoubleScalarFloor(c1_b_table[0U].SimMax);
    if (c1_d1 < 65536.0) {
      if (c1_d1 >= -65536.0) {
        c1_i1 = (int32_T)c1_d1;
      } else {
        c1_i1 = -65536;
      }
    } else if (c1_d1 >= 65536.0) {
      c1_i1 = 65535;
    } else {
      c1_i1 = 0;
    }

    c1_localMax = c1_i1;
    c1_histTable = c1_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c1_in < c1_localMin) {
      c1_localMin = c1_in;
    }

    if (c1_in > c1_localMax) {
      c1_localMax = c1_in;
    }

    /* Histogram logging. */
    c1_inDouble = (real_T)c1_in;
    c1_histTable->TotalNumberOfValues++;
    if (c1_inDouble == 0.0) {
      c1_histTable->NumberOfZeros++;
    } else {
      c1_histTable->SimSum += c1_inDouble;
      if ((!muDoubleScalarIsInf(c1_inDouble)) && (!muDoubleScalarIsNaN
           (c1_inDouble))) {
        c1_significand = frexp(c1_inDouble, &c1_exponent);
        if (c1_exponent > 128) {
          c1_exponent = 128;
        }

        if (c1_exponent < -127) {
          c1_exponent = -127;
        }

        if (c1_significand < 0.0) {
          c1_histTable->NumberOfNegativeValues++;
          c1_histTable->HistogramOfNegativeValues[127 + c1_exponent]++;
        } else {
          c1_histTable->NumberOfPositiveValues++;
          c1_histTable->HistogramOfPositiveValues[127 + c1_exponent]++;
        }
      }
    }

    c1_b_table[0U].SimMin = (real_T)c1_localMin;
    c1_b_table[0U].SimMax = (real_T)c1_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c1_in;
}

static void c1_emlrtInitVarDataTables(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c1_dataTables[24],
  emlrtLocationLoggingHistogramType c1_histTables[24])
{
  int32_T c1_i;
  int32_T c1_iH;
  (void)chartInstance;
  for (c1_i = 0; c1_i < 24; c1_i++) {
    c1_dataTables[c1_i].SimMin = rtInf;
    c1_dataTables[c1_i].SimMax = rtMinusInf;
    c1_dataTables[c1_i].OverflowWraps = 0;
    c1_dataTables[c1_i].Saturations = 0;
    c1_dataTables[c1_i].IsAlwaysInteger = true;
    c1_dataTables[c1_i].HistogramTable = &c1_histTables[c1_i];
    c1_histTables[c1_i].NumberOfZeros = 0.0;
    c1_histTables[c1_i].NumberOfPositiveValues = 0.0;
    c1_histTables[c1_i].NumberOfNegativeValues = 0.0;
    c1_histTables[c1_i].TotalNumberOfValues = 0.0;
    c1_histTables[c1_i].SimSum = 0.0;
    for (c1_iH = 0; c1_iH < 256; c1_iH++) {
      c1_histTables[c1_i].HistogramOfPositiveValues[c1_iH] = 0.0;
      c1_histTables[c1_i].HistogramOfNegativeValues[c1_iH] = 0.0;
    }
  }
}

const mxArray *sf_c1_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c1_nameCaptureInfo;
}

static uint16_T c1_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c1_sp, const mxArray *c1_b_cont, const
  char_T *c1_identifier)
{
  uint16_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, c1_sp, sf_mex_dup(c1_b_cont),
    &c1_thisId);
  sf_mex_destroy(&c1_b_cont);
  return c1_y;
}

static uint16_T c1_b_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c1_sp, const mxArray *c1_u, const
  emlrtMsgIdentifier *c1_parentId)
{
  uint16_T c1_y;
  const mxArray *c1_mxFi = NULL;
  const mxArray *c1_mxInt = NULL;
  uint16_T c1_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c1_parentId, c1_u, false, 0U, NULL, c1_eml_mx, c1_b_eml_mx);
  sf_mex_assign(&c1_mxFi, sf_mex_dup(c1_u), false);
  sf_mex_assign(&c1_mxInt, sf_mex_call(c1_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c1_mxFi)), false);
  sf_mex_import(c1_parentId, sf_mex_dup(c1_mxInt), &c1_b_u, 1, 5, 0U, 0, 0U, 0);
  sf_mex_destroy(&c1_mxFi);
  sf_mex_destroy(&c1_mxInt);
  c1_y = c1_b_u;
  sf_mex_destroy(&c1_mxFi);
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static uint8_T c1_c_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c1_sp, const mxArray *c1_b_out_init, const
  char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_d_emlrt_marshallIn(chartInstance, c1_sp, sf_mex_dup(c1_b_out_init),
    &c1_thisId);
  sf_mex_destroy(&c1_b_out_init);
  return c1_y;
}

static uint8_T c1_d_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c1_sp, const mxArray *c1_u, const
  emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  const mxArray *c1_mxFi = NULL;
  const mxArray *c1_mxInt = NULL;
  uint8_T c1_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c1_parentId, c1_u, false, 0U, NULL, c1_eml_mx, c1_c_eml_mx);
  sf_mex_assign(&c1_mxFi, sf_mex_dup(c1_u), false);
  sf_mex_assign(&c1_mxInt, sf_mex_call(c1_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c1_mxFi)), false);
  sf_mex_import(c1_parentId, sf_mex_dup(c1_mxInt), &c1_b_u, 1, 3, 0U, 0, 0U, 0);
  sf_mex_destroy(&c1_mxFi);
  sf_mex_destroy(&c1_mxInt);
  c1_y = c1_b_u;
  sf_mex_destroy(&c1_mxFi);
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static uint8_T c1_e_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c1_b_is_active_c1_PWM_28_HalfB, const char_T
  *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_is_active_c1_PWM_28_HalfB), &c1_thisId);
  sf_mex_destroy(&c1_b_is_active_c1_PWM_28_HalfB);
  return c1_y;
}

static uint8_T c1_f_emlrt_marshallIn(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_b_u;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_b_u, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_b_u;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static const mxArray *c1_chart_data_browse_helper
  (SFc1_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c1_ssIdNumber)
{
  const mxArray *c1_mxData = NULL;
  uint8_T c1_u;
  int16_T c1_i;
  int16_T c1_i1;
  int16_T c1_i2;
  uint16_T c1_u1;
  uint8_T c1_u2;
  uint8_T c1_u3;
  real_T c1_d;
  real_T c1_d1;
  real_T c1_d2;
  real_T c1_d3;
  real_T c1_d4;
  real_T c1_d5;
  c1_mxData = NULL;
  switch (c1_ssIdNumber) {
   case 18U:
    c1_u = *chartInstance->c1_enable;
    c1_d = (real_T)c1_u;
    sf_mex_assign(&c1_mxData, sf_mex_create("mxData", &c1_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c1_i = *chartInstance->c1_offset;
    sf_mex_assign(&c1_mxData, sf_mex_create("mxData", &c1_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c1_i1 = *chartInstance->c1_max;
    c1_d1 = (real_T)c1_i1;
    sf_mex_assign(&c1_mxData, sf_mex_create("mxData", &c1_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c1_i2 = *chartInstance->c1_sum;
    c1_d2 = (real_T)c1_i2;
    sf_mex_assign(&c1_mxData, sf_mex_create("mxData", &c1_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c1_u1 = *chartInstance->c1_cont;
    c1_d3 = (real_T)c1_u1;
    sf_mex_assign(&c1_mxData, sf_mex_create("mxData", &c1_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c1_u2 = *chartInstance->c1_init;
    c1_d4 = (real_T)c1_u2;
    sf_mex_assign(&c1_mxData, sf_mex_create("mxData", &c1_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c1_u3 = *chartInstance->c1_out_init;
    c1_d5 = (real_T)c1_u3;
    sf_mex_assign(&c1_mxData, sf_mex_create("mxData", &c1_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c1_mxData;
}

static int32_T c1__s32_add__(SFc1_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c1_b, int32_T c1_c, int32_T c1_EMLOvCount_src_loc, uint32_T
  c1_ssid_src_loc, int32_T c1_offset_src_loc, int32_T c1_length_src_loc)
{
  int32_T c1_a;
  int32_T c1_PICOffset;
  real_T c1_d;
  observerLogReadPIC(&c1_PICOffset);
  c1_a = c1_b + c1_c;
  if (((c1_a ^ c1_b) & (c1_a ^ c1_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c1_ssid_src_loc, c1_offset_src_loc,
      c1_length_src_loc);
    c1_d = 1.0;
    observerLog(c1_EMLOvCount_src_loc + c1_PICOffset, &c1_d, 1);
  }

  return c1_a;
}

static void init_dsm_address_info(SFc1_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c1_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c1_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c1_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c1_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c1_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c1_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c1_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c1_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c1_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c1_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c1_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMSzR8gfmZxfGJySWZZanyyYXxAuG+8kUW8R"
    "2JOmhOSuSAAAOnUIFs="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c1_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c1_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c1_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c1_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_PWM_28_HalfB(SimStruct* S, const mxArray *
  st)
{
  set_sim_state_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c1_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c1_PWM_28_HalfB
      ((SFc1_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c1_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c1_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc1_PWM_28_HalfB((SFc1_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c1_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5iNHGcLopDARodu2qiIsmyCpxTEmxAKlWQ+dnJ4yHT+JAwxlmfmQ758gl2hN",
    "02UUv0F0XRW9QoEfoG4qSZYqk4igxEiADUMQMv/fN+5+RV+v0PBxb+Lz8yvOu4/sGPnVvOjayeW",
    "3hma43vG+z+SMUEjbuE0Vi7VUOQWJ4Clpya5gUHTGUhTAmhqBAUMQmUpkyNs1iy5kYt62gjk+/i",
    "BiNgkhaHu6jLAkPBT9DtsSaPvI0mQJq2gChiZS0o6jNyWiusTInfgR0rG1cZYIGE9jEqaV7lhuW",
    "cGidAu0IbQhqrM91Cwwx4JvTUjOdpTqYAWWccEZEobUR0QEk6GADz5IQfw+tQaPyMBoRZfYhIhP",
    "QXTZOOaWAPCfT+OGYCWKkYoS3Yu47wWXd+hz16ckQeIVDULd9BWScSCZMefyDNlraEuSYQxOO7a",
    "icLYBX1gX/OYMTUKV+G/pyAoqM4FCUbpo6pHWaRmueJcsww2J4TtRjivHTEJZmL2aODgjGCY5Qo",
    "gwGqZEdfaTYBN1bymbjjsvMVSVj42mw9SpYytaaQFUU5mxtKnzCuS6FHcmkCxPgKWuTGFINm7IW",
    "47Rm4ZFEB7v0Lq8GKxgGPoP5UoSsMFyTHCDtOz9hY7mIpFYbGfuYvM1ud/nzMqwjDKghoVDUBRR",
    "hGtBnqXvL2UKmXewRiFqZVL0i8DRDVqE8PbSieSLVGH1S0UTOTXARLQXGeoSxxEp4prFoqmAulq",
    "twlNAIQtdgGIcelg1iC3yiXWt7jHU3YeasCZoqlhRF1Z0/d73z8+fLtzh/ZnL593cLPLUCHm/h7",
    "fAPFvCb9Yv4jdy+9dmaG5n83oL8zdx+jZy8w227ynnzx6+/3Pr3i79//u3Pzj9g8vbn9agt6VHz",
    "Zvsn1y53bm9l89uzBjlP+MlSnjnswYJejQL+rxf4t7O5/vGgHfTGL3dfPd2FJ8rw17ES9/2U7/d",
    "6tb7XcvrO1u+4Tn2WpH1XK9oJswuFmxM7PWbz8by+wh+b2fp0/PdoPfnv9/JxLPJX44K/Gh6Vwm",
    "yV5OPV6n9vLy9fpP+NXLzdXFozYIK9Jzvu7K0nP92/v8KOnZwdO+m9YkBct4IB3R30X/QGPzwYH",
    "BA+3C/oM+9ar5eV865Y7lPR87NfPrx9b3MOb7yjXH3Nc/+q5Na177L3kY8NX3WeeTn89kdsx7r3",
    "xA+N/8u73D3um2z+cP4Xy48YDwtu29nnLpBh0dcrsO9/mYSgSA==",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c1_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c1_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c1_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c1_PWM_28_HalfB(SimStruct *S)
{
  SFc1_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc1_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc1_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc1_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c1_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c1_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c1_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c1_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c1_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c1_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c1_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c1_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c1_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c1_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c1_JITStateAnimation,
    chartInstance->c1_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c1_PWM_28_HalfB(chartInstance);
}

void c1_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c1_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
