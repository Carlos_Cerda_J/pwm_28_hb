/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c22_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c22_eml_mx;
static const mxArray *c22_b_eml_mx;
static const mxArray *c22_c_eml_mx;

/* Function Declarations */
static void initialize_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c22_update_jit_animation_state_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance);
static void c22_do_animation_call_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c22_st);
static void sf_gateway_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c22_emlrt_update_log_1(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index);
static int16_T c22_emlrt_update_log_2(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index);
static int16_T c22_emlrt_update_log_3(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index);
static boolean_T c22_emlrt_update_log_4(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index);
static uint16_T c22_emlrt_update_log_5(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index);
static int32_T c22_emlrt_update_log_6(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index);
static void c22_emlrtInitVarDataTables(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c22_dataTables[24],
  emlrtLocationLoggingHistogramType c22_histTables[24]);
static uint16_T c22_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c22_sp, const mxArray *c22_b_cont, const
  char_T *c22_identifier);
static uint16_T c22_b_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c22_sp, const mxArray *c22_u, const
  emlrtMsgIdentifier *c22_parentId);
static uint8_T c22_c_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c22_sp, const mxArray *c22_b_out_init, const
  char_T *c22_identifier);
static uint8_T c22_d_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c22_sp, const mxArray *c22_u, const
  emlrtMsgIdentifier *c22_parentId);
static uint8_T c22_e_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c22_b_is_active_c22_PWM_28_HalfB, const char_T *
  c22_identifier);
static uint8_T c22_f_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c22_u, const emlrtMsgIdentifier *c22_parentId);
static const mxArray *c22_chart_data_browse_helper
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c22_ssIdNumber);
static int32_T c22__s32_add__(SFc22_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c22_b, int32_T c22_c, int32_T c22_EMLOvCount_src_loc, uint32_T
  c22_ssid_src_loc, int32_T c22_offset_src_loc, int32_T c22_length_src_loc);
static void init_dsm_address_info(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c22_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c22_st.tls = chartInstance->c22_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c22_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c22_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c22_is_active_c22_PWM_28_HalfB = 0U;
  sf_mex_assign(&c22_c_eml_mx, sf_mex_call(&c22_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c22_b_eml_mx, sf_mex_call(&c22_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c22_eml_mx, sf_mex_call(&c22_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c22_emlrtLocLogSimulated = false;
  c22_emlrtInitVarDataTables(chartInstance,
    chartInstance->c22_emlrtLocationLoggingDataTables,
    chartInstance->c22_emlrtLocLogHistTables);
}

static void initialize_params_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c22_update_jit_animation_state_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c22_do_animation_call_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c22_st;
  const mxArray *c22_y = NULL;
  const mxArray *c22_b_y = NULL;
  uint16_T c22_u;
  const mxArray *c22_c_y = NULL;
  const mxArray *c22_d_y = NULL;
  uint8_T c22_b_u;
  const mxArray *c22_e_y = NULL;
  const mxArray *c22_f_y = NULL;
  c22_st = NULL;
  c22_st = NULL;
  c22_y = NULL;
  sf_mex_assign(&c22_y, sf_mex_createcellmatrix(3, 1), false);
  c22_b_y = NULL;
  c22_u = *chartInstance->c22_cont;
  c22_c_y = NULL;
  sf_mex_assign(&c22_c_y, sf_mex_create("y", &c22_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c22_b_y, sf_mex_create_fi(sf_mex_dup(c22_eml_mx), sf_mex_dup
    (c22_b_eml_mx), "simulinkarray", c22_c_y, false, false), false);
  sf_mex_setcell(c22_y, 0, c22_b_y);
  c22_d_y = NULL;
  c22_b_u = *chartInstance->c22_out_init;
  c22_e_y = NULL;
  sf_mex_assign(&c22_e_y, sf_mex_create("y", &c22_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c22_d_y, sf_mex_create_fi(sf_mex_dup(c22_eml_mx), sf_mex_dup
    (c22_c_eml_mx), "simulinkarray", c22_e_y, false, false), false);
  sf_mex_setcell(c22_y, 1, c22_d_y);
  c22_f_y = NULL;
  sf_mex_assign(&c22_f_y, sf_mex_create("y",
    &chartInstance->c22_is_active_c22_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c22_y, 2, c22_f_y);
  sf_mex_assign(&c22_st, c22_y, false);
  return c22_st;
}

static void set_sim_state_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c22_st)
{
  emlrtStack c22_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c22_u;
  c22_b_st.tls = chartInstance->c22_fEmlrtCtx;
  chartInstance->c22_doneDoubleBufferReInit = true;
  c22_u = sf_mex_dup(c22_st);
  *chartInstance->c22_cont = c22_emlrt_marshallIn(chartInstance, &c22_b_st,
    sf_mex_dup(sf_mex_getcell(c22_u, 0)), "cont");
  *chartInstance->c22_out_init = c22_c_emlrt_marshallIn(chartInstance, &c22_b_st,
    sf_mex_dup(sf_mex_getcell(c22_u, 1)), "out_init");
  chartInstance->c22_is_active_c22_PWM_28_HalfB = c22_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c22_u, 2)),
     "is_active_c22_PWM_28_HalfB");
  sf_mex_destroy(&c22_u);
  sf_mex_destroy(&c22_st);
}

static void sf_gateway_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c22_PICOffset;
  uint8_T c22_b_enable;
  int16_T c22_b_offset;
  int16_T c22_b_max;
  int16_T c22_b_sum;
  uint8_T c22_b_init;
  uint8_T c22_a0;
  uint8_T c22_a;
  uint8_T c22_b_a0;
  uint8_T c22_a1;
  uint8_T c22_b_a1;
  boolean_T c22_c;
  int8_T c22_i;
  int8_T c22_i1;
  real_T c22_d;
  uint8_T c22_c_a0;
  uint16_T c22_b_cont;
  uint8_T c22_b_a;
  uint8_T c22_b_out_init;
  uint8_T c22_d_a0;
  uint8_T c22_c_a1;
  uint8_T c22_d_a1;
  boolean_T c22_b_c;
  int8_T c22_i2;
  int8_T c22_i3;
  real_T c22_d1;
  int16_T c22_varargin_1;
  int16_T c22_b_varargin_1;
  int16_T c22_c_varargin_1;
  int16_T c22_d_varargin_1;
  int16_T c22_var1;
  int16_T c22_b_var1;
  int16_T c22_i4;
  int16_T c22_i5;
  boolean_T c22_covSaturation;
  boolean_T c22_b_covSaturation;
  uint16_T c22_hfi;
  uint16_T c22_b_hfi;
  uint16_T c22_u;
  uint16_T c22_u1;
  int16_T c22_e_varargin_1;
  int16_T c22_f_varargin_1;
  int16_T c22_g_varargin_1;
  int16_T c22_h_varargin_1;
  int16_T c22_c_var1;
  int16_T c22_d_var1;
  int16_T c22_i6;
  int16_T c22_i7;
  boolean_T c22_c_covSaturation;
  boolean_T c22_d_covSaturation;
  uint16_T c22_c_hfi;
  uint16_T c22_d_hfi;
  uint16_T c22_u2;
  uint16_T c22_u3;
  uint16_T c22_e_a0;
  uint16_T c22_f_a0;
  uint16_T c22_b0;
  uint16_T c22_b_b0;
  uint16_T c22_c_a;
  uint16_T c22_d_a;
  uint16_T c22_b;
  uint16_T c22_b_b;
  uint16_T c22_g_a0;
  uint16_T c22_h_a0;
  uint16_T c22_c_b0;
  uint16_T c22_d_b0;
  uint16_T c22_e_a1;
  uint16_T c22_f_a1;
  uint16_T c22_b1;
  uint16_T c22_b_b1;
  uint16_T c22_g_a1;
  uint16_T c22_h_a1;
  uint16_T c22_c_b1;
  uint16_T c22_d_b1;
  boolean_T c22_c_c;
  boolean_T c22_d_c;
  int16_T c22_i8;
  int16_T c22_i9;
  int16_T c22_i10;
  int16_T c22_i11;
  int16_T c22_i12;
  int16_T c22_i13;
  int16_T c22_i14;
  int16_T c22_i15;
  int16_T c22_i16;
  int16_T c22_i17;
  int16_T c22_i18;
  int16_T c22_i19;
  int32_T c22_i20;
  int32_T c22_i21;
  int16_T c22_i22;
  int16_T c22_i23;
  int16_T c22_i24;
  int16_T c22_i25;
  int16_T c22_i26;
  int16_T c22_i27;
  int16_T c22_i28;
  int16_T c22_i29;
  int16_T c22_i30;
  int16_T c22_i31;
  int16_T c22_i32;
  int16_T c22_i33;
  int32_T c22_i34;
  int32_T c22_i35;
  int16_T c22_i36;
  int16_T c22_i37;
  int16_T c22_i38;
  int16_T c22_i39;
  int16_T c22_i40;
  int16_T c22_i41;
  int16_T c22_i42;
  int16_T c22_i43;
  real_T c22_d2;
  real_T c22_d3;
  int16_T c22_i_varargin_1;
  int16_T c22_i_a0;
  int16_T c22_j_varargin_1;
  int16_T c22_e_b0;
  int16_T c22_e_var1;
  int16_T c22_k_varargin_1;
  int16_T c22_i44;
  int16_T c22_v;
  boolean_T c22_e_covSaturation;
  int16_T c22_val;
  int16_T c22_c_b;
  int32_T c22_i45;
  uint16_T c22_e_hfi;
  real_T c22_d4;
  int32_T c22_i46;
  real_T c22_d5;
  real_T c22_d6;
  real_T c22_d7;
  int32_T c22_i47;
  int32_T c22_i48;
  int32_T c22_i49;
  real_T c22_d8;
  real_T c22_d9;
  int32_T c22_e_c;
  int32_T c22_l_varargin_1;
  int32_T c22_m_varargin_1;
  int32_T c22_f_var1;
  int32_T c22_i50;
  boolean_T c22_f_covSaturation;
  uint16_T c22_f_hfi;
  observerLogReadPIC(&c22_PICOffset);
  chartInstance->c22_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c22_covrtInstance, 4U, (real_T)
                    *chartInstance->c22_init);
  covrtSigUpdateFcn(chartInstance->c22_covrtInstance, 3U, (real_T)
                    *chartInstance->c22_sum);
  covrtSigUpdateFcn(chartInstance->c22_covrtInstance, 2U, (real_T)
                    *chartInstance->c22_max);
  covrtSigUpdateFcn(chartInstance->c22_covrtInstance, 1U, (real_T)
                    *chartInstance->c22_offset);
  covrtSigUpdateFcn(chartInstance->c22_covrtInstance, 0U, (real_T)
                    *chartInstance->c22_enable);
  chartInstance->c22_sfEvent = CALL_EVENT;
  c22_b_enable = *chartInstance->c22_enable;
  c22_b_offset = *chartInstance->c22_offset;
  c22_b_max = *chartInstance->c22_max;
  c22_b_sum = *chartInstance->c22_sum;
  c22_b_init = *chartInstance->c22_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c22_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c22_emlrt_update_log_1(chartInstance, c22_b_enable,
    chartInstance->c22_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c22_emlrt_update_log_2(chartInstance, c22_b_offset,
    chartInstance->c22_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c22_emlrt_update_log_3(chartInstance, c22_b_max,
    chartInstance->c22_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c22_emlrt_update_log_3(chartInstance, c22_b_sum,
    chartInstance->c22_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c22_emlrt_update_log_1(chartInstance, c22_b_init,
    chartInstance->c22_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c22_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c22_covrtInstance, 4U, 0, 0, false);
  c22_a0 = c22_b_enable;
  c22_a = c22_a0;
  c22_b_a0 = c22_a;
  c22_a1 = c22_b_a0;
  c22_b_a1 = c22_a1;
  c22_c = (c22_b_a1 == 0);
  c22_i = (int8_T)c22_b_enable;
  if ((int8_T)(c22_i & 2) != 0) {
    c22_i1 = (int8_T)(c22_i | -2);
  } else {
    c22_i1 = (int8_T)(c22_i & 1);
  }

  if (c22_i1 > 0) {
    c22_d = 3.0;
  } else {
    c22_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c22_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c22_covrtInstance,
        4U, 0U, 0U, c22_d, 0.0, -2, 0U, (int32_T)c22_emlrt_update_log_4
        (chartInstance, c22_c, chartInstance->c22_emlrtLocationLoggingDataTables,
         5)))) {
    c22_b_cont = c22_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c22_emlrtLocationLoggingDataTables, 6);
    c22_b_out_init = c22_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c22_emlrtLocationLoggingDataTables, 7);
  } else {
    c22_c_a0 = c22_b_init;
    c22_b_a = c22_c_a0;
    c22_d_a0 = c22_b_a;
    c22_c_a1 = c22_d_a0;
    c22_d_a1 = c22_c_a1;
    c22_b_c = (c22_d_a1 == 0);
    c22_i2 = (int8_T)c22_b_init;
    if ((int8_T)(c22_i2 & 2) != 0) {
      c22_i3 = (int8_T)(c22_i2 | -2);
    } else {
      c22_i3 = (int8_T)(c22_i2 & 1);
    }

    if (c22_i3 > 0) {
      c22_d1 = 3.0;
    } else {
      c22_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c22_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c22_covrtInstance, 4U, 0U, 1U, c22_d1,
                        0.0, -2, 0U, (int32_T)c22_emlrt_update_log_4
                        (chartInstance, c22_b_c,
                         chartInstance->c22_emlrtLocationLoggingDataTables, 8))))
    {
      c22_b_varargin_1 = c22_b_sum;
      c22_d_varargin_1 = c22_b_varargin_1;
      c22_b_var1 = c22_d_varargin_1;
      c22_i5 = c22_b_var1;
      c22_b_covSaturation = false;
      if (c22_i5 < 0) {
        c22_i5 = 0;
      } else {
        if (c22_i5 > 4095) {
          c22_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c22_covrtInstance, 4, 0, 0, 0,
          c22_b_covSaturation);
      }

      c22_b_hfi = (uint16_T)c22_i5;
      c22_u1 = c22_emlrt_update_log_5(chartInstance, c22_b_hfi,
        chartInstance->c22_emlrtLocationLoggingDataTables, 10);
      c22_f_varargin_1 = c22_b_max;
      c22_h_varargin_1 = c22_f_varargin_1;
      c22_d_var1 = c22_h_varargin_1;
      c22_i7 = c22_d_var1;
      c22_d_covSaturation = false;
      if (c22_i7 < 0) {
        c22_i7 = 0;
      } else {
        if (c22_i7 > 4095) {
          c22_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c22_covrtInstance, 4, 0, 1, 0,
          c22_d_covSaturation);
      }

      c22_d_hfi = (uint16_T)c22_i7;
      c22_u3 = c22_emlrt_update_log_5(chartInstance, c22_d_hfi,
        chartInstance->c22_emlrtLocationLoggingDataTables, 11);
      c22_f_a0 = c22_u1;
      c22_b_b0 = c22_u3;
      c22_d_a = c22_f_a0;
      c22_b_b = c22_b_b0;
      c22_h_a0 = c22_d_a;
      c22_d_b0 = c22_b_b;
      c22_f_a1 = c22_h_a0;
      c22_b_b1 = c22_d_b0;
      c22_h_a1 = c22_f_a1;
      c22_d_b1 = c22_b_b1;
      c22_d_c = (c22_h_a1 < c22_d_b1);
      c22_i9 = (int16_T)c22_u1;
      c22_i11 = (int16_T)c22_u3;
      c22_i13 = (int16_T)c22_u3;
      c22_i15 = (int16_T)c22_u1;
      if ((int16_T)(c22_i13 & 4096) != 0) {
        c22_i17 = (int16_T)(c22_i13 | -4096);
      } else {
        c22_i17 = (int16_T)(c22_i13 & 4095);
      }

      if ((int16_T)(c22_i15 & 4096) != 0) {
        c22_i19 = (int16_T)(c22_i15 | -4096);
      } else {
        c22_i19 = (int16_T)(c22_i15 & 4095);
      }

      c22_i21 = c22_i17 - c22_i19;
      if (c22_i21 > 4095) {
        c22_i21 = 4095;
      } else {
        if (c22_i21 < -4096) {
          c22_i21 = -4096;
        }
      }

      c22_i23 = (int16_T)c22_u1;
      c22_i25 = (int16_T)c22_u3;
      c22_i27 = (int16_T)c22_u1;
      c22_i29 = (int16_T)c22_u3;
      if ((int16_T)(c22_i27 & 4096) != 0) {
        c22_i31 = (int16_T)(c22_i27 | -4096);
      } else {
        c22_i31 = (int16_T)(c22_i27 & 4095);
      }

      if ((int16_T)(c22_i29 & 4096) != 0) {
        c22_i33 = (int16_T)(c22_i29 | -4096);
      } else {
        c22_i33 = (int16_T)(c22_i29 & 4095);
      }

      c22_i35 = c22_i31 - c22_i33;
      if (c22_i35 > 4095) {
        c22_i35 = 4095;
      } else {
        if (c22_i35 < -4096) {
          c22_i35 = -4096;
        }
      }

      if ((int16_T)(c22_i9 & 4096) != 0) {
        c22_i37 = (int16_T)(c22_i9 | -4096);
      } else {
        c22_i37 = (int16_T)(c22_i9 & 4095);
      }

      if ((int16_T)(c22_i11 & 4096) != 0) {
        c22_i39 = (int16_T)(c22_i11 | -4096);
      } else {
        c22_i39 = (int16_T)(c22_i11 & 4095);
      }

      if ((int16_T)(c22_i23 & 4096) != 0) {
        c22_i41 = (int16_T)(c22_i23 | -4096);
      } else {
        c22_i41 = (int16_T)(c22_i23 & 4095);
      }

      if ((int16_T)(c22_i25 & 4096) != 0) {
        c22_i43 = (int16_T)(c22_i25 | -4096);
      } else {
        c22_i43 = (int16_T)(c22_i25 & 4095);
      }

      if (c22_i37 < c22_i39) {
        c22_d3 = (real_T)((int16_T)c22_i21 <= 1);
      } else if (c22_i41 > c22_i43) {
        if ((int16_T)c22_i35 <= 1) {
          c22_d3 = 3.0;
        } else {
          c22_d3 = 0.0;
        }
      } else {
        c22_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c22_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c22_covrtInstance, 4U, 0U, 2U, c22_d3,
                          0.0, -2, 2U, (int32_T)c22_emlrt_update_log_4
                          (chartInstance, c22_d_c,
                           chartInstance->c22_emlrtLocationLoggingDataTables, 9))))
      {
        c22_i_a0 = c22_b_sum;
        c22_e_b0 = c22_b_offset;
        c22_k_varargin_1 = c22_e_b0;
        c22_v = c22_k_varargin_1;
        c22_val = c22_v;
        c22_c_b = c22_val;
        c22_i45 = c22_i_a0;
        if (c22_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c22_d4 = 1.0;
          observerLog(387 + c22_PICOffset, &c22_d4, 1);
        }

        if (c22_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c22_d5 = 1.0;
          observerLog(387 + c22_PICOffset, &c22_d5, 1);
        }

        c22_i46 = c22_c_b;
        if (c22_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c22_d6 = 1.0;
          observerLog(390 + c22_PICOffset, &c22_d6, 1);
        }

        if (c22_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c22_d7 = 1.0;
          observerLog(390 + c22_PICOffset, &c22_d7, 1);
        }

        if ((c22_i45 & 65536) != 0) {
          c22_i47 = c22_i45 | -65536;
        } else {
          c22_i47 = c22_i45 & 65535;
        }

        if ((c22_i46 & 65536) != 0) {
          c22_i48 = c22_i46 | -65536;
        } else {
          c22_i48 = c22_i46 & 65535;
        }

        c22_i49 = c22__s32_add__(chartInstance, c22_i47, c22_i48, 389, 1U, 323,
          12);
        if (c22_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c22_d8 = 1.0;
          observerLog(395 + c22_PICOffset, &c22_d8, 1);
        }

        if (c22_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c22_d9 = 1.0;
          observerLog(395 + c22_PICOffset, &c22_d9, 1);
        }

        if ((c22_i49 & 65536) != 0) {
          c22_e_c = c22_i49 | -65536;
        } else {
          c22_e_c = c22_i49 & 65535;
        }

        c22_l_varargin_1 = c22_emlrt_update_log_6(chartInstance, c22_e_c,
          chartInstance->c22_emlrtLocationLoggingDataTables, 13);
        c22_m_varargin_1 = c22_l_varargin_1;
        c22_f_var1 = c22_m_varargin_1;
        c22_i50 = c22_f_var1;
        c22_f_covSaturation = false;
        if (c22_i50 < 0) {
          c22_i50 = 0;
        } else {
          if (c22_i50 > 4095) {
            c22_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c22_covrtInstance, 4, 0, 2, 0,
            c22_f_covSaturation);
        }

        c22_f_hfi = (uint16_T)c22_i50;
        c22_b_cont = c22_emlrt_update_log_5(chartInstance, c22_f_hfi,
          chartInstance->c22_emlrtLocationLoggingDataTables, 12);
        c22_b_out_init = c22_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c22_emlrtLocationLoggingDataTables, 14);
      } else {
        c22_b_cont = c22_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c22_emlrtLocationLoggingDataTables, 15);
        c22_b_out_init = c22_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c22_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c22_varargin_1 = c22_b_sum;
      c22_c_varargin_1 = c22_varargin_1;
      c22_var1 = c22_c_varargin_1;
      c22_i4 = c22_var1;
      c22_covSaturation = false;
      if (c22_i4 < 0) {
        c22_i4 = 0;
      } else {
        if (c22_i4 > 4095) {
          c22_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c22_covrtInstance, 4, 0, 3, 0,
          c22_covSaturation);
      }

      c22_hfi = (uint16_T)c22_i4;
      c22_u = c22_emlrt_update_log_5(chartInstance, c22_hfi,
        chartInstance->c22_emlrtLocationLoggingDataTables, 18);
      c22_e_varargin_1 = c22_b_max;
      c22_g_varargin_1 = c22_e_varargin_1;
      c22_c_var1 = c22_g_varargin_1;
      c22_i6 = c22_c_var1;
      c22_c_covSaturation = false;
      if (c22_i6 < 0) {
        c22_i6 = 0;
      } else {
        if (c22_i6 > 4095) {
          c22_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c22_covrtInstance, 4, 0, 4, 0,
          c22_c_covSaturation);
      }

      c22_c_hfi = (uint16_T)c22_i6;
      c22_u2 = c22_emlrt_update_log_5(chartInstance, c22_c_hfi,
        chartInstance->c22_emlrtLocationLoggingDataTables, 19);
      c22_e_a0 = c22_u;
      c22_b0 = c22_u2;
      c22_c_a = c22_e_a0;
      c22_b = c22_b0;
      c22_g_a0 = c22_c_a;
      c22_c_b0 = c22_b;
      c22_e_a1 = c22_g_a0;
      c22_b1 = c22_c_b0;
      c22_g_a1 = c22_e_a1;
      c22_c_b1 = c22_b1;
      c22_c_c = (c22_g_a1 < c22_c_b1);
      c22_i8 = (int16_T)c22_u;
      c22_i10 = (int16_T)c22_u2;
      c22_i12 = (int16_T)c22_u2;
      c22_i14 = (int16_T)c22_u;
      if ((int16_T)(c22_i12 & 4096) != 0) {
        c22_i16 = (int16_T)(c22_i12 | -4096);
      } else {
        c22_i16 = (int16_T)(c22_i12 & 4095);
      }

      if ((int16_T)(c22_i14 & 4096) != 0) {
        c22_i18 = (int16_T)(c22_i14 | -4096);
      } else {
        c22_i18 = (int16_T)(c22_i14 & 4095);
      }

      c22_i20 = c22_i16 - c22_i18;
      if (c22_i20 > 4095) {
        c22_i20 = 4095;
      } else {
        if (c22_i20 < -4096) {
          c22_i20 = -4096;
        }
      }

      c22_i22 = (int16_T)c22_u;
      c22_i24 = (int16_T)c22_u2;
      c22_i26 = (int16_T)c22_u;
      c22_i28 = (int16_T)c22_u2;
      if ((int16_T)(c22_i26 & 4096) != 0) {
        c22_i30 = (int16_T)(c22_i26 | -4096);
      } else {
        c22_i30 = (int16_T)(c22_i26 & 4095);
      }

      if ((int16_T)(c22_i28 & 4096) != 0) {
        c22_i32 = (int16_T)(c22_i28 | -4096);
      } else {
        c22_i32 = (int16_T)(c22_i28 & 4095);
      }

      c22_i34 = c22_i30 - c22_i32;
      if (c22_i34 > 4095) {
        c22_i34 = 4095;
      } else {
        if (c22_i34 < -4096) {
          c22_i34 = -4096;
        }
      }

      if ((int16_T)(c22_i8 & 4096) != 0) {
        c22_i36 = (int16_T)(c22_i8 | -4096);
      } else {
        c22_i36 = (int16_T)(c22_i8 & 4095);
      }

      if ((int16_T)(c22_i10 & 4096) != 0) {
        c22_i38 = (int16_T)(c22_i10 | -4096);
      } else {
        c22_i38 = (int16_T)(c22_i10 & 4095);
      }

      if ((int16_T)(c22_i22 & 4096) != 0) {
        c22_i40 = (int16_T)(c22_i22 | -4096);
      } else {
        c22_i40 = (int16_T)(c22_i22 & 4095);
      }

      if ((int16_T)(c22_i24 & 4096) != 0) {
        c22_i42 = (int16_T)(c22_i24 | -4096);
      } else {
        c22_i42 = (int16_T)(c22_i24 & 4095);
      }

      if (c22_i36 < c22_i38) {
        c22_d2 = (real_T)((int16_T)c22_i20 <= 1);
      } else if (c22_i40 > c22_i42) {
        if ((int16_T)c22_i34 <= 1) {
          c22_d2 = 3.0;
        } else {
          c22_d2 = 0.0;
        }
      } else {
        c22_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c22_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c22_covrtInstance, 4U, 0U, 3U, c22_d2,
                          0.0, -2, 2U, (int32_T)c22_emlrt_update_log_4
                          (chartInstance, c22_c_c,
                           chartInstance->c22_emlrtLocationLoggingDataTables, 17))))
      {
        c22_i_varargin_1 = c22_b_sum;
        c22_j_varargin_1 = c22_i_varargin_1;
        c22_e_var1 = c22_j_varargin_1;
        c22_i44 = c22_e_var1;
        c22_e_covSaturation = false;
        if (c22_i44 < 0) {
          c22_i44 = 0;
        } else {
          if (c22_i44 > 4095) {
            c22_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c22_covrtInstance, 4, 0, 5, 0,
            c22_e_covSaturation);
        }

        c22_e_hfi = (uint16_T)c22_i44;
        c22_b_cont = c22_emlrt_update_log_5(chartInstance, c22_e_hfi,
          chartInstance->c22_emlrtLocationLoggingDataTables, 20);
        c22_b_out_init = c22_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c22_emlrtLocationLoggingDataTables, 21);
      } else {
        c22_b_cont = c22_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c22_emlrtLocationLoggingDataTables, 22);
        c22_b_out_init = c22_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c22_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c22_cont = c22_b_cont;
  *chartInstance->c22_out_init = c22_b_out_init;
  c22_do_animation_call_c22_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c22_covrtInstance, 5U, (real_T)
                    *chartInstance->c22_cont);
  covrtSigUpdateFcn(chartInstance->c22_covrtInstance, 6U, (real_T)
                    *chartInstance->c22_out_init);
}

static void mdl_start_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c22_decisionTxtStartIdx = 0U;
  static const uint32_T c22_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c22_chart_data_browse_helper);
  chartInstance->c22_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c22_RuntimeVar,
    &chartInstance->c22_IsDebuggerActive,
    &chartInstance->c22_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c22_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c22_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c22_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c22_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c22_decisionTxtStartIdx, &c22_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c22_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c22_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c22_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c22_PWM_28_HalfB
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c22_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7705",              /* mexFileName */
    "Thu May 27 10:27:27 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c22_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c22_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c22_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c22_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7705");
    emlrtLocationLoggingPushLog(&c22_emlrtLocationLoggingFileInfo,
      c22_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c22_emlrtLocationLoggingDataTables, c22_emlrtLocationInfo,
      NULL, 0U, c22_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7705");
  }

  sfListenerLightTerminate(chartInstance->c22_RuntimeVar);
  sf_mex_destroy(&c22_eml_mx);
  sf_mex_destroy(&c22_b_eml_mx);
  sf_mex_destroy(&c22_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c22_covrtInstance);
}

static void initSimStructsc22_PWM_28_HalfB(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c22_emlrt_update_log_1(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index)
{
  boolean_T c22_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c22_b_table;
  real_T c22_d;
  uint8_T c22_u;
  uint8_T c22_localMin;
  real_T c22_d1;
  uint8_T c22_u1;
  uint8_T c22_localMax;
  emlrtLocationLoggingHistogramType *c22_histTable;
  real_T c22_inDouble;
  real_T c22_significand;
  int32_T c22_exponent;
  (void)chartInstance;
  c22_isLoggingEnabledHere = (c22_index >= 0);
  if (c22_isLoggingEnabledHere) {
    c22_b_table = (emlrtLocationLoggingDataType *)&c22_table[c22_index];
    c22_d = c22_b_table[0U].SimMin;
    if (c22_d < 2.0) {
      if (c22_d >= 0.0) {
        c22_u = (uint8_T)c22_d;
      } else {
        c22_u = 0U;
      }
    } else if (c22_d >= 2.0) {
      c22_u = 1U;
    } else {
      c22_u = 0U;
    }

    c22_localMin = c22_u;
    c22_d1 = c22_b_table[0U].SimMax;
    if (c22_d1 < 2.0) {
      if (c22_d1 >= 0.0) {
        c22_u1 = (uint8_T)c22_d1;
      } else {
        c22_u1 = 0U;
      }
    } else if (c22_d1 >= 2.0) {
      c22_u1 = 1U;
    } else {
      c22_u1 = 0U;
    }

    c22_localMax = c22_u1;
    c22_histTable = c22_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c22_in < c22_localMin) {
      c22_localMin = c22_in;
    }

    if (c22_in > c22_localMax) {
      c22_localMax = c22_in;
    }

    /* Histogram logging. */
    c22_inDouble = (real_T)c22_in;
    c22_histTable->TotalNumberOfValues++;
    if (c22_inDouble == 0.0) {
      c22_histTable->NumberOfZeros++;
    } else {
      c22_histTable->SimSum += c22_inDouble;
      if ((!muDoubleScalarIsInf(c22_inDouble)) && (!muDoubleScalarIsNaN
           (c22_inDouble))) {
        c22_significand = frexp(c22_inDouble, &c22_exponent);
        if (c22_exponent > 128) {
          c22_exponent = 128;
        }

        if (c22_exponent < -127) {
          c22_exponent = -127;
        }

        if (c22_significand < 0.0) {
          c22_histTable->NumberOfNegativeValues++;
          c22_histTable->HistogramOfNegativeValues[127 + c22_exponent]++;
        } else {
          c22_histTable->NumberOfPositiveValues++;
          c22_histTable->HistogramOfPositiveValues[127 + c22_exponent]++;
        }
      }
    }

    c22_b_table[0U].SimMin = (real_T)c22_localMin;
    c22_b_table[0U].SimMax = (real_T)c22_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c22_in;
}

static int16_T c22_emlrt_update_log_2(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index)
{
  boolean_T c22_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c22_b_table;
  real_T c22_d;
  int16_T c22_i;
  int16_T c22_localMin;
  real_T c22_d1;
  int16_T c22_i1;
  int16_T c22_localMax;
  emlrtLocationLoggingHistogramType *c22_histTable;
  real_T c22_inDouble;
  real_T c22_significand;
  int32_T c22_exponent;
  (void)chartInstance;
  c22_isLoggingEnabledHere = (c22_index >= 0);
  if (c22_isLoggingEnabledHere) {
    c22_b_table = (emlrtLocationLoggingDataType *)&c22_table[c22_index];
    c22_d = muDoubleScalarFloor(c22_b_table[0U].SimMin);
    if (c22_d < 32768.0) {
      if (c22_d >= -32768.0) {
        c22_i = (int16_T)c22_d;
      } else {
        c22_i = MIN_int16_T;
      }
    } else if (c22_d >= 32768.0) {
      c22_i = MAX_int16_T;
    } else {
      c22_i = 0;
    }

    c22_localMin = c22_i;
    c22_d1 = muDoubleScalarFloor(c22_b_table[0U].SimMax);
    if (c22_d1 < 32768.0) {
      if (c22_d1 >= -32768.0) {
        c22_i1 = (int16_T)c22_d1;
      } else {
        c22_i1 = MIN_int16_T;
      }
    } else if (c22_d1 >= 32768.0) {
      c22_i1 = MAX_int16_T;
    } else {
      c22_i1 = 0;
    }

    c22_localMax = c22_i1;
    c22_histTable = c22_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c22_in < c22_localMin) {
      c22_localMin = c22_in;
    }

    if (c22_in > c22_localMax) {
      c22_localMax = c22_in;
    }

    /* Histogram logging. */
    c22_inDouble = (real_T)c22_in;
    c22_histTable->TotalNumberOfValues++;
    if (c22_inDouble == 0.0) {
      c22_histTable->NumberOfZeros++;
    } else {
      c22_histTable->SimSum += c22_inDouble;
      if ((!muDoubleScalarIsInf(c22_inDouble)) && (!muDoubleScalarIsNaN
           (c22_inDouble))) {
        c22_significand = frexp(c22_inDouble, &c22_exponent);
        if (c22_exponent > 128) {
          c22_exponent = 128;
        }

        if (c22_exponent < -127) {
          c22_exponent = -127;
        }

        if (c22_significand < 0.0) {
          c22_histTable->NumberOfNegativeValues++;
          c22_histTable->HistogramOfNegativeValues[127 + c22_exponent]++;
        } else {
          c22_histTable->NumberOfPositiveValues++;
          c22_histTable->HistogramOfPositiveValues[127 + c22_exponent]++;
        }
      }
    }

    c22_b_table[0U].SimMin = (real_T)c22_localMin;
    c22_b_table[0U].SimMax = (real_T)c22_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c22_in;
}

static int16_T c22_emlrt_update_log_3(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index)
{
  boolean_T c22_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c22_b_table;
  real_T c22_d;
  int16_T c22_i;
  int16_T c22_localMin;
  real_T c22_d1;
  int16_T c22_i1;
  int16_T c22_localMax;
  emlrtLocationLoggingHistogramType *c22_histTable;
  real_T c22_inDouble;
  real_T c22_significand;
  int32_T c22_exponent;
  (void)chartInstance;
  c22_isLoggingEnabledHere = (c22_index >= 0);
  if (c22_isLoggingEnabledHere) {
    c22_b_table = (emlrtLocationLoggingDataType *)&c22_table[c22_index];
    c22_d = muDoubleScalarFloor(c22_b_table[0U].SimMin);
    if (c22_d < 2048.0) {
      if (c22_d >= -2048.0) {
        c22_i = (int16_T)c22_d;
      } else {
        c22_i = -2048;
      }
    } else if (c22_d >= 2048.0) {
      c22_i = 2047;
    } else {
      c22_i = 0;
    }

    c22_localMin = c22_i;
    c22_d1 = muDoubleScalarFloor(c22_b_table[0U].SimMax);
    if (c22_d1 < 2048.0) {
      if (c22_d1 >= -2048.0) {
        c22_i1 = (int16_T)c22_d1;
      } else {
        c22_i1 = -2048;
      }
    } else if (c22_d1 >= 2048.0) {
      c22_i1 = 2047;
    } else {
      c22_i1 = 0;
    }

    c22_localMax = c22_i1;
    c22_histTable = c22_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c22_in < c22_localMin) {
      c22_localMin = c22_in;
    }

    if (c22_in > c22_localMax) {
      c22_localMax = c22_in;
    }

    /* Histogram logging. */
    c22_inDouble = (real_T)c22_in;
    c22_histTable->TotalNumberOfValues++;
    if (c22_inDouble == 0.0) {
      c22_histTable->NumberOfZeros++;
    } else {
      c22_histTable->SimSum += c22_inDouble;
      if ((!muDoubleScalarIsInf(c22_inDouble)) && (!muDoubleScalarIsNaN
           (c22_inDouble))) {
        c22_significand = frexp(c22_inDouble, &c22_exponent);
        if (c22_exponent > 128) {
          c22_exponent = 128;
        }

        if (c22_exponent < -127) {
          c22_exponent = -127;
        }

        if (c22_significand < 0.0) {
          c22_histTable->NumberOfNegativeValues++;
          c22_histTable->HistogramOfNegativeValues[127 + c22_exponent]++;
        } else {
          c22_histTable->NumberOfPositiveValues++;
          c22_histTable->HistogramOfPositiveValues[127 + c22_exponent]++;
        }
      }
    }

    c22_b_table[0U].SimMin = (real_T)c22_localMin;
    c22_b_table[0U].SimMax = (real_T)c22_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c22_in;
}

static boolean_T c22_emlrt_update_log_4(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index)
{
  boolean_T c22_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c22_b_table;
  boolean_T c22_localMin;
  boolean_T c22_localMax;
  emlrtLocationLoggingHistogramType *c22_histTable;
  real_T c22_inDouble;
  real_T c22_significand;
  int32_T c22_exponent;
  (void)chartInstance;
  c22_isLoggingEnabledHere = (c22_index >= 0);
  if (c22_isLoggingEnabledHere) {
    c22_b_table = (emlrtLocationLoggingDataType *)&c22_table[c22_index];
    c22_localMin = (c22_b_table[0U].SimMin > 0.0);
    c22_localMax = (c22_b_table[0U].SimMax > 0.0);
    c22_histTable = c22_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c22_in < (int32_T)c22_localMin) {
      c22_localMin = c22_in;
    }

    if ((int32_T)c22_in > (int32_T)c22_localMax) {
      c22_localMax = c22_in;
    }

    /* Histogram logging. */
    c22_inDouble = (real_T)c22_in;
    c22_histTable->TotalNumberOfValues++;
    if (c22_inDouble == 0.0) {
      c22_histTable->NumberOfZeros++;
    } else {
      c22_histTable->SimSum += c22_inDouble;
      if ((!muDoubleScalarIsInf(c22_inDouble)) && (!muDoubleScalarIsNaN
           (c22_inDouble))) {
        c22_significand = frexp(c22_inDouble, &c22_exponent);
        if (c22_exponent > 128) {
          c22_exponent = 128;
        }

        if (c22_exponent < -127) {
          c22_exponent = -127;
        }

        if (c22_significand < 0.0) {
          c22_histTable->NumberOfNegativeValues++;
          c22_histTable->HistogramOfNegativeValues[127 + c22_exponent]++;
        } else {
          c22_histTable->NumberOfPositiveValues++;
          c22_histTable->HistogramOfPositiveValues[127 + c22_exponent]++;
        }
      }
    }

    c22_b_table[0U].SimMin = (real_T)c22_localMin;
    c22_b_table[0U].SimMax = (real_T)c22_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c22_in;
}

static uint16_T c22_emlrt_update_log_5(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index)
{
  boolean_T c22_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c22_b_table;
  real_T c22_d;
  uint16_T c22_u;
  uint16_T c22_localMin;
  real_T c22_d1;
  uint16_T c22_u1;
  uint16_T c22_localMax;
  emlrtLocationLoggingHistogramType *c22_histTable;
  real_T c22_inDouble;
  real_T c22_significand;
  int32_T c22_exponent;
  (void)chartInstance;
  c22_isLoggingEnabledHere = (c22_index >= 0);
  if (c22_isLoggingEnabledHere) {
    c22_b_table = (emlrtLocationLoggingDataType *)&c22_table[c22_index];
    c22_d = c22_b_table[0U].SimMin;
    if (c22_d < 4096.0) {
      if (c22_d >= 0.0) {
        c22_u = (uint16_T)c22_d;
      } else {
        c22_u = 0U;
      }
    } else if (c22_d >= 4096.0) {
      c22_u = 4095U;
    } else {
      c22_u = 0U;
    }

    c22_localMin = c22_u;
    c22_d1 = c22_b_table[0U].SimMax;
    if (c22_d1 < 4096.0) {
      if (c22_d1 >= 0.0) {
        c22_u1 = (uint16_T)c22_d1;
      } else {
        c22_u1 = 0U;
      }
    } else if (c22_d1 >= 4096.0) {
      c22_u1 = 4095U;
    } else {
      c22_u1 = 0U;
    }

    c22_localMax = c22_u1;
    c22_histTable = c22_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c22_in < c22_localMin) {
      c22_localMin = c22_in;
    }

    if (c22_in > c22_localMax) {
      c22_localMax = c22_in;
    }

    /* Histogram logging. */
    c22_inDouble = (real_T)c22_in;
    c22_histTable->TotalNumberOfValues++;
    if (c22_inDouble == 0.0) {
      c22_histTable->NumberOfZeros++;
    } else {
      c22_histTable->SimSum += c22_inDouble;
      if ((!muDoubleScalarIsInf(c22_inDouble)) && (!muDoubleScalarIsNaN
           (c22_inDouble))) {
        c22_significand = frexp(c22_inDouble, &c22_exponent);
        if (c22_exponent > 128) {
          c22_exponent = 128;
        }

        if (c22_exponent < -127) {
          c22_exponent = -127;
        }

        if (c22_significand < 0.0) {
          c22_histTable->NumberOfNegativeValues++;
          c22_histTable->HistogramOfNegativeValues[127 + c22_exponent]++;
        } else {
          c22_histTable->NumberOfPositiveValues++;
          c22_histTable->HistogramOfPositiveValues[127 + c22_exponent]++;
        }
      }
    }

    c22_b_table[0U].SimMin = (real_T)c22_localMin;
    c22_b_table[0U].SimMax = (real_T)c22_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c22_in;
}

static int32_T c22_emlrt_update_log_6(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c22_in, emlrtLocationLoggingDataType c22_table[],
  int32_T c22_index)
{
  boolean_T c22_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c22_b_table;
  real_T c22_d;
  int32_T c22_i;
  int32_T c22_localMin;
  real_T c22_d1;
  int32_T c22_i1;
  int32_T c22_localMax;
  emlrtLocationLoggingHistogramType *c22_histTable;
  real_T c22_inDouble;
  real_T c22_significand;
  int32_T c22_exponent;
  (void)chartInstance;
  c22_isLoggingEnabledHere = (c22_index >= 0);
  if (c22_isLoggingEnabledHere) {
    c22_b_table = (emlrtLocationLoggingDataType *)&c22_table[c22_index];
    c22_d = muDoubleScalarFloor(c22_b_table[0U].SimMin);
    if (c22_d < 65536.0) {
      if (c22_d >= -65536.0) {
        c22_i = (int32_T)c22_d;
      } else {
        c22_i = -65536;
      }
    } else if (c22_d >= 65536.0) {
      c22_i = 65535;
    } else {
      c22_i = 0;
    }

    c22_localMin = c22_i;
    c22_d1 = muDoubleScalarFloor(c22_b_table[0U].SimMax);
    if (c22_d1 < 65536.0) {
      if (c22_d1 >= -65536.0) {
        c22_i1 = (int32_T)c22_d1;
      } else {
        c22_i1 = -65536;
      }
    } else if (c22_d1 >= 65536.0) {
      c22_i1 = 65535;
    } else {
      c22_i1 = 0;
    }

    c22_localMax = c22_i1;
    c22_histTable = c22_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c22_in < c22_localMin) {
      c22_localMin = c22_in;
    }

    if (c22_in > c22_localMax) {
      c22_localMax = c22_in;
    }

    /* Histogram logging. */
    c22_inDouble = (real_T)c22_in;
    c22_histTable->TotalNumberOfValues++;
    if (c22_inDouble == 0.0) {
      c22_histTable->NumberOfZeros++;
    } else {
      c22_histTable->SimSum += c22_inDouble;
      if ((!muDoubleScalarIsInf(c22_inDouble)) && (!muDoubleScalarIsNaN
           (c22_inDouble))) {
        c22_significand = frexp(c22_inDouble, &c22_exponent);
        if (c22_exponent > 128) {
          c22_exponent = 128;
        }

        if (c22_exponent < -127) {
          c22_exponent = -127;
        }

        if (c22_significand < 0.0) {
          c22_histTable->NumberOfNegativeValues++;
          c22_histTable->HistogramOfNegativeValues[127 + c22_exponent]++;
        } else {
          c22_histTable->NumberOfPositiveValues++;
          c22_histTable->HistogramOfPositiveValues[127 + c22_exponent]++;
        }
      }
    }

    c22_b_table[0U].SimMin = (real_T)c22_localMin;
    c22_b_table[0U].SimMax = (real_T)c22_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c22_in;
}

static void c22_emlrtInitVarDataTables(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c22_dataTables[24],
  emlrtLocationLoggingHistogramType c22_histTables[24])
{
  int32_T c22_i;
  int32_T c22_iH;
  (void)chartInstance;
  for (c22_i = 0; c22_i < 24; c22_i++) {
    c22_dataTables[c22_i].SimMin = rtInf;
    c22_dataTables[c22_i].SimMax = rtMinusInf;
    c22_dataTables[c22_i].OverflowWraps = 0;
    c22_dataTables[c22_i].Saturations = 0;
    c22_dataTables[c22_i].IsAlwaysInteger = true;
    c22_dataTables[c22_i].HistogramTable = &c22_histTables[c22_i];
    c22_histTables[c22_i].NumberOfZeros = 0.0;
    c22_histTables[c22_i].NumberOfPositiveValues = 0.0;
    c22_histTables[c22_i].NumberOfNegativeValues = 0.0;
    c22_histTables[c22_i].TotalNumberOfValues = 0.0;
    c22_histTables[c22_i].SimSum = 0.0;
    for (c22_iH = 0; c22_iH < 256; c22_iH++) {
      c22_histTables[c22_i].HistogramOfPositiveValues[c22_iH] = 0.0;
      c22_histTables[c22_i].HistogramOfNegativeValues[c22_iH] = 0.0;
    }
  }
}

const mxArray *sf_c22_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c22_nameCaptureInfo = NULL;
  c22_nameCaptureInfo = NULL;
  sf_mex_assign(&c22_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c22_nameCaptureInfo;
}

static uint16_T c22_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c22_sp, const mxArray *c22_b_cont, const
  char_T *c22_identifier)
{
  uint16_T c22_y;
  emlrtMsgIdentifier c22_thisId;
  c22_thisId.fIdentifier = (const char *)c22_identifier;
  c22_thisId.fParent = NULL;
  c22_thisId.bParentIsCell = false;
  c22_y = c22_b_emlrt_marshallIn(chartInstance, c22_sp, sf_mex_dup(c22_b_cont),
    &c22_thisId);
  sf_mex_destroy(&c22_b_cont);
  return c22_y;
}

static uint16_T c22_b_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c22_sp, const mxArray *c22_u, const
  emlrtMsgIdentifier *c22_parentId)
{
  uint16_T c22_y;
  const mxArray *c22_mxFi = NULL;
  const mxArray *c22_mxInt = NULL;
  uint16_T c22_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c22_parentId, c22_u, false, 0U, NULL, c22_eml_mx, c22_b_eml_mx);
  sf_mex_assign(&c22_mxFi, sf_mex_dup(c22_u), false);
  sf_mex_assign(&c22_mxInt, sf_mex_call(c22_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c22_mxFi)), false);
  sf_mex_import(c22_parentId, sf_mex_dup(c22_mxInt), &c22_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c22_mxFi);
  sf_mex_destroy(&c22_mxInt);
  c22_y = c22_b_u;
  sf_mex_destroy(&c22_mxFi);
  sf_mex_destroy(&c22_u);
  return c22_y;
}

static uint8_T c22_c_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c22_sp, const mxArray *c22_b_out_init, const
  char_T *c22_identifier)
{
  uint8_T c22_y;
  emlrtMsgIdentifier c22_thisId;
  c22_thisId.fIdentifier = (const char *)c22_identifier;
  c22_thisId.fParent = NULL;
  c22_thisId.bParentIsCell = false;
  c22_y = c22_d_emlrt_marshallIn(chartInstance, c22_sp, sf_mex_dup
    (c22_b_out_init), &c22_thisId);
  sf_mex_destroy(&c22_b_out_init);
  return c22_y;
}

static uint8_T c22_d_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c22_sp, const mxArray *c22_u, const
  emlrtMsgIdentifier *c22_parentId)
{
  uint8_T c22_y;
  const mxArray *c22_mxFi = NULL;
  const mxArray *c22_mxInt = NULL;
  uint8_T c22_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c22_parentId, c22_u, false, 0U, NULL, c22_eml_mx, c22_c_eml_mx);
  sf_mex_assign(&c22_mxFi, sf_mex_dup(c22_u), false);
  sf_mex_assign(&c22_mxInt, sf_mex_call(c22_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c22_mxFi)), false);
  sf_mex_import(c22_parentId, sf_mex_dup(c22_mxInt), &c22_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c22_mxFi);
  sf_mex_destroy(&c22_mxInt);
  c22_y = c22_b_u;
  sf_mex_destroy(&c22_mxFi);
  sf_mex_destroy(&c22_u);
  return c22_y;
}

static uint8_T c22_e_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c22_b_is_active_c22_PWM_28_HalfB, const char_T *
  c22_identifier)
{
  uint8_T c22_y;
  emlrtMsgIdentifier c22_thisId;
  c22_thisId.fIdentifier = (const char *)c22_identifier;
  c22_thisId.fParent = NULL;
  c22_thisId.bParentIsCell = false;
  c22_y = c22_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c22_b_is_active_c22_PWM_28_HalfB), &c22_thisId);
  sf_mex_destroy(&c22_b_is_active_c22_PWM_28_HalfB);
  return c22_y;
}

static uint8_T c22_f_emlrt_marshallIn(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c22_u, const emlrtMsgIdentifier *c22_parentId)
{
  uint8_T c22_y;
  uint8_T c22_b_u;
  (void)chartInstance;
  sf_mex_import(c22_parentId, sf_mex_dup(c22_u), &c22_b_u, 1, 3, 0U, 0, 0U, 0);
  c22_y = c22_b_u;
  sf_mex_destroy(&c22_u);
  return c22_y;
}

static const mxArray *c22_chart_data_browse_helper
  (SFc22_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c22_ssIdNumber)
{
  const mxArray *c22_mxData = NULL;
  uint8_T c22_u;
  int16_T c22_i;
  int16_T c22_i1;
  int16_T c22_i2;
  uint16_T c22_u1;
  uint8_T c22_u2;
  uint8_T c22_u3;
  real_T c22_d;
  real_T c22_d1;
  real_T c22_d2;
  real_T c22_d3;
  real_T c22_d4;
  real_T c22_d5;
  c22_mxData = NULL;
  switch (c22_ssIdNumber) {
   case 18U:
    c22_u = *chartInstance->c22_enable;
    c22_d = (real_T)c22_u;
    sf_mex_assign(&c22_mxData, sf_mex_create("mxData", &c22_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c22_i = *chartInstance->c22_offset;
    sf_mex_assign(&c22_mxData, sf_mex_create("mxData", &c22_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c22_i1 = *chartInstance->c22_max;
    c22_d1 = (real_T)c22_i1;
    sf_mex_assign(&c22_mxData, sf_mex_create("mxData", &c22_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c22_i2 = *chartInstance->c22_sum;
    c22_d2 = (real_T)c22_i2;
    sf_mex_assign(&c22_mxData, sf_mex_create("mxData", &c22_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c22_u1 = *chartInstance->c22_cont;
    c22_d3 = (real_T)c22_u1;
    sf_mex_assign(&c22_mxData, sf_mex_create("mxData", &c22_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c22_u2 = *chartInstance->c22_init;
    c22_d4 = (real_T)c22_u2;
    sf_mex_assign(&c22_mxData, sf_mex_create("mxData", &c22_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c22_u3 = *chartInstance->c22_out_init;
    c22_d5 = (real_T)c22_u3;
    sf_mex_assign(&c22_mxData, sf_mex_create("mxData", &c22_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c22_mxData;
}

static int32_T c22__s32_add__(SFc22_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c22_b, int32_T c22_c, int32_T c22_EMLOvCount_src_loc, uint32_T
  c22_ssid_src_loc, int32_T c22_offset_src_loc, int32_T c22_length_src_loc)
{
  int32_T c22_a;
  int32_T c22_PICOffset;
  real_T c22_d;
  observerLogReadPIC(&c22_PICOffset);
  c22_a = c22_b + c22_c;
  if (((c22_a ^ c22_b) & (c22_a ^ c22_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c22_ssid_src_loc,
      c22_offset_src_loc, c22_length_src_loc);
    c22_d = 1.0;
    observerLog(c22_EMLOvCount_src_loc + c22_PICOffset, &c22_d, 1);
  }

  return c22_a;
}

static void init_dsm_address_info(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc22_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c22_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c22_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c22_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c22_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c22_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c22_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c22_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c22_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c22_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c22_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c22_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c22_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c22_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c22_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyykVF8QLhvvJFFv"
    "EdiTpoTwlwQAADrmSCQ"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c22_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c22_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c22_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c22_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c22_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c22_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c22_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c22_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc22_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c22_PWM_28_HalfB
      ((SFc22_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c22_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c22_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c22_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc22_PWM_28_HalfB((SFc22_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c22_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5iNHWcLIJYARodu0qSIw2yCpxTEm1AKlWQ+dnJ4yHT+JAwxlmfmQ758gl2hN",
    "02UUv0F0XRW9QoEfoG4qSZYqk4qgxEiADUMQMv/fN+5+RV+v0PBxb+Ly65XnX8X0Dn7o3HRvZvL",
    "bwTNcb3jfZ/AkKCRv3iSKx9iqHIDE8Ay25NUyKjhjKQhgTQ1AgKGITqUwZm2ax5UyM21ZQx6dfR",
    "oxGQSQtD/dRloSHgp8hW2JNH3maTAE1bYDQREraUdTmZDTXWJkTPwI61jauMkGDCWzi1NI9yw1L",
    "OLROgXaENgQ11ue6BYYY8M1pqZnOUh3MgDJOOCOi0NqI6AASdLCB50mIv4fWoFF5GI2IMvsQkQn",
    "oLhunnFJAnpNp/HDMBDFSMcJbMfed4LJufY769GQIvMIhqNu+AjJOJBOmPP5BGy1tCXLMoQnHdl",
    "TOFsBr64L/gsEJqFK/DX05AUVGcChKN00d0jpNozXPkmWYYTG8IOopxfhpCEuzFzNHBwTjBEcoU",
    "QaD1MiOPlJsgu4tZbNxx2XmqpKx8TTYehUsZWtNoCoKc7Y2FT7hXJfCjmTShQnwlLVJDKmGTVmL",
    "cVqz8Eiig116l1eDFQwDn8F8KUJWGK5JDpD2nR+xsVxEUquNjH1M3ma3u/x5GdYRBtSQUCjqAoo",
    "wDeiz1L3lbCHTLvYIRK1Mql4ReJohq1CeHlrRPJFqjD6paCLnJriIlgJjPcJYYiU811g0VTAXy1",
    "U4SmgEoWswjEMPywaxBT7RrrU9xbqbMHPWBE0VS4qi6s6f+975+fPlO5w/M7n8+9sFnloBj7fwd",
    "vhHC/jN+kX8Rm7f+mzNjUx+b0H+q9x+jZy8w227ynn7+y8/3/zni79++vWPzt9g8vbn9agt6VHz",
    "Zvsn1y53bm9l8zuzBjlP+MlSnjnswYJejQL+2wv829lcf3fQDnrjVw9eP3sAPyjD38RKPPRTvt/",
    "q1fpey+k7W7/rOvVZkvZdrWgnzC4Ubk7s9JjNx/P6Cn9sZuvT8e+T9eTv7eXjWOSvxgV/NTwqhd",
    "kqycer1f/7vbx8kf43cvF2c2nNgAn2P9lxd289+en+/RV27OTs2EnvFQPiuhUM6O7uoP+yN9h9N",
    "DggfLi/3Gfet14vK+ddsdynoudnv3x4+97lHN54T7n6muf+Vcmta99l7yMfG77qPPNy+O2P2I51",
    "74kfGv+nd7l73NfZ/PH8L5YfMR4W3Lazz10gw6KvV2DffxhXoH0=",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c22_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c22_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c22_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c22_PWM_28_HalfB(SimStruct *S)
{
  SFc22_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc22_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc22_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc22_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c22_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c22_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c22_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c22_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c22_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c22_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c22_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c22_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c22_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c22_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c22_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c22_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c22_JITStateAnimation,
    chartInstance->c22_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c22_PWM_28_HalfB(chartInstance);
}

void c22_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c22_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c22_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c22_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c22_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
