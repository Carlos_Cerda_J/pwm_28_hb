#ifndef __c27_PWM_28_HalfB_h__
#define __c27_PWM_28_HalfB_h__

/* Type Definitions */
#ifndef typedef_SFc27_PWM_28_HalfBInstanceStruct
#define typedef_SFc27_PWM_28_HalfBInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  int32_T c27_sfEvent;
  boolean_T c27_doneDoubleBufferReInit;
  uint8_T c27_is_active_c27_PWM_28_HalfB;
  uint8_T c27_JITStateAnimation[1];
  uint8_T c27_JITTransitionAnimation[1];
  emlrtLocationLoggingDataType c27_emlrtLocationLoggingDataTables[24];
  int32_T c27_IsDebuggerActive;
  int32_T c27_IsSequenceViewerPresent;
  int32_T c27_SequenceViewerOptimization;
  void *c27_RuntimeVar;
  emlrtLocationLoggingHistogramType c27_emlrtLocLogHistTables[24];
  boolean_T c27_emlrtLocLogSimulated;
  uint32_T c27_mlFcnLineNumber;
  void *c27_fcnDataPtrs[7];
  char_T *c27_dataNames[7];
  uint32_T c27_numFcnVars;
  uint32_T c27_ssIds[7];
  uint32_T c27_statuses[7];
  void *c27_outMexFcns[7];
  void *c27_inMexFcns[7];
  CovrtStateflowInstance *c27_covrtInstance;
  void *c27_fEmlrtCtx;
  uint8_T *c27_enable;
  int16_T *c27_offset;
  int16_T *c27_max;
  int16_T *c27_sum;
  uint16_T *c27_cont;
  uint8_T *c27_init;
  uint8_T *c27_out_init;
} SFc27_PWM_28_HalfBInstanceStruct;

#endif                                 /*typedef_SFc27_PWM_28_HalfBInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c27_PWM_28_HalfB_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c27_PWM_28_HalfB_get_check_sum(mxArray *plhs[]);
extern void c27_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
