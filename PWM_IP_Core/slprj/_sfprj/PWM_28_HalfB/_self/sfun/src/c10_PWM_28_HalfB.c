/* Include files */

#include "PWM_28_HalfB_sfun.h"
#include "c10_PWM_28_HalfB.h"
#include <math.h>
#include "mwmathutil.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const mxArray *c10_eml_mx;
static const mxArray *c10_b_eml_mx;
static const mxArray *c10_c_eml_mx;

/* Function Declarations */
static void initialize_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void initialize_params_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct *
  chartInstance);
static void enable_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void disable_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void c10_update_jit_animation_state_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance);
static void c10_do_animation_call_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance);
static void ext_mode_exec_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance);
static void set_sim_state_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c10_st);
static void sf_gateway_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_start_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_terminate_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void mdl_setup_runtime_resources_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance);
static void mdl_cleanup_runtime_resources_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance);
static void initSimStructsc10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static uint8_T c10_emlrt_update_log_1(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index);
static int16_T c10_emlrt_update_log_2(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index);
static int16_T c10_emlrt_update_log_3(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index);
static boolean_T c10_emlrt_update_log_4(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index);
static uint16_T c10_emlrt_update_log_5(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index);
static int32_T c10_emlrt_update_log_6(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index);
static void c10_emlrtInitVarDataTables(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c10_dataTables[24],
  emlrtLocationLoggingHistogramType c10_histTables[24]);
static uint16_T c10_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c10_sp, const mxArray *c10_b_cont, const
  char_T *c10_identifier);
static uint16_T c10_b_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c10_sp, const mxArray *c10_u, const
  emlrtMsgIdentifier *c10_parentId);
static uint8_T c10_c_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c10_sp, const mxArray *c10_b_out_init, const
  char_T *c10_identifier);
static uint8_T c10_d_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c10_sp, const mxArray *c10_u, const
  emlrtMsgIdentifier *c10_parentId);
static uint8_T c10_e_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c10_b_is_active_c10_PWM_28_HalfB, const char_T *
  c10_identifier);
static uint8_T c10_f_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static const mxArray *c10_chart_data_browse_helper
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c10_ssIdNumber);
static int32_T c10__s32_add__(SFc10_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c10_b, int32_T c10_c, int32_T c10_EMLOvCount_src_loc, uint32_T
  c10_ssid_src_loc, int32_T c10_offset_src_loc, int32_T c10_length_src_loc);
static void init_dsm_address_info(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  emlrtStack c10_st = { NULL,          /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  c10_st.tls = chartInstance->c10_fEmlrtCtx;
  emlrtLicenseCheckR2012b(&c10_st, "Fixed_Point_Toolbox", 2);
  sim_mode_is_external(chartInstance->S);
  chartInstance->c10_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c10_is_active_c10_PWM_28_HalfB = 0U;
  sf_mex_assign(&c10_c_eml_mx, sf_mex_call(&c10_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 1.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c10_b_eml_mx, sf_mex_call(&c10_st, NULL, "numerictype", 1U, 14U,
    15, "SignednessBool", 3, false, 15, "Signedness", 15, "Unsigned", 15,
    "WordLength", 6, 12.0, 15, "FractionLength", 6, 0.0, 15, "BinaryPoint", 6,
    0.0, 15, "Slope", 6, 1.0, 15, "FixedExponent", 6, 0.0), true);
  sf_mex_assign(&c10_eml_mx, sf_mex_call(&c10_st, NULL, "fimath", 1U, 42U, 15,
    "RoundMode", 15, "nearest", 15, "RoundingMethod", 15, "Nearest", 15,
    "OverflowMode", 15, "saturate", 15, "OverflowAction", 15, "Saturate", 15,
    "ProductMode", 15, "FullPrecision", 15, "SumMode", 15, "FullPrecision", 15,
    "ProductWordLength", 6, 32.0, 15, "SumWordLength", 6, 32.0, 15,
    "MaxProductWordLength", 6, 65535.0, 15, "MaxSumWordLength", 6, 65535.0, 15,
    "ProductFractionLength", 6, 30.0, 15, "ProductFixedExponent", 6, -30.0, 15,
    "SumFractionLength", 6, 30.0, 15, "SumFixedExponent", 6, -30.0, 15,
    "SumSlopeAdjustmentFactor", 6, 1.0, 15, "SumBias", 6, 0.0, 15,
    "ProductSlopeAdjustmentFactor", 6, 1.0, 15, "ProductBias", 6, 0.0, 15,
    "CastBeforeSum", 3, true, 15, "SumSlope", 6, 9.3132257461547852E-10, 15,
    "ProductSlope", 6, 9.3132257461547852E-10), true);
  chartInstance->c10_emlrtLocLogSimulated = false;
  c10_emlrtInitVarDataTables(chartInstance,
    chartInstance->c10_emlrtLocationLoggingDataTables,
    chartInstance->c10_emlrtLocLogHistTables);
}

static void initialize_params_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c10_update_jit_animation_state_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c10_do_animation_call_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance)
{
  sfDoAnimationWrapper(chartInstance->S, false, true);
  sfDoAnimationWrapper(chartInstance->S, false, false);
}

static void ext_mode_exec_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const mxArray *c10_st;
  const mxArray *c10_y = NULL;
  const mxArray *c10_b_y = NULL;
  uint16_T c10_u;
  const mxArray *c10_c_y = NULL;
  const mxArray *c10_d_y = NULL;
  uint8_T c10_b_u;
  const mxArray *c10_e_y = NULL;
  const mxArray *c10_f_y = NULL;
  c10_st = NULL;
  c10_st = NULL;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_createcellmatrix(3, 1), false);
  c10_b_y = NULL;
  c10_u = *chartInstance->c10_cont;
  c10_c_y = NULL;
  sf_mex_assign(&c10_c_y, sf_mex_create("y", &c10_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c10_b_y, sf_mex_create_fi(sf_mex_dup(c10_eml_mx), sf_mex_dup
    (c10_b_eml_mx), "simulinkarray", c10_c_y, false, false), false);
  sf_mex_setcell(c10_y, 0, c10_b_y);
  c10_d_y = NULL;
  c10_b_u = *chartInstance->c10_out_init;
  c10_e_y = NULL;
  sf_mex_assign(&c10_e_y, sf_mex_create("y", &c10_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c10_d_y, sf_mex_create_fi(sf_mex_dup(c10_eml_mx), sf_mex_dup
    (c10_c_eml_mx), "simulinkarray", c10_e_y, false, false), false);
  sf_mex_setcell(c10_y, 1, c10_d_y);
  c10_f_y = NULL;
  sf_mex_assign(&c10_f_y, sf_mex_create("y",
    &chartInstance->c10_is_active_c10_PWM_28_HalfB, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c10_y, 2, c10_f_y);
  sf_mex_assign(&c10_st, c10_y, false);
  return c10_st;
}

static void set_sim_state_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c10_st)
{
  emlrtStack c10_b_st = { NULL,        /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  const mxArray *c10_u;
  c10_b_st.tls = chartInstance->c10_fEmlrtCtx;
  chartInstance->c10_doneDoubleBufferReInit = true;
  c10_u = sf_mex_dup(c10_st);
  *chartInstance->c10_cont = c10_emlrt_marshallIn(chartInstance, &c10_b_st,
    sf_mex_dup(sf_mex_getcell(c10_u, 0)), "cont");
  *chartInstance->c10_out_init = c10_c_emlrt_marshallIn(chartInstance, &c10_b_st,
    sf_mex_dup(sf_mex_getcell(c10_u, 1)), "out_init");
  chartInstance->c10_is_active_c10_PWM_28_HalfB = c10_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c10_u, 2)),
     "is_active_c10_PWM_28_HalfB");
  sf_mex_destroy(&c10_u);
  sf_mex_destroy(&c10_st);
}

static void sf_gateway_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  int32_T c10_PICOffset;
  uint8_T c10_b_enable;
  int16_T c10_b_offset;
  int16_T c10_b_max;
  int16_T c10_b_sum;
  uint8_T c10_b_init;
  uint8_T c10_a0;
  uint8_T c10_a;
  uint8_T c10_b_a0;
  uint8_T c10_a1;
  uint8_T c10_b_a1;
  boolean_T c10_c;
  int8_T c10_i;
  int8_T c10_i1;
  real_T c10_d;
  uint8_T c10_c_a0;
  uint16_T c10_b_cont;
  uint8_T c10_b_a;
  uint8_T c10_b_out_init;
  uint8_T c10_d_a0;
  uint8_T c10_c_a1;
  uint8_T c10_d_a1;
  boolean_T c10_b_c;
  int8_T c10_i2;
  int8_T c10_i3;
  real_T c10_d1;
  int16_T c10_varargin_1;
  int16_T c10_b_varargin_1;
  int16_T c10_c_varargin_1;
  int16_T c10_d_varargin_1;
  int16_T c10_var1;
  int16_T c10_b_var1;
  int16_T c10_i4;
  int16_T c10_i5;
  boolean_T c10_covSaturation;
  boolean_T c10_b_covSaturation;
  uint16_T c10_hfi;
  uint16_T c10_b_hfi;
  uint16_T c10_u;
  uint16_T c10_u1;
  int16_T c10_e_varargin_1;
  int16_T c10_f_varargin_1;
  int16_T c10_g_varargin_1;
  int16_T c10_h_varargin_1;
  int16_T c10_c_var1;
  int16_T c10_d_var1;
  int16_T c10_i6;
  int16_T c10_i7;
  boolean_T c10_c_covSaturation;
  boolean_T c10_d_covSaturation;
  uint16_T c10_c_hfi;
  uint16_T c10_d_hfi;
  uint16_T c10_u2;
  uint16_T c10_u3;
  uint16_T c10_e_a0;
  uint16_T c10_f_a0;
  uint16_T c10_b0;
  uint16_T c10_b_b0;
  uint16_T c10_c_a;
  uint16_T c10_d_a;
  uint16_T c10_b;
  uint16_T c10_b_b;
  uint16_T c10_g_a0;
  uint16_T c10_h_a0;
  uint16_T c10_c_b0;
  uint16_T c10_d_b0;
  uint16_T c10_e_a1;
  uint16_T c10_f_a1;
  uint16_T c10_b1;
  uint16_T c10_b_b1;
  uint16_T c10_g_a1;
  uint16_T c10_h_a1;
  uint16_T c10_c_b1;
  uint16_T c10_d_b1;
  boolean_T c10_c_c;
  boolean_T c10_d_c;
  int16_T c10_i8;
  int16_T c10_i9;
  int16_T c10_i10;
  int16_T c10_i11;
  int16_T c10_i12;
  int16_T c10_i13;
  int16_T c10_i14;
  int16_T c10_i15;
  int16_T c10_i16;
  int16_T c10_i17;
  int16_T c10_i18;
  int16_T c10_i19;
  int32_T c10_i20;
  int32_T c10_i21;
  int16_T c10_i22;
  int16_T c10_i23;
  int16_T c10_i24;
  int16_T c10_i25;
  int16_T c10_i26;
  int16_T c10_i27;
  int16_T c10_i28;
  int16_T c10_i29;
  int16_T c10_i30;
  int16_T c10_i31;
  int16_T c10_i32;
  int16_T c10_i33;
  int32_T c10_i34;
  int32_T c10_i35;
  int16_T c10_i36;
  int16_T c10_i37;
  int16_T c10_i38;
  int16_T c10_i39;
  int16_T c10_i40;
  int16_T c10_i41;
  int16_T c10_i42;
  int16_T c10_i43;
  real_T c10_d2;
  real_T c10_d3;
  int16_T c10_i_varargin_1;
  int16_T c10_i_a0;
  int16_T c10_j_varargin_1;
  int16_T c10_e_b0;
  int16_T c10_e_var1;
  int16_T c10_k_varargin_1;
  int16_T c10_i44;
  int16_T c10_v;
  boolean_T c10_e_covSaturation;
  int16_T c10_val;
  int16_T c10_c_b;
  int32_T c10_i45;
  uint16_T c10_e_hfi;
  real_T c10_d4;
  int32_T c10_i46;
  real_T c10_d5;
  real_T c10_d6;
  real_T c10_d7;
  int32_T c10_i47;
  int32_T c10_i48;
  int32_T c10_i49;
  real_T c10_d8;
  real_T c10_d9;
  int32_T c10_e_c;
  int32_T c10_l_varargin_1;
  int32_T c10_m_varargin_1;
  int32_T c10_f_var1;
  int32_T c10_i50;
  boolean_T c10_f_covSaturation;
  uint16_T c10_f_hfi;
  observerLogReadPIC(&c10_PICOffset);
  chartInstance->c10_JITTransitionAnimation[0] = 0U;
  _sfTime_ = sf_get_time(chartInstance->S);
  covrtSigUpdateFcn(chartInstance->c10_covrtInstance, 4U, (real_T)
                    *chartInstance->c10_init);
  covrtSigUpdateFcn(chartInstance->c10_covrtInstance, 3U, (real_T)
                    *chartInstance->c10_sum);
  covrtSigUpdateFcn(chartInstance->c10_covrtInstance, 2U, (real_T)
                    *chartInstance->c10_max);
  covrtSigUpdateFcn(chartInstance->c10_covrtInstance, 1U, (real_T)
                    *chartInstance->c10_offset);
  covrtSigUpdateFcn(chartInstance->c10_covrtInstance, 0U, (real_T)
                    *chartInstance->c10_enable);
  chartInstance->c10_sfEvent = CALL_EVENT;
  c10_b_enable = *chartInstance->c10_enable;
  c10_b_offset = *chartInstance->c10_offset;
  c10_b_max = *chartInstance->c10_max;
  c10_b_sum = *chartInstance->c10_sum;
  c10_b_init = *chartInstance->c10_init;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  /* logging input variable 'max' for function 'eML_blk_kernel' */
  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  /* logging input variable 'init' for function 'eML_blk_kernel' */
  chartInstance->c10_emlrtLocLogSimulated = true;

  /* logging input variable 'enable' for function 'eML_blk_kernel' */
  c10_emlrt_update_log_1(chartInstance, c10_b_enable,
    chartInstance->c10_emlrtLocationLoggingDataTables, 0);

  /* logging input variable 'offset' for function 'eML_blk_kernel' */
  c10_emlrt_update_log_2(chartInstance, c10_b_offset,
    chartInstance->c10_emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'max' for function 'eML_blk_kernel' */
  c10_emlrt_update_log_3(chartInstance, c10_b_max,
    chartInstance->c10_emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'sum' for function 'eML_blk_kernel' */
  c10_emlrt_update_log_3(chartInstance, c10_b_sum,
    chartInstance->c10_emlrtLocationLoggingDataTables, 3);

  /* logging input variable 'init' for function 'eML_blk_kernel' */
  c10_emlrt_update_log_1(chartInstance, c10_b_init,
    chartInstance->c10_emlrtLocationLoggingDataTables, 4);
  covrtEmlFcnEval(chartInstance->c10_covrtInstance, 4U, 0, 0);
  covrtEmlIfEval(chartInstance->c10_covrtInstance, 4U, 0, 0, false);
  c10_a0 = c10_b_enable;
  c10_a = c10_a0;
  c10_b_a0 = c10_a;
  c10_a1 = c10_b_a0;
  c10_b_a1 = c10_a1;
  c10_c = (c10_b_a1 == 0);
  c10_i = (int8_T)c10_b_enable;
  if ((int8_T)(c10_i & 2) != 0) {
    c10_i1 = (int8_T)(c10_i | -2);
  } else {
    c10_i1 = (int8_T)(c10_i & 1);
  }

  if (c10_i1 > 0) {
    c10_d = 3.0;
  } else {
    c10_d = 2.0;
  }

  if (covrtEmlIfEval(chartInstance->c10_covrtInstance, 4U, 0, 1,
                     covrtRelationalopUpdateFcn(chartInstance->c10_covrtInstance,
        4U, 0U, 0U, c10_d, 0.0, -2, 0U, (int32_T)c10_emlrt_update_log_4
        (chartInstance, c10_c, chartInstance->c10_emlrtLocationLoggingDataTables,
         5)))) {
    c10_b_cont = c10_emlrt_update_log_5(chartInstance, 0U,
      chartInstance->c10_emlrtLocationLoggingDataTables, 6);
    c10_b_out_init = c10_emlrt_update_log_1(chartInstance, 0U,
      chartInstance->c10_emlrtLocationLoggingDataTables, 7);
  } else {
    c10_c_a0 = c10_b_init;
    c10_b_a = c10_c_a0;
    c10_d_a0 = c10_b_a;
    c10_c_a1 = c10_d_a0;
    c10_d_a1 = c10_c_a1;
    c10_b_c = (c10_d_a1 == 0);
    c10_i2 = (int8_T)c10_b_init;
    if ((int8_T)(c10_i2 & 2) != 0) {
      c10_i3 = (int8_T)(c10_i2 | -2);
    } else {
      c10_i3 = (int8_T)(c10_i2 & 1);
    }

    if (c10_i3 > 0) {
      c10_d1 = 3.0;
    } else {
      c10_d1 = 2.0;
    }

    if (covrtEmlIfEval(chartInstance->c10_covrtInstance, 4U, 0, 2,
                       covrtRelationalopUpdateFcn
                       (chartInstance->c10_covrtInstance, 4U, 0U, 1U, c10_d1,
                        0.0, -2, 0U, (int32_T)c10_emlrt_update_log_4
                        (chartInstance, c10_b_c,
                         chartInstance->c10_emlrtLocationLoggingDataTables, 8))))
    {
      c10_b_varargin_1 = c10_b_sum;
      c10_d_varargin_1 = c10_b_varargin_1;
      c10_b_var1 = c10_d_varargin_1;
      c10_i5 = c10_b_var1;
      c10_b_covSaturation = false;
      if (c10_i5 < 0) {
        c10_i5 = 0;
      } else {
        if (c10_i5 > 4095) {
          c10_i5 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c10_covrtInstance, 4, 0, 0, 0,
          c10_b_covSaturation);
      }

      c10_b_hfi = (uint16_T)c10_i5;
      c10_u1 = c10_emlrt_update_log_5(chartInstance, c10_b_hfi,
        chartInstance->c10_emlrtLocationLoggingDataTables, 10);
      c10_f_varargin_1 = c10_b_max;
      c10_h_varargin_1 = c10_f_varargin_1;
      c10_d_var1 = c10_h_varargin_1;
      c10_i7 = c10_d_var1;
      c10_d_covSaturation = false;
      if (c10_i7 < 0) {
        c10_i7 = 0;
      } else {
        if (c10_i7 > 4095) {
          c10_i7 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c10_covrtInstance, 4, 0, 1, 0,
          c10_d_covSaturation);
      }

      c10_d_hfi = (uint16_T)c10_i7;
      c10_u3 = c10_emlrt_update_log_5(chartInstance, c10_d_hfi,
        chartInstance->c10_emlrtLocationLoggingDataTables, 11);
      c10_f_a0 = c10_u1;
      c10_b_b0 = c10_u3;
      c10_d_a = c10_f_a0;
      c10_b_b = c10_b_b0;
      c10_h_a0 = c10_d_a;
      c10_d_b0 = c10_b_b;
      c10_f_a1 = c10_h_a0;
      c10_b_b1 = c10_d_b0;
      c10_h_a1 = c10_f_a1;
      c10_d_b1 = c10_b_b1;
      c10_d_c = (c10_h_a1 < c10_d_b1);
      c10_i9 = (int16_T)c10_u1;
      c10_i11 = (int16_T)c10_u3;
      c10_i13 = (int16_T)c10_u3;
      c10_i15 = (int16_T)c10_u1;
      if ((int16_T)(c10_i13 & 4096) != 0) {
        c10_i17 = (int16_T)(c10_i13 | -4096);
      } else {
        c10_i17 = (int16_T)(c10_i13 & 4095);
      }

      if ((int16_T)(c10_i15 & 4096) != 0) {
        c10_i19 = (int16_T)(c10_i15 | -4096);
      } else {
        c10_i19 = (int16_T)(c10_i15 & 4095);
      }

      c10_i21 = c10_i17 - c10_i19;
      if (c10_i21 > 4095) {
        c10_i21 = 4095;
      } else {
        if (c10_i21 < -4096) {
          c10_i21 = -4096;
        }
      }

      c10_i23 = (int16_T)c10_u1;
      c10_i25 = (int16_T)c10_u3;
      c10_i27 = (int16_T)c10_u1;
      c10_i29 = (int16_T)c10_u3;
      if ((int16_T)(c10_i27 & 4096) != 0) {
        c10_i31 = (int16_T)(c10_i27 | -4096);
      } else {
        c10_i31 = (int16_T)(c10_i27 & 4095);
      }

      if ((int16_T)(c10_i29 & 4096) != 0) {
        c10_i33 = (int16_T)(c10_i29 | -4096);
      } else {
        c10_i33 = (int16_T)(c10_i29 & 4095);
      }

      c10_i35 = c10_i31 - c10_i33;
      if (c10_i35 > 4095) {
        c10_i35 = 4095;
      } else {
        if (c10_i35 < -4096) {
          c10_i35 = -4096;
        }
      }

      if ((int16_T)(c10_i9 & 4096) != 0) {
        c10_i37 = (int16_T)(c10_i9 | -4096);
      } else {
        c10_i37 = (int16_T)(c10_i9 & 4095);
      }

      if ((int16_T)(c10_i11 & 4096) != 0) {
        c10_i39 = (int16_T)(c10_i11 | -4096);
      } else {
        c10_i39 = (int16_T)(c10_i11 & 4095);
      }

      if ((int16_T)(c10_i23 & 4096) != 0) {
        c10_i41 = (int16_T)(c10_i23 | -4096);
      } else {
        c10_i41 = (int16_T)(c10_i23 & 4095);
      }

      if ((int16_T)(c10_i25 & 4096) != 0) {
        c10_i43 = (int16_T)(c10_i25 | -4096);
      } else {
        c10_i43 = (int16_T)(c10_i25 & 4095);
      }

      if (c10_i37 < c10_i39) {
        c10_d3 = (real_T)((int16_T)c10_i21 <= 1);
      } else if (c10_i41 > c10_i43) {
        if ((int16_T)c10_i35 <= 1) {
          c10_d3 = 3.0;
        } else {
          c10_d3 = 0.0;
        }
      } else {
        c10_d3 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c10_covrtInstance, 4U, 0, 3,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c10_covrtInstance, 4U, 0U, 2U, c10_d3,
                          0.0, -2, 2U, (int32_T)c10_emlrt_update_log_4
                          (chartInstance, c10_d_c,
                           chartInstance->c10_emlrtLocationLoggingDataTables, 9))))
      {
        c10_i_a0 = c10_b_sum;
        c10_e_b0 = c10_b_offset;
        c10_k_varargin_1 = c10_e_b0;
        c10_v = c10_k_varargin_1;
        c10_val = c10_v;
        c10_c_b = c10_val;
        c10_i45 = c10_i_a0;
        if (c10_i45 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c10_d4 = 1.0;
          observerLog(171 + c10_PICOffset, &c10_d4, 1);
        }

        if (c10_i45 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c10_d5 = 1.0;
          observerLog(171 + c10_PICOffset, &c10_d5, 1);
        }

        c10_i46 = c10_c_b;
        if (c10_i46 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c10_d6 = 1.0;
          observerLog(174 + c10_PICOffset, &c10_d6, 1);
        }

        if (c10_i46 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c10_d7 = 1.0;
          observerLog(174 + c10_PICOffset, &c10_d7, 1);
        }

        if ((c10_i45 & 65536) != 0) {
          c10_i47 = c10_i45 | -65536;
        } else {
          c10_i47 = c10_i45 & 65535;
        }

        if ((c10_i46 & 65536) != 0) {
          c10_i48 = c10_i46 | -65536;
        } else {
          c10_i48 = c10_i46 & 65535;
        }

        c10_i49 = c10__s32_add__(chartInstance, c10_i47, c10_i48, 173, 1U, 323,
          12);
        if (c10_i49 > 65535) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c10_d8 = 1.0;
          observerLog(179 + c10_PICOffset, &c10_d8, 1);
        }

        if (c10_i49 < -65536) {
          sf_data_overflow_error(chartInstance->S, 1U, 323, 12);
          c10_d9 = 1.0;
          observerLog(179 + c10_PICOffset, &c10_d9, 1);
        }

        if ((c10_i49 & 65536) != 0) {
          c10_e_c = c10_i49 | -65536;
        } else {
          c10_e_c = c10_i49 & 65535;
        }

        c10_l_varargin_1 = c10_emlrt_update_log_6(chartInstance, c10_e_c,
          chartInstance->c10_emlrtLocationLoggingDataTables, 13);
        c10_m_varargin_1 = c10_l_varargin_1;
        c10_f_var1 = c10_m_varargin_1;
        c10_i50 = c10_f_var1;
        c10_f_covSaturation = false;
        if (c10_i50 < 0) {
          c10_i50 = 0;
        } else {
          if (c10_i50 > 4095) {
            c10_i50 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c10_covrtInstance, 4, 0, 2, 0,
            c10_f_covSaturation);
        }

        c10_f_hfi = (uint16_T)c10_i50;
        c10_b_cont = c10_emlrt_update_log_5(chartInstance, c10_f_hfi,
          chartInstance->c10_emlrtLocationLoggingDataTables, 12);
        c10_b_out_init = c10_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c10_emlrtLocationLoggingDataTables, 14);
      } else {
        c10_b_cont = c10_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c10_emlrtLocationLoggingDataTables, 15);
        c10_b_out_init = c10_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c10_emlrtLocationLoggingDataTables, 16);
      }
    } else {
      c10_varargin_1 = c10_b_sum;
      c10_c_varargin_1 = c10_varargin_1;
      c10_var1 = c10_c_varargin_1;
      c10_i4 = c10_var1;
      c10_covSaturation = false;
      if (c10_i4 < 0) {
        c10_i4 = 0;
      } else {
        if (c10_i4 > 4095) {
          c10_i4 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c10_covrtInstance, 4, 0, 3, 0,
          c10_covSaturation);
      }

      c10_hfi = (uint16_T)c10_i4;
      c10_u = c10_emlrt_update_log_5(chartInstance, c10_hfi,
        chartInstance->c10_emlrtLocationLoggingDataTables, 18);
      c10_e_varargin_1 = c10_b_max;
      c10_g_varargin_1 = c10_e_varargin_1;
      c10_c_var1 = c10_g_varargin_1;
      c10_i6 = c10_c_var1;
      c10_c_covSaturation = false;
      if (c10_i6 < 0) {
        c10_i6 = 0;
      } else {
        if (c10_i6 > 4095) {
          c10_i6 = 4095;
        }

        covrtSaturationUpdateFcn(chartInstance->c10_covrtInstance, 4, 0, 4, 0,
          c10_c_covSaturation);
      }

      c10_c_hfi = (uint16_T)c10_i6;
      c10_u2 = c10_emlrt_update_log_5(chartInstance, c10_c_hfi,
        chartInstance->c10_emlrtLocationLoggingDataTables, 19);
      c10_e_a0 = c10_u;
      c10_b0 = c10_u2;
      c10_c_a = c10_e_a0;
      c10_b = c10_b0;
      c10_g_a0 = c10_c_a;
      c10_c_b0 = c10_b;
      c10_e_a1 = c10_g_a0;
      c10_b1 = c10_c_b0;
      c10_g_a1 = c10_e_a1;
      c10_c_b1 = c10_b1;
      c10_c_c = (c10_g_a1 < c10_c_b1);
      c10_i8 = (int16_T)c10_u;
      c10_i10 = (int16_T)c10_u2;
      c10_i12 = (int16_T)c10_u2;
      c10_i14 = (int16_T)c10_u;
      if ((int16_T)(c10_i12 & 4096) != 0) {
        c10_i16 = (int16_T)(c10_i12 | -4096);
      } else {
        c10_i16 = (int16_T)(c10_i12 & 4095);
      }

      if ((int16_T)(c10_i14 & 4096) != 0) {
        c10_i18 = (int16_T)(c10_i14 | -4096);
      } else {
        c10_i18 = (int16_T)(c10_i14 & 4095);
      }

      c10_i20 = c10_i16 - c10_i18;
      if (c10_i20 > 4095) {
        c10_i20 = 4095;
      } else {
        if (c10_i20 < -4096) {
          c10_i20 = -4096;
        }
      }

      c10_i22 = (int16_T)c10_u;
      c10_i24 = (int16_T)c10_u2;
      c10_i26 = (int16_T)c10_u;
      c10_i28 = (int16_T)c10_u2;
      if ((int16_T)(c10_i26 & 4096) != 0) {
        c10_i30 = (int16_T)(c10_i26 | -4096);
      } else {
        c10_i30 = (int16_T)(c10_i26 & 4095);
      }

      if ((int16_T)(c10_i28 & 4096) != 0) {
        c10_i32 = (int16_T)(c10_i28 | -4096);
      } else {
        c10_i32 = (int16_T)(c10_i28 & 4095);
      }

      c10_i34 = c10_i30 - c10_i32;
      if (c10_i34 > 4095) {
        c10_i34 = 4095;
      } else {
        if (c10_i34 < -4096) {
          c10_i34 = -4096;
        }
      }

      if ((int16_T)(c10_i8 & 4096) != 0) {
        c10_i36 = (int16_T)(c10_i8 | -4096);
      } else {
        c10_i36 = (int16_T)(c10_i8 & 4095);
      }

      if ((int16_T)(c10_i10 & 4096) != 0) {
        c10_i38 = (int16_T)(c10_i10 | -4096);
      } else {
        c10_i38 = (int16_T)(c10_i10 & 4095);
      }

      if ((int16_T)(c10_i22 & 4096) != 0) {
        c10_i40 = (int16_T)(c10_i22 | -4096);
      } else {
        c10_i40 = (int16_T)(c10_i22 & 4095);
      }

      if ((int16_T)(c10_i24 & 4096) != 0) {
        c10_i42 = (int16_T)(c10_i24 | -4096);
      } else {
        c10_i42 = (int16_T)(c10_i24 & 4095);
      }

      if (c10_i36 < c10_i38) {
        c10_d2 = (real_T)((int16_T)c10_i20 <= 1);
      } else if (c10_i40 > c10_i42) {
        if ((int16_T)c10_i34 <= 1) {
          c10_d2 = 3.0;
        } else {
          c10_d2 = 0.0;
        }
      } else {
        c10_d2 = 2.0;
      }

      if (covrtEmlIfEval(chartInstance->c10_covrtInstance, 4U, 0, 4,
                         covrtRelationalopUpdateFcn
                         (chartInstance->c10_covrtInstance, 4U, 0U, 3U, c10_d2,
                          0.0, -2, 2U, (int32_T)c10_emlrt_update_log_4
                          (chartInstance, c10_c_c,
                           chartInstance->c10_emlrtLocationLoggingDataTables, 17))))
      {
        c10_i_varargin_1 = c10_b_sum;
        c10_j_varargin_1 = c10_i_varargin_1;
        c10_e_var1 = c10_j_varargin_1;
        c10_i44 = c10_e_var1;
        c10_e_covSaturation = false;
        if (c10_i44 < 0) {
          c10_i44 = 0;
        } else {
          if (c10_i44 > 4095) {
            c10_i44 = 4095;
          }

          covrtSaturationUpdateFcn(chartInstance->c10_covrtInstance, 4, 0, 5, 0,
            c10_e_covSaturation);
        }

        c10_e_hfi = (uint16_T)c10_i44;
        c10_b_cont = c10_emlrt_update_log_5(chartInstance, c10_e_hfi,
          chartInstance->c10_emlrtLocationLoggingDataTables, 20);
        c10_b_out_init = c10_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c10_emlrtLocationLoggingDataTables, 21);
      } else {
        c10_b_cont = c10_emlrt_update_log_5(chartInstance, 0U,
          chartInstance->c10_emlrtLocationLoggingDataTables, 22);
        c10_b_out_init = c10_emlrt_update_log_1(chartInstance, 1U,
          chartInstance->c10_emlrtLocationLoggingDataTables, 23);
      }
    }
  }

  *chartInstance->c10_cont = c10_b_cont;
  *chartInstance->c10_out_init = c10_b_out_init;
  c10_do_animation_call_c10_PWM_28_HalfB(chartInstance);
  covrtSigUpdateFcn(chartInstance->c10_covrtInstance, 5U, (real_T)
                    *chartInstance->c10_cont);
  covrtSigUpdateFcn(chartInstance->c10_covrtInstance, 6U, (real_T)
                    *chartInstance->c10_out_init);
}

static void mdl_start_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_terminate_c10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void mdl_setup_runtime_resources_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance)
{
  static const uint32_T c10_decisionTxtStartIdx = 0U;
  static const uint32_T c10_decisionTxtEndIdx = 0U;
  setLegacyDebuggerFlag(chartInstance->S, false);
  setDebuggerFlag(chartInstance->S, true);
  setDataBrowseFcn(chartInstance->S, (void *)&c10_chart_data_browse_helper);
  chartInstance->c10_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerInitializeRuntimeVars(chartInstance->c10_RuntimeVar,
    &chartInstance->c10_IsDebuggerActive,
    &chartInstance->c10_IsSequenceViewerPresent, 0, 0,
    &chartInstance->c10_mlFcnLineNumber);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c10_covrtInstance, 1U, 0U, 1U,
    203U);
  covrtChartInitFcn(chartInstance->c10_covrtInstance, 0U, false, false, false);
  covrtStateInitFcn(chartInstance->c10_covrtInstance, 0U, 0U, false, false,
                    false, 0U, &c10_decisionTxtStartIdx, &c10_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c10_covrtInstance, 0U, 0, NULL, NULL, 0U,
                    NULL);
  covrtEmlInitFcn(chartInstance->c10_covrtInstance, "", 4U, 0U, 1U, 0U, 5U, 0U,
                  6U, 0U, 0U, 0U, 0U, 0U);
  covrtEmlFcnInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 0U,
                     "eML_blk_kernel", 0, -1, 756);
  covrtEmlSaturationInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 0U, 266,
    -1, 279);
  covrtEmlSaturationInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 1U, 282,
    -1, 295);
  covrtEmlSaturationInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 2U, 319,
    -1, 341);
  covrtEmlSaturationInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 3U, 518,
    -1, 531);
  covrtEmlSaturationInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 4U, 534,
    -1, 547);
  covrtEmlSaturationInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 5U, 571,
    -1, 584);
  covrtEmlIfInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 0U, 70, 86, -1,
                    121);
  covrtEmlIfInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 1U, 127, 150, 216,
                    752);
  covrtEmlIfInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 2U, 229, 250, 498,
                    744);
  covrtEmlIfInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 3U, 263, 295, 394,
                    489);
  covrtEmlIfInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 4U, 515, 547, 637,
                    732);
  covrtEmlRelationalInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 0U, 130,
    150, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 1U, 232,
    250, -2, 0U);
  covrtEmlRelationalInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 2U, 266,
    295, -2, 2U);
  covrtEmlRelationalInitFcn(chartInstance->c10_covrtInstance, 4U, 0U, 3U, 518,
    547, -2, 2U);
}

static void mdl_cleanup_runtime_resources_c10_PWM_28_HalfB
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance)
{
  const emlrtLocationLoggingFileInfoType c10_emlrtLocationLoggingFileInfo = {
    "#PWM_28_HalfB:7441",              /* mexFileName */
    "Thu May 27 10:27:20 2021",        /* timestamp */
    "",                                /* buildDir */
    1,                                 /* numFcns */
    256                                /* numHistogramBins */
  };

  const emlrtLocationLoggingFunctionInfoType
    c10_emlrtLocationLoggingFunctionInfoTable[1] = { { "eML_blk_kernel",/* fcnName */
      1,                               /* fcnId */
      24                               /* numInstrPoints */
    } };

  const emlrtLocationLoggingLocationType c10_emlrtLocationInfo[26] = { { 1,/* MxInfoID */
      37,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 2,                            /* MxInfoID */
      44,                              /* TextStart */
      6,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      51,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 3,                            /* MxInfoID */
      55,                              /* TextStart */
      3,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      59,                              /* TextStart */
      4,                               /* TextLength */
      1,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      130,                             /* TextStart */
      20,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      159,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      187,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      232,                             /* TextStart */
      18,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      266,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      266,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      282,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      312,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      319,                             /* TextStart */
      22,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 6,                            /* MxInfoID */
      323,                             /* TextStart */
      12,                              /* TextLength */
      5,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      359,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      415,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      451,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 4,                            /* MxInfoID */
      518,                             /* TextStart */
      29,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      518,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      534,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      564,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      true                             /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      571,                             /* TextStart */
      13,                              /* TextLength */
      3,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      602,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 5,                            /* MxInfoID */
      658,                             /* TextStart */
      4,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    }, { 1,                            /* MxInfoID */
      694,                             /* TextStart */
      8,                               /* TextLength */
      2,                               /* Reason */
      false                            /* MoreLocations */
    } };

  const int32_T c10_emlrtLocationLoggingFieldCounts[24] = { 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  if (chartInstance->c10_emlrtLocLogSimulated) {
    emlrtLocationLoggingClearLog("#PWM_28_HalfB:7441");
    emlrtLocationLoggingPushLog(&c10_emlrtLocationLoggingFileInfo,
      c10_emlrtLocationLoggingFunctionInfoTable,
      chartInstance->c10_emlrtLocationLoggingDataTables, c10_emlrtLocationInfo,
      NULL, 0U, c10_emlrtLocationLoggingFieldCounts, NULL);
    addResultsToFPTRepository("#PWM_28_HalfB:7441");
  }

  sfListenerLightTerminate(chartInstance->c10_RuntimeVar);
  sf_mex_destroy(&c10_eml_mx);
  sf_mex_destroy(&c10_b_eml_mx);
  sf_mex_destroy(&c10_c_eml_mx);
  covrtDeleteStateflowInstanceData(chartInstance->c10_covrtInstance);
}

static void initSimStructsc10_PWM_28_HalfB(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c10_emlrt_update_log_1(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, uint8_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index)
{
  boolean_T c10_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c10_b_table;
  real_T c10_d;
  uint8_T c10_u;
  uint8_T c10_localMin;
  real_T c10_d1;
  uint8_T c10_u1;
  uint8_T c10_localMax;
  emlrtLocationLoggingHistogramType *c10_histTable;
  real_T c10_inDouble;
  real_T c10_significand;
  int32_T c10_exponent;
  (void)chartInstance;
  c10_isLoggingEnabledHere = (c10_index >= 0);
  if (c10_isLoggingEnabledHere) {
    c10_b_table = (emlrtLocationLoggingDataType *)&c10_table[c10_index];
    c10_d = c10_b_table[0U].SimMin;
    if (c10_d < 2.0) {
      if (c10_d >= 0.0) {
        c10_u = (uint8_T)c10_d;
      } else {
        c10_u = 0U;
      }
    } else if (c10_d >= 2.0) {
      c10_u = 1U;
    } else {
      c10_u = 0U;
    }

    c10_localMin = c10_u;
    c10_d1 = c10_b_table[0U].SimMax;
    if (c10_d1 < 2.0) {
      if (c10_d1 >= 0.0) {
        c10_u1 = (uint8_T)c10_d1;
      } else {
        c10_u1 = 0U;
      }
    } else if (c10_d1 >= 2.0) {
      c10_u1 = 1U;
    } else {
      c10_u1 = 0U;
    }

    c10_localMax = c10_u1;
    c10_histTable = c10_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c10_in < c10_localMin) {
      c10_localMin = c10_in;
    }

    if (c10_in > c10_localMax) {
      c10_localMax = c10_in;
    }

    /* Histogram logging. */
    c10_inDouble = (real_T)c10_in;
    c10_histTable->TotalNumberOfValues++;
    if (c10_inDouble == 0.0) {
      c10_histTable->NumberOfZeros++;
    } else {
      c10_histTable->SimSum += c10_inDouble;
      if ((!muDoubleScalarIsInf(c10_inDouble)) && (!muDoubleScalarIsNaN
           (c10_inDouble))) {
        c10_significand = frexp(c10_inDouble, &c10_exponent);
        if (c10_exponent > 128) {
          c10_exponent = 128;
        }

        if (c10_exponent < -127) {
          c10_exponent = -127;
        }

        if (c10_significand < 0.0) {
          c10_histTable->NumberOfNegativeValues++;
          c10_histTable->HistogramOfNegativeValues[127 + c10_exponent]++;
        } else {
          c10_histTable->NumberOfPositiveValues++;
          c10_histTable->HistogramOfPositiveValues[127 + c10_exponent]++;
        }
      }
    }

    c10_b_table[0U].SimMin = (real_T)c10_localMin;
    c10_b_table[0U].SimMax = (real_T)c10_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c10_in;
}

static int16_T c10_emlrt_update_log_2(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index)
{
  boolean_T c10_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c10_b_table;
  real_T c10_d;
  int16_T c10_i;
  int16_T c10_localMin;
  real_T c10_d1;
  int16_T c10_i1;
  int16_T c10_localMax;
  emlrtLocationLoggingHistogramType *c10_histTable;
  real_T c10_inDouble;
  real_T c10_significand;
  int32_T c10_exponent;
  (void)chartInstance;
  c10_isLoggingEnabledHere = (c10_index >= 0);
  if (c10_isLoggingEnabledHere) {
    c10_b_table = (emlrtLocationLoggingDataType *)&c10_table[c10_index];
    c10_d = muDoubleScalarFloor(c10_b_table[0U].SimMin);
    if (c10_d < 32768.0) {
      if (c10_d >= -32768.0) {
        c10_i = (int16_T)c10_d;
      } else {
        c10_i = MIN_int16_T;
      }
    } else if (c10_d >= 32768.0) {
      c10_i = MAX_int16_T;
    } else {
      c10_i = 0;
    }

    c10_localMin = c10_i;
    c10_d1 = muDoubleScalarFloor(c10_b_table[0U].SimMax);
    if (c10_d1 < 32768.0) {
      if (c10_d1 >= -32768.0) {
        c10_i1 = (int16_T)c10_d1;
      } else {
        c10_i1 = MIN_int16_T;
      }
    } else if (c10_d1 >= 32768.0) {
      c10_i1 = MAX_int16_T;
    } else {
      c10_i1 = 0;
    }

    c10_localMax = c10_i1;
    c10_histTable = c10_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c10_in < c10_localMin) {
      c10_localMin = c10_in;
    }

    if (c10_in > c10_localMax) {
      c10_localMax = c10_in;
    }

    /* Histogram logging. */
    c10_inDouble = (real_T)c10_in;
    c10_histTable->TotalNumberOfValues++;
    if (c10_inDouble == 0.0) {
      c10_histTable->NumberOfZeros++;
    } else {
      c10_histTable->SimSum += c10_inDouble;
      if ((!muDoubleScalarIsInf(c10_inDouble)) && (!muDoubleScalarIsNaN
           (c10_inDouble))) {
        c10_significand = frexp(c10_inDouble, &c10_exponent);
        if (c10_exponent > 128) {
          c10_exponent = 128;
        }

        if (c10_exponent < -127) {
          c10_exponent = -127;
        }

        if (c10_significand < 0.0) {
          c10_histTable->NumberOfNegativeValues++;
          c10_histTable->HistogramOfNegativeValues[127 + c10_exponent]++;
        } else {
          c10_histTable->NumberOfPositiveValues++;
          c10_histTable->HistogramOfPositiveValues[127 + c10_exponent]++;
        }
      }
    }

    c10_b_table[0U].SimMin = (real_T)c10_localMin;
    c10_b_table[0U].SimMax = (real_T)c10_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c10_in;
}

static int16_T c10_emlrt_update_log_3(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, int16_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index)
{
  boolean_T c10_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c10_b_table;
  real_T c10_d;
  int16_T c10_i;
  int16_T c10_localMin;
  real_T c10_d1;
  int16_T c10_i1;
  int16_T c10_localMax;
  emlrtLocationLoggingHistogramType *c10_histTable;
  real_T c10_inDouble;
  real_T c10_significand;
  int32_T c10_exponent;
  (void)chartInstance;
  c10_isLoggingEnabledHere = (c10_index >= 0);
  if (c10_isLoggingEnabledHere) {
    c10_b_table = (emlrtLocationLoggingDataType *)&c10_table[c10_index];
    c10_d = muDoubleScalarFloor(c10_b_table[0U].SimMin);
    if (c10_d < 2048.0) {
      if (c10_d >= -2048.0) {
        c10_i = (int16_T)c10_d;
      } else {
        c10_i = -2048;
      }
    } else if (c10_d >= 2048.0) {
      c10_i = 2047;
    } else {
      c10_i = 0;
    }

    c10_localMin = c10_i;
    c10_d1 = muDoubleScalarFloor(c10_b_table[0U].SimMax);
    if (c10_d1 < 2048.0) {
      if (c10_d1 >= -2048.0) {
        c10_i1 = (int16_T)c10_d1;
      } else {
        c10_i1 = -2048;
      }
    } else if (c10_d1 >= 2048.0) {
      c10_i1 = 2047;
    } else {
      c10_i1 = 0;
    }

    c10_localMax = c10_i1;
    c10_histTable = c10_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c10_in < c10_localMin) {
      c10_localMin = c10_in;
    }

    if (c10_in > c10_localMax) {
      c10_localMax = c10_in;
    }

    /* Histogram logging. */
    c10_inDouble = (real_T)c10_in;
    c10_histTable->TotalNumberOfValues++;
    if (c10_inDouble == 0.0) {
      c10_histTable->NumberOfZeros++;
    } else {
      c10_histTable->SimSum += c10_inDouble;
      if ((!muDoubleScalarIsInf(c10_inDouble)) && (!muDoubleScalarIsNaN
           (c10_inDouble))) {
        c10_significand = frexp(c10_inDouble, &c10_exponent);
        if (c10_exponent > 128) {
          c10_exponent = 128;
        }

        if (c10_exponent < -127) {
          c10_exponent = -127;
        }

        if (c10_significand < 0.0) {
          c10_histTable->NumberOfNegativeValues++;
          c10_histTable->HistogramOfNegativeValues[127 + c10_exponent]++;
        } else {
          c10_histTable->NumberOfPositiveValues++;
          c10_histTable->HistogramOfPositiveValues[127 + c10_exponent]++;
        }
      }
    }

    c10_b_table[0U].SimMin = (real_T)c10_localMin;
    c10_b_table[0U].SimMax = (real_T)c10_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c10_in;
}

static boolean_T c10_emlrt_update_log_4(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, boolean_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index)
{
  boolean_T c10_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c10_b_table;
  boolean_T c10_localMin;
  boolean_T c10_localMax;
  emlrtLocationLoggingHistogramType *c10_histTable;
  real_T c10_inDouble;
  real_T c10_significand;
  int32_T c10_exponent;
  (void)chartInstance;
  c10_isLoggingEnabledHere = (c10_index >= 0);
  if (c10_isLoggingEnabledHere) {
    c10_b_table = (emlrtLocationLoggingDataType *)&c10_table[c10_index];
    c10_localMin = (c10_b_table[0U].SimMin > 0.0);
    c10_localMax = (c10_b_table[0U].SimMax > 0.0);
    c10_histTable = c10_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if ((int32_T)c10_in < (int32_T)c10_localMin) {
      c10_localMin = c10_in;
    }

    if ((int32_T)c10_in > (int32_T)c10_localMax) {
      c10_localMax = c10_in;
    }

    /* Histogram logging. */
    c10_inDouble = (real_T)c10_in;
    c10_histTable->TotalNumberOfValues++;
    if (c10_inDouble == 0.0) {
      c10_histTable->NumberOfZeros++;
    } else {
      c10_histTable->SimSum += c10_inDouble;
      if ((!muDoubleScalarIsInf(c10_inDouble)) && (!muDoubleScalarIsNaN
           (c10_inDouble))) {
        c10_significand = frexp(c10_inDouble, &c10_exponent);
        if (c10_exponent > 128) {
          c10_exponent = 128;
        }

        if (c10_exponent < -127) {
          c10_exponent = -127;
        }

        if (c10_significand < 0.0) {
          c10_histTable->NumberOfNegativeValues++;
          c10_histTable->HistogramOfNegativeValues[127 + c10_exponent]++;
        } else {
          c10_histTable->NumberOfPositiveValues++;
          c10_histTable->HistogramOfPositiveValues[127 + c10_exponent]++;
        }
      }
    }

    c10_b_table[0U].SimMin = (real_T)c10_localMin;
    c10_b_table[0U].SimMax = (real_T)c10_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c10_in;
}

static uint16_T c10_emlrt_update_log_5(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, uint16_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index)
{
  boolean_T c10_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c10_b_table;
  real_T c10_d;
  uint16_T c10_u;
  uint16_T c10_localMin;
  real_T c10_d1;
  uint16_T c10_u1;
  uint16_T c10_localMax;
  emlrtLocationLoggingHistogramType *c10_histTable;
  real_T c10_inDouble;
  real_T c10_significand;
  int32_T c10_exponent;
  (void)chartInstance;
  c10_isLoggingEnabledHere = (c10_index >= 0);
  if (c10_isLoggingEnabledHere) {
    c10_b_table = (emlrtLocationLoggingDataType *)&c10_table[c10_index];
    c10_d = c10_b_table[0U].SimMin;
    if (c10_d < 4096.0) {
      if (c10_d >= 0.0) {
        c10_u = (uint16_T)c10_d;
      } else {
        c10_u = 0U;
      }
    } else if (c10_d >= 4096.0) {
      c10_u = 4095U;
    } else {
      c10_u = 0U;
    }

    c10_localMin = c10_u;
    c10_d1 = c10_b_table[0U].SimMax;
    if (c10_d1 < 4096.0) {
      if (c10_d1 >= 0.0) {
        c10_u1 = (uint16_T)c10_d1;
      } else {
        c10_u1 = 0U;
      }
    } else if (c10_d1 >= 4096.0) {
      c10_u1 = 4095U;
    } else {
      c10_u1 = 0U;
    }

    c10_localMax = c10_u1;
    c10_histTable = c10_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c10_in < c10_localMin) {
      c10_localMin = c10_in;
    }

    if (c10_in > c10_localMax) {
      c10_localMax = c10_in;
    }

    /* Histogram logging. */
    c10_inDouble = (real_T)c10_in;
    c10_histTable->TotalNumberOfValues++;
    if (c10_inDouble == 0.0) {
      c10_histTable->NumberOfZeros++;
    } else {
      c10_histTable->SimSum += c10_inDouble;
      if ((!muDoubleScalarIsInf(c10_inDouble)) && (!muDoubleScalarIsNaN
           (c10_inDouble))) {
        c10_significand = frexp(c10_inDouble, &c10_exponent);
        if (c10_exponent > 128) {
          c10_exponent = 128;
        }

        if (c10_exponent < -127) {
          c10_exponent = -127;
        }

        if (c10_significand < 0.0) {
          c10_histTable->NumberOfNegativeValues++;
          c10_histTable->HistogramOfNegativeValues[127 + c10_exponent]++;
        } else {
          c10_histTable->NumberOfPositiveValues++;
          c10_histTable->HistogramOfPositiveValues[127 + c10_exponent]++;
        }
      }
    }

    c10_b_table[0U].SimMin = (real_T)c10_localMin;
    c10_b_table[0U].SimMax = (real_T)c10_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c10_in;
}

static int32_T c10_emlrt_update_log_6(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, int32_T c10_in, emlrtLocationLoggingDataType c10_table[],
  int32_T c10_index)
{
  boolean_T c10_isLoggingEnabledHere;
  emlrtLocationLoggingDataType *c10_b_table;
  real_T c10_d;
  int32_T c10_i;
  int32_T c10_localMin;
  real_T c10_d1;
  int32_T c10_i1;
  int32_T c10_localMax;
  emlrtLocationLoggingHistogramType *c10_histTable;
  real_T c10_inDouble;
  real_T c10_significand;
  int32_T c10_exponent;
  (void)chartInstance;
  c10_isLoggingEnabledHere = (c10_index >= 0);
  if (c10_isLoggingEnabledHere) {
    c10_b_table = (emlrtLocationLoggingDataType *)&c10_table[c10_index];
    c10_d = muDoubleScalarFloor(c10_b_table[0U].SimMin);
    if (c10_d < 65536.0) {
      if (c10_d >= -65536.0) {
        c10_i = (int32_T)c10_d;
      } else {
        c10_i = -65536;
      }
    } else if (c10_d >= 65536.0) {
      c10_i = 65535;
    } else {
      c10_i = 0;
    }

    c10_localMin = c10_i;
    c10_d1 = muDoubleScalarFloor(c10_b_table[0U].SimMax);
    if (c10_d1 < 65536.0) {
      if (c10_d1 >= -65536.0) {
        c10_i1 = (int32_T)c10_d1;
      } else {
        c10_i1 = -65536;
      }
    } else if (c10_d1 >= 65536.0) {
      c10_i1 = 65535;
    } else {
      c10_i1 = 0;
    }

    c10_localMax = c10_i1;
    c10_histTable = c10_b_table[0U].HistogramTable;

    /* Simulation Min-Max logging. */
    if (c10_in < c10_localMin) {
      c10_localMin = c10_in;
    }

    if (c10_in > c10_localMax) {
      c10_localMax = c10_in;
    }

    /* Histogram logging. */
    c10_inDouble = (real_T)c10_in;
    c10_histTable->TotalNumberOfValues++;
    if (c10_inDouble == 0.0) {
      c10_histTable->NumberOfZeros++;
    } else {
      c10_histTable->SimSum += c10_inDouble;
      if ((!muDoubleScalarIsInf(c10_inDouble)) && (!muDoubleScalarIsNaN
           (c10_inDouble))) {
        c10_significand = frexp(c10_inDouble, &c10_exponent);
        if (c10_exponent > 128) {
          c10_exponent = 128;
        }

        if (c10_exponent < -127) {
          c10_exponent = -127;
        }

        if (c10_significand < 0.0) {
          c10_histTable->NumberOfNegativeValues++;
          c10_histTable->HistogramOfNegativeValues[127 + c10_exponent]++;
        } else {
          c10_histTable->NumberOfPositiveValues++;
          c10_histTable->HistogramOfPositiveValues[127 + c10_exponent]++;
        }
      }
    }

    c10_b_table[0U].SimMin = (real_T)c10_localMin;
    c10_b_table[0U].SimMax = (real_T)c10_localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return c10_in;
}

static void c10_emlrtInitVarDataTables(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, emlrtLocationLoggingDataType c10_dataTables[24],
  emlrtLocationLoggingHistogramType c10_histTables[24])
{
  int32_T c10_i;
  int32_T c10_iH;
  (void)chartInstance;
  for (c10_i = 0; c10_i < 24; c10_i++) {
    c10_dataTables[c10_i].SimMin = rtInf;
    c10_dataTables[c10_i].SimMax = rtMinusInf;
    c10_dataTables[c10_i].OverflowWraps = 0;
    c10_dataTables[c10_i].Saturations = 0;
    c10_dataTables[c10_i].IsAlwaysInteger = true;
    c10_dataTables[c10_i].HistogramTable = &c10_histTables[c10_i];
    c10_histTables[c10_i].NumberOfZeros = 0.0;
    c10_histTables[c10_i].NumberOfPositiveValues = 0.0;
    c10_histTables[c10_i].NumberOfNegativeValues = 0.0;
    c10_histTables[c10_i].TotalNumberOfValues = 0.0;
    c10_histTables[c10_i].SimSum = 0.0;
    for (c10_iH = 0; c10_iH < 256; c10_iH++) {
      c10_histTables[c10_i].HistogramOfPositiveValues[c10_iH] = 0.0;
      c10_histTables[c10_i].HistogramOfNegativeValues[c10_iH] = 0.0;
    }
  }
}

const mxArray *sf_c10_PWM_28_HalfB_get_eml_resolved_functions_info(void)
{
  const mxArray *c10_nameCaptureInfo = NULL;
  c10_nameCaptureInfo = NULL;
  sf_mex_assign(&c10_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c10_nameCaptureInfo;
}

static uint16_T c10_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c10_sp, const mxArray *c10_b_cont, const
  char_T *c10_identifier)
{
  uint16_T c10_y;
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = (const char *)c10_identifier;
  c10_thisId.fParent = NULL;
  c10_thisId.bParentIsCell = false;
  c10_y = c10_b_emlrt_marshallIn(chartInstance, c10_sp, sf_mex_dup(c10_b_cont),
    &c10_thisId);
  sf_mex_destroy(&c10_b_cont);
  return c10_y;
}

static uint16_T c10_b_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c10_sp, const mxArray *c10_u, const
  emlrtMsgIdentifier *c10_parentId)
{
  uint16_T c10_y;
  const mxArray *c10_mxFi = NULL;
  const mxArray *c10_mxInt = NULL;
  uint16_T c10_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c10_parentId, c10_u, false, 0U, NULL, c10_eml_mx, c10_b_eml_mx);
  sf_mex_assign(&c10_mxFi, sf_mex_dup(c10_u), false);
  sf_mex_assign(&c10_mxInt, sf_mex_call(c10_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c10_mxFi)), false);
  sf_mex_import(c10_parentId, sf_mex_dup(c10_mxInt), &c10_b_u, 1, 5, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c10_mxFi);
  sf_mex_destroy(&c10_mxInt);
  c10_y = c10_b_u;
  sf_mex_destroy(&c10_mxFi);
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static uint8_T c10_c_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c10_sp, const mxArray *c10_b_out_init, const
  char_T *c10_identifier)
{
  uint8_T c10_y;
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = (const char *)c10_identifier;
  c10_thisId.fParent = NULL;
  c10_thisId.bParentIsCell = false;
  c10_y = c10_d_emlrt_marshallIn(chartInstance, c10_sp, sf_mex_dup
    (c10_b_out_init), &c10_thisId);
  sf_mex_destroy(&c10_b_out_init);
  return c10_y;
}

static uint8_T c10_d_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const emlrtStack *c10_sp, const mxArray *c10_u, const
  emlrtMsgIdentifier *c10_parentId)
{
  uint8_T c10_y;
  const mxArray *c10_mxFi = NULL;
  const mxArray *c10_mxInt = NULL;
  uint8_T c10_b_u;
  (void)chartInstance;
  sf_mex_check_fi(c10_parentId, c10_u, false, 0U, NULL, c10_eml_mx, c10_c_eml_mx);
  sf_mex_assign(&c10_mxFi, sf_mex_dup(c10_u), false);
  sf_mex_assign(&c10_mxInt, sf_mex_call(c10_sp, NULL, "interleavedsimulinkarray",
    1U, 1U, 14, sf_mex_dup(c10_mxFi)), false);
  sf_mex_import(c10_parentId, sf_mex_dup(c10_mxInt), &c10_b_u, 1, 3, 0U, 0, 0U,
                0);
  sf_mex_destroy(&c10_mxFi);
  sf_mex_destroy(&c10_mxInt);
  c10_y = c10_b_u;
  sf_mex_destroy(&c10_mxFi);
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static uint8_T c10_e_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c10_b_is_active_c10_PWM_28_HalfB, const char_T *
  c10_identifier)
{
  uint8_T c10_y;
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = (const char *)c10_identifier;
  c10_thisId.fParent = NULL;
  c10_thisId.bParentIsCell = false;
  c10_y = c10_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c10_b_is_active_c10_PWM_28_HalfB), &c10_thisId);
  sf_mex_destroy(&c10_b_is_active_c10_PWM_28_HalfB);
  return c10_y;
}

static uint8_T c10_f_emlrt_marshallIn(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  uint8_T c10_y;
  uint8_T c10_b_u;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), &c10_b_u, 1, 3, 0U, 0, 0U, 0);
  c10_y = c10_b_u;
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static const mxArray *c10_chart_data_browse_helper
  (SFc10_PWM_28_HalfBInstanceStruct *chartInstance, int32_T c10_ssIdNumber)
{
  const mxArray *c10_mxData = NULL;
  uint8_T c10_u;
  int16_T c10_i;
  int16_T c10_i1;
  int16_T c10_i2;
  uint16_T c10_u1;
  uint8_T c10_u2;
  uint8_T c10_u3;
  real_T c10_d;
  real_T c10_d1;
  real_T c10_d2;
  real_T c10_d3;
  real_T c10_d4;
  real_T c10_d5;
  c10_mxData = NULL;
  switch (c10_ssIdNumber) {
   case 18U:
    c10_u = *chartInstance->c10_enable;
    c10_d = (real_T)c10_u;
    sf_mex_assign(&c10_mxData, sf_mex_create("mxData", &c10_d, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 16U:
    c10_i = *chartInstance->c10_offset;
    sf_mex_assign(&c10_mxData, sf_mex_create("mxData", &c10_i, 4, 0U, 0U, 0U, 0),
                  false);
    break;

   case 7U:
    c10_i1 = *chartInstance->c10_max;
    c10_d1 = (real_T)c10_i1;
    sf_mex_assign(&c10_mxData, sf_mex_create("mxData", &c10_d1, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 4U:
    c10_i2 = *chartInstance->c10_sum;
    c10_d2 = (real_T)c10_i2;
    sf_mex_assign(&c10_mxData, sf_mex_create("mxData", &c10_d2, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 15U:
    c10_u1 = *chartInstance->c10_cont;
    c10_d3 = (real_T)c10_u1;
    sf_mex_assign(&c10_mxData, sf_mex_create("mxData", &c10_d3, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 19U:
    c10_u2 = *chartInstance->c10_init;
    c10_d4 = (real_T)c10_u2;
    sf_mex_assign(&c10_mxData, sf_mex_create("mxData", &c10_d4, 0, 0U, 0U, 0U, 0),
                  false);
    break;

   case 21U:
    c10_u3 = *chartInstance->c10_out_init;
    c10_d5 = (real_T)c10_u3;
    sf_mex_assign(&c10_mxData, sf_mex_create("mxData", &c10_d5, 0, 0U, 0U, 0U, 0),
                  false);
    break;
  }

  return c10_mxData;
}

static int32_T c10__s32_add__(SFc10_PWM_28_HalfBInstanceStruct *chartInstance,
  int32_T c10_b, int32_T c10_c, int32_T c10_EMLOvCount_src_loc, uint32_T
  c10_ssid_src_loc, int32_T c10_offset_src_loc, int32_T c10_length_src_loc)
{
  int32_T c10_a;
  int32_T c10_PICOffset;
  real_T c10_d;
  observerLogReadPIC(&c10_PICOffset);
  c10_a = c10_b + c10_c;
  if (((c10_a ^ c10_b) & (c10_a ^ c10_c)) < 0) {
    sf_data_overflow_error(chartInstance->S, c10_ssid_src_loc,
      c10_offset_src_loc, c10_length_src_loc);
    c10_d = 1.0;
    observerLog(c10_EMLOvCount_src_loc + c10_PICOffset, &c10_d, 1);
  }

  return c10_a;
}

static void init_dsm_address_info(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc10_PWM_28_HalfBInstanceStruct
  *chartInstance)
{
  chartInstance->c10_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c10_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c10_enable = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c10_offset = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c10_max = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c10_sum = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c10_cont = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c10_init = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c10_out_init = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* SFunction Glue Code */
void sf_c10_PWM_28_HalfB_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3394927160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(761447754U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3055287284U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2907148159U);
}

mxArray *sf_c10_PWM_28_HalfB_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c10_PWM_28_HalfB_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("fixpt_minmax_logging");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c10_PWM_28_HalfB_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c10_PWM_28_HalfB(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNpjYPT0ZQACPiA+wMTAwAakOYCYiQECWKF8RiBmhtIQcRa4uAIQl1QWpILEi4uSPVOAdF5iLpi"
    "fWFrhmZeWDzbfggFhPhsW8xmRzOeEikPAB3vK9Os5gPQbIOlnwaKfBUm/AJCXnJ9XwgflD6z7TR"
    "3Q9WNzPweK+yH8/NKS+My8TCr5Q8GBMv0Q+wMI+EMKzR8gfmZxfGJySWZZanyyoUF8QLhvvJFFv"
    "EdiTpoTwlwQAADrRCCN"
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c10_PWM_28_HalfB_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "s4HFSMkX1qR1eGrtlzmrn6C";
}

static void sf_opaque_initialize_c10_PWM_28_HalfB(void *chartInstanceVar)
{
  initialize_params_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
  initialize_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c10_PWM_28_HalfB(void *chartInstanceVar)
{
  enable_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c10_PWM_28_HalfB(void *chartInstanceVar)
{
  disable_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c10_PWM_28_HalfB(void *chartInstanceVar)
{
  sf_gateway_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c10_PWM_28_HalfB(SimStruct* S)
{
  return get_sim_state_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c10_PWM_28_HalfB(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_cleanup_runtime_resources_c10_PWM_28_HalfB(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc10_PWM_28_HalfBInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_PWM_28_HalfB_optimization_info();
    }

    mdl_cleanup_runtime_resources_c10_PWM_28_HalfB
      ((SFc10_PWM_28_HalfBInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_mdl_start_c10_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_start_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_mdl_terminate_c10_PWM_28_HalfB(void *chartInstanceVar)
{
  mdl_terminate_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c10_PWM_28_HalfB(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  sf_warn_if_symbolic_dimension_param_changed(S);
  if (sf_machine_global_initializer_called()) {
    initialize_params_c10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
    initSimStructsc10_PWM_28_HalfB((SFc10_PWM_28_HalfBInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c10_PWM_28_HalfB_get_post_codegen_info(void)
{
  int i;
  const char* encStrCodegen [18] = {
    "eNrtV01u20YUphQ5iNHGcLIJYARodu0qiIs2yCpxTEm1AKlWQ+dnJ4yHT+JAwxlmfmQ758gl2hN",
    "02UUv0F0XRW9QoEfoG4qSZYqk4qgxEiADUMQMv/fN+5+RV+v0PBxb+Ly67XnX8X0Dn7o3HRvZvL",
    "bwTNcb3tfZ/AkKCRv3iSKx9iqHIDE8Ay25NUyKjhjKQhgTQ1AgKGITqUwZm2ax5UyM21ZQx6dfR",
    "oxGQSQtD/dRloSHgp8hW2JNH3maTAE1bYDQREraUdTmZDTXWJkTPwI61jauMkGDCWzi1NI9yw1L",
    "OLROgXaENgQ11ue6BYYY8M1pqZnOUh3MgDJOOCOi0NqI6AASdLCB50mIv4fWoFF5GI2IMvsQkQn",
    "oLhunnFJAnpNp/HDMBDFSMcJbMfed4LJufY769GQIvMIhqNu+AjJOJBOmPP5BGy1tCXLMoQnHdl",
    "TOFsBr64L/gsEJqFK/DX05AUVGcChKN00d0jpNozXPkmWYYTG8IOopxfhpCEuzFzNHBwTjBEcoU",
    "QaD1MiOPlJsgu4tZbNxx2XmqpKx8TTYehUsZWtNoCoKc7Y2FT7hXJfCjmTShQnwlLVJDKmGTVmL",
    "cVqz8Eiig116l1eDFQwDn8F8KUJWGK5JDpD2nR+xsVxEUquNjH1M3ma3u/x5GdYRBtSQUCjqAoo",
    "wDeiz1L3lbCHTLvYIRK1Mql4ReJohq1CeHlrRPJFqjD6paCLnJriIlgJjPcJYYiU811g0VTAXy1",
    "U4SmgEoWswjEMPywaxBT7RrrU9xbqbMHPWBE0VS4qi6s6fB975+fPlO5w/M7n8+5sFnloBj7fwd",
    "vhHC/jN+kX8Rm7f+mzNjUx+b0H+Zm6/Rk7e4bZd5bz9/Zefb/3zxV8//fpH528wefvzetSW9Kh5",
    "s/2Ta5c7t7ey+d1Zg5wn/GQpzxz2YEGvRgH/nQX+7WyuvztoB73xq93Xz3bhB2X4m1iJh37K91u",
    "9Wt9rOX1n6/dcpz5L0r6rFe2E2YXCzYmdHrP5eF5f4Y/NbH06/n2ynvz9vXwci/zVuOCvhkelMF",
    "sl+Xi1+n+/l5cv0v9GLt5uLq0ZMMH+Jzvu7a0nP92/v8KOnZwdO+m9YkBct4IB3X0w6L/sDb59N",
    "DggfLi/3Gfet14vK+ddsdynoudnv3x4+97lHN54T7n6muf+Vcmta99l7yMfG77qPPNy+O2P2I51",
    "74kfGv+nd7l73FfZ/PH8L5YfMR4W3Lazz10gw6KvV2DffwJyoHo=",
    ""
  };

  static char newstr [1253] = "";
  newstr[0] = '\0';
  for (i = 0; i < 18; i++) {
    strcat(newstr, encStrCodegen[i]);
  }

  return newstr;
}

static void mdlSetWorkWidths_c10_PWM_28_HalfB(SimStruct *S)
{
  const char* newstr = sf_c10_PWM_28_HalfB_get_post_codegen_info();
  sf_set_work_widths(S, newstr);
  ssSetChecksum0(S,(2865009774U));
  ssSetChecksum1(S,(200085929U));
  ssSetChecksum2(S,(3518517724U));
  ssSetChecksum3(S,(1952833097U));
}

static void mdlRTW_c10_PWM_28_HalfB(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlSetupRuntimeResources_c10_PWM_28_HalfB(SimStruct *S)
{
  SFc10_PWM_28_HalfBInstanceStruct *chartInstance;
  chartInstance = (SFc10_PWM_28_HalfBInstanceStruct *)utMalloc(sizeof
    (SFc10_PWM_28_HalfBInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc10_PWM_28_HalfBInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c10_PWM_28_HalfB;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c10_PWM_28_HalfB;
  chartInstance->chartInfo.mdlStart = sf_opaque_mdl_start_c10_PWM_28_HalfB;
  chartInstance->chartInfo.mdlTerminate =
    sf_opaque_mdl_terminate_c10_PWM_28_HalfB;
  chartInstance->chartInfo.mdlCleanupRuntimeResources =
    sf_opaque_cleanup_runtime_resources_c10_PWM_28_HalfB;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c10_PWM_28_HalfB;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c10_PWM_28_HalfB;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c10_PWM_28_HalfB;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c10_PWM_28_HalfB;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c10_PWM_28_HalfB;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c10_PWM_28_HalfB;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c10_PWM_28_HalfB;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0,
    chartInstance->c10_JITStateAnimation,
    chartInstance->c10_JITTransitionAnimation);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  mdl_setup_runtime_resources_c10_PWM_28_HalfB(chartInstance);
}

void c10_PWM_28_HalfB_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_SETUP_RUNTIME_RESOURCES:
    mdlSetupRuntimeResources_c10_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c10_PWM_28_HalfB(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c10_PWM_28_HalfB(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c10_PWM_28_HalfB_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
