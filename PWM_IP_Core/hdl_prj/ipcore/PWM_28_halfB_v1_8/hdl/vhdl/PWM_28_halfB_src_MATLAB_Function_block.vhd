-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\PWM_28_HalfB\PWM_28_halfB_src_MATLAB_Function_block.vhd
-- Created: 2021-04-21 09:27:00
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: PWM_28_halfB_src_MATLAB_Function_block
-- Source Path: PWM_28_HalfB/PWM_28/PWM_HB10/MATLAB Function
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY PWM_28_halfB_src_MATLAB_Function_block IS
  PORT( enable                            :   IN    std_logic;  -- ufix1
        offset                            :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
        max                               :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12
        sum                               :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12
        init                              :   IN    std_logic;  -- ufix1
        cont                              :   OUT   std_logic_vector(11 DOWNTO 0);  -- ufix12
        out_init                          :   OUT   std_logic  -- ufix1
        );
END PWM_28_halfB_src_MATLAB_Function_block;


ARCHITECTURE rtl OF PWM_28_halfB_src_MATLAB_Function_block IS

  -- Signals
  SIGNAL offset_signed                    : signed(15 DOWNTO 0);  -- int16
  SIGNAL max_signed                       : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL sum_signed                       : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL cont_tmp                         : unsigned(11 DOWNTO 0);  -- ufix12

BEGIN
  offset_signed <= signed(offset);

  max_signed <= signed(max);

  sum_signed <= signed(sum);

  MATLAB_Function_output : PROCESS (enable, init, max_signed, offset_signed, sum_signed)
    VARIABLE b : signed(15 DOWNTO 0);
    VARIABLE cast : unsigned(11 DOWNTO 0);
    VARIABLE cast_0 : unsigned(11 DOWNTO 0);
    VARIABLE cast_1 : unsigned(11 DOWNTO 0);
    VARIABLE cast_2 : unsigned(11 DOWNTO 0);
    VARIABLE add_temp : signed(16 DOWNTO 0);
  BEGIN
    b := to_signed(16#0000#, 16);
    add_temp := to_signed(16#00000#, 17);
    cast := to_unsigned(16#000#, 12);
    cast_0 := to_unsigned(16#000#, 12);
    cast_1 := to_unsigned(16#000#, 12);
    cast_2 := to_unsigned(16#000#, 12);
    --MATLAB Function 'PWM_28/PWM_HB10/MATLAB Function'
    IF enable = '0' THEN 
      cont_tmp <= to_unsigned(16#000#, 12);
      out_init <= '0';
    ELSIF init = '0' THEN 
      IF sum_signed(11) = '1' THEN 
        cast_1 := "000000000000";
      ELSE 
        cast_1 := unsigned(sum_signed);
      END IF;
      IF max_signed(11) = '1' THEN 
        cast_2 := "000000000000";
      ELSE 
        cast_2 := unsigned(max_signed);
      END IF;
      IF cast_1 < cast_2 THEN 
        b := offset_signed;
        add_temp := resize(sum_signed, 17) + resize(b, 17);
        IF (add_temp(16) = '0') AND (add_temp(15 DOWNTO 12) /= "0000") THEN 
          cont_tmp <= "111111111111";
        ELSIF add_temp(16) = '1' THEN 
          cont_tmp <= "000000000000";
        ELSE 
          cont_tmp <= unsigned(add_temp(11 DOWNTO 0));
        END IF;
        out_init <= '1';
      ELSE 
        cont_tmp <= to_unsigned(16#000#, 12);
        out_init <= '1';
      END IF;
    ELSE 
      IF sum_signed(11) = '1' THEN 
        cast := "000000000000";
      ELSE 
        cast := unsigned(sum_signed);
      END IF;
      IF max_signed(11) = '1' THEN 
        cast_0 := "000000000000";
      ELSE 
        cast_0 := unsigned(max_signed);
      END IF;
      IF cast < cast_0 THEN 
        IF sum_signed(11) = '1' THEN 
          cont_tmp <= "000000000000";
        ELSE 
          cont_tmp <= unsigned(sum_signed);
        END IF;
        out_init <= '1';
      ELSE 
        cont_tmp <= to_unsigned(16#000#, 12);
        out_init <= '1';
      END IF;
    END IF;
  END PROCESS MATLAB_Function_output;


  cont <= std_logic_vector(cont_tmp);

END rtl;

