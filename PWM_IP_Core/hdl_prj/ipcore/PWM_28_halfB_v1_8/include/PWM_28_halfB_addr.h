/*
 * File Name:         hdl_prj\ipcore\PWM_28_halfB_v1_8\include\PWM_28_halfB_addr.h
 * Description:       C Header File
 * Created:           2021-04-21 09:27:47
*/

#ifndef PWM_28_HALFB_H_
#define PWM_28_HALFB_H_

#define  IPCore_Reset_PWM_28_halfB       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PWM_28_halfB      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PWM_28_halfB   0x8  //contains unique IP timestamp (yymmddHHMM): 2104210927
#define  enable_Data_PWM_28_halfB        0x100  //data register for Inport enable
#define  Max_point_Data_PWM_28_halfB     0x104  //data register for Inport Max_point
#define  offset_1_Data_PWM_28_halfB      0x108  //data register for Inport offset_1
#define  Ref_1_Data_PWM_28_halfB         0x10C  //data register for Inport Ref_1
#define  offset_2_Data_PWM_28_halfB      0x110  //data register for Inport offset_2
#define  Ref_2_Data_PWM_28_halfB         0x114  //data register for Inport Ref_2
#define  offset_3_Data_PWM_28_halfB      0x118  //data register for Inport offset_3
#define  Ref_3_Data_PWM_28_halfB         0x11C  //data register for Inport Ref_3
#define  offset_4_Data_PWM_28_halfB      0x120  //data register for Inport offset_4
#define  Ref_4_Data_PWM_28_halfB         0x124  //data register for Inport Ref_4
#define  offset_5_Data_PWM_28_halfB      0x128  //data register for Inport offset_5
#define  Ref_5_Data_PWM_28_halfB         0x12C  //data register for Inport Ref_5
#define  offset_6_Data_PWM_28_halfB      0x130  //data register for Inport offset_6
#define  Ref_6_Data_PWM_28_halfB         0x134  //data register for Inport Ref_6
#define  offset_7_Data_PWM_28_halfB      0x138  //data register for Inport offset_7
#define  Ref_7_Data_PWM_28_halfB         0x13C  //data register for Inport Ref_7
#define  offset_8_Data_PWM_28_halfB      0x140  //data register for Inport offset_8
#define  Ref_8_Data_PWM_28_halfB         0x144  //data register for Inport Ref_8
#define  offset_9_Data_PWM_28_halfB      0x148  //data register for Inport offset_9
#define  Ref_9_Data_PWM_28_halfB         0x14C  //data register for Inport Ref_9
#define  offset_10_Data_PWM_28_halfB     0x150  //data register for Inport offset_10
#define  Ref_10_Data_PWM_28_halfB        0x154  //data register for Inport Ref_10
#define  offset_11_Data_PWM_28_halfB     0x158  //data register for Inport offset_11
#define  Ref_11_Data_PWM_28_halfB        0x15C  //data register for Inport Ref_11
#define  offset_12_Data_PWM_28_halfB     0x160  //data register for Inport offset_12
#define  Ref_12_Data_PWM_28_halfB        0x164  //data register for Inport Ref_12
#define  offset_13_Data_PWM_28_halfB     0x168  //data register for Inport offset_13
#define  Ref_13_Data_PWM_28_halfB        0x16C  //data register for Inport Ref_13
#define  offset_14_Data_PWM_28_halfB     0x170  //data register for Inport offset_14
#define  Ref_14_Data_PWM_28_halfB        0x174  //data register for Inport Ref_14
#define  offset_15_Data_PWM_28_halfB     0x178  //data register for Inport offset_15
#define  Ref_15_Data_PWM_28_halfB        0x17C  //data register for Inport Ref_15
#define  offset_16_Data_PWM_28_halfB     0x180  //data register for Inport offset_16
#define  Ref_16_Data_PWM_28_halfB        0x184  //data register for Inport Ref_16
#define  offset_17_Data_PWM_28_halfB     0x188  //data register for Inport offset_17
#define  Ref_17_Data_PWM_28_halfB        0x18C  //data register for Inport Ref_17
#define  offset_18_Data_PWM_28_halfB     0x190  //data register for Inport offset_18
#define  Ref_18_Data_PWM_28_halfB        0x194  //data register for Inport Ref_18
#define  offset_19_Data_PWM_28_halfB     0x198  //data register for Inport offset_19
#define  Ref_19_Data_PWM_28_halfB        0x19C  //data register for Inport Ref_19
#define  offset_20_Data_PWM_28_halfB     0x1A0  //data register for Inport offset_20
#define  Ref_20_Data_PWM_28_halfB        0x1A4  //data register for Inport Ref_20
#define  offset_21_Data_PWM_28_halfB     0x1A8  //data register for Inport offset_21
#define  Ref_21_Data_PWM_28_halfB        0x1AC  //data register for Inport Ref_21
#define  offset_22_Data_PWM_28_halfB     0x1B0  //data register for Inport offset_22
#define  Ref_22_Data_PWM_28_halfB        0x1B4  //data register for Inport Ref_22
#define  offset_23_Data_PWM_28_halfB     0x1B8  //data register for Inport offset_23
#define  Ref_23_Data_PWM_28_halfB        0x1BC  //data register for Inport Ref_23
#define  offset_24_Data_PWM_28_halfB     0x1C0  //data register for Inport offset_24
#define  Ref_24_Data_PWM_28_halfB        0x1C4  //data register for Inport Ref_24
#define  offset_25_Data_PWM_28_halfB     0x1C8  //data register for Inport offset_25
#define  Ref_25_Data_PWM_28_halfB        0x1CC  //data register for Inport Ref_25
#define  offset_26_Data_PWM_28_halfB     0x1D0  //data register for Inport offset_26
#define  Ref_26_Data_PWM_28_halfB        0x1D4  //data register for Inport Ref_26
#define  offset_27_Data_PWM_28_halfB     0x1D8  //data register for Inport offset_27
#define  Ref_27_Data_PWM_28_halfB        0x1DC  //data register for Inport Ref_27
#define  offset_28_Data_PWM_28_halfB     0x1E0  //data register for Inport offset_28
#define  Ref_28_Data_PWM_28_halfB        0x1E4  //data register for Inport Ref_28

#endif /* PWM_28_HALFB_H_ */
